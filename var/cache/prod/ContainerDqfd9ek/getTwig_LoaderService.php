<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'twig.loader' shared service.

$this->services['twig.loader'] = $instance = new \Symfony\Bundle\TwigBundle\Loader\FilesystemLoader(${($_ = isset($this->services['templating.locator']) ? $this->services['templating.locator'] : $this->load('getTemplating_LocatorService.php')) && false ?: '_'}, ${($_ = isset($this->services['templating.name_parser']) ? $this->services['templating.name_parser'] : ($this->services['templating.name_parser'] = new \Symfony\Bundle\FrameworkBundle\Templating\TemplateNameParser(${($_ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel', 1)) && false ?: '_'}))) && false ?: '_'}, $this->targetDirs[3]);

$instance->addPath(($this->targetDirs[3].'/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views'), 'Framework');
$instance->addPath(($this->targetDirs[3].'/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views'), '!Framework');
$instance->addPath(($this->targetDirs[3].'/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle/Resources/views'), 'Security');
$instance->addPath(($this->targetDirs[3].'/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle/Resources/views'), '!Security');
$instance->addPath(($this->targetDirs[3].'/app/Resources/TwigBundle/views'), 'Twig');
$instance->addPath(($this->targetDirs[3].'/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views'), 'Twig');
$instance->addPath(($this->targetDirs[3].'/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views'), '!Twig');
$instance->addPath(($this->targetDirs[3].'/vendor/symfony/swiftmailer-bundle/Resources/views'), 'Swiftmailer');
$instance->addPath(($this->targetDirs[3].'/vendor/symfony/swiftmailer-bundle/Resources/views'), '!Swiftmailer');
$instance->addPath(($this->targetDirs[3].'/vendor/doctrine/doctrine-bundle/Resources/views'), 'Doctrine');
$instance->addPath(($this->targetDirs[3].'/vendor/doctrine/doctrine-bundle/Resources/views'), '!Doctrine');
$instance->addPath(($this->targetDirs[3].'/app/Resources/FOSUserBundle/views'), 'FOSUser');
$instance->addPath(($this->targetDirs[3].'/vendor/friendsofsymfony/user-bundle/Resources/views'), 'FOSUser');
$instance->addPath(($this->targetDirs[3].'/vendor/friendsofsymfony/user-bundle/Resources/views'), '!FOSUser');
$instance->addPath(($this->targetDirs[3].'/src/Nte/UsuarioBundle/Resources/views'), 'NteUsuario');
$instance->addPath(($this->targetDirs[3].'/src/Nte/UsuarioBundle/Resources/views'), '!NteUsuario');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Config/MoodleBundle/Resources/views'), 'NteConfigMoodle');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Config/MoodleBundle/Resources/views'), '!NteConfigMoodle');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Aplicacao/ExportadorBundle/Resources/views'), 'NteAplicacaoExportador');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Aplicacao/ExportadorBundle/Resources/views'), '!NteAplicacaoExportador');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Aplicacao/BuscadorBundle/Resources/views'), 'NteAplicacaoBuscador');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Aplicacao/BuscadorBundle/Resources/views'), '!NteAplicacaoBuscador');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Admin/PainelBundle/Resources/views'), 'NteAdminPainel');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Admin/PainelBundle/Resources/views'), '!NteAdminPainel');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Config/BaseBundle/Resources/views'), 'NteConfigBase');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Config/BaseBundle/Resources/views'), '!NteConfigBase');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Middleware/SieBundle/Resources/views'), 'NteMiddlewareSie');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Middleware/SieBundle/Resources/views'), '!NteMiddlewareSie');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Middleware/MoodleBundle/Resources/views'), 'NteMiddlewareMoodle');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Middleware/MoodleBundle/Resources/views'), '!NteMiddlewareMoodle');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Config/SieBundle/Resources/views'), 'NteConfigSie');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Config/SieBundle/Resources/views'), '!NteConfigSie');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views'), 'NteAplicacaoPessoalExterno');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views'), '!NteAplicacaoPessoalExterno');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Aplicacao/AlmoxarifadoBundle/Resources/views'), 'Almoxarifado');
$instance->addPath(($this->targetDirs[3].'/src/Nte/Aplicacao/AlmoxarifadoBundle/Resources/views'), '!Almoxarifado');
$instance->addPath(($this->targetDirs[3].'/app/Resources/views'));
$instance->addPath(($this->targetDirs[3].'/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form'));

return $instance;
