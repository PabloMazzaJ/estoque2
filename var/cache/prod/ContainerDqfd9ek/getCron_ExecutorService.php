<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'cron.executor' shared service.

return $this->services['cron.executor'] = new \Cron\Executor\Executor();
