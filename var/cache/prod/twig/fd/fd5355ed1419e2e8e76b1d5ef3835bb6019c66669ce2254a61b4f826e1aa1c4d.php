<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAdminPainelBundle:pablo:pabloteste.html.twig */
class __TwigTemplate_1022f72dea9eac7de46a81356425ad98f2849cb7bccf0a000b3f003d5fdf7750 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAdminPainelBundle:pablo:pabloteste.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Dashboard";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Painel ";
    }

    // line 4
    public function block_conteudo($context, array $blocks = [])
    {
        // line 5
        echo "

<table class=\"table table-striped\">
    <thead>
    <tr>
        <th scope=\"col\">cursoId</th>
        <th scope=\"col\">curso código</th>
        <th scope=\"col\">codigo unificado</th>
        <th scope=\"col\">nome unificado</th>
        <th scope=\"col\">nome</th>
        <th scope=\"col\">IdUsuario</th>
        <th scope=\"col\"></th>
        <th scope=\"col\"></th>
        <th scope=\"col\"></th>
    </tr>
    </thead>
    <tbody>


";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["sieCurso"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 25
            echo "
        <tr>
            <th scope=\"row\">";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "id", [], "any", false, false, false, 27), "html", null, true);
            echo "</th>
            <th scope=\"row\">";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "codigo", [], "any", false, false, false, 28), "html", null, true);
            echo "</th>
            <th scope=\"row\">";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "codigoUnificado", [], "any", false, false, false, 29), "html", null, true);
            echo "</th>
            <th scope=\"row\">";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "id", [], "any", false, false, false, 30), "html", null, true);
            echo "</th>
            <th scope=\"row\">";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "nome", [], "any", false, false, false, 31), "html", null, true);
            echo "</th>

            <th scope=\"row\">
                <ul>
                    ";
            // line 35
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["c"], "idUsuario", [], "any", false, false, false, 35));
            foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
                // line 36
                echo "                        <li>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "id", [], "any", false, false, false, 36), "html", null, true);
                echo "</li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 38
            echo "                </ul>
            </th>



            <th scope=\"row\">
                <ul>
                ";
            // line 45
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["c"], "idDisciplina", [], "any", false, false, false, 45));
            foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
                // line 46
                echo "                    <li>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "nome", [], "any", false, false, false, 46), "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "                </ul>
            </th>

        </tr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "



";
    }

    public function getTemplateName()
    {
        return "NteAdminPainelBundle:pablo:pabloteste.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 53,  148 => 48,  139 => 46,  135 => 45,  126 => 38,  117 => 36,  113 => 35,  106 => 31,  102 => 30,  98 => 29,  94 => 28,  90 => 27,  86 => 25,  82 => 24,  61 => 5,  58 => 4,  52 => 3,  46 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAdminPainelBundle:pablo:pabloteste.html.twig", "/var/www/narfi/src/Nte/Admin/PainelBundle/Resources/views/pablo/pabloteste.html.twig");
    }
}
