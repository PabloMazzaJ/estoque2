<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* AlmoxarifadoBundle:Requisicao:lista-produtos-requisitados.html.twig */
class __TwigTemplate_d5ee1aa56f18627f191cef0641814949651a93429e313c1e9964cb72e9da7bf0 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "AlmoxarifadoBundle:Requisicao:lista-produtos-requisitados.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
    }

    // line 3
    public function block_conteudo($context, array $blocks = [])
    {
        // line 4
        echo "
    <div class=\"container-requisitar\">
        <div class=\"panel panel-primary panel-size\">
            <div class=\"panel-heading\">
                <span>Lista de Produtos</span>
            </div>
            <div class=\"panel panel-primary panel-display-row\">
                ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["listaRequisitada"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 12
            echo "                    <div class=\"product-subcontainer\">

                        <div class=\"panel-body container-selects\" id=\"clone-container\" disabled>
                            <div class=\"subcontainer-produto\">
                                <label for=\"select-produto\">Produto</label>
                                <fieldset disabled>
                                    <input class=\"form-control\" value=\"";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "produto", [], "any", false, false, false, 18), "nome", [], "any", false, false, false, 18), "html", null, true);
            echo "\">
                                </fieldset>
                            </div>

                            <div class=\"subcontainer-cod-produto\">
                                <label>Cod. Produto</label>
                                <fieldset disabled>
                                    <input class=\"form-control\" value=\"";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "produto", [], "any", false, false, false, 25), "codreduzido", [], "any", false, false, false, 25), "html", null, true);
            echo "\">
                                </fieldset>
                            </div>

                            <div class=\"subcontainer-second\">
                                <label>Quantidade</label>
                                <fieldset disabled>
                                    <input class=\"form-control\" value=\"";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "quantidade", [], "any", false, false, false, 32), "html", null, true);
            echo "\">
                                </fieldset>
                            </div>

                            <div class=\"subcontainer-valor\">
                                <label>Preço un.</label>
                                <fieldset disabled>
                                    <input class=\"form-control\" value=\"";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "valorUnitario", [], "any", false, false, false, 39), "html", null, true);
            echo "\">
                                </fieldset>
                            </div>

                            <div class=\"subcontainer-valor\">
                                <label>Preço Total</label>
                                <fieldset disabled>
                                    <input class=\"form-control\" value=\"";
            // line 46
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["item"], "quantidade", [], "any", false, false, false, 46) * twig_get_attribute($this->env, $this->source, $context["item"], "valorUnitario", [], "any", false, false, false, 46)), "html", null, true);
            echo "\">
                                </fieldset>
                            </div>
                        </div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "                <div class=\"panel-body container-selects\">
                    <div class=\"subcontainer-second\" id=\"container-btn-pos\">
                        ";
        // line 55
        echo "                        <button class=\"btn btn-success btn-sm btn-sizing\" id=\"btn-check\" title=\"Confirmar pedido\">
                            <i class='fa fa-check'></i>
                        </button>
                        <button class=\"btn btn-danger btn-sm btn-sizing\" id=\"btn-plus\" name=\"produto\" title=\"Retorne para editar o pedido\">
                            <i class='fa fa-arrow-left'></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

";
    }

    // line 69
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 70
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>

        (function () {

            const model = {};

            const controller = {

                init() {
                    view.init();
                },

                setValorModel(nome, valor) {
                    model[nome] = valor;
                },

                imprimeModel() {
                    console.table(model);
                },
            };

            const view = {

                init() {
                    this.inicializarReferenciaHtml();
                    this.inicializarEventListeners();
                },

                inicializarReferenciaHtml() {

                },

                inicializarEventListeners() {

                },
            };
            controller.init();
        }());


    </script>
";
    }

    // line 114
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 115
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <style>

        input.form-control {
            text-align: center;
        }

        #container-btn-pos {
            margin-left: auto;
        }

        #btn-plus {
            margin-right: 3px;
        }

        .panel-body {
            border-radius: 0;
        }

        .container-requisitar {
            width: 100%;
            margin: 0 auto;
        }

        .panel-heading {
            font-size: 1.1em;
            font-weight: bold;
        }

        .btn-sizing {
            min-width: 34px;
            float: right;
        }


        .subcontainer-cod-produto {
            display: block;
            flex-grow: 1;
            margin-left: 1em;
        }

        .subcontainer-produto {
            display: block;
            flex-grow: 3;
        }

        .subcontainer-second {
            margin-left: 1em;
            flex-grow: 1;
        }

        .subcontainer-valor {
            margin-left: 1em;
            flex-grow: 1;
        }

        .container-selects {
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
        }


    </style>
";
    }

    public function getTemplateName()
    {
        return "AlmoxarifadoBundle:Requisicao:lista-produtos-requisitados.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 115,  199 => 114,  151 => 70,  148 => 69,  132 => 55,  128 => 52,  116 => 46,  106 => 39,  96 => 32,  86 => 25,  76 => 18,  68 => 12,  64 => 11,  55 => 4,  52 => 3,  47 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "AlmoxarifadoBundle:Requisicao:lista-produtos-requisitados.html.twig", "/var/www/narfi/src/Nte/Aplicacao/AlmoxarifadoBundle/Resources/views/Requisicao/lista-produtos-requisitados.html.twig");
    }
}
