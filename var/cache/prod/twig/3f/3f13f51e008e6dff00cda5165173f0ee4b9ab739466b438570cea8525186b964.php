<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_mostra_endereco.html.twig */
class __TwigTemplate_5e3176dca3e332a99e7a1c17cbb0ade1f5f24bf4654f70d6082e707134ab9f06 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_mostra_endereco.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo "Pessoas";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\">Pessoal Externo</a></span></li>
     <li><span><a href=\"";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_mostra");
        echo "\">Pessoa</a></span></li>
     <li><span><a href=\"#\">Endereço</a></span></li>
     <li><span><a href=\"\">";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoa"] ?? null), "nome", [], "any", false, false, false, 8), "html", null, true);
        echo "</a></span></li>
 ";
    }

    // line 10
    public function block_conteudo($context, array $blocks = [])
    {
        // line 11
        echo "

    <header class=\"panel-heading\">
        <h2 class=\"panel-title\">
            <span class=\"va-middle\"><i class=\"fa fa-users\"></i> Endereço: ";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoa"] ?? null), "nome", [], "any", false, false, false, 15), "html", null, true);
        echo "</span>
        </h2>
    </header>
    <div class=\"panel-body\">
        <table class=\"table table-striped\">
            <tr>
                <th class=\"col-md-2\">Endereço</th>
                <th class=\"col-md-2\">Bairro</th>
                <th class=\"col-md-1\">Número</th>
                <th class=\"col-md-2\">Complemento</th>
                <th class=\"col-md-1\">UF</th>
                <th class=\"col-md-1\">CEP</th>
                <th class=\"col-md-1\">Cidade</th>
            </tr>

            <tr>
                <td>";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoa"] ?? null), "enderecoLogradouro", [], "any", false, false, false, 31), "html", null, true);
        echo "</td>
                <td>";
        // line 32
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoa"] ?? null), "enderecoBairro", [], "any", false, false, false, 32), "html", null, true);
        echo "</td>
                <td>";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoa"] ?? null), "enderecoNumero", [], "any", false, false, false, 33), "html", null, true);
        echo "</td>
                <td>";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoa"] ?? null), "enderecoComplemento", [], "any", false, false, false, 34), "html", null, true);
        echo "</td>
                <td>";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoa"] ?? null), "enderecoUf", [], "any", false, false, false, 35), "html", null, true);
        echo "</td>
                <td>";
        // line 36
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoa"] ?? null), "enderecoCep", [], "any", false, false, false, 36), "html", null, true);
        echo "</td>
                <td>";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoa"] ?? null), "enderecoCidade", [], "any", false, false, false, 37), "html", null, true);
        echo "</td>
            </tr>

        </table>
        <a href=\"";
        // line 41
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_mostra");
        echo "\" class=\"btn-gerar-relatorio2\">
            <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"expButton\"><i class=\"fa fa-arrow-circle-left\"></i></button>
        </a>
    </div>


";
    }

    // line 49
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 50
        echo "
    ";
        // line 51
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>


    </script>

";
    }

    // line 59
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 60
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>

        .btn {
            min-width: 9em;
            max-width: 9em;
        }
        table th,
        table  {
            text-align: center;
            vertical-align: middle;
        }
    </style>

";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_mostra_endereco.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 60,  164 => 59,  153 => 51,  150 => 50,  147 => 49,  136 => 41,  129 => 37,  125 => 36,  121 => 35,  117 => 34,  113 => 33,  109 => 32,  105 => 31,  86 => 15,  80 => 11,  77 => 10,  71 => 8,  66 => 6,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_mostra_endereco.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntepessoa/pessoa_mostra_endereco.html.twig");
    }
}
