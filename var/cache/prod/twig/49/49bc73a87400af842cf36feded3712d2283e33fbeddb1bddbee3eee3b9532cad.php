<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoBuscadorBundle:Pessoas:index.html.twig */
class __TwigTemplate_97cc831eaec999b088d9f38d3f5151890d363e9bc9212ca0fef93160153cc284 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Buscar pessoas";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Buscar Pessoas";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span>Painel de Controle</span></li>
     <li><span>Buscar Pessoas</span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "    ";
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_filtro_pesquisa.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:index.html.twig", 9)->display($context);
        // line 10
        echo "
    <div id=\"resposta\" class=\"\">
        <div class=\"panel\">
            <div class=\"panel-body text-center \">
                <h1><i class=\"fa fa-search\"></i> Procure uma pessoa</h1>
            </div>
        </div>
    </div>

";
    }

    // line 20
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 21
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>
        function buscarPeloDocumento(nro, doc) {
            \$.loader.open();
            \$.ajax({
                type: 'POST',
                url: '";
        // line 27
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("buscador_pessoas_pesquisar");
        echo "',
                data: {nro: nro,doc: doc},
                dataType: 'json',
                complete: function (data) {
                    \$('#resposta').html(data.responseText);
                    \$.loader.close();
                },
            });
        }

        \$('#doc').change(function () {
            if (\$(this).find(':selected').data)
                if (\$(this).find(':selected').data('mask') != '1') {
                    \$('#nro').mask(\$(this).find(':selected').data('mask'));
                } else {
                    \$('#nro').unmask();
                }

            \$('#nro').attr('placeholder', \$(this).find(':selected').data('mask'));
        });


        \$(\"#fnome\").change(function () {
            window.location='";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("buscador_pessoas_perfil");
        echo "/'+ this.value;
        });
        \$('#pesquisar-pessoa').click(function () {
            buscarPeloDocumento(\$('#nro').val(), \$('#doc').val())
        });

        \$(document).ready(function () {
            \$(\"#fnome\").select2({
                minimumInputLength: 3,
                width: \"300px\",
                placeholder: \"Buscar pessoas pelo nome...\",
                ajax: {
                    url: '";
        // line 62
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("buscador_pessoas_filtrar_nome_middleware");
        echo "',
                    dataType: 'json',
                    method: \"POST\",
                    data: function (params) {
                        return {
                            termo: params.term,
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                }
            });
        })

    </script>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoBuscadorBundle:Pessoas:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 62,  127 => 50,  101 => 27,  91 => 21,  88 => 20,  75 => 10,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  54 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoBuscadorBundle:Pessoas:index.html.twig", "/var/www/narfi/src/Nte/Aplicacao/BuscadorBundle/Resources/views/Pessoas/index.html.twig");
    }
}
