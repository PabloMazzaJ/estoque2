<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* FOSUserBundle::layout.html.twig */
class __TwigTemplate_4388e58ad1336d46532adfcaf8918c3bb8426b37cca6b79d9d75dd34b914367b extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'marca' => [$this, 'block_marca'],
            'titulo' => [$this, 'block_titulo'],
            'fos_user_content' => [$this, 'block_fos_user_content'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        echo "<!doctype html>
<html class=\"fixed\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\"/>
    <meta name=\"keywords\" content=\"\"/>
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"Nte\">

    <title>Friga - NTE</title>
    <link rel=\"shortcut icon\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap/css/bootstrap.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/font-awesome/css/font-awesome.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/pnotify/pnotify.custom.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/theme.admin.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/theme-custom.admin.css"), "html", null, true);
        echo "\">
    <script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/modernizr/modernizr.js"), "html", null, true);
        echo "\"></script>
</head>
<body>
<section class=\"body-sign\">
    <div class=\"center-sign\">
        ";
        // line 24
        $this->displayBlock('marca', $context, $blocks);
        // line 29
        echo "        <div class=\"panel panel-sign\">
            <div class=\"panel-title-sign mt-xl text-right\">
                <h2 class=\"title text-uppercase text-weight-bold m-none\">
                    <i class=\"fa fa-user mr-xs\"></i>";
        // line 32
        $this->displayBlock('titulo', $context, $blocks);
        // line 33
        echo "
                </h2>
            </div>
            <div class=\"panel-body\">
                ";
        // line 37
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 39
        echo "            </div>
        </div>

    </div>
</section>
";
        // line 44
        $this->displayBlock('javascripts', $context, $blocks);
        // line 58
        echo "<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());
    gtag('config', 'UA-77043326-7');
</script>
<script>
    ";
        // line 69
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 69), "hasPreviousSession", [], "any", false, false, false, 69)) {
            // line 70
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 70), "flashbag", [], "any", false, false, false, 70), "all", [], "method", false, false, false, 70));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 71
                echo "    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 72
                    echo "    new PNotify({
        title: 'Erro',
        text: \"";
                    // line 74
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "\",
        type: '";
                    // line 75
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "',
    });
    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 78
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 79
            echo "    ";
        }
        // line 80
        echo "</script>
</body>
</html>";
    }

    // line 24
    public function block_marca($context, array $blocks = [])
    {
        // line 25
        echo "        <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_admin_painel_homepage");
        echo "\" class=\"logo pull-left\">
            <img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/img/marca_principal_horizontal.svg"), "html", null, true);
        echo "\" height=\"54\" alt=\"Marca do NTE\"/>
        </a>
        ";
    }

    // line 32
    public function block_titulo($context, array $blocks = [])
    {
        echo " Usuário ";
    }

    // line 37
    public function block_fos_user_content($context, array $blocks = [])
    {
        // line 38
        echo "                ";
    }

    // line 44
    public function block_javascripts($context, array $blocks = [])
    {
        // line 45
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery/jquery.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-cookie/jquery-cookie.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/nanoscroller/nanoscroller.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery.magnific-popup/jquery.magnific-popup.min.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/pnotify/pnotify.custom.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/themeadmin.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/themeaadmin.init.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/themeadmin.custom.js"), "html", null, true);
        echo "\"></script>
<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-77043326-7\"></script>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 55,  232 => 54,  228 => 53,  224 => 52,  219 => 50,  215 => 49,  211 => 48,  207 => 47,  203 => 46,  198 => 45,  195 => 44,  191 => 38,  188 => 37,  182 => 32,  175 => 26,  170 => 25,  167 => 24,  161 => 80,  158 => 79,  152 => 78,  143 => 75,  139 => 74,  135 => 72,  130 => 71,  125 => 70,  123 => 69,  110 => 58,  108 => 44,  101 => 39,  99 => 37,  93 => 33,  91 => 32,  86 => 29,  84 => 24,  76 => 19,  72 => 18,  68 => 17,  64 => 16,  60 => 15,  56 => 14,  52 => 13,  39 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "FOSUserBundle::layout.html.twig", "/var/www/narfi/app/Resources/FOSUserBundle/views/layout.html.twig");
    }
}
