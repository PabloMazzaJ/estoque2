<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoBuscadorBundle:Disciplinas:lista.html.twig */
class __TwigTemplate_12317fb8380144337c8e9844dbc1b41b56c91308eca65a10598af6a77d1dadb9 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoBuscadorBundle:Disciplinas:lista.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Exportador";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Exportador";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span>Aplicação</span></li>
     <li><span>Exportador</span></li>
     <li><span>Visualização de Disciplina</span></li>
 ";
    }

    // line 9
    public function block_conteudo($context, array $blocks = [])
    {
        // line 10
        echo "    <section class=\"panel\">
        <div class=\"panel-body\">
            <ul class=\".cursos-lista\">
                ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["listaDisciplina"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
            // line 14
            echo "                    <li class=\"\">
                        <a href=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_aplicacao_exportador_buscar_cursos_disciplina_turmas", ["turma" => twig_get_attribute($this->env, $this->source,             // line 16
$context["d"], "id", [], "any", false, false, false, 16)]), "html", null, true);
            echo "\">
                            ";
            // line 17
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["d"], "disciplina", [], "any", false, false, false, 17), "codigo", [], "any", false, false, false, 17) . " - ") . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["d"], "disciplina", [], "any", false, false, false, 17), "nome", [], "any", false, false, false, 17)), "html", null, true);
            echo "
                        </a>
                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "            </ul>
        </div>
    </section>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoBuscadorBundle:Disciplinas:lista.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 21,  89 => 17,  85 => 16,  84 => 15,  81 => 14,  77 => 13,  72 => 10,  69 => 9,  62 => 5,  59 => 4,  53 => 3,  47 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoBuscadorBundle:Disciplinas:lista.html.twig", "/var/www/narfi/src/Nte/Aplicacao/BuscadorBundle/Resources/views/Disciplinas/lista.html.twig");
    }
}
