<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_vincular_turmas.html.twig */
class __TwigTemplate_66cdf9956d1887db390465f7fdd25dab52c1b6dd39d6b55595ce09596ff3277f extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_vincular_turmas.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo "Exportação - Turmas";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Pessoal Externo</a></span></li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Exportação Turmas</a></span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "
<div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"modal\">
  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h4 class=\"modal-title\">Tem certeza que deseja excluir esta turma da exportação?</h4>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Fechar\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
        <p id=\"conteudoModal\">Dados Turma</p>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" id=\"modalcancelar\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancelar</button>
        <button type=\"button\" id=\"modalremover\" class=\"btn btn-success\" data-dismiss=\"modal\">Desvincular</button>
      </div>
    </div>
  </div>
</div>

<header class=\"panel-heading\">
        <div>
            <h2 class=\"panel-title\">

            </h2>
        </div>
</header>
<div class=\"panel-body\">
    <table id=\"expTable\" class=\"table table-striped\" data-pessoacontrato=\"";
        // line 38
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["ntePessoaContrato"] ?? null), "id", [], "any", false, false, false, 38), "html", null, true);
        echo "\">
        <tr>
            <th class=\"col-md-1\">Curso</th>
            <th class=\"col-md-4\">Disciplina</th>
            <th class=\"col-md-1\">Ano</th>
            <th class=\"col-md-1\">Período</th>
            <th class=\"col-md-3\">Turmas</th>
            <th class=\"col-md-1\">Exportação</th>
        </tr>
        ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cursos"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["curso"]) {
            // line 48
            echo "             ";
            // line 49
            echo "             ";
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["curso"], "idDisciplina", [], "any", false, false, false, 49)) > 0)) {
                // line 50
                echo "                <tr>
                    <td rowspan=\"";
                // line 51
                echo twig_escape_filter($this->env, (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["curso"], "idDisciplina", [], "any", false, false, false, 51)) + 1), "html", null, true);
                echo "\" data-cursoid=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["curso"], "id", [], "any", false, false, false, 51), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["curso"], "nome", [], "any", false, false, false, 51), "html", null, true);
                echo "</td>
                     ";
                // line 52
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["curso"], "idDisciplina", [], "any", false, false, false, 52));
                foreach ($context['_seq'] as $context["_key"] => $context["disciplina"]) {
                    // line 53
                    echo "                         <tr>
                            <td>";
                    // line 54
                    echo twig_escape_filter($this->env, $context["disciplina"], "html", null, true);
                    echo "</td>
                            <td class=\"tdAno\">
                                <select multiple=\"multiple\" class=\"selectAno\"
                                data-cursoidselectano=\"";
                    // line 57
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["curso"], "id", [], "any", false, false, false, 57), "html", null, true);
                    echo "\" data-disciplinaid=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["disciplina"], "id", [], "any", false, false, false, 57), "html", null, true);
                    echo "\" data-disciplinanome=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["disciplina"], "nome", [], "any", false, false, false, 57), "html", null, true);
                    echo "\" >
                                </select>
                            </td>
                            <td data-cursoidselectperiodo=\"";
                    // line 60
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["curso"], "id", [], "any", false, false, false, 60), "html", null, true);
                    echo "\" class=\"tdPeriodo\">
                                <select multiple=\"multiple\" class=\"selectPeriodo\" data-cursoidselectperiodo=\"";
                    // line 61
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["curso"], "id", [], "any", false, false, false, 61), "html", null, true);
                    echo "\"></select>
                            </td>
                            <td class=\"tdTurmas\">
                                <select id=\"idselectturmas\" multiple=\"multiple\" class=\"selectTurma\" data-cursoid=\"";
                    // line 64
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["curso"], "id", [], "any", false, false, false, 64), "html", null, true);
                    echo "\" data-disciplinaid=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["disciplina"], "id", [], "any", false, false, false, 64), "html", null, true);
                    echo "\"></select>
                            </td>
                            <td>
                                 <button id='fix-tamanho-botao' class=\"btn btn-success btn-sm aux\">
                                    Exportar
                                 </button>
                            </td>
                        </tr>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['disciplina'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 73
                echo "             ";
            }
            // line 74
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['curso'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "    </table>

";
    }

    // line 78
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 79
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>
        //função para centralizar Pnotify
        function get_center_pos(width, top)
        {
            if (!top)
            {
                top = 30;
                \$('.ui-pnotify').each(function() { top += \$(this).outerHeight() + 20; });
            }
            return {
                \"top\": top, \"left\": (\$(window).width() / 2) - (width / 2)
            }
        }

        let pessoaContratoId = \$('table').data('pessoacontrato');

        \$(function ()
        {

            \$(window).resize(function () {
                \$(\".ui-pnotify\").each(function () {
                    \$(this).css(get_center_pos(\$(this).width(), \$(this).position().top))
                });
            });

            // inicializaEventos();

            verificaTurmas();
            populaSelectPeriodo();
            populaSelectAno();
            populaSelectTurma();
            removerTurmas();

            //adiciona turmas selecionadas
            \$( \".btn.btn-success.btn-sm.aux\" ).click(function()
            {
                let turmasVal = \$(this).parent().siblings(\".tdTurmas\").find(\".selectTurma.select2-hidden-accessible\").val();
                this.turmasVal = turmasVal;
                inserirTurmas(pessoaContratoId, turmasVal);
            });
        });

         function notificaUsuario(titulo, texto, tipo)
         {
              new PNotify(
                {
                    title: titulo,
                    text: texto,
                    type: tipo,
                    delay: 3000,
                    before_open: function (PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    }
                }
            );
         }

        const estado = {
            idTurmaRemover: null
        };

         //dividir funcionalidades por botão, botar o clique dum botão em outro método.
        function removerTurmas()
        {
            \$('#modalremover').click(function() {
                if (!estado.idTurmaRemover) return;
                let idVinculo = \$('#expTable').data('pessoacontrato');

                \$.ajax({
                    url: \"";
        // line 149
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_remove_turma_exportacao");
        echo "\",
                    dataType: 'json',
                    method: \"POST\",
                    data:{
                        idVinculoKey: idVinculo,
                        idTurmaKey: estado.idTurmaRemover,
                    },
                    success: function (data) {
                        let opcoes = data;

                        for (let i = 0; i < opcoes.length; i++)
                        {
                            let object = opcoes[i];
                            for (let propriedade in object) {
                                if (propriedade === 'removeControl')
                                {
                                    if (object[propriedade] == 'turmaRemovida') {
                                        notificaUsuario(\"A Turma \" + object['nome'] + \" foi removida\" , \"\", 'info');
                                    }
                                }
                            }
                        }
                        estado.idTurmaRemover = null;
                    },
                    error: function (a) {
                        console.log(a);
                    }
                });
            });

            \$('.selectTurma')
                .on(\"select2:unselect\", function(e){

                    let dadosTurma = e.params.data;
                    let idTurma = e.params.data.id;
                    estado.idTurmaRemover = e.params.data.id;

                    \$(\"#modal\").modal();
                    let element = \$(this).parent().parent().find('.selectTurma');

                    \$(this).parent().parent().find('.selectTurma').select2({
                       width: '250px',
                       minimumResultsForSearch: Infinity
                    });

                    let nomeCodTurma = e.params.data.text;
                    let disciplina = \$(this).parent().siblings('.tdAno').find('.selectAno.select2-hidden-accessible').data('disciplinanome');
                    let idVinculo = \$('#expTable').data('pessoacontrato');


                    \$(\"#conteudoModal\").html(
                        \"Disciplina: \" + disciplina + \"<br>\" +
                        \"Turma: \" + nomeCodTurma
                    );

                    \$('#modalcancelar').click(function()
                    {
                        let appendTurma = new Option(nomeCodTurma, idTurma , true, true);

                        //if control n duplicar.
                        \$(element).append(appendTurma).trigger('change');
                    });

            });
        }

         /**
         * Apresenta turmas já exportadas (no select2), (Turmas que existem na tabela nte_contrato_tem_sie_turma relacionadas ao id_pessoa_contrato).
         */
        function verificaTurmas()
        {
            let idpessoacontrato = \$('table').data('pessoacontrato');
            fetch('";
        // line 221
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_verifica_turmas_exportadas");
        echo "' + `?id=\${idpessoacontrato}`,{
            credentials: 'include',
            }).then(res => {
               return res.json();
            }).then(res => {
                let arrayTurmas = (JSON.parse(res));
                let arrayDisciplinasId = [];
                let arrayTurmasId = [];
                let arrayNomeTurmas = [];
                let arrayTurmasIdIndex = [];
                let arrayIndexDisciplinas = [];
                let arrayCursosId = [];

                for (let i = 0; i < arrayTurmas.length; i++)
                {
                    let object = arrayTurmas[i];
                    for (let propriedade in object)
                    {
                        //add id do curso relacionado à turma
                        if (propriedade === 'idcurso')
                        {
                            arrayCursosId.push(object[propriedade]);
                        }

                        //add id da disciplina relacionada à turma
                        if (propriedade === 'iddisciplina')
                        {
                            arrayDisciplinasId.push(object[propriedade]);
                        }
                        //add id turma
                        if (propriedade === 'idTurma')
                        {
                            arrayTurmasId.push(object[propriedade])
                        }
                        //add nome turma
                        if (propriedade === 'codigo')
                        {
                            arrayNomeTurmas.push(object[propriedade])
                        }
                    }
                }

                //cada select turma
                \$('.selectTurma.select2-hidden-accessible').each(function()
                {
                    //id da disciplina da row do select
                    let disciplinaId = \$(this).data('disciplinaid');
                    let cursoId = \$(this).data('cursoid');

                    arrayDisciplinasId.forEach((value, index) =>
                    {
                        //encontra curso e disciplina
                        if (value === disciplinaId && cursoId == arrayCursosId[index])
                        {
                            //doc append option select2 -> \$(\"#my_select\").append(new Option(\"Label\", \"value\", true, true));
                            let appendTurma = new Option(arrayNomeTurmas[index], arrayTurmasId[index] , true, true);
                            \$(this).parent().parent().find('.selectTurma.select2-hidden-accessible').append(appendTurma).trigger('change');
                        }
                    });
                });
                return JSON.parse(res);
            });

        }

        /**
        * Captura turmas selecionadas e insere -> nte_contrato_tem_sie_turma, vínculando turmas ao id_pessoa_contrato
        *
        * @param contratoPessoa
        * @param turmas
        */
        function inserirTurmas(contratoPessoa, turmas)
        {
            \$.ajax({
                url: \"";
        // line 295
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_insere_vinculo_tem_turma");
        echo "\",
                dataType: 'json',
                method: \"POST\",
                data:{
                    ntePessoaContratoKey: contratoPessoa,
                    turmasKey: turmas,
                },
                success: function (data) {
                   let opcoes = data;

                    for (let i = 0; i < opcoes.length; i++) {
                        let object = opcoes[i];
                        for (let propriedade in object) {
                            if (propriedade === 'control')
                            {
                                if (object[propriedade] == 'rejeitada') {
                                    notificaUsuario(\"A Turma \" + object['nome'] + \" já foi selecionada para exportação\" , \"\", 'danger');
                                } else if (object[propriedade] == 'permitida') {
                                    notificaUsuario(\"A Turma \" + object['nome'] + \" foi selecionada para exportação\" , \"\", 'success');
                                }
                            }
                        }
                    }
                },
                error: function (a) {
                    console.log(a)
                }
            });
        }

        //busca turmas baseado na oferta gerada pelos selects Ano e Período
        function populaSelectTurma()
        {
            \$('.selectTurma').each( function() {
                    \$(this).select2({
                        placeholder: 'Turmas',
                        width: '250px',
                        ajax: {
                            url: '";
        // line 333
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_turmas");
        echo "' +
                                 `?ano=\${\$(this).parent().siblings('.tdAno').find('.selectAno.select2-hidden-accessible').val()}
                                 &disciplina=\${\$(this).parent().siblings('.tdAno').find('.selectAno').data('disciplinaid')}
                                 &periodo=\${\$(this).parent().siblings('.tdPeriodo').find('.selectPeriodo.select2-hidden-accessible').val()}
                                 &curso=\${\$(this).data('cursoid')}`,
                            dataType: 'json',
                            method: \"GET\",
                            processResults: (data) => {
                                    console.log(data);
                                return {
                                    results: data
                                };
                            },

                        }
                    });
            });
        }

        //busca em SieCursoOferta os anos existentes das ofertas existentes de um curso
        function populaSelectAno()
        {
            \$('.selectAno').each( function() {
                let cursoid = \$(this).data('cursoidselectano');
                \$(this).select2({
                    placeholder: 'Ano',
                    width: \"150px\",
                    ajax: {
                        url: '";
        // line 361
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_ano");
        echo "',
                        dataType: 'json',
                        method: \"POST\",
                        data: {
                            cursoidKey: cursoid
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                }})
            }).on('change', function (){
               // console.log(\$(this).val());
               populaSelectTurma();
            })
        }

        //busca em SieCursoOferta os anos existentes das ofertas existentes de um curso
        function populaSelectPeriodo()
        {
            \$('.selectPeriodo').each(function(){
                let cursoid = \$(this).data('cursoidselectperiodo');
                \$(this).select2({
                    placeholder: 'Período',
                    width: \"250px\",
                    ajax: {
                        url: '";
        // line 388
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_periodo");
        echo "',
                        dataType: 'json',
                        method: \"POST\",
                        data: {cursoidKey: cursoid},
                        processResults: function (data) {
                            return {results: data};
                        },
                }})
            }).on('change', function()
            {
                // console.log(\$(this).val());
                populaSelectTurma();
            })
        }

    </script>
";
    }

    // line 405
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 406
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <style>
        button a
        {
            color:#ffffff !important;
            text-decoration:none;
        }
    </style>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_vincular_turmas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  555 => 406,  552 => 405,  531 => 388,  501 => 361,  470 => 333,  429 => 295,  352 => 221,  277 => 149,  203 => 79,  200 => 78,  194 => 75,  188 => 74,  185 => 73,  168 => 64,  162 => 61,  158 => 60,  148 => 57,  142 => 54,  139 => 53,  135 => 52,  127 => 51,  124 => 50,  121 => 49,  119 => 48,  115 => 47,  103 => 38,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_vincular_turmas.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontrato/contrato_vincular_turmas.html.twig");
    }
}
