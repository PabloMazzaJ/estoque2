<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:Relatorio:log-resumo.html.twig */
class __TwigTemplate_91fd97b3046a684af96d669033979966820398f2214640b5f81d3a7c8ce20c8c extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoExportadorBundle:Relatorio:log-resumo.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Log Resumido - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "nomeCurso", [], "any", false, false, false, 2), "html", null, true);
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "nomeCurso", [], "any", false, false, false, 3), "html", null, true);
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span>Relatório</span></li>
     <li><span>Log</span></li>
     <li><span>";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "nomeCurso", [], "any", false, false, false, 7), "html", null, true);
        echo "</span></li>
 ";
    }

    // line 9
    public function block_conteudo($context, array $blocks = [])
    {
        // line 10
        echo "    <section class=\"panel \" id=\"relatorio\">
        <div class=\"panel-body\">
            <table class=\"table table-responsive table-condensed table-striped table-bordered\">
                <tr>
                    <th rowspan=\"2\">Data</th>
                    <th rowspan=\"2\">Status</th>
                    <!-- <th colspan=\"4\">Usuário</th>-->

                    <th colspan=\"2\">Usuários</th>

                    <th colspan=\"2\">Categoria</th>
                    <th colspan=\"2\">Subcategoria</th>
                    <th colspan=\"3\">Disciplina do Curso</th>
                    <th colspan=\"3\">Agrupamento</th>
                    <th colspan=\"3\">Grupos</th>
                    <th rowspan=\"2\">Inscritos</th>
                    <th rowspan=\"2\">Desinscritos</th>
                </tr>
                <tr>
              <!--      <th>Sie Ativo</th>
                    <th>Sie Inativo</th>
                    -->
                    <th>Novo</th>
                    <th>Existente</th>

                    <th>Novo</th>
                    <th>Existente</th>

                    <th>Novo</th>
                    <th>Existente</th>

                    <th>Novo</th>
                    <th>Existente</th>
                    <th>Oculta</th>

                    <th>Novo</th>
                    <th>Existente</th>
                    <th>Removidos</th>

                    <th>Novo</th>
                    <th>Existente</th>
                    <th>Removidos</th>
                </tr>
                ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rel"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 54
            echo "
                    <tr>
                        <td>
                            <a title=\"Clique aqui para ver o log completo\"
                               href=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 58), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 58)]), "html", null, true);
            echo "\">
                                ";
            // line 59
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "registroDataCriacao", [], "any", false, false, false, 59), "d/m/Y - H:i:s"), "html", null, true);
            echo "
                            </a>
                        </td>
                        <td class=\"center\">
                            ";
            // line 63
            if ((twig_get_attribute($this->env, $this->source, $context["r"], "exportacaoStatus", [], "any", false, false, false, 63) == 99)) {
                // line 64
                echo "                                <div class=\"label label-success\">Ok</div>
                            ";
            } elseif ((twig_get_attribute($this->env, $this->source,             // line 65
$context["r"], "exportacaoStatus", [], "any", false, false, false, 65) > 100)) {
                // line 66
                echo "                                <div class=\"label label-danger\">Erro</div>
                            ";
            } else {
                // line 68
                echo "                                <div class=\"label label-primary\">Exportando</div>
                            ";
            }
            // line 70
            echo "                        </td>
                  <!--      <td>";
            // line 71
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdUsuarioSieValido", [], "any", false, false, false, 71), "html", null, true);
            echo "</td>
                            <td>";
            // line 72
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdUsuarioSieInvalido", [], "any", false, false, false, 72), "html", null, true);
            echo "</td>
                        -->
                        <td>
                            <a href=\"";
            // line 75
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 75), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 75), "tipo" => 1, "op" => 2]), "html", null, true);
            echo "\">
                                ";
            // line 76
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdUsuarioMoodlenovo", [], "any", false, false, false, 76), "html", null, true);
            echo "
                            </a>
                        </td>
                        <td>
                            <a href=\"";
            // line 80
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 80), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 80), "tipo" => 1, "op" => 1]), "html", null, true);
            echo "\">
                                ";
            // line 81
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdUsuarioMoodleExistente", [], "any", false, false, false, 81), "html", null, true);
            echo "
                            </a>
                        </td>

                        <td>
                            <a href=\"";
            // line 86
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 86), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 86), "tipo" => 2, "op" => 2]), "html", null, true);
            echo "\">
                                ";
            // line 87
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdCategoriaNovo", [], "any", false, false, false, 87), "html", null, true);
            echo "
                            </a>
                        </td>
                        <td>
                            <a href=\"";
            // line 91
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 91), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 91), "tipo" => 2, "op" => 1]), "html", null, true);
            echo "\">
                                ";
            // line 92
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdCategoriaExistente", [], "any", false, false, false, 92), "html", null, true);
            echo "
                            </a>
                        </td>

                        <td>
                            <a href=\"";
            // line 97
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 97), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 97), "tipo" => 3, "op" => 2]), "html", null, true);
            echo "\">
                                ";
            // line 98
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdSubcategoriaNovo", [], "any", false, false, false, 98), "html", null, true);
            echo "
                            </a>
                        </td>
                        <td>
                            <a href=\"";
            // line 102
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 102), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 102), "tipo" => 3, "op" => 1]), "html", null, true);
            echo "\">
                                ";
            // line 103
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdSubcategoriaExistente", [], "any", false, false, false, 103), "html", null, true);
            echo "
                            </a>
                        </td>

                        <td>
                            <a href=\"";
            // line 108
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 108), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 108), "tipo" => 4, "op" => 2]), "html", null, true);
            echo "\">
                                ";
            // line 109
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdDisciplinaNovo", [], "any", false, false, false, 109), "html", null, true);
            echo "
                            </a>
                        </td>
                        <td>
                            <a href=\"";
            // line 113
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 113), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 113), "tipo" => 4, "op" => 1]), "html", null, true);
            echo "\">
                                ";
            // line 114
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdDisciplinaExistente", [], "any", false, false, false, 114), "html", null, true);
            echo "
                            </a>
                        </td>
                        <td>
                            <a href=\"";
            // line 118
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 118), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 118), "tipo" => 4, "op" => 5]), "html", null, true);
            echo "\">
                                ";
            // line 119
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdDisciplinaInexistente", [], "any", false, false, false, 119), "html", null, true);
            echo "
                            </a>
                        </td>

                        <td>
                            <a href=\"";
            // line 124
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 124), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 124), "tipo" => 5, "op" => 2]), "html", null, true);
            echo "\">
                                ";
            // line 125
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdAgrupamentoNovo", [], "any", false, false, false, 125), "html", null, true);
            echo "
                            </a>
                        </td>
                        <td>
                            <a href=\"";
            // line 129
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 129), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 129), "tipo" => 5, "op" => 1]), "html", null, true);
            echo "\">
                                ";
            // line 130
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdAgrupamentoExistente", [], "any", false, false, false, 130), "html", null, true);
            echo "
                            </a>
                        </td>
                        <td>
                            <a href=\"";
            // line 134
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 134), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 134), "tipo" => 5, "op" => 1]), "html", null, true);
            echo "\">
                                ";
            // line 135
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdAgrupamentoRemovido", [], "any", false, false, false, 135), "html", null, true);
            echo "
                            </a>
                        </td>

                        <td>
                            <a href=\"";
            // line 140
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 140), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 140), "tipo" => 6, "op" => 2]), "html", null, true);
            echo "\">
                                ";
            // line 141
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdGrupoNovo", [], "any", false, false, false, 141), "html", null, true);
            echo "
                            </a>
                        </td>
                        <td>
                            <a href=\"";
            // line 145
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 145), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 145), "tipo" => 6, "op" => 1]), "html", null, true);
            echo "\">
                                ";
            // line 146
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdGrupoExistente", [], "any", false, false, false, 146), "html", null, true);
            echo "
                            </a>
                        </td>
                        <td>
                            <a href=\"";
            // line 150
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 150), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 150), "tipo" => 6, "op" => 2]), "html", null, true);
            echo "\">
                                ";
            // line 151
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdGrupoRemovido", [], "any", false, false, false, 151), "html", null, true);
            echo "
                            </a>
                        </td>
                        <td>
                            <a href=\"";
            // line 155
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 155), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 155), "tipo" => 7, "op" => 2]), "html", null, true);
            echo "\">
                                ";
            // line 156
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdMatricula", [], "any", false, false, false, 156), "html", null, true);
            echo "
                            </a>
                        </td>
                        <td>
                            <a href=\"";
            // line 160
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "id", [], "any", false, false, false, 160), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 160), "tipo" => 8, "op" => 2]), "html", null, true);
            echo "\">
                                ";
            // line 161
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "qtdDesmatricula", [], "any", false, false, false, 161), "html", null, true);
            echo "
                            </a>
                        </td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 166
        echo "            </table>
        </div>
    </section>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:Relatorio:log-resumo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  366 => 166,  355 => 161,  351 => 160,  344 => 156,  340 => 155,  333 => 151,  329 => 150,  322 => 146,  318 => 145,  311 => 141,  307 => 140,  299 => 135,  295 => 134,  288 => 130,  284 => 129,  277 => 125,  273 => 124,  265 => 119,  261 => 118,  254 => 114,  250 => 113,  243 => 109,  239 => 108,  231 => 103,  227 => 102,  220 => 98,  216 => 97,  208 => 92,  204 => 91,  197 => 87,  193 => 86,  185 => 81,  181 => 80,  174 => 76,  170 => 75,  164 => 72,  160 => 71,  157 => 70,  153 => 68,  149 => 66,  147 => 65,  144 => 64,  142 => 63,  135 => 59,  131 => 58,  125 => 54,  121 => 53,  76 => 10,  73 => 9,  67 => 7,  63 => 5,  60 => 4,  54 => 3,  47 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:Relatorio:log-resumo.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/Relatorio/log-resumo.html.twig");
    }
}
