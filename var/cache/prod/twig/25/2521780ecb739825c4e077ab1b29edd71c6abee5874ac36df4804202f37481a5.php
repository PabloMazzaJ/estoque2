<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteConfigBaseBundle:Default:index.html.twig */
class __TwigTemplate_05509de7e1dee9545132391b5875cd32965f618b0923d5c9c72402536b6c97eb extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteConfigBaseBundle:Default:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Configurações";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Configurações";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "<li><span>Painel de Configurações</span></li>
";
    }

    // line 7
    public function block_conteudo($context, array $blocks = [])
    {
        // line 8
        echo "
    <section class=\"panel text-center\">
        <div class=\" panel-body\">
            <div class=\"row\">
                <div class=\"col-md-4 \">
                    <a href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_homepage");
        echo "\">
                        <div class=\"featured-box featured-box-primary featured-box-effect-3\" style=\"height: 168px;\">
                            <div class=\"box-content\">
                                <i class=\"icon-featured fa fa-cloud-download fa-5x\"></i>
                                <h4>Moodle</h4>
                                <p>Gerencia as configurações do ambiente moodle</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class=\"col-md-4 \">
                    <a href=\"";
        // line 24
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_sie_homepage");
        echo "\">
                        <div class=\"featured-box featured-box-primary featured-box-effect-3\" style=\"height: 168px;\">
                            <div class=\"box-content\">
                                <i class=\"icon-featured fa fa-users fa-5x\"></i>
                                <h4>Sie</h4>
                                <p>Gerencia as configurações do Sistema Acadêmico</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class=\"col-md-4\">
                    <a href=\"#\">
                        <div class=\"featured-box featured-box-primary featured-box-effect-3\" style=\"height: 168px;\">
                            <div class=\"box-content\">
                                <i class=\"icon-featured fa fa-clock-o fa-5x\"></i>
                                <h4>Cron</h4>
                                <p>Gerencia o agendamento de tarefas</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
";
    }

    public function getTemplateName()
    {
        return "NteConfigBaseBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 24,  77 => 13,  70 => 8,  67 => 7,  62 => 5,  59 => 4,  53 => 3,  47 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteConfigBaseBundle:Default:index.html.twig", "/var/www/narfi/src/Nte/Config/BaseBundle/Resources/views/Default/index.html.twig");
    }
}
