<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* AlmoxarifadoBundle:Requisicao:analisar-requisicao.html.twig */
class __TwigTemplate_b0c3820fb4b2f6d19ac71f45d2ef94fe4668a9a37533bb582409f69266ada68d extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "analisar requisicao";
    }

    public function getTemplateName()
    {
        return "AlmoxarifadoBundle:Requisicao:analisar-requisicao.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "AlmoxarifadoBundle:Requisicao:analisar-requisicao.html.twig", "/var/www/narfi/src/Nte/Aplicacao/AlmoxarifadoBundle/Resources/views/Requisicao/analisar-requisicao.html.twig");
    }
}
