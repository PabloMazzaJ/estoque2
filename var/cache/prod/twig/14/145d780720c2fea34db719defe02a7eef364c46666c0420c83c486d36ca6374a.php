<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_edit.html.twig */
class __TwigTemplate_17f818ccec632264b92bd2de26bf145f11bb3d07d2943475989c542a10530f55 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_edit.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo "Pessoas";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\">Pessoal Externo</a></span></li>
     <li><span><a href=\"";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_mostra");
        echo "\">Pessoa</a></span></li>
     <li><span><a href=\"#\">Edição</a></span></li>
     <li><span><a href=\"#\">";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoa"] ?? null), "nome", [], "any", false, false, false, 8), "html", null, true);
        echo "</a></span></li>

 ";
    }

    // line 11
    public function block_conteudo($context, array $blocks = [])
    {
        // line 12
        echo "    <header class=\"panel-heading\">
        <div>
            <h2 class=\"panel-title\">
                <span class=\"va-middle\"><i class=\"fa fa-users\"></i> Edição de Pessoa</span>
            </h2>
        </div>
    </header>
    <div class=\"panel-body\">
        ";
        // line 20
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
        <div class=\"row\">
            <div class=\"form-group col-md-3\">
                ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nome", [], "any", false, false, false, 23), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-4\">
                ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "sobrenome", [], "any", false, false, false, 26), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "identidade", [], "any", false, false, false, 29), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 32
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "cpf", [], "any", false, false, false, 32), 'row');
        echo "
            </div>

        </div>

        <div class=\"row\">
            <div class=\"form-group col-md-3\">
                ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, false, 39), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "usuarioMoodle", [], "any", false, false, false, 42), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "telefone0", [], "any", false, false, false, 45), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "telefone1", [], "any", false, false, false, 48), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "dataNascimento", [], "any", false, false, false, 51), 'row');
        echo "
            </div>
        </div>

        <div class=\"row\">
            <div class=\"form-group col-md-4\">
                ";
        // line 57
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoLogradouro", [], "any", false, false, false, 57), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-3\">
                ";
        // line 60
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoNumero", [], "any", false, false, false, 60), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-4\">
                ";
        // line 63
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoBairro", [], "any", false, false, false, 63), 'row');
        echo "
            </div>
        </div>

        <div class=\"row\">
            <div class=\"form-group col-md-4\">
                ";
        // line 69
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoComplemento", [], "any", false, false, false, 69), 'row', ["label" => "Complemento"]);
        echo "
            </div>
            <div class=\"form-group col-md-3\">
                ";
        // line 72
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoCidade", [], "any", false, false, false, 72), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 75
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoUf", [], "any", false, false, false, 75), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 78
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoCep", [], "any", false, false, false, 78), 'row');
        echo "
            </div>
        </div>

        <div class=\"row\">
            <div class=\"form-group col-md-6\">
                <a href=\"";
        // line 84
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_mostra");
        echo "\">
                    <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"expButton\"><i class=\"fa fa-arrow-circle-left\"></i></button>
                </a>
                <button type=\"submit\" value=\"Cadastrar\" class=\"btn btn-primary btn-sm\" id=\"expButton\">Cadastrar</button>
            </div>

            <div class=\"form-group col-md-5\"></div>
        </div>
        ";
        // line 92
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
    </div>

";
    }

    // line 97
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 98
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>
    </script>
";
    }

    // line 103
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 104
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>
        .btn {
            min-width: 9em;
            max-width: 9em;
        }

        .btn.btn-success {
            margin-top: 1em;
        }

        .row {
            display: flex;
            justify-content: center;
        }





    </style>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  240 => 104,  237 => 103,  228 => 98,  225 => 97,  217 => 92,  206 => 84,  197 => 78,  191 => 75,  185 => 72,  179 => 69,  170 => 63,  164 => 60,  158 => 57,  149 => 51,  143 => 48,  137 => 45,  131 => 42,  125 => 39,  115 => 32,  109 => 29,  103 => 26,  97 => 23,  91 => 20,  81 => 12,  78 => 11,  71 => 8,  66 => 6,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_edit.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntepessoa/pessoa_edit.html.twig");
    }
}
