<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:Default:regra-index.html.twig */
class __TwigTemplate_47c07f2106c270ce174dac66ad367748e6c2775592ce235727d37d425bfbac6a extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"panel-body\">
    <table class=\"table table-striped\">
        <tr>
            <th>Cod</th>
            <th>Disciplina</th>
            <th>Curso</th>
            <th>Turma</th>
            <th>Regra de Agrupamento</th>
        </tr>
        ";
        // line 10
        $context["disciplinaPassadas"] = [];
        // line 11
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["disciplina"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
            // line 12
            echo "
        <tr>
            ";
            // line 14
            if (!twig_in_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["d"], "idDisciplina", [], "any", false, false, false, 14), "id", [], "any", false, false, false, 14), ($context["disciplinaPassadas"] ?? null))) {
                // line 15
                echo "                ";
                $context["disciplinaPassadas"] = twig_array_merge(($context["disciplinaPassadas"] ?? null), [0 => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["d"], "idDisciplina", [], "any", false, false, false, 15), "id", [], "any", false, false, false, 15)]);
                // line 16
                echo "                <td class=\"alinhamento-vertical\"
                    rowspan=\"";
                // line 17
                echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["rel"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["d"], "idDisciplina", [], "any", false, false, false, 17), "id", [], "any", false, false, false, 17)] ?? null) : null), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["d"], "idDisciplina", [], "any", false, false, false, 17), "codigo", [], "any", false, false, false, 17), "html", null, true);
                echo "</td>
                <td class=\"alinhamento-vertical\"
                    rowspan=\"";
                // line 19
                echo twig_escape_filter($this->env, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["rel"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["d"], "idDisciplina", [], "any", false, false, false, 19), "id", [], "any", false, false, false, 19)] ?? null) : null), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["d"], "idDisciplina", [], "any", false, false, false, 19), "nome", [], "any", false, false, false, 19), "html", null, true);
                echo " </td>
            ";
            }
            // line 21
            echo "            <td>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["d"], "idCurso", [], "any", false, false, false, 21), "nome", [], "any", false, false, false, 21), "html", null, true);
            echo " </td>
            <td>";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "codigo", [], "any", false, false, false, 22), "html", null, true);
            echo " </td>
            <td>
                <select data-idturma=\"";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "id", [], "any", false, false, false, 24), "html", null, true);
            echo "\" class=\"select22\">
                    <option></option>
                    ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["listaRegras"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 27
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "id", [], "any", false, false, false, 27), "html", null, true);
                echo "\" ";
                if ((twig_in_filter(twig_get_attribute($this->env, $this->source, $context["d"], "id", [], "any", false, false, false, 27), twig_get_array_keys_filter(($context["regras"] ?? null))) && ((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["regras"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[twig_get_attribute($this->env, $this->source, $context["d"], "id", [], "any", false, false, false, 27)] ?? null) : null) == twig_get_attribute($this->env, $this->source, $context["r"], "id", [], "any", false, false, false, 27)))) {
                    echo "selected=\"selected\"";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "descricao", [], "any", false, false, false, 27), "html", null, true);
                echo "</option>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "                </select>
            </td>
        </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "    </table>
</div>
<style>
    .alinhamento-vertical {
        vertical-align: middle !important;
    }
</style>
<script>
    \$select2 = \$('.select22').select2({
        placeholder: \"Selecione um agrupamento\",
        allowClear: true,
        width: \"100%\"
    });
    \$select2.on(\"select2:select\", function (e) {
        console.log(this.value);
        atualizarRegra(\$(this).data('idturma'),this.value);
    });

    \$select2.on(\"select2:unselect\", function (e) {
        console.log('vazio');
        atualizarRegra(\$(this).data('idturma'),null);
    });
    function atualizarRegra(turma,regra) {
        \$.ajax({
            type: 'POST',
            url: '";
        // line 58
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_aplicacao_exportador_regras_atualizar");
        echo "',
            data: {
                idTurma: turma,
                opt: regra
            },
            complete: function (data) {
                new PNotify({
                    title: data.responseJSON.title,
                    text: data.responseJSON.text,
                    type: data.responseJSON.response
                });
            }
        });
    }
</script>";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:Default:regra-index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 58,  122 => 33,  113 => 29,  98 => 27,  94 => 26,  89 => 24,  84 => 22,  79 => 21,  72 => 19,  65 => 17,  62 => 16,  59 => 15,  57 => 14,  53 => 12,  48 => 11,  46 => 10,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:Default:regra-index.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/Default/regra-index.html.twig");
    }
}
