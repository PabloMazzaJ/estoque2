<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_cadastro.html.twig */
class __TwigTemplate_c89598ab5abe53751907bfbd1bbb8d05364fc369a652ffd4f8a7ff41dd3fc31d extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_cadastro.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo "Cadastro Contrato";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Pessoal Externo</a></span></li>
     <li><span><a href=\"";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_index");
        echo "\" class=\"breadcumbComLink\">Contrato</a></span></li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Cadastro</a></span></li>
 ";
    }

    // line 9
    public function block_conteudo($context, array $blocks = [])
    {
        // line 10
        echo "    <header class=\"panel-heading\">
        <h2 class=\"panel-title\">
            <span class=\"va-middle\"><i class=\"fa fa-paste\"></i> Cadastro de Contrato</span>
        </h2>
    </header>
    <div class=\"panel-body\">
        ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
           <div class=\"col-md-5\">
                ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "descricao", [], "any", false, false, false, 18), 'row');
        echo "
                ";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "editalNumero", [], "any", false, false, false, 19), 'row');
        echo "
                ";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "idPapel", [], "any", false, false, false, 20), 'row');
        echo "
                ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "idCurso", [], "any", false, false, false, 21), 'row', ["attr" => ["id" => "cursoClass"]]);
        echo "
                ";
        // line 22
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "editalDataPublicacao", [], "any", false, false, false, 22), 'row');
        echo "
                ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "periodoInicial", [], "any", false, false, false, 23), 'row');
        echo "
                ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "periodoFinal", [], "any", false, false, false, 24), 'row');
        echo "
               <div>
                   <a href=\"";
        // line 26
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_index");
        echo "\" class=\"btn-gerar-relatorio2\">
                       <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"expButton\"><i class=\"fa fa-arrow-circle-left\"></i></button>
                   </a>
                   <button type=\"submit\" class=\"btn btn-primary btn-sm\" id=\"expButton\">Cadastrar</button>
               </div>
           </div>

            ";
        // line 34
        echo "                ";
        // line 35
        echo "                    ";
        // line 36
        echo "                    ";
        // line 37
        echo "                ";
        // line 38
        echo "                ";
        // line 39
        echo "                    ";
        // line 40
        echo "                    ";
        // line 41
        echo "                ";
        // line 42
        echo "            ";
        // line 43
        echo "

    </div>
";
        // line 46
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
";
    }

    // line 49
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 50
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>

    \$(function() {
        \$('#nteContrato_Enviar').hide();
    });

    </script>
";
    }

    // line 60
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 61
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>
       .btn {
            margin-top: 1em;
            min-width: 9em;
            max-width: 9em;
        }

       #nteContrato_idCurso{
           height: 14em;
       }

       tbody tr th {
            text-align: center;
        }

        table th,
        table  {
            text-align: left;
            vertical-align: middle;
        }

        #aux {
            margin-left: 2em;
        }

        .col-md-1 label {
            width: 80%;
        }
        .col-md-1 input {
            width: 20%;
        }
        .col-md-2 label {
            width: 90%;
        }
        .col-md-2 input {
            width: 10%;
        }

       li span a:hover{
            text-decoration: none;
        }

        #nteContrato_contratoItem > input {
            clear: left;
        }

        .breadcumbSemLink {
            cursor: no-drop;
        }

        .breadcumbComLink {
            cursor: pointer;
        }
    </style>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_cadastro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 61,  174 => 60,  160 => 50,  157 => 49,  151 => 46,  146 => 43,  144 => 42,  142 => 41,  140 => 40,  138 => 39,  136 => 38,  134 => 37,  132 => 36,  130 => 35,  128 => 34,  118 => 26,  113 => 24,  109 => 23,  105 => 22,  101 => 21,  97 => 20,  93 => 19,  89 => 18,  84 => 16,  76 => 10,  73 => 9,  66 => 6,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_cadastro.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontrato/contrato_cadastro.html.twig");
    }
}
