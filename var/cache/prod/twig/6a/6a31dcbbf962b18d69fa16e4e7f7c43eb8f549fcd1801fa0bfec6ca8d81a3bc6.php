<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteUsuarioBundle:Usuario:form.html.twig */
class __TwigTemplate_e648d81db39fe54408d268e9640fc3d24e037b4c6f42bc44fcfb43f20f4f6d28 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 3
        if ((twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 3) == null)) {
            // line 4
            $context["titulo"] = "Novo";
        } else {
            // line 6
            $context["titulo"] = "Edição de ";
        }
        // line 2
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteUsuarioBundle:Usuario:form.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_titulo($context, array $blocks = [])
    {
        echo twig_escape_filter($this->env, ($context["titulo"] ?? null), "html", null, true);
        echo " Usuário ";
    }

    // line 10
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo twig_escape_filter($this->env, ($context["titulo"] ?? null), "html", null, true);
        echo " Usuário ";
    }

    // line 11
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 12
        echo "     <li><span><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_index");
        echo "\"> Usuários </a></span></li>
     <li><span>";
        // line 13
        echo twig_escape_filter($this->env, ($context["titulo"] ?? null), "html", null, true);
        echo " Usuários</span></li>
 ";
    }

    // line 15
    public function block_conteudo($context, array $blocks = [])
    {
        // line 16
        echo "    <div class=\"row\">
        <div class=\"col-md-4 col-lg-3\">
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <div class=\"kv-avatar\">
                        <div class=\"file-loading\">
                            <input id=\"file\" name=\"file\" type=\"file\" required>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class=\"col-md-8 col-lg-9\">
            <div class=\"panel\">
                <div class=\"panel-heading\">
                    <h2 class=\"panel-title\"><i class=\"fa fa-user-plus\"></i> ";
        // line 31
        echo twig_escape_filter($this->env, ($context["titulo"] ?? null), "html", null, true);
        echo " Usuário</h2>
                </div>
                <div id=\"formulario\" class=\"panel-body\">
                    ";
        // line 34
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
                    <div class=\"form-group\">
                        <div class=\"col-md-6\">
                            ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nome", [], "any", false, false, false, 37), 'row');
        echo "
                            ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "cpf", [], "any", false, false, false, 38), 'row');
        echo "

                        </div>
                        <div class=\"col-md-6\">
                            ";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "username", [], "any", false, false, false, 42), 'row');
        echo "
                            ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, false, 43), 'row');
        echo "
                            ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <button type=\"reset\" class=\"btn btn-default pull-left\">
                            <i class=\"fa fa-trash-o\"></i> Resetar
                        </button>
                        <button type=\"submit\" class=\"btn btn-primary pull-right\">
                            <i class=\"fa fa-save\"> </i> Salvar
                        </button>
                    </div>
                    ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
                    ";
        // line 56
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 62
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 63
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-fileinput/css/fileinput.min.css"), "html", null, true);
        echo "\">
    <style>
        .kv-avatar .krajee-default.file-preview-frame, .kv-avatar .krajee-default.file-preview-frame:hover {
            margin: 0;
            padding: 0;
            border: none;
            box-shadow: none;
            text-align: center;
        }

        .kv-avatar {
            display: inline-block;
        }

        .kv-avatar .file-input {
            display: table-cell;
            width: 213px;
        }

        .kv-reqd {
            color: red;
            font-family: monospace;
            font-weight: normal;
        }
        .file-drop-zone.clickable:hover {
            border: 1px dashed #999;
        }
    </style>
";
    }

    // line 95
    public function block_javascripts($context, array $blocks = [])
    {
        // line 96
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-fileinput/js/fileinput.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-fileinput/js/locales/pt-BR.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-validation/jquery.validate.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/CPF/CPF.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$.ajaxSetup({
            timeout: 99999999999,
            headers:{
                'X-CSRFToken':\"";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("friga-upload-usuario-imagem-cabecalho"), "html", null, true);
        echo "\"
            }
        });
        \$('select').select2({allowClear: true});
        \$('button').submit(function (e) {
            e.preventDefault();
            var validated = \$('form').valid();
            if (validated) {
                \$('form').submit();
            }
        });

        \$.validator.addMethod(\"cpf\", function (value, element) {
            return this.optional(element) || CPF.validate(value);
        }, \"Por favor, Informe um CPF válido.\");

        \$.extend(\$.validator.messages, {
            required: \"Este campo é obrigatório\",
            email: \"Por favor, preencha com o endereço de e-mail válido\",
        });

        \$(\"form\").validate({
            rules: {
                nte_usuario_primeiro_acesso_plainPassword_first: \"required\",
                nte_usuario_primeiro_acesso_plainPassword_second: {equalTo: \"#nte_usuario_primeiro_acesso_plainPassword_first\"}
            }
        });
        \$('#";
        // line 132
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "cpf", [], "any", false, false, false, 132), "vars", [], "any", false, false, false, 132), "id", [], "any", false, false, false, 132), "html", null, true);
        echo "').mask('000.000.000-00');

        \$(\"#file\").fileinput({
            language: \"pt-BR\",
            uploadUrl: \"";
        // line 136
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_uploadimagem", ["id" => twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 136)]), "html", null, true);
        echo "\",
            uploadAsync: false,
            minFileCount: 1,
            maxFileCount: 1,
            overwriteInitial: true,
            maxFileSize: 5000,
            showUpload: false,
            showClose: false,
            showCaption: false,
            showBrowse: false,
            showRemove: false,
            removeFromPreviewOnError: true,
            browseOnZoneClick: true,
            removeLabel: '',
            elErrorContainer: '#kv-avatar-errors-2',
            msgErrorClass: 'alert alert-block alert-danger',
           // defaultPreviewContent: '<img src=\"{ path('nte_usuario_imagem_perfil',{id: entity.id}) }}\" class=\"img-rounded img-responsive\" ><h6 class=\"text-muted\">Clique aqui para trocar a sua imagem</h6>',
            layoutTemplates: {
            },
            uploadExtraData:{
                xtk:\"";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("friga-upload-usuario-imagem-form"), "html", null, true);
        echo "\"
            },
            allowedFileTypes: [\"image\"]
        }).on(\"filebatchselected\", function(event, files) {
            \$(\"#file\").fileinput(\"upload\");
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "NteUsuarioBundle:Usuario:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  294 => 156,  271 => 136,  264 => 132,  234 => 105,  226 => 100,  222 => 99,  218 => 98,  214 => 97,  209 => 96,  206 => 95,  173 => 64,  168 => 63,  165 => 62,  155 => 56,  151 => 55,  137 => 44,  133 => 43,  129 => 42,  122 => 38,  118 => 37,  112 => 34,  106 => 31,  89 => 16,  86 => 15,  80 => 13,  75 => 12,  72 => 11,  65 => 10,  58 => 9,  53 => 2,  50 => 6,  47 => 4,  45 => 3,  39 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteUsuarioBundle:Usuario:form.html.twig", "/var/www/narfi/src/Nte/UsuarioBundle/Resources/views/Usuario/form.html.twig");
    }
}
