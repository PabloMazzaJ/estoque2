<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoBuscadorBundle:Pessoas:perfil.html.twig */
class __TwigTemplate_5bd3549daed7c11e157370f0efc181d019e0143b0e513ca510a82791c30ae1ad extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"row\">
    ";
        // line 2
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_foto.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil.html.twig", 2)->display($context);
        // line 3
        echo "    <div class=\"col-lg-9 col-md-8 col-sm-12\">
        <div class=\"row\">
            ";
        // line 5
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_cadastro.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil.html.twig", 5)->display($context);
        // line 6
        echo "            ";
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_contato.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil.html.twig", 6)->display($context);
        // line 7
        echo "        </div>
        <div class=\"row\">
            ";
        // line 9
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_usuario.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil.html.twig", 9)->display($context);
        // line 10
        echo "        </div>
    </div>
</div>
<div class=\"row\">
    ";
        // line 14
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_alteracao.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil.html.twig", 14)->display($context);
        // line 15
        echo "    ";
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_turma.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil.html.twig", 15)->display($context);
        // line 16
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoBuscadorBundle:Pessoas:perfil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 16,  63 => 15,  61 => 14,  55 => 10,  53 => 9,  49 => 7,  46 => 6,  44 => 5,  40 => 3,  38 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoBuscadorBundle:Pessoas:perfil.html.twig", "/var/www/narfi/src/Nte/Aplicacao/BuscadorBundle/Resources/views/Pessoas/perfil.html.twig");
    }
}
