<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_criacao_detalhes.html.twig */
class __TwigTemplate_c0e3a6f81033656b2ff49fa3c5536e73b9231fb9126d76dc1a84971f72e91897 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_criacao_detalhes.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Detalhes Contrato ";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Pessoal Externo</a></span></li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Contrato</a></span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "<header class=\"panel-heading center\">
    <div>
        <h2 class=\"panel-title\">
            <span class=\"va-middle\">Lista de Termos e Declarações</span>
        </h2>
    </div>
</header>
<div class=\"panel-body center\">
    <table class=\"table table-striped\" id=\"myTable\">
        <tr>
            <th class=\"col-md-1 \">Ordem</th>
            <th class=\"col-md-1\">Item</th>
            <th class=\"col-md-1\">Obrigatório</th>
        </tr>

        ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["lista"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 25
            echo "            <tr>
                <td style=\"vertical-align: middle\">
                    ";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 27), "html", null, true);
            echo "
                </td>
                <td style=\"vertical-align: middle\">
                    ";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "item", [], "any", false, false, false, 30), "html", null, true);
            echo "
                </td>
                <td style=\"vertical-align: middle\">
                    ";
            // line 33
            if ((twig_get_attribute($this->env, $this->source, $context["item"], "obrigatorio", [], "any", false, false, false, 33) == true)) {
                // line 34
                echo "                        <span class=\"check-obrigatorio\">&#10003;</span>
                    ";
            } else {
                // line 36
                echo "                        <span class=\"x-mark-obrigatorio\">&#10005;</span>
                    ";
            }
            // line 38
            echo "                </td>
            </tr>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "    </table>
    ";
        // line 43
        echo "    <a href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_cadastro_contrato_parte_dois", ["id" => twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "id", [], "any", false, false, false, 43)]), "html", null, true);
        echo "\" class=\"btn-gerar-relatorio2\">
        <div style=\"float: left\" title=\"Clique aqui para realizar alterações na lista\">
            <button type=\"button\" class=\"btn btn-primary btn-sm btn-strict\" id=\"buttonRetornar\"><i class=\"fa fa-arrow-circle-left\"></i></button>
        </div>
    </a>

    <div style=\"float: right\" title=\"Clique aqui para vincular este contrato a uma pessoa\">
        <a href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_contrato_vinculo_direto_pos_lista_termos", ["id" => twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "id", [], "any", false, false, false, 50)]), "html", null, true);
        echo "\" class=\"btn-gerar-relatorio2\">
            <button type=\"button\" class=\"btn btn-primary btn-sm\">Vincular Pessoas</button>
        </a>
    </div>
</div>

";
    }

    // line 57
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 58
        echo "
    ";
        // line 59
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "

    <script>
        \$(function ()
        {
            //todo - vincuklar


        });

    </script>
";
    }

    // line 72
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 73
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>

        table {
            cursor: default;
        }
        
        .x-mark-obrigatorio {
            color: #b61124;
        }

        .check-obrigatorio {
            font-size: larger;
            color: #00e158;
        }

        table th {
            text-align: center;
        }

        .center {
            margin: auto;
            width: 60%;
        }

        .btn-strict {
            min-width: 9em;
            max-width: 9em;
        }

        .breadcumbSemLink { cursor: no-drop; }

        .breadcumbComLink { cursor: pointer; }

    </style>

";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_criacao_detalhes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 73,  195 => 72,  179 => 59,  176 => 58,  173 => 57,  162 => 50,  151 => 43,  148 => 41,  132 => 38,  128 => 36,  124 => 34,  122 => 33,  116 => 30,  110 => 27,  106 => 25,  89 => 24,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_criacao_detalhes.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontrato/contrato_criacao_detalhes.html.twig");
    }
}
