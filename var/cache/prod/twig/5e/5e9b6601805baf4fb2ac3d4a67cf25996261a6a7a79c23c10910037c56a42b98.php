<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* AlmoxarifadoBundle:Requisicao:criar-requisicao.html.twig */
class __TwigTemplate_c36872104d19553bf20d6864e2abfbf4aabac12c1d25c11a67823117278f9596 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "AlmoxarifadoBundle:Requisicao:criar-requisicao.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
    }

    // line 3
    public function block_conteudo($context, array $blocks = [])
    {
        // line 4
        echo "
    <div class=\"container-requisitar\">
        <button type=\"button\" class=\"btn btn-warning obs-btn\" id=\"swal\">
            Observação
            <i class='fa fa-plus-circle'></i>
        </button>
        <button type=\"button\" class=\"btn btn-info obs-btn\" id=\"print-model\">
            Printa Model
            <i class='fa fa-plus-circle'></i>
        </button>
    </div>

    <div class=\"container-requisitar\">
        <div class=\"panel panel-primary panel-size\">
            <div class=\"panel-heading\">
                <span data-requisicao=\"teste\">Requisitar Produto</span>
            </div>

            <div class=\"panel panel-primary panel-display-row\">
                <div class=\"product-subcontainer\">

                    <div class=\"panel-body container-selects\" id=\"clone-container\">
                        <div class=\"subcontainer-produto\">
                            <label for=\"select-produto\">Produto</label>
                            <select class=\"form-control form-control-sm fullwidth\" id=\"select-0\"
                                    name=\"select-produto\"></select>
                        </div>

                        <fieldset disabled>
                            <div class=\"subcontainer-valor\">
                                <label>Preço un.</label>
                                <input class=\"form-control\">
                            </div>
                        </fieldset>

                        <div class=\"subcontainer-second\">
                            <label>Quantidade</label>
                            <input type=\"number\" min=\"0\" class=\"form-control\">
                        </div>

                        <div class=\"subcontainer-remove-row\">
                            <label class=\"label-trash\">Quantidade</label>
                            <button class=\"btn btn-danger btn-sm btn-sizing-trash\" name=\"remover\">
                                <i class='fa fa-trash'></i>
                            </button>
                        </div>

                    </div>
                </div>

                <div class=\"panel-body container-selects\">
                    <div class=\"subcontainer-second\" id=\"container-btn-pos\">
                        <button class=\"btn btn-success btn-sm btn-sizing\" id=\"btn-check\">
                            <i class='fa fa-check'></i>
                        </button>
                        <button class=\"btn btn-success btn-sm btn-sizing\" id=\"btn-plus\" name=\"produto\">
                            <i class='fa fa-plus'></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

";
    }

    // line 70
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 71
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/swal/sweetalert2.min.js"), "html", null, true);
        echo "\"></script>
    <script>

        (function () {

            const model = {
                produtos: [],
                selectIdCounter: 1,
                containerRemover: 1,
                observacao: '',
                ultimaRequisicao: '',
            };

            const controller = {

                init() {
                    this.buscarProdutos();
                    view.init();
                },

                setValorModel(nome, valor) {
                    model[nome] = valor;
                },

                pushProduto(produto) {
                    model.produtos.push(produto);
                },

                updateProdutoValor(target) {
                    if (target.name === 'select-produto') {
                        let selectedProdutoValor = \$(target).find(':selected').attr('data-valor');
                        \$(target).parent().parent().find('.subcontainer-valor > input').val(selectedProdutoValor);
                    }
                },

                buscarProdutos() {
                    \$.ajax({
                        url: \"";
        // line 109
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("almox_lista_produtos");
        echo "\",
                        dataType: 'json',
                        method: \"POST\",
                        data: {},
                        success: function (data) {
                            let dataUtil = JSON.parse(data);
                            view.inicializarSelectProdutos(dataUtil);
                        },
                        error: function (data) {
                        },
                    })
                },

                getProdutosData() {
                    let containerProduto = document.querySelector('.product-subcontainer');
                    let children = containerProduto.children;

                    for (let i = 0; i < children.length; i++) {
                        let itemId = \$(children[i]).find('select').val();
                        let preco = \$(children[i]).find('input').val();
                        let quantidade = \$(children[i]).find('.subcontainer-second > input').val();
                        let produtoObj = {'idItem': itemId, 'preco': preco, 'quantidade': quantidade};
                        this.pushProduto(produtoObj)
                    }
                    console.table(model.produtos);
                },

                inserirProdutos() {
                    this.getProdutosData();
                    \$.ajax({
                        url: \"";
        // line 139
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("almox_persiste_requisicao");
        echo "\",
                        dataType: 'json',
                        method: \"POST\",
                        data: {
                            produtos: model.produtos,
                            observacao: model.observacao
                        },
                        success: function (data) {
                            controller.setValorModel('ultimaRequisicao', data);
                            controller.launchSwalProdutos();
                        },
                        error: function (data) {
                        }
                    })
                },

                launchSwalObservacao() {
                    Swal.fire({
                        title: '<strong>Registre uma observação:</strong>',
                        width: '100em',
                        html:
                            '<textarea rows=\"15\" id=\"swal-observacao-textarea\"> </textarea>',
                        showCancelButton: true,
                        showCloseButton: true,
                        focusConfirm: false,
                        reverseButtons: true,
                        cancelButtonText:
                            '<i class=\"fa fa-times\"></i> Não registrar observação.',
                        cancelButtonAriaLabel: 'Thumbs down',
                        confirmButtonText:
                            '<i class=\"fa fa-check\"></i> Registrar observação',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                    }).then((result) => {
                        if (result.value) {
                            let textareaSwal = document.querySelector('#swal-observacao-textarea');
                            let observacao = textareaSwal.value;
                            this.setValorModel('observacao', observacao);
                        } else {
                            this.setValorModel('observacao', null);
                        }
                    });
                },

                launchSwalProdutos() {

                    Swal.fire({
                        title: 'Confirmar lista de produtos?',
                        // text: \"You won't be able to revert this!\",
                        width: '60em',
                        showCancelButton: true,
                        reverseButtons: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '<i class=\"fa fa-check\"></i> Confirmar',
                        cancelButtonText: '<i class=\"fa fa-times\"></i> Cancelar'
                    }).then((result) => {
                        if (result.value) {

                            let baseURL = 'requisicao/lista-requisitada/';
                            let requisicao = model.ultimaRequisicao;
                            let route = baseURL + requisicao;
                            console.log(route);

                            window.location.href = route;

                        } else {
                            this.removerUltimaRequisicao();
                        }
                    })
                },

                removerUltimaRequisicao() {
                    \$.ajax({
                        url: \"";
        // line 214
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("almox_remover_ultimo_pedido");
        echo "\",
                        dataType: 'json',
                        method: \"POST\",
                        data: {
                            requisicao: model.ultimaRequisicao
                        },
                        success: function (data) {
                        },
                        error: function (data) {
                        }
                    })
                },

                adicionarProduto() {
                    let clone = \$('#clone-container').clone();
                    \$(clone).find('select').attr(\"id\", \"select-\" + model.selectIdCounter);
                    \$(clone).find('.subcontainer-btn-x').attr(\"id\", \"remover-\" + model.containerRemover);

                    let clonedPrice = \$(clone).find(':selected').attr('data-valor');
                    \$(clone).find('input').val(clonedPrice);

                    \$('.product-subcontainer').append(clone);
                    \$(clone).find('.subcontainer-remove-row > button').prop(\"disabled\", false);
                    controller.setValorModel('selectIdCounter', model.selectIdCounter + 1);
                    controller.setValorModel('containerRemover', model.containerRemover + 1);

                },

                removerRow() {
                    \$('body').on('click', '.btn-danger', function () {
                        let trashBtn = this;
                        \$(trashBtn).parent().parent().remove();
                    })
                },

                fixFirstTimeUi() {
                    let firstInputValue = \$('.subcontainer-produto').find(':selected').attr('data-valor');
                    let firstPriceInput = \$('.subcontainer-valor > input');
                    \$(firstPriceInput).val(firstInputValue);
                    \$('.subcontainer-remove-row > button').prop(\"disabled\", true);
                },

                imprimeModel() {
                    console.table(model);
                },
            };

            const view = {

                init() {
                    this.inicializarReferenciaHtml();
                    this.inicializarEventListeners();
                },

                inicializarSelectProdutos(listaProdutos) {
                    let selectInnerHTML = '';
                    selectInnerHTML = listaProdutos.reduce((acc, opt) => {
                        return acc += `<option \${false} value='\${opt.idItem}' data-valor='\${opt.valor}'> \${opt.nome} </option>`;
                    }, selectInnerHTML);
                    view.produtosSelect.innerHTML = selectInnerHTML;
                    controller.fixFirstTimeUi();
                },

                inicializarReferenciaHtml() {
                    this.addProdutoBtn = document.querySelector('#btn-plus');
                    this.checkBtn = document.querySelector('#btn-check');
                    this.produtosSelect = document.querySelector('#select-0');
                    this.produtoContainer = document.querySelector('.product-subcontainer');
                    this.printModelBtn = document.querySelector('#print-model');
                    this.observacaoBtn = document.querySelector('#swal');
                },

                inicializarEventListeners() {

                    this.printModelBtn.addEventListener('click', evt => {
                        controller.imprimeModel();
                    });

                    this.observacaoBtn.addEventListener('click', evt => {
                        controller.launchSwalObservacao();
                    });

                    this.checkBtn.addEventListener('click', evt => {
                        model.produtos = [];
                        controller.inserirProdutos();
                    });

                    this.addProdutoBtn.addEventListener('click', evt => {
                        controller.adicionarProduto();
                    });

                    this.produtoContainer.addEventListener('change', evt => {
                        controller.updateProdutoValor(evt.target);
                    });

                    controller.removerRow();
                },
            };
            controller.init();
        }());


    </script>
";
    }

    // line 319
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 320
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 321
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/swal/sweetalert2.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <style>

        div.modal-header {
            text-align: center;
        }

        .label-trash {
            visibility: hidden;
        }

        .modal-body {
            font-size: 1.5em;
            text-align: center;
        }

        .modal-header {
            border-bottom: 0 none;
        }

        .modal-footer {
            border-top: 0 none;
        }

        input.form-control {
            text-align: center;
        }

        #container-btn-pos {
            margin-left: auto;
        }

        #btn-plus {
            margin-right: 3px;
        }

        .panel-body {
            border-radius: 0;
        }

        .container-requisitar {
            flex-grow: 2;
            width: 90%;
            margin: 0 auto;
        }

        .panel-heading {
            font-size: 1.1em;
            font-weight: bold;
        }

        .btn-sizing {
            min-width: 34px;
            float: right;
        }

        .fullwidth {
            width: 100%;
        }

        .subcontainer-produto {
            display: block;
            flex-grow: 2;
        }

        .subcontainer-second {
            margin-left: 1em;
            flex-grow: 1;
        }

        .subcontainer-remove-row {
            display: flex;
            flex-direction: column;
            align-content: center;
            align-items: center;
        }

        .subcontainer-valor {
            margin-left: 1em;
            flex-grow: 1;
        }

        .container-selects {
            display: flex;
            justify-content: flex-start;
        }

        .btn-sizing-trash {
            max-width: 50%;
        }

        .obs-btn {
            margin-bottom: 1em;
        }

        textarea {
            resize: none;
            width: 85%;
            border-radius: 4px;
            font-size: 15px;
        }

    </style>

";
    }

    public function getTemplateName()
    {
        return "AlmoxarifadoBundle:Requisicao:criar-requisicao.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  398 => 321,  393 => 320,  390 => 319,  282 => 214,  204 => 139,  171 => 109,  131 => 72,  126 => 71,  123 => 70,  55 => 4,  52 => 3,  47 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "AlmoxarifadoBundle:Requisicao:criar-requisicao.html.twig", "/var/www/narfi/src/Nte/Aplicacao/AlmoxarifadoBundle/Resources/views/Requisicao/criar-requisicao.html.twig");
    }
}
