<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:Turmas:lista.html.twig */
class __TwigTemplate_5b838663b70c65e1718427b678e35a9dcc9f51b2b7eeb9924f1215fd682f55d1 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoExportadorBundle:Turmas:lista.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Exportador";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Exportador";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span>Aplicação</span></li>
     <li><span>Exportador</span></li>
     <li><span>Visualização de Disciplina</span></li>
 ";
    }

    // line 9
    public function block_conteudo($context, array $blocks = [])
    {
        // line 10
        echo "<div class=\"col-md-12\">
    <section class=\"panel\">
        <header class=\"panel-heading\">
            <div class=\"panel-actions\">
                <a href=\"#\" class=\"panel-action panel-action-toggle\" data-panel-toggle=\"\"></a>
                <a href=\"#\" class=\"panel-action panel-action-dismiss\" data-panel-dismiss=\"\"></a>
            </div>
            <h2 class=\"panel-title\">
                <span class=\"label label-primary label-sm text-weight-normal va-middle mr-sm\">";
        // line 18
        echo twig_escape_filter($this->env, twig_length_filter($this->env, ($context["listaTurmas"] ?? null)), "html", null, true);
        echo "</span>
                <span class=\"va-middle\">";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["turma"] ?? null), "idDisciplina", [], "any", false, false, false, 19), "nome", [], "any", false, false, false, 19), "html", null, true);
        echo "</span>
            </h2>
        </header>
        <div class=\"panel-body\">
            <div class=\"content\">
                <ul class=\"simple-user-list\">
                    ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["listaTurmas"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 26
            echo "                        <li ";
            if ((twig_get_attribute($this->env, $this->source, $context["p"], "ativo", [], "any", false, false, false, 26) == 0)) {
                echo " class=\"diferente\"";
            }
            echo ">
                            <figure class=\"image rounded \">
                                <img width=\"32\" src=\"";
            // line 28
            (( !(null === twig_get_attribute($this->env, $this->source, $context["p"], "foto", [], "any", false, false, false, 28))) ? (print (twig_escape_filter($this->env, ("data:image/png;base64, " . twig_get_attribute($this->env, $this->source, $context["p"], "foto", [], "any", false, false, false, 28)), "html", null, true))) : (print ("/assets/img/default_user.png")));
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "nome", [], "any", false, false, false, 28), "html", null, true);
            echo "\" class=\"img-circle img-responsive\">
                            </figure>
                            <span class=\"title\">";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "nome", [], "any", false, false, false, 30), "html", null, true);
            echo "</span>
                            <span class=\"message truncate\">";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "papel", [], "any", false, false, false, 31), "html", null, true);
            echo "</span>
                        </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "                </ul>
            </div>
        </div>
        <!--  <div class=\"panel-footer\">
              <div class=\"input-group input-search\">
                  <input type=\"text\" class=\"form-control\" name=\"q\" id=\"q\" placeholder=\"Search...\">
                  <span class=\"input-group-btn\">
                      <button class=\"btn btn-default\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
                  </span>
              </div>
          </div>-->
    </section>
</div>
<style>
    .diferente{
        background: #f2dede;
    }
</style>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:Turmas:lista.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 34,  118 => 31,  114 => 30,  107 => 28,  99 => 26,  95 => 25,  86 => 19,  82 => 18,  72 => 10,  69 => 9,  62 => 5,  59 => 4,  53 => 3,  47 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:Turmas:lista.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/Turmas/lista.html.twig");
    }
}
