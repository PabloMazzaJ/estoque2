<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteConfigBaseBundle:Crontab:index.html.twig */
class __TwigTemplate_6eb60c5088e2f03840f9f7ae364eda36a30c8c5ce80ae832b1fa7dfbb69a7db7 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts3' => [$this, 'block_javascripts3'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteConfigBaseBundle:Crontab:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Cron";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Tarefas Agendadas";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span>Configurações</span></li>
     <li><span>Cron</span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "    <nav class=\"navbar navbar-default\">
        <div id=\"menu\" class=\"container-fluid\">
            <div class=\"navbar-form navbar-left\">
                <div class=\"form-group\">
                    <ul class=\"notifications\">
                        <li data-toggle=\"tooltip\" data-placement=\"bottom\"
                            title=\"Habilitar todas as tarefas selecionadas\">
                            <a class=\"notification-icon btn-ativar-todos\">
                                <i class=\"fa fa fa-check\"></i>
                            </a>
                        </li>
                        <li data-toggle=\"tooltip\" data-placement=\"bottom\"
                            title=\"desabilitar todas as tarefas selecionadas\">
                            <a class=\"notification-icon btn-desativar-todos\">
                                <i class=\"fa fa fa-remove\"></i>
                            </a>
                        </li>
                        <li data-toggle=\"tooltip\" data-placement=\"top\"
                            title=\"Remover todas as  tarefas selecionadas\">
                            <a class=\"notification-icon btn-remover-todos\"
                               data-btn-ok-label=\"Sim\" data-btn-ok-icon=\"fa fa-trash\"
                               data-btn-ok-class=\"btn btn-danger\" data-btn-cancel-label=\"Não\"
                               data-btn-cancel-icon=\"fa  fa-ban\" data-btn-cancel-class=\"btn btn-default\"
                               data-content=\"Você deseja remover as tarefas selecionadas?\" data-title=\"Remover\"
                               data-toggle=\"confirmation\" data-placement=\"bottom\"
                               data-on-cancel=\"notificacaoPadrao\"
                               data-on-confirm=\"removerTarefas\">
                                <i class=\"fa fa-trash\"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class=\"navbar-form navbar-right\">
                <div class=\"form-group\">
                    <ul class=\"notifications\">
                        <li data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Lista todos os comandos do crontab\">
                            <a class=\"notification-icon btn-simple-ajax\" href=\"";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_crontab_listar");
        echo "\">
                                <i class=\"fa fa-list-alt \"></i>
                            </a>
                        </li>
                        <li data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Adiciona um novo comando ao crontab\">
                            <a class=\"notification-icon btn-simple-ajax\" href=\"";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_crontab_novo");
        echo "\">
                                <i class=\"fa fa-plus text-success\"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div class=\"panel\" id=\"response\">

    </div>

";
    }

    // line 66
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 67
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 69
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 70
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-confirmation/bootstrap-confirmation.js"), "html", null, true);
        echo "\"></script>
    <script>

        function notificacaoPadrao() {
            new PNotify({title: 'Info', type: 'info', text: 'Nada a ser feito...'});
        }
        function principal() {
            \$.ajax({
                type: 'GET',
                url: \"";
        // line 79
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_crontab_listar");
        echo "\",
                dataType: 'json',
                complete: function (data) {
                    \$(\"#response\").html(data.responseText);
                }
            });
            updateDomJS();
        }
        function habilitarTarefas(ids, status) {
            \$.ajax({
                type: 'POST',
                data: {ids: ids, status: status},
                url: \"";
        // line 91
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_crontab_habilitar_tarefas");
        echo "\",
                dataType: 'json',
                complete: function (data) {
                    new PNotify(data.responseJSON);
                    principal();
                }
            });

        }
        function removerTarefas() {
            ids = [];
            \$(':checkbox:checked:not(#jobs) ').each(function () {
                ids.push(this.value);
            });
            if (ids.length > 0) {
                \$.ajax({
                    type: 'POST',
                    data: {ids: ids},
                    url: \"";
        // line 109
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_crontab_remover_tarefas");
        echo "\",
                    dataType: 'json',
                    complete: function (data) {
                        new PNotify(data.responseJSON);
                        principal();
                    }
                });
            } else {
                notificacaoPadrao();
            }

        }

        function updateDomJS() {
            \$(\".btn-simple-ajax\").unbind('click');
            \$(\".btn-cancelar\").unbind('click');
            \$(\"#jobs\").unbind('click');
            \$('[data-toggle=\"confirmation\"]').confirmation();

            \$('.btn-ativar-todos').unbind('click');
            \$('.btn-ativar-todos').click(function () {
                ids = [];
                \$(':checkbox:checked:not(#jobs) ').each(function () {
                    ids.push(this.value);
                });
                if (ids.length > 0) {
                    habilitarTarefas(ids, 1);
                } else {
                    notificacaoPadrao();
                }
            });
            \$('.btn-desativar-todos').unbind('click');
            \$('.btn-desativar-todos').click(function () {
                ids = [];
                \$(':checkbox:checked:not(#jobs) ').each(function () {
                    ids.push(this.value);
                });
                if (ids.length > 0) {
                    habilitarTarefas(ids, 0);
                } else {
                    notificacaoPadrao();
                }
            });
            \$('.btn-remover-todos').unbind('click');
            \$('.btn-remover-todos').click(function () {
                \$(this).confirmation('show');
            });

            \$('.btn-cancelar').click(function () {
                principal();
            });
            \$('#jobs').click(function (event) {
                var status = this.checked;
                \$(':checkbox').each(function () {
                    this.checked = status;
                });
            });
            \$(\".btn-simple-ajax\").click(function (e) {
                e.preventDefault();
                \$.ajax({
                    type: 'GET',
                    url: \$(this).attr('href'),
                    dataType: 'json',
                    complete: function (data) {
                        \$(\"#response\").html(data.responseText);
                        updateDomJS();
                    }
                });
            });
        }

        principal();
    </script>
";
    }

    public function getTemplateName()
    {
        return "NteConfigBaseBundle:Crontab:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 109,  180 => 91,  165 => 79,  152 => 70,  149 => 69,  142 => 67,  139 => 66,  120 => 51,  112 => 46,  73 => 9,  70 => 8,  64 => 5,  61 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteConfigBaseBundle:Crontab:index.html.twig", "/var/www/narfi/src/Nte/Config/BaseBundle/Resources/views/Crontab/index.html.twig");
    }
}
