<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:exportacao:show.html.twig */
class __TwigTemplate_d3e885178e77495040fc9b59f45a146da986a5bcabf177a0c396333b3319c666 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("::base.admin.html.twig", "NteAplicacaoExportadorBundle:exportacao:show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_conteudo($context, array $blocks = [])
    {
        // line 4
        echo "
    <h1>Middlewareexportacao</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["middlewareExportacao"] ?? null), "id", [], "any", false, false, false, 11), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Status</th>
                <td>";
        // line 15
        if (twig_get_attribute($this->env, $this->source, ($context["middlewareExportacao"] ?? null), "status", [], "any", false, false, false, 15)) {
            echo "Yes";
        } else {
            echo "No";
        }
        echo "</td>
            </tr>
            <tr>
                <th>Periodico</th>
                <td>";
        // line 19
        if (twig_get_attribute($this->env, $this->source, ($context["middlewareExportacao"] ?? null), "periodico", [], "any", false, false, false, 19)) {
            echo "Yes";
        } else {
            echo "No";
        }
        echo "</td>
            </tr>
            <tr>
                <th>Registrodatacriacao</th>
                <td>";
        // line 23
        if (twig_get_attribute($this->env, $this->source, ($context["middlewareExportacao"] ?? null), "registroDataCriacao", [], "any", false, false, false, 23)) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["middlewareExportacao"] ?? null), "registroDataCriacao", [], "any", false, false, false, 23), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</td>
            </tr>
            <tr>
                <th>Registrodataatualizacao</th>
                <td>";
        // line 27
        if (twig_get_attribute($this->env, $this->source, ($context["middlewareExportacao"] ?? null), "registroDataAtualizacao", [], "any", false, false, false, 27)) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["middlewareExportacao"] ?? null), "registroDataAtualizacao", [], "any", false, false, false, 27), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportacao_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportacao_edit", ["id" => twig_get_attribute($this->env, $this->source, ($context["middlewareExportacao"] ?? null), "id", [], "any", false, false, false, 37)]), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 40
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["delete_form"] ?? null), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 42
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["delete_form"] ?? null), 'form_end');
        echo "
        </li>
    </ul>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:exportacao:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 42,  118 => 40,  112 => 37,  106 => 34,  94 => 27,  85 => 23,  74 => 19,  63 => 15,  56 => 11,  47 => 4,  44 => 3,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:exportacao:show.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/exportacao/show.html.twig");
    }
}
