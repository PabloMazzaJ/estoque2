<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoBuscadorBundle:Pessoas:_usuario.html.twig */
class __TwigTemplate_ca279532b2e97859165e2596f1ae1b54112d81d56eb41be739d8aeb909fe824d extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\" col-sm-12 col-md-6\">
    <section class=\"panel\">
        <header class=\"panel-heading\">
            <h2 class=\"panel-title\">
                <span class=\"va-middle\">Usuários</span>
            </h2>
        </header>
        <div class=\"panel-body\">
            <div class=\"content\">
                <table class=\"table table-responsive\">
                    ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "usuarios", [], "any", false, false, false, 11));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 12
            echo "                        <tr class=\"text-left\">
                            <td>";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "login", [], "any", false, false, false, 13), "html", null, true);
            echo "</td>
                            <td>
                                ";
            // line 15
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["usuariosMoodle"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[twig_get_attribute($this->env, $this->source, $context["e"], "login", [], "any", false, false, false, 15)] ?? null) : null));
            foreach ($context['_seq'] as $context["_key"] => $context["x"]) {
                // line 16
                echo "                                    ";
                if ((twig_get_attribute($this->env, $this->source, $context["x"], "usuariosTotal", [], "any", false, false, false, 16) >= 1)) {
                    // line 17
                    echo "                                        <a target=\"_blank\"
                                           href=\"";
                    // line 18
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["x"], "ambienteUrl", [], "any", false, false, false, 18), "html", null, true);
                    echo "/user/profile.php?id=";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = twig_get_attribute($this->env, $this->source, $context["x"], "usuarios", [], "any", false, false, false, 18)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[0] ?? null) : null), "id", [], "any", false, false, false, 18), "html", null, true);
                    echo "\">
                                            ";
                    // line 19
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["x"], "ambienteNome", [], "any", false, false, false, 19), "html", null, true);
                    echo " /
                                            Último
                                            acesso: ";
                    // line 21
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = twig_get_attribute($this->env, $this->source, $context["x"], "usuarios", [], "any", false, false, false, 21)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[0] ?? null) : null), "lastaccess", [], "any", false, false, false, 21), "d/m/Y H:i", null, "America/Sao_Paulo"), "html", null, true);
                    echo "
                                        </a><br>
                                    ";
                } else {
                    // line 24
                    echo "                                    ";
                }
                // line 25
                echo "                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['x'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 26
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "                </table>
            </div>
        </div>
    </section>
</div>";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoBuscadorBundle:Pessoas:_usuario.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 30,  95 => 26,  89 => 25,  86 => 24,  80 => 21,  75 => 19,  69 => 18,  66 => 17,  63 => 16,  59 => 15,  54 => 13,  51 => 12,  47 => 11,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoBuscadorBundle:Pessoas:_usuario.html.twig", "/var/www/narfi/src/Nte/Aplicacao/BuscadorBundle/Resources/views/Pessoas/_usuario.html.twig");
    }
}
