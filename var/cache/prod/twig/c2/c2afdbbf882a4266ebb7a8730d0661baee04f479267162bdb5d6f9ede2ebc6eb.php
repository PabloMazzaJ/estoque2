<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteUsuarioBundle:Usuario:form-primeiro-acesso.html.twig */
class __TwigTemplate_ebae0bddde0eeb79c2af6df1f8111150e094c035c0e0f140d93cecbcc3ce5433 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'marca' => [$this, 'block_marca'],
            'titulo' => [$this, 'block_titulo'],
            'fos_user_content' => [$this, 'block_fos_user_content'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 7
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        $context["arquivoImagemPerfil"] = $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/img/default_user.png");
        // line 7
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "NteUsuarioBundle:Usuario:form-primeiro-acesso.html.twig", 7);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_marca($context, array $blocks = [])
    {
    }

    // line 9
    public function block_titulo($context, array $blocks = [])
    {
        echo " Redefinição de Senha ";
    }

    // line 10
    public function block_fos_user_content($context, array $blocks = [])
    {
        // line 11
        echo "    <div class=\"row\">
        <div class=\"alert alert-info\">
            Olá <strong>";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "nome", [], "any", false, false, false, 13), "html", null, true);
        echo "</strong>,<br>
            Para você ter acesso ao sistema, será necessário completar o cadastro com os seguintes dados:
            <ul>
                ";
        // line 16
        if ((twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "img", [], "any", false, false, false, 16) == "assets/img/default_user.png")) {
            // line 17
            echo "                    <li><b>Inserir uma imagem de perfil</b></li>
                ";
        }
        // line 19
        echo "                ";
        if ((null === twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "cpf", [], "any", false, false, false, 19))) {
            // line 20
            echo "                    <li><b>Informar o seu CPF</b></li>
                ";
        }
        // line 22
        echo "                <li><b>Informar uma nova senha</b></li>
            </ul>
        </div>

        <div class=\"col-md-12\">
            <div class=\"row text-center\">
                <div class=\"kv-avatar\">
                    <div class=\"file-loading\">
                        <input id=\"file\" name=\"file\" type=\"file\" required>
                    </div>
                </div>
            </div>
        </div>
        ";
        // line 35
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
        <div class=\"form-group text-center\">
            <div class=\"\">
                <input type=\"checkbox\" class='hidden validar' id=\"foto\" name=\"foto\"
                       data-validate=\"\"
                       data-msg=\"Por favor, atualize sua foto.\"
                       required=\"required\" value=\"1\"
                       aria-required=\"true\">
            </div>
        </div>
        <div class=\"col-md-12\">
            <div class=\"row\">
                <div class=\"form-group\">
                    ";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
                </div>
                <div class=\"form-group\">
                    <button type=\"submit\" class=\"btn btn-primary btn-block\">
                        <i class=\"fa fa-save\"> </i> Salvar
                    </button>
                </div>
            </div>
        </div>
        ";
        // line 57
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
    </div>
";
    }

    // line 60
    public function block_javascripts($context, array $blocks = [])
    {
        // line 61
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-fileinput/css/fileinput.min.css"), "html", null, true);
        echo "\">
    <style>
        .kv-avatar .krajee-default.file-preview-frame,
        .kv-avatar .krajee-default.file-preview-frame:hover {
            margin: 0;
            padding: 0;
            border: none;
            box-shadow: none;
            text-align: center;
            width: 100%;
        }

        .kv-avatar {
            display: inline-block;
        }

        .kv-avatar .file-input {
            display: table-cell;
            width: 213px;
        }

        .file-preview {
            border: none;
        }

        .kv-reqd {
            color: red;
            font-family: monospace;
            font-weight: normal;
        }

        .file-drop-zone.clickable:hover {
            border: 1px dashed #999;
        }
    </style>
    <script src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery/jquery.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-cookie/jquery-cookie.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-fileinput/js/fileinput.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-fileinput/js/locales/pt-BR.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-validation/jquery.validate.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-mask/jquery.mask.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/CPF/CPF.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$.ajaxSetup({
            timeout: 99999999999,
            headers: {
                'X-CSRFToken': \"";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("friga-upload-usuario-imagem-cabecalho"), "html", null, true);
        echo "\"
            }
        });
        \$('form').submit(function (e) {
            e.preventDefault();
            \$(\"#foto\").removeClass('hidden');
            var validated = \$('form').valid();
            if (validated) {
                \$(this).unbind('submit').submit();
            }
            setTimeout(function () {
                \$(\"#foto\").addClass('hidden');
            },1);

        });


        \$.validator.addMethod(\"cpf\", function (value, element) {
            return this.optional(element) || CPF.validate(value);
        }, \"Por favor, Informe um CPF válido.\");
        \$.extend(\$.validator.messages, {
            required: \"Este campo é obrigatório\",
            email: \"Por favor, preencha com o endereço de e-mail válido\",
        });
        \$(\"form\").validate({
            rules: {
                nte_usuario_primeiro_acesso_plainPassword_first: \"required\",
                nte_usuario_primeiro_acesso_plainPassword_second: {equalTo: \"#nte_usuario_primeiro_acesso_plainPassword_first\"}
            }
        });
        \$('#";
        // line 139
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "cpf", [], "any", false, false, false, 139), "vars", [], "any", false, false, false, 139), "id", [], "any", false, false, false, 139), "html", null, true);
        echo "').mask('000.000.000-00');
        \$(\"#file\").fileinput({
            language: \"pt-BR\",
            uploadUrl: \"";
        // line 142
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_uploadimagem", ["id" => twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 142)]), "html", null, true);
        echo "\",
            uploadAsync: false,
            minFileCount: 1,
            maxFileCount: 1,
            overwriteInitial: true,
            maxFileSize: 15000,
            showUpload: false,
            showClose: false,
            showCaption: false,
            showBrowse: false,
            showRemove: false,
            removeFromPreviewOnError: true,
            browseOnZoneClick: true,
            removeLabel: '',
            elErrorContainer: '#kv-avatar-errors-2',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_imagem_perfil", ["id" => twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 158)]), "html", null, true);
        echo "\" class=\"img-rounded img-responsive\" ><h6 class=\"text-muted\">Clique aqui para inserir a sua imagem de perfil</h6>',
            layoutTemplates: {},
            uploadExtraData: {
                xtk: \"";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("friga-upload-usuario-imagem-form"), "html", null, true);
        echo "\"
            },
            //allowedFileExtensions: [\"jpg\", \"jpeg\", \"png\", \"gif\"]
            allowedFileTypes: [\"image\"]
        }).on(\"filebatchselected\", function (event, files) {
            \$(\"#file\").fileinput(\"upload\");
        }).on('filebatchuploadsuccess', function (event, data) {
            \$(\"#foto\").prop(\"checked\", true);
            \$('form').valid();
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "NteUsuarioBundle:Usuario:form-primeiro-acesso.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 161,  277 => 158,  258 => 142,  252 => 139,  219 => 109,  211 => 104,  207 => 103,  203 => 102,  199 => 101,  195 => 100,  191 => 99,  187 => 98,  183 => 97,  179 => 96,  140 => 61,  137 => 60,  130 => 57,  118 => 48,  102 => 35,  87 => 22,  83 => 20,  80 => 19,  76 => 17,  74 => 16,  68 => 13,  64 => 11,  61 => 10,  55 => 9,  50 => 8,  45 => 7,  43 => 6,  37 => 7,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteUsuarioBundle:Usuario:form-primeiro-acesso.html.twig", "/var/www/narfi/src/Nte/UsuarioBundle/Resources/views/Usuario/form-primeiro-acesso.html.twig");
    }
}
