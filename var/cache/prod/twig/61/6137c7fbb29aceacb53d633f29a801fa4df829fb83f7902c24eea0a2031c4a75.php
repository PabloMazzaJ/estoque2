<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontratoitem:item_cadastro.html.twig */
class __TwigTemplate_35235672b2be62c296f6f4c55ba1d69a9f09e67ebb8f50411f82cc0e2c499565 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontratoitem:item_cadastro.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo "Cadastro Item";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\">Pessoal Externo</a></span></li>
     <li><span><a href=\"";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontratoitem_mostra");
        echo "\">Item</a></span></li>
     <li><span><a href=\"#\">Cadastro</a></span></li>
 ";
    }

    // line 9
    public function block_conteudo($context, array $blocks = [])
    {
        // line 10
        echo "
    ";
        // line 11
        echo "  <table class=\"table table-striped\" style=\"margin-top: 1em\">
        <tr>
            <th scope=\"row\"> <a href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontratoitem_mostra");
        echo "\">Ver todos</a></th>
        </tr>
    </table>



    <header class=\"panel-heading\">
        <div>
            <h2 class=\"panel-title\">
                <span class=\"va-middle\"><i class=\"fa fa-list\"></i> Cadastro de Item</span>
                <ul class=\"notifications pull-right hidden-xs contexto-oferta-menu\">
                    <li data-toggle=\"tooltip\" title=\"Retornar para lista de itens\">
                        <a href=\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontratoitem_mostra");
        echo "\" class=\"btn-gerar-relatorio2\">
                            <i class=\"fa fa-arrow-circle-left\"></i>
                        </a>
                    </li>
                </ul>
            </h2>
        </div>
    </header>
    <div class=\"panel-body\">
        ";
        // line 34
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
        ";
        // line 35
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
        <input type=\"submit\" value=\"Cadastrar\" class=\"btn btn-success\" id=\"idSubmit\" />
        ";
        // line 37
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
    </div>




";
    }

    // line 46
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 47
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>
        #idSubmit {
            margin-top: 1em;


        }
        .col-md-3 label {
            width: 80%;
        }
        .col-md-3 input {
            width: 20%;
        }
    </style>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontratoitem:item_cadastro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 47,  129 => 46,  118 => 37,  113 => 35,  109 => 34,  97 => 25,  82 => 13,  78 => 11,  75 => 10,  72 => 9,  65 => 6,  62 => 5,  59 => 4,  54 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontratoitem:item_cadastro.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontratoitem/item_cadastro.html.twig");
    }
}
