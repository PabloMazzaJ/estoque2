<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoBuscadorBundle:Turmas:lista.html.twig */
class __TwigTemplate_26e7bad1c7e528798b5eb89c4ee33c5b09e7e720b727812cd1a654f86a4b57d7 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoBuscadorBundle:Turmas:lista.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Exportador";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Exportador";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span>Aplicação</span></li>
     <li><span>Exportador</span></li>
     <li><span>Visualização de Disciplina</span></li>
 ";
    }

    // line 9
    public function block_conteudo($context, array $blocks = [])
    {
        // line 10
        echo "<div class=\"col-md-12\">
    <section class=\"panel\">
        <header class=\"panel-heading\">
            <div class=\"panel-actions\">
                <a href=\"#\" class=\"panel-action panel-action-toggle\" data-panel-toggle=\"\"></a>
                <a href=\"#\" class=\"panel-action panel-action-dismiss\" data-panel-dismiss=\"\"></a>
            </div>
            <h2 class=\"panel-title\">
                <span class=\"label label-primary label-sm text-weight-normal va-middle mr-sm\">";
        // line 18
        echo twig_escape_filter($this->env, twig_length_filter($this->env, ($context["listaTurmas"] ?? null)), "html", null, true);
        echo "</span>
                <span class=\"va-middle\">";
        // line 19
        echo twig_escape_filter($this->env, ($context["turma"] ?? null), "html", null, true);
        echo "</span>
            </h2>
        </header>
        <div class=\"panel-body\">
            <div class=\"content\">
                <ul class=\"simple-user-list\">
                    ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["listaTurmas"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 26
            echo "                        <li style=\"background: ";
            if ((twig_get_attribute($this->env, $this->source, $context["p"], "ativo", [], "any", false, false, false, 26) == 0)) {
                echo " #f2dede ";
            } elseif ((twig_get_attribute($this->env, $this->source, $context["p"], "situacao", [], "any", false, false, false, 26) == 0)) {
                echo " #FFEB3B ";
            }
            echo " ;\">
                            <figure class=\"image rounded \">
                                <img width=\"32\" src=\"";
            // line 28
            (( !(null === twig_get_attribute($this->env, $this->source, $context["p"], "foto", [], "any", false, false, false, 28))) ? (print (twig_escape_filter($this->env, ("data:image/png;base64, " . twig_get_attribute($this->env, $this->source, $context["p"], "foto", [], "any", false, false, false, 28)), "html", null, true))) : (print ("/assets/img/default_user.png")));
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "nome", [], "any", false, false, false, 28), "html", null, true);
            echo "\" class=\"img-circle img-responsive\">
                            </figure>
                            <span class=\"title\">
                                <a title=\"clique aqui para visualizar o cadastro de ";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "nome", [], "any", false, false, false, 31), "html", null, true);
            echo "\"  href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("buscador_pessoas_perfil", ["id" => twig_get_attribute($this->env, $this->source, $context["p"], "idPessoa", [], "any", false, false, false, 31)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "nome", [], "any", false, false, false, 31), "html", null, true);
            echo "</a>
                            </span>
                            <span class=\"message truncate\">";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "papel", [], "any", false, false, false, 33), "html", null, true);
            echo "
                            ";
            // line 34
            if (((twig_get_attribute($this->env, $this->source, $context["p"], "situacao", [], "any", false, false, false, 34) == 0) &&  !(null === twig_get_attribute($this->env, $this->source, $context["p"], "situacao", [], "any", false, false, false, 34)))) {
                echo " (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "situacaoDescricao", [], "any", false, false, false, 34), "html", null, true);
                echo ")
                            ";
            }
            // line 36
            echo "                            </span>
                        </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "                </ul>
            </div>
        </div>
    </section>
</div>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoBuscadorBundle:Turmas:lista.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 39,  137 => 36,  130 => 34,  126 => 33,  117 => 31,  109 => 28,  99 => 26,  95 => 25,  86 => 19,  82 => 18,  72 => 10,  69 => 9,  62 => 5,  59 => 4,  53 => 3,  47 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoBuscadorBundle:Turmas:lista.html.twig", "/var/www/narfi/src/Nte/Aplicacao/BuscadorBundle/Resources/views/Turmas/lista.html.twig");
    }
}
