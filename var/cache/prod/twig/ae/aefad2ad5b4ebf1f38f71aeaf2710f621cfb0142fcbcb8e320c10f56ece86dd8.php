<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:Info:empty.html.twig */
class __TwigTemplate_597bc2fd1a795c98d218b8166d690d2d5522cafc49cca7431c7fdf41be1b1680 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div id='info-msg' class=\"text-center hidden\">
    <br>
    <i class=\"fa fa-5x fa-search-minus\"></i><br>
    <h2>Nenhum curso encontrado</h2><br>
    <h4>Verifique os parametros selecionados acima e refaça a  sua busca</h4><br>
    <br>
</div>";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:Info:empty.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:Info:empty.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/Info/empty.html.twig");
    }
}
