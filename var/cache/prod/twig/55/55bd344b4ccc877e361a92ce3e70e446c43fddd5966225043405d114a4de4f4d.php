<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteConfigSieBundle:Default:index.html.twig */
class __TwigTemplate_e278ea678b8369dad04afd10d1dbdb082c6b4ef41d07d6d98fe85d931c3b7ce1 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteConfigSieBundle:Default:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Configurações - Sie";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Configurações do SIE";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "    <li><span>Configurações</span></li>
    <li><span>SIE</span></li>
";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "    <div class=\"panel\" id=\"response\">

    </div>
";
    }

    // line 13
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 14
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-confirmation/bootstrap-confirmation.js"), "html", null, true);
        echo "\"></script>
    <script>

        function notificacaoPadrao() {
            new PNotify({title: 'Info', type: 'info', text: 'Nada a ser feito...'});
        }
        function principal() {
            \$.ajax({
                type: 'GET',
                url: \"";
        // line 24
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_sie_listar");
        echo "\",
                dataType: 'json',
                complete: function (data) {
                    \$(\"#response\").html(data.responseText);
                }
            });
            updateDomJS();
        }
        function configurarInscricao(id, valor) {
            \$.ajax({
                type: 'POST',
                data: {idSie: id, valor: valor},
                url: \"";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_sie_configurar_inscricao");
        echo "\",
                dataType: 'json',
                complete: function (data) {
                    new PNotify(data.responseJSON);
                    principal();
                }
            });
        }
        function removerConfiguracoes() {
            ids = [];
            \$(':checkbox:checked:not(#jobs) ').each(function () {
                ids.push(this.value);
            });
            if (ids.length > 0) {
                \$.ajax({
                    type: 'POST',
                    data: {ids: ids},
                    url: \"";
        // line 53
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_remover_todos");
        echo "\",
                    dataType: 'json',
                    complete: function (data) {
                        new PNotify(data.responseJSON);
                        principal();
                    }
                });
            } else {
                notificacaoPadrao();
            }
        }
        function updateDomJS() {
            \$('.sie-config-inscricao').change(function (){
                configurarInscricao(\$(this).attr('name'),\$(this).val());
            });
            \$(\".btn-simple-ajax\").unbind('click');
            \$(\".btn-cancelar\").unbind('click');
            \$(\"#jobs\").unbind('click');
            \$('[data-toggle=\"confirmation\"]').confirmation();

            \$('.btn-ativar-todos').unbind('click');
            \$('.btn-ativar-todos').click(function () {
                ids = [];
                \$(':checkbox:checked:not(#jobs) ').each(function () {
                    ids.push(this.value);
                });
                if (ids.length > 0) {
                    habilitarConfiguracoes(ids, 1);
                } else {
                    notificacaoPadrao();
                }
            });
            \$('.btn-desativar-todos').unbind('click');
            \$('.btn-desativar-todos').click(function () {
                ids = [];
                \$(':checkbox:checked:not(#jobs) ').each(function () {
                    ids.push(this.value);
                });
                if (ids.length > 0) {
                    habilitarConfiguracoes(ids, 0);
                } else {
                    notificacaoPadrao();
                }
            });
            \$('.btn-remover-todos').unbind('click');
            \$('.btn-remover-todos').click(function () {
                \$(this).confirmation('show');
            });

            \$('.btn-cancelar').click(function () {
                principal();
            });
            \$('#jobs').click(function (event) {
                var status = this.checked;
                \$(':checkbox').each(function () {
                    this.checked = status;
                });
            });
            \$(\".btn-simple-ajax\").click(function (e) {
                e.preventDefault();
                \$.ajax({
                    type: 'GET',
                    url: \$(this).attr('href'),
                    dataType: 'json',
                    complete: function (data) {
                        \$(\"#response\").html(data.responseText);
                        updateDomJS();
                    }
                });
            });
        }
        principal();
    </script>
";
    }

    public function getTemplateName()
    {
        return "NteConfigSieBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 53,  114 => 36,  99 => 24,  87 => 15,  82 => 14,  79 => 13,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  54 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteConfigSieBundle:Default:index.html.twig", "/var/www/narfi/src/Nte/Config/SieBundle/Resources/views/Default/index.html.twig");
    }
}
