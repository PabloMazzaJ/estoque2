<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ::menu.admin.html.twig */
class __TwigTemplate_4e1f39bfbe20b6393424ba7e1678109a4948b306ccec81e8ed199849d3c54f34 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["url"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 1), "attributes", [], "any", false, false, false, 1), "get", [0 => "_route"], "method", false, false, false, 1), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 1), "attributes", [], "any", false, false, false, 1), "get", [0 => "_route_params"], "method", false, false, false, 1));
        // line 2
        list($context["classe1"], $context["classe2"]) =         ["", ""];
        // line 3
        echo "<aside id=\"sidebar-left\" class=\"sidebar-left\">

    <div class=\"sidebar-header\">
        <div class=\"sidebar-title\">
            Navegação
        </div>
        <div class=\"sidebar-toggle hidden-xs\" data-toggle-class=\"sidebar-left-collapsed\" data-target=\"html\"
             data-fire-event=\"sidebar-left-toggle\">
            <i class=\"fa fa-bars\" aria-label=\"Toggle sidebar\"></i>
        </div>
    </div>

    <div class=\"nano\">
        <div class=\"nano-content\">
            <nav id=\"menu\" class=\"nav-main\" role=\"navigation\">
                <ul class=\"nav nav-main\">
                    <li id=\"menu-lateral-dashboard\"
                        class=\"";
        // line 20
        if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_admin_painel_homepage") == ($context["url"] ?? null))) {
            echo " nav-active";
        }
        echo "\">
                        <a href=\"";
        // line 21
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_admin_painel_homepage");
        echo "\">
                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>
                            <span>Painel de Controle</span>
                        </a>
                    </li>
                    ";
        // line 26
        if ((($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_aplicacao_exportador_homepage") == ($context["url"] ?? null)) || ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("buscador_pessoas_index") == ($context["url"] ?? null)))) {
            // line 27
            echo "                        ";
            $context["classe1"] = "nav-expanded nav-active";
            // line 28
            echo "                    ";
        } else {
            // line 29
            echo "                        ";
            $context["classe1"] = "";
            // line 30
            echo "                    ";
        }
        // line 31
        echo "                    <li id=\"menu-lateral-aplicacao\" class=\"nav-parent  ";
        echo twig_escape_filter($this->env, ($context["classe1"] ?? null), "html", null, true);
        echo "\">
                        <a>
                            <i class=\"fa fa-th\" aria-hidden=\"true\"></i>
                            <span>Aplicação</span>
                        </a>
                        <ul class=\"nav nav-children\">
                            <li class=\"";
        // line 37
        if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_aplicacao_exportador_homepage") == ($context["url"] ?? null))) {
            echo " nav-active ";
        }
        echo "\">
                                <a href=\"";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_aplicacao_exportador_homepage");
        echo "\">
                                    <i class=\"fa fa-cloud-download \"></i> Exportador
                                </a>
                            </li>

                            <li class=\"";
        // line 43
        if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("buscador_pessoas_index") == ($context["url"] ?? null))) {
            echo " nav-active";
        }
        echo " \">
                                <a href=\"";
        // line 44
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("buscador_pessoas_index");
        echo "\">
                                    <i class=\"fa fa-user \"></i> Buscar Pessoas
                                </a>
                            </li>

                            <li>
                                <a href=\"\">
                                    <i class=\"fa fa-dashboard\"></i> Notas
                                </a>
                            </li>
                            ";
        // line 54
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CADASTRO_PESSOA_EXTERNA")) {
            // line 55
            echo "                                <li class=\"";
            if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_index") == ($context["url"] ?? null))) {
                echo " nav-active";
            }
            echo " \">
                                    <a href=\"";
            // line 56
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_index");
            echo "\">
                                        <i class=\"fa fa-paste \"></i> Contratos
                                    </a>
                                </li>
                            ";
        }
        // line 61
        echo "                            ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CADASTRO_PESSOA_EXTERNA")) {
            // line 62
            echo "                                <li class=\"";
            if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_mostra") == ($context["url"] ?? null))) {
                echo " nav-active";
            }
            echo " \">
                                    <a href=\"";
            // line 63
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_mostra");
            echo "\">
                                        <i class=\"fa fa-users \"></i> Pessoas
                                    </a>
                                </li>
                            ";
        }
        // line 68
        echo "                            <li class=\"";
        if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntevinculo_pessoa_contrato") == ($context["url"] ?? null))) {
            echo " nav-active";
        }
        echo " \">
                                <a href=\"";
        // line 69
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntevinculo_pessoa_contrato");
        echo "\">
                                    <i class=\"fa fa-bullseye\"></i> Vínculos
                                </a>
                            </li>
                        </ul>
                    </li>
                    ";
        // line 75
        if ((((($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_index") ==         // line 76
($context["url"] ?? null)) || ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_realtorio_sie_alteracao") ==         // line 77
($context["url"] ?? null))) || ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_geral_fila_exportacao") ==         // line 78
($context["url"] ?? null))) || ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_realtorio_sie_problema_cadastro_tipo") ==         // line 79
($context["url"] ?? null)))) {
            // line 80
            echo "                        ";
            $context["classe1"] = "nav-expanded nav-active";
            // line 81
            echo "                    ";
        } else {
            // line 82
            echo "                        ";
            $context["classe1"] = "";
            // line 83
            echo "                    ";
        }
        // line 84
        echo "
                    <li id=\"menu-lateral-relatorio\" class=\"nav-parent  ";
        // line 85
        echo twig_escape_filter($this->env, ($context["classe1"] ?? null), "html", null, true);
        echo "\">
                        <a>
                            <i class=\"fa fa-list\" aria-hidden=\"true\"></i>
                            <span>Relatórios</span>
                        </a>
                        <ul class=\"nav nav-children\">
                            <li class=\"";
        // line 91
        if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_index") == ($context["url"] ?? null))) {
            echo " nav-active";
        }
        echo "\">
                                <a href=\"";
        // line 92
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_index");
        echo "\">
                                    <i class=\"fa fa-file-text-o\"></i>
                                    Logs
                                </a>
                            </li>
                            <li class=\"";
        // line 97
        if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_realtorio_sie_alteracao") == ($context["url"] ?? null))) {
            echo " nav-active";
        }
        echo "\">
                                <a href=\"";
        // line 98
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_realtorio_sie_alteracao");
        echo "\">
                                    <i class=\"fa fa-file-text-o\"></i>
                                    Alterações por curso
                                </a>
                            </li>
                            <li class=\"";
        // line 103
        if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_realtorio_sie_problema_cadastro_tipo") == ($context["url"] ?? null))) {
            echo " nav-active";
        }
        echo "\">
                                <a href=\"";
        // line 104
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_realtorio_sie_problema_cadastro_tipo");
        echo "\">
                                    <i class=\"fa fa-file-text-o\"></i>
                                    Cadastro de tipo incorreto
                                </a>
                            </li>
                            <li class=\"";
        // line 109
        if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_geral_fila_exportacao") == ($context["url"] ?? null))) {
            echo " nav-active";
        }
        echo "\">
                                <a href=\"";
        // line 110
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_geral_fila_exportacao");
        echo "\">
                                    <i class=\"fa fa-file-text-o\"></i>
                                    Previsão da lista de exportação
                                </a>
                            </li>
                        </ul>
                    </li>
                    ";
        // line 117
        if (((((($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_crontab") ==         // line 118
($context["url"] ?? null)) || ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_sie_homepage") ==         // line 119
($context["url"] ?? null))) || ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_base_homepage") ==         // line 120
($context["url"] ?? null))) || ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_index") ==         // line 121
($context["url"] ?? null))) || ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_homepage") ==         // line 122
($context["url"] ?? null)))) {
            // line 124
            echo "                        ";
            $context["classe1"] = "nav-expanded nav-active";
            // line 125
            echo "                    ";
        } else {
            // line 126
            echo "                        ";
            $context["classe1"] = "";
            // line 127
            echo "                    ";
        }
        // line 128
        echo "                    <li id=\"menu-lateral-configuracao\"
                        class=\"nav-parent ";
        // line 129
        echo twig_escape_filter($this->env, ($context["classe1"] ?? null), "html", null, true);
        echo "\">
                        <a>
                            <i class=\"fa fa-cogs\" aria-hidden=\"true\"></i>
                            <span> Configurações</span>
                        </a>
                        <ul class=\"nav nav-children\">
                            <li class=\"";
        // line 135
        if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_crontab") == ($context["url"] ?? null))) {
            echo " nav-active";
        }
        echo "\">
                                <a href=\"";
        // line 136
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_crontab");
        echo "\">
                                    <i class=\"glyphicon glyphicon-calendar\"></i>
                                    Cron </a>
                            </li>
                            <li class=\"";
        // line 140
        if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_homepage") == ($context["url"] ?? null))) {
            echo " nav-active";
        }
        echo "\">
                                <a href=\"";
        // line 141
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_homepage");
        echo "\">
                                    <i class=\"glyphicon glyphicon-cloud\"></i> Moodle
                                </a>
                            </li>
                            <li class=\"";
        // line 145
        if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_sie_homepage") == ($context["url"] ?? null))) {
            echo " nav-active";
        }
        echo "\">
                                <a href=\"";
        // line 146
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_sie_homepage");
        echo "\">
                                    <i class=\"glyphicon glyphicon-education\"></i> Sie
                                </a>
                            </li>
                            <li class=\"";
        // line 150
        if (($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_index") == ($context["url"] ?? null))) {
            echo " nav-active";
        }
        echo "\">
                                <a href=\"";
        // line 151
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_index");
        echo "\">
                                    <i class=\"glyphicon glyphicon-user\"></i>
                                    Usuários
                                </a>
                            </li>
                            <li><a href=\"\" class=\" \">
                                    <i class=\"glyphicon glyphicon-lock\"></i>
                                    Permissões
                                </a>
                            </li>
                        </ul>
                    </li>
                    ";
        // line 163
        $context["classe1"] = "";
        // line 164
        echo "                    <li id=\"menu-lateral-ajuda\" class=\"nav-parent ";
        echo twig_escape_filter($this->env, ($context["classe1"] ?? null), "html", null, true);
        echo "\">
                        <a>
                            <i class=\"fa fa-question-circle\" aria-hidden=\"true\"></i>
                            <span>Ajuda</span>
                        </a>
                        <ul class=\"nav nav-children\">
                            <li><a href=\"\" class=\" \">
                                    <i class=\"glyphicon glyphicon-book\"></i>
                                    Manual</a></li>
                            <li><a href=\"\" class=\" \">
                                    <i class=\"glyphicon glyphicon-tower\"></i>
                                    Versão</a></li>
                            <li><a href=\"\" class=\" \">
                                    <i class=\"glyphicon glyphicon-usd\"></i>
                                    Contato
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <hr class=\"separator\"/>
            <div id=\"exportacao-status\" class=\"sidebar-widget widget-stats hidden\">
                <div class=\"widget-header\">
                    <h6>Exportações em andamento</h6>
                    <div class=\"widget-toggle\">+</div>
                </div>
                <div class=\"widget-content\">
                    <ul id=\"visualizacao-exportacao\"></ul>
                </div>
            </div>

        </div>

    </div>
</aside>
";
    }

    public function getTemplateName()
    {
        return "::menu.admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  361 => 164,  359 => 163,  344 => 151,  338 => 150,  331 => 146,  325 => 145,  318 => 141,  312 => 140,  305 => 136,  299 => 135,  290 => 129,  287 => 128,  284 => 127,  281 => 126,  278 => 125,  275 => 124,  273 => 122,  272 => 121,  271 => 120,  270 => 119,  269 => 118,  268 => 117,  258 => 110,  252 => 109,  244 => 104,  238 => 103,  230 => 98,  224 => 97,  216 => 92,  210 => 91,  201 => 85,  198 => 84,  195 => 83,  192 => 82,  189 => 81,  186 => 80,  184 => 79,  183 => 78,  182 => 77,  181 => 76,  180 => 75,  171 => 69,  164 => 68,  156 => 63,  149 => 62,  146 => 61,  138 => 56,  131 => 55,  129 => 54,  116 => 44,  110 => 43,  102 => 38,  96 => 37,  86 => 31,  83 => 30,  80 => 29,  77 => 28,  74 => 27,  72 => 26,  64 => 21,  58 => 20,  39 => 3,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "::menu.admin.html.twig", "/var/www/narfi/app/Resources/views/menu.admin.html.twig");
    }
}
