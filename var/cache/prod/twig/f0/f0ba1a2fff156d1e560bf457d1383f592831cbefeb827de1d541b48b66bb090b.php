<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteMiddlewareMoodleBundle:Default:index.html.twig */
class __TwigTemplate_3e6d442e51802e5cb85128d365ae4001a56cf1ba41515d4004a540adb8883c6d extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteMiddlewareMoodleBundle:Default:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Dashboard";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Painel ";
    }

    // line 4
    public function block_conteudo($context, array $blocks = [])
    {
        // line 5
        echo "    <section class=\"panel text-center\">
        <div class=\" panel-body\">
            <div class=\"row\">
                <div class=\"col-md-4\">
                    <a href=\"\">
                        <div class=\"featured-box featured-box-primary featured-box-effect-3\">
                            <div class=\"box-content\">
                                <i class=\"icon-featured fa fa-refresh fa-5x\"></i>
                                <h4>Sincronizar Alterações</h4>
                                <p>Sincronizar as notas aferidas no Moodle com o SIE</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-4\">
                    <a href=\"\">
                        <div class=\"featured-box featured-box-primary featured-box-effect-3\">
                            <div class=\"box-content\">
                                <i class=\"icon-featured fa fa-bar-chart fa-5x\"></i>
                                <h4>Relatórios</h4>
                                <p>Visualiza relatório e estátisticas do sistema</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class=\"col-md-4\">
                    <a href=\"";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_base_homepage");
        echo "\">
                        <div class=\"featured-box featured-box-primary featured-box-effect-3\">
                            <div class=\"box-content\">
                                <i class=\"icon-featured fa fa-cog fa-5x\"></i>
                                <h4>Configurações</h4>

                                <p>Gerencia as configurações do sistema</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
";
    }

    public function getTemplateName()
    {
        return "NteMiddlewareMoodleBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 33,  61 => 5,  58 => 4,  52 => 3,  46 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteMiddlewareMoodleBundle:Default:index.html.twig", "/var/www/narfi/src/Nte/Middleware/MoodleBundle/Resources/views/Default/index.html.twig");
    }
}
