<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_cadastro_parte_2.html.twig */
class __TwigTemplate_1138a60ea596dec0b12d916be18bf56fa520f488436bce0b5cb9f954cd80b6bf extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_cadastro_parte_2.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Detalhes Contrato ";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "    <li><span><a href=\"#\" class=\"breadcumbSemLink\">Pessoal Externo</a></span></li>
    <li><span><a href=\"";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_index");
        echo "\" class=\"breadcumbComLink\">Criar Contrato</a></span></li>
    <li><span><a href=\"#\" class=\"breadcumbSemLink\">Lista de Termos e Declarações</a></span></li>
";
    }

    // line 9
    public function block_conteudo($context, array $blocks = [])
    {
        // line 10
        echo "    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"modal\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Fechar\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <h4 class=\"modal-title\" style=\"text-align: center\"> Selecionar Colunas </h4>
                </div>
                <div class=\"modal-body\">
                    <div class=\"funkyradio check-inputs\">
                        <div class=\"grupo-modal\">
                            <div class=\"funkyradio-success\">
                                <input type=\"radio\" name=\"adicionar\" id=\"add-all-itens\" class=\"radioBtnClassAdicionar\"/>
                                <label for=\"add-all-itens\">Adicionar todos itens</label>
                            </div>
                            <div class=\"funkyradio-danger\">
                                <input type=\"radio\" name=\"adicionar\" id=\"remover-all-itens\" class=\"radioBtnClassAdicionar\"/>
                                <label for=\"remover-all-itens\">Remover todos itens</label>
                            </div>
                        </div>
                        <div class=\"grupo-modal\">
                            <div class=\"funkyradio-success\">
                                <input type=\"radio\" name=\"obrigatorio\" id=\"add-obrigatorios\" class=\"radioBtnClassObrigatorio\"/>
                                <label for=\"add-obrigatorios\">Tornar todos itens obrigatórios</label>
                            </div>
                            <div class=\"funkyradio-danger\">
                                <input type=\"radio\" name=\"obrigatorio\" id=\"remove-obrigatorios\" class=\"radioBtnClassObrigatorio\" />
                                <label for=\"remove-obrigatorios\">Tornar todos itens não obrigatórios</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" id=\"modalcancelar\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancelar</button>
                    <button type=\"button\" id=\"modal-confirmar\" class=\"btn btn-success\" data-dismiss=\"modal\">Confirmar</button>
                </div>
            </div>
        </div>
    </div>

    ";
        // line 52
        echo "
    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"modal-lista-confirm\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Fechar\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <h4 class=\"modal-title\" style=\"text-align: center\"> Este contrato já possuí uma configuração, deseja substituir configuração? </h4>
                </div>

                <div class=\"modal-footer\">
                    <button type=\"button\" id=\"modalcancelar\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_contrato_detalhes_criacao", ["id" => twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "id", [], "any", false, false, false, 65)]), "html", null, true);
        echo "\" class=\"btn-gerar-relatorio2\">
                        <button type=\"button\" id=\"modal-confirmar-substituir\" class=\"btn btn-success\" >Substituir</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    ";
        // line 74
        echo "
    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"modal-salvar-lista-primeira-vez\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Fechar\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <h4 class=\"modal-title\" style=\"text-align: center\"> Confirmar lista com os itens escolhidos? </h4>
                </div>

                <div class=\"modal-footer\">
                    <button type=\"button\" id=\"modalcancelar\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_contrato_detalhes_criacao", ["id" => twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "id", [], "any", false, false, false, 87)]), "html", null, true);
        echo "\" class=\"btn-gerar-relatorio2\">
                        <button type=\"button\" id=\"modal-confirmar-salvar-lista\" class=\"btn btn-success\" >Confirmar</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    ";
        // line 96
        echo "
    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"modal-obs\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Fechar\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <h4 class=\"modal-title\" style=\"text-align: center\"> nova obs </h4>
                </div>

                <div class=\"modal-footer\">
                    <button type=\"button\" id=\"modalcancelar\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_contrato_detalhes_criacao", ["id" => twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "id", [], "any", false, false, false, 109)]), "html", null, true);
        echo "\" class=\"btn-gerar-relatorio2\">
                        <button type=\"button\" id=\"modal-confirmar-salvar-lista\" class=\"btn btn-success\" >Confirmar</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <header class=\"panel-heading center\" style=\"width: 100%\">
        <div>
            <h2 class=\"panel-title\" >
                <span class=\"va-middle\">Lista de Termos e Declarações</span>
                <i class=\"fa fa-cog fa-lg breadcumbComLink\" aria-hidden=\"true\" style=\"float: right\" title=\"Clique aqui para configurar todos itens da lista\"></i>
            </h2>
        </div>
    </header>

    <div class=\"panel-body center\" style=\"width: 100%\">
        <table class=\"table table-striped\" id=\"myTable\" data-contrato-id=\"";
        // line 127
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "id", [], "any", false, false, false, 127), "html", null, true);
        echo "\">
            <tr>
                <th class=\"col-md-2\">Item</th>
                <th class=\"col-md-1\">Adicionar</th>
                <th class=\"col-md-1\">Obrigatório</th>
                ";
        // line 133
        echo "                <th class=\"col-md-1\">Ordem</th>
            </tr>
            ";
        // line 135
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["itensContrato"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["item"]) {
            // line 136
            echo "                <tr data-item-id=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 136), "html", null, true);
            echo "\" data-row-key=\"";
            echo twig_escape_filter($this->env, ($context["key"] + 1), "html", null, true);
            echo "\">
                    <td style=\"vertical-align: middle\">
                        ";
            // line 138
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "descricao", [], "any", false, false, false, 138), "html", null, true);
            echo "
                    </td>
                    <td style=\"vertical-align: middle\">
                        <label class=\"switch\">
                            <input type=\"checkbox\" class=\"switch-adicionar success\" >
                            <span class=\"slider round\"></span>
                        </label>
                    </td>
                    <td style=\"vertical-align: middle\">
                        <label class=\"switch\">
                            <input type=\"checkbox\" class=\"switch-obrigatorio success\" id=\"teste\" name=\"obrigatorio\">
                            <span class=\"slider round\"></span>
                        </label>
                    </td>
                    ";
            // line 153
            echo "                        ";
            // line 154
            echo "                        ";
            // line 155
            echo "                    ";
            // line 156
            echo "                    <td>
                        <div class=\"btn-group-vertical\">
                            <a title=\"Clique aqui para subir a prioridade do item\" class=\"btn btn-sm btn-default simple-ajax-action move up\">
                                <i class=\"fa fa fa-arrow-up text-success\"></i>
                            </a>
                            <div class=\"btn btn-sm btn-default testandoAuxOrdem\" id=\"prioridade-ordem-";
            // line 161
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 161), "html", null, true);
            echo "\" data-valor-prioridade=\"";
            echo twig_escape_filter($this->env, ($context["key"] + 1), "html", null, true);
            echo "\"></div>
                            <a title=\"Clique aqui para descer a prioridade do item\" class=\"btn btn-sm btn-default simple-ajax-action move down\">
                                <i class=\"fa fa fa-arrow-down text-success\"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 169
        echo "        </table>
        <div style=\"float: left\">
            <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"buttonRetornar\"><i class=\"fa fa-arrow-circle-left\"></i></button>
        </div>

        <div style=\"float: right\">
            <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"buttonGerarLista\">Salvar Lista</button>
        </div>

    </div>
";
    }

    // line 180
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 181
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script src=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/vcom.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function ()
        {
            // openObsModal();
            ordenarSelect();
            openCogModal();
            tableMovement();
            modalConfirmarCog();
            modalConfirmarListaFirstTime();
            verificarConfigExistente();
            modalConfirmarSubstituir();
        });

        function openObsModal()
        {
            \$('.btn-add-obs').click(function ()
            {
                let button = \$(this);

                vcom.modalForm(
                    (values, closemodal) => {
                        //  tratando valores do formularuio
                        let sibling = button.siblings()[0];
                        sibling.value = values.observacoes;
                        console.log(values.observacoes);
                        closemodal();
                    }, [
                    {
                        name: 'observacoes',
                        type: 'textarea',
                        label: 'Observações'
                    }
                ]);

                // \$('#modal-obs').modal();
            });
        }

        function modalConfirmarListaFirstTime()
        {
            \$('#modal-confirmar-salvar-lista').click(function() {
                let substituir = false;
                confirmarConfigLista(substituir);
            });
        }

        function modalConfirmarSubstituir()
        {
            \$('#modal-confirmar-substituir').click(function() {
                let substituir = true;
                confirmarConfigLista(substituir);
            });
        }

        /**
         * verifica se já não existe uma configuração para o contrato,
         * caso exista, retorna um modal perguntando se user quer substituir a config existente.
         * Caso user deseje substituir, cria-se um boolean true, que será enviado para o modal webservice inserirContratoConfigAction(),
         * neste webservice o boolean é verificado e caso seja true, deleta-se a config existente e cria-se uma nova com os novos itens.
         */
        function verificarConfigExistente()
        {
            let nteContratoId = \$('table').data('contrato-id');

            \$('#buttonGerarLista').click(function() {
                \$.ajax({
                    url: \"";
        // line 249
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_verifica_configuracao_contrato");
        echo "\",
                    dataType: 'json',
                    method: 'POST',
                    data: {
                        nteContratoIdKey: nteContratoId,
                    },
                    success: function (data)
                    {
                        console.log(data);
                        if (data === true) {
                            \$('#modal-lista-confirm').modal();
                        } else {
                            \$('#modal-salvar-lista-primeira-vez').modal();
                            console.log('config n existe, criar..');
                            // let substituir = false;
                            // //abrir modal e chamar o confirmar após o ara confirmar dentro do modal.
                            // confirmarConfigLista(substituir);
                        }
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            });
        }

        function confirmarConfigLista(substituir)
        {
            let itemArray = [];
            let adicionarArray = [];
            let obrigatorioArray = [];
            let ordemArray = [];

            //pega todos itens, insere e redireciona.
            let oTable = document.getElementById('myTable');
            let rowLength = oTable.rows.length;

            //loops through rows
            for (let i = 0; i < rowLength; i++)
            {
                //gets cells of current row
                let oCells = oTable.rows.item(i).cells;

                //gets amount of cells of current row
                let cellLength = oCells.length;

                //loops through each cell in current row
                for(let j = 0; j < cellLength; j++)
                {
                    //primeira (item) coluna da linha
                    if (j === 0)
                    {
                        let cellVal = oCells.item(j).innerText;
                        itemArray.push(cellVal);
                    }

                    //segunda (adicionar) coluna da linha

                    else if (j === 1)
                    {
                        let cellChildren = oCells.item(j).children[0];
                        let aux = \$(cellChildren).find('input').is(':checked') ? 1 : 0;

                        adicionarArray.push(aux);
                    }

                    //terceira (obrigatorio) coluna da linha

                    else if (j === 2)
                    {
                        // console.log('Obrigatório Switch');
                        let cellChildren = oCells.item(j).children[0];
                        let aux = \$(cellChildren).find('input').is(':checked') ? 1 : 0;
                        obrigatorioArray.push(aux);
                    }

                    // //quarta (ordem) coluna da linha
                    //
                    // else if (j === 3)
                    // {
                    //     let cellChildren = oCells.item(j).children[0];
                    //     let aux = \$(cellChildren).find('input').val();
                    //     //achar input data if not blank
                    // }

                    //quarta (ordem) coluna da linha

                    else if (j === 3)
                    {
                        let cellChildren = oCells.item(j).children[0];
                        let aux = \$(cellChildren).find('div').contents().text();
                        ordemArray.push(aux);
                    }
                }
            }

            itemArray.shift();
            adicionarArray.shift();
            obrigatorioArray.shift();
            ordemArray.shift();

            contratoConfigWebService(substituir, itemArray, adicionarArray, obrigatorioArray, ordemArray);
        }

        function contratoConfigWebService(substituir, itemArray, adicionarArray, obrigatorioArray, ordemArray)
        {
            let nteContratoId = \$('table').data('contrato-id');

            \$.ajax({
                url: \"";
        // line 358
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_insere_configuracao_contrato");
        echo "\",
                dataType: 'json',
                method: 'POST',
                data: {
                    nteContratoIdKey: nteContratoId,
                    substituirKey: substituir,
                    itemArrayKey: itemArray,
                    adicionarArrayKey: adicionarArray,
                    obrigatorioArrayKey: obrigatorioArray,
                    ordemArrayKey: ordemArray,
                },
                success: function (data)
                {
                    console.log(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }

        function modalConfirmarCog()
        {
            \$('#modal-confirmar').click(function() {

                if (\$(\"input[type='radio'].radioBtnClassAdicionar\").is(':checked'))
                {
                    let resultAdd = \$(\"input[name='adicionar'].radioBtnClassAdicionar:checked\").attr('id');

                    if (resultAdd == 'add-all-itens')
                    {
                        \$('table td:nth-child(2) .switch-adicionar').prop('checked', true);
                        console.log('aaaa');
                    } else if (resultAdd == 'remover-all-itens')
                    {
                        \$('table td:nth-child(2) .switch-adicionar').prop('checked', false);
                    }
                }

                if (\$(\"input[type='radio'].radioBtnClassObrigatorio\").is(':checked'))
                {
                    let resultObrigatorio = \$(\"input[name='obrigatorio'].radioBtnClassObrigatorio:checked\").attr('id');
                    console.log(resultObrigatorio);

                    if (\$(\"input[type='radio'].radioBtnClassObrigatorio\").is(':checked'))
                    {
                        let resultObrigatorio = \$(\"input[name='obrigatorio'].radioBtnClassObrigatorio:checked\").attr('id');

                        if (resultObrigatorio == 'add-obrigatorios')
                        {
                            \$('table td:nth-child(3) .switch-obrigatorio').prop('checked', true);
                            console.log('aaaa');
                        } else if (resultObrigatorio == 'remove-obrigatorios')
                        {
                            \$('table td:nth-child(3) .switch-obrigatorio').prop('checked', false);
                        }
                    }
                }
            })
        }

        function ordenarSelect()
        {
            for (let i = 0 ; i <= 7 ; i ++)
            {
                \$(\"#prioridade-ordem-\" + i).html(i);
            }
        }

        function openCogModal()
        {
            \$('.fa.fa-cog.fa-lg').click( function()
            {
                \$(\"#modal\").modal();
            });
        }

        function tableMovement()
        {
            \$('table a.move').click( function(a)
            {
                let row = \$(this).closest('tr');
                let rowIndex = row.index();

                if (\$(this).hasClass('up') && rowIndex != 1)
                {
                    let posicaoAtual = \$(this).parent().parent().parent().find('.testandoAuxOrdem').text();
                    let valorAcima = \$(this).closest('tr').prev().find('.testandoAuxOrdem').text();
                    \$(this).closest('tr').prev().find('.testandoAuxOrdem').text(posicaoAtual);
                    \$(this).parent().parent().parent().find('.testandoAuxOrdem').text(valorAcima);
                    row.fadeOut(700,function (a) {\$(this).prev().before(row)}).fadeIn(700);
                }
                else if (\$(this).hasClass('down') && rowIndex != 7) {

                    let posicaoAtual = \$(this).parent().parent().parent().find('.testandoAuxOrdem').text();
                    let valorAcima = \$(this).closest('tr').next().find('.testandoAuxOrdem').text();
                    \$(this).closest('tr').next().find('.testandoAuxOrdem').text(posicaoAtual);
                    \$(this).parent().parent().parent().find('.testandoAuxOrdem').text(valorAcima);
                    row.fadeOut(700,function (a) {\$(this).next().after(row)}).fadeIn(700);
                }
            });
        }

    </script>
";
    }

    // line 464
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 465
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 466
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/vcom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <style>

        .vcom-modal-popup {
            max-width: 60vw;
            width: 60vw;
        }

        .vcom-modal-popup form {
            width: 80%;
        }

        .modal-header {
            border-bottom: 0 none;
        }

        .modal-footer {
            border-top: 0 none;
        }

        .grupo-modal {
            display: flex;
            justify-content: space-between;
        }

        .grupo-modal > div {
            width: calc(50% - 2px);
        }

        .funkyradio div {
            clear: both;
            /*margin: 0 50px;*/
            overflow: hidden;
        }
        .funkyradio label {
            /*min-width: 400px;*/
            width: 100%;
            border-radius: 3px;
            border: 1px solid #D1D3D4;
            font-weight: normal;
        }
        .funkyradio input[type=\"radio\"]:empty, .funkyradio input[type=\"checkbox\"]:empty {
            display: none;
        }
        .funkyradio input[type=\"radio\"]:empty ~ label, .funkyradio input[type=\"checkbox\"]:empty ~ label {
            position: relative;
            line-height: 2.5em;
            text-indent: 3.25em;

            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .funkyradio input[type=\"radio\"]:empty ~ label:before, .funkyradio input[type=\"checkbox\"]:empty ~ label:before {
            position: absolute;
            display: block;
            top: 0;
            bottom: 0;
            left: 0;
            content:'';
            width: 2.5em;
            background: #D1D3D4;
            border-radius: 3px 0 0 3px;
        }
        .funkyradio input[type=\"radio\"]:hover:not(:checked) ~ label:before, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label:before {
            content:'\\2714';
            text-indent: .9em;
            color: #C2C2C2;
        }
        .funkyradio input[type=\"radio\"]:hover:not(:checked) ~ label, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label {
            color: #888;
        }
        .funkyradio input[type=\"radio\"]:checked ~ label:before, .funkyradio input[type=\"checkbox\"]:checked ~ label:before {
            content:'\\2714';
            text-indent: .9em;
            color: #333;
            background-color: #ccc;
        }
        .funkyradio-danger input[type=\"radio\"]:checked ~ label:before, .funkyradio input[type=\"checkbox\"]:checked ~ label:before {
            content:'\\03a7';
            text-indent: .9em;
            color: #333;
            background-color: #ccc;
        }
        .funkyradio-danger input[type=\"radio\"]:hover:not(:checked) ~ label:before, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label:before {
            content:'\\03a7';
            text-indent: .9em;
            color: #C2C2C2;
        }
        .funkyradio-warning input[type=\"radio\"]:hover:not(:checked) ~ label:before, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label:before {
            content:'\\2713';
            text-indent: .9em;
            color: #C2C2C2;
            font-weight: bold;
        }

        .funkyradio-warning input[type=\"radio\"]:checked ~ label:before, .funkyradio input[type=\"checkbox\"]:checked ~ label:before {
            content: '\\2713';
            text-indent: .9em;
            color: #333;
            background-color: #ccc;
            font-weight: bold;

        }
        .funkyradio input[type=\"radio\"]:checked ~ label, .funkyradio input[type=\"checkbox\"]:checked ~ label {
            color: #777;
        }
        .funkyradio input[type=\"radio\"]:focus ~ label:before, .funkyradio input[type=\"checkbox\"]:focus ~ label:before {
            box-shadow: 0 0 0 3px #999;
        }
        .funkyradio-default input[type=\"radio\"]:checked ~ label:before, .funkyradio-default input[type=\"checkbox\"]:checked ~ label:before {
            color: #333;
            background-color: #ccc;
        }
        .funkyradio-primary input[type=\"radio\"]:checked ~ label:before, .funkyradio-primary input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #337ab7;
        }
        .funkyradio-success input[type=\"radio\"]:checked ~ label:before, .funkyradio-success input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #5cb85c;
        }
        .funkyradio-danger input[type=\"radio\"]:checked ~ label:before, .funkyradio-danger input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #d9534f;
        }
        .funkyradio-warning input[type=\"radio\"]:checked ~ label:before, .funkyradio-warning input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #f0ad4e;
        }
        .funkyradio-info input[type=\"radio\"]:checked ~ label:before, .funkyradio-info input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #5bc0de;
        }


        /* The switch - the box around the slider */
        .switch
        {
            position: relative;
            display: inline-block;
            width: 50px;
            height: 24px;
        }

        /* Hide default HTML checkbox */
        .switch input {display:none;}

        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: \"\";
            height: 16px;
            width: 16px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input.default:checked + .slider {
            background-color: #444;
        }
        input.primary:checked + .slider {
            background-color: #2196F3;
        }
        input.success:checked + .slider {
            background-color: #8bc34a;
        }
        input.info:checked + .slider {
            background-color: #3de0f5;
        }
        input.warning:checked + .slider {
            background-color: #FFC107;
        }
        input.danger:checked + .slider {
            background-color: #f44336;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(16px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }


        #select-ordem {
            width: 5em;
        }

        .left {
            margin: auto;
            float: left;
        }

        .center {
            margin: auto;
            width: 50%;
        }

        .fa-times {
            color: white;
        }

        .fa-exclamation {
            color: red;
        }

        .fa-check {
            color: white;
        }

        .btn {
            min-width: 9em;
            max-width: 9em;
        }

        tr td { text-align: center; }

        textarea { resize: none; }

        table th, td { text-align: center;
            vertical-align: middle; }

        li span a:hover{ text-decoration: none; }

        .breadcumbSemLink { cursor: no-drop; }

        .breadcumbComLink { cursor: pointer; }



    </style>

";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_cadastro_parte_2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  598 => 466,  593 => 465,  590 => 464,  481 => 358,  369 => 249,  299 => 182,  294 => 181,  291 => 180,  277 => 169,  261 => 161,  254 => 156,  252 => 155,  250 => 154,  248 => 153,  231 => 138,  223 => 136,  219 => 135,  215 => 133,  207 => 127,  186 => 109,  171 => 96,  160 => 87,  145 => 74,  134 => 65,  119 => 52,  76 => 10,  73 => 9,  66 => 6,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_cadastro_parte_2.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontrato/contrato_cadastro_parte_2.html.twig");
    }
}
