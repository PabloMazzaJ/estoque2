<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ::base.admin.html.twig */
class __TwigTemplate_0aaeb62991cec9d5d1cb7f7df6d52fb5bd6571e1fee4a0ac4720f4c00a781875 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'cabecalho' => [$this, 'block_cabecalho'],
            'menu1' => [$this, 'block_menu1'],
            'breadcumbs' => [$this, 'block_breadcumbs'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'menu2' => [$this, 'block_menu2'],
            'rodape' => [$this, 'block_rodape'],
            'javascripts' => [$this, 'block_javascripts'],
            'javascripts2' => [$this, 'block_javascripts2'],
            'javascripts3' => [$this, 'block_javascripts3'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["imgUser"] = (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 1), "img", [], "any", true, true, false, 1) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 1), "img", [], "any", false, false, false, 1)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 1), "img", [], "any", false, false, false, 1)) : ("assets/img/default_user.png"));
        // line 2
        echo "<!doctype html>
";
        // line 4
        echo "<html class=\"fixed ";
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 4), "cookies", [], "any", false, false, false, 4), "get", [0 => "sidebar-state"], "method", false, false, false, 4) == "true")) {
            echo " sidebar-left-collapsed";
        }
        echo "\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <title>NARFI - ";
        // line 8
        $this->displayBlock('titulo', $context, $blocks);
        echo "</title>

    <meta name=\"keywords\" content=\"\"/>
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"NTE - UFSM\">
    <link rel=\"shortcut icon\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\"/>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\"/>

    ";
        // line 32
        echo "    ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 50
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/modernizr/modernizr.js"), "html", null, true);
        echo "\"></script>
</head>
<body>

<section class=\"body\">
    ";
        // line 55
        $this->displayBlock('cabecalho', $context, $blocks);
        // line 58
        echo "    <div class=\"inner-wrapper\">
        ";
        // line 59
        $this->displayBlock('menu1', $context, $blocks);
        // line 62
        echo "        <section role=\"main\" class=\"content-body\">
            ";
        // line 63
        $this->displayBlock('breadcumbs', $context, $blocks);
        // line 76
        echo "            ";
        $this->displayBlock('conteudo', $context, $blocks);
        // line 78
        echo "        </section>
        ";
        // line 79
        $this->displayBlock('menu2', $context, $blocks);
        // line 82
        echo "    </div>

    ";
        // line 84
        $this->displayBlock('rodape', $context, $blocks);
        // line 87
        echo "</section>
";
        // line 88
        $this->displayBlock('javascripts', $context, $blocks);
        // line 112
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/themeadmin.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/themeaadmin.init.js"), "html", null, true);
        echo "\"></script>

";
        // line 115
        $this->displayBlock('javascripts2', $context, $blocks);
        // line 117
        $this->displayBlock('javascripts3', $context, $blocks);
        // line 224
        echo "<a class=\"scroll-to-top hidden-mobile visible\" href=\"#\"><i class=\"fa fa-chevron-up\"></i></a>
<script>
    /*
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-77043326-6', 'auto');
    ga('set', 'metric1', \"";
        // line 239
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 239), "server", [], "any", false, false, false, 239), "get", [0 => "REMOTE_ADDR"], "method", false, false, false, 239), "html", null, true);
        echo "\");
    ga('set', 'dimension1', \"";
        // line 240
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 240), "server", [], "any", false, false, false, 240), "get", [0 => "REMOTE_ADDR"], "method", false, false, false, 240), "html", null, true);
        echo "\");
    ga('send', 'pageview');
*/
</script>

</body>
</html>";
    }

    // line 8
    public function block_titulo($context, array $blocks = [])
    {
    }

    // line 32
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 33
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap/css/bootstrap.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/font-awesome/css/font-awesome.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/magnific-popup/magnific-popup.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-ui/jquery-ui.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-ui/jquery-ui.theme.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/morris.js/morris.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2/css/select2.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2-bootstrap-theme/select2-bootstrap.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/pnotify/pnotify.custom.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2/css/select2.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2-bootstrap-theme/select2-bootstrap.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/theme.admin.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/theme-custom.admin.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jstree/themes/default/style.css"), "html", null, true);
        echo "\">
    ";
    }

    // line 55
    public function block_cabecalho($context, array $blocks = [])
    {
        // line 56
        echo "        ";
        $this->loadTemplate("::topo.admin.html.twig", "::base.admin.html.twig", 56)->display($context);
        // line 57
        echo "    ";
    }

    // line 59
    public function block_menu1($context, array $blocks = [])
    {
        // line 60
        echo "            ";
        $this->loadTemplate("::menu.admin.html.twig", "::base.admin.html.twig", 60)->display($context);
        // line 61
        echo "        ";
    }

    // line 63
    public function block_breadcumbs($context, array $blocks = [])
    {
        // line 64
        echo "                <header class=\"page-header\">
                    <h2>";
        // line 65
        $this->displayBlock('breadcumbsTitulo', $context, $blocks);
        echo "</h2>
                    <div class=\"right-wrapper pull-right\">
                        <ol class=\"breadcrumbs\">
                            <li><a href=\"";
        // line 68
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_admin_painel_homepage");
        echo "\"><i class=\"fa fa-home\"></i></a></li>
                            ";
        // line 69
        $this->displayBlock('breadcumbsItens', $context, $blocks);
        // line 71
        echo "                        </ol>
                        <a class=\"sidebar-right-toggle\" data-open=\"sidebar-right\"><i class=\"fa fa-chevron-left\"></i></a>
                    </div>
                </header>
            ";
    }

    // line 65
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Inicio";
    }

    // line 69
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 70
        echo "                                <li><span></span></li>";
    }

    // line 76
    public function block_conteudo($context, array $blocks = [])
    {
        // line 77
        echo "            ";
    }

    // line 79
    public function block_menu2($context, array $blocks = [])
    {
        // line 80
        echo "            ";
        $this->loadTemplate("::menu.admin2.html.twig", "::base.admin.html.twig", 80)->display($context);
        // line 81
        echo "        ";
    }

    // line 84
    public function block_rodape($context, array $blocks = [])
    {
        // line 85
        echo "
    ";
    }

    // line 88
    public function block_javascripts($context, array $blocks = [])
    {
        // line 89
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery/jquery.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-cookie/jquery-cookie.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-confirmation/bootstrap-confirmation.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/nanoscroller/nanoscroller.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.pt-BR.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/magnific-popup/jquery.magnific-popup.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-ui/jquery-ui.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/raphael/raphael.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/morris.js/morris.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2/js/select2.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2/js/pt-BR.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/pnotify/pnotify.custom.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery.validation/jquery.validation.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-mask/jquery.mask.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jstree/jstree.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-ui/jquery-ui.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 115
    public function block_javascripts2($context, array $blocks = [])
    {
    }

    // line 117
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 118
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/themeadmin.custom.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$visExportacao = \$(\"#visualizacao-exportacao\");

        function existeExportacoes() {
            if (\$visExportacao.children().size()) {
                \$(\"#exportacao-status\").removeClass('hidden');
            } else {
                \$(\"#exportacao-status\").addClass('hidden');
            }
        }


        function templateProgressoExportacao(id, nome, porcentagem) {
            return \"\"
                + \"<li id='e-\" + id + \"' class='valido'>\"
                + \"<span class='stats-title'>\" + nome + \"</span>\"
                + \"<span class='stats-complete'>\" + porcentagem + \"%</span>\"
                + \"  <div class='progress progress-striped light active'>\"
                + \"  <div class='progress-bar progress-bar-success progress-without-number' role='progressbar' \"
                + \"aria-valuenow='\" + porcentagem + \"' aria-valuemin='0' aria-valuemax='100' style='width: \" + porcentagem + \"%;'>\"
                + \"  <span class='sr-only'>\" + porcentagem + \"% Complete</span>\"
                + \"  </div>\"
                + \"  </div>\"
                + \"  </li>\";
        }

        /**
         * Retorna um template html
         * @param item
         * @returns {string}
         */
        function  templateNotificacao(item) {

            var template = \"<li>\"
            +'<a href=\"'+item.url+'\" class=\"clearfix\">'
            + '<div class=\"image\">'
            + '<i class=\"fa '+item.icone+' bg-danger\"></i>'
            + \"</div>\"
            + '<span class=\"title\">'+item.titulo+'</span>'
            + '<span class=\"message\">'+item.subtitulo+'</span>'
            + \"</a>\"
            + \"</li>\";
            return template;
        }

        function verificarNotificacao() {
            \$notTarefaContador = \$('.navbar-notification-tasks-number');
            \$notTarefaListagem = \$('#navbar-notification-tasks-list');
            \$.ajax({
                type: 'POST',
                url: '";
        // line 169
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_admin_checagem_service");
        echo "',
                complete: function (data) {
                   // console.log(data.responseJSON.tarefa);
                    if(data.responseJSON.tarefa.contagem){
                        \$notTarefaContador.text(data.responseJSON.tarefa.contagem);
                        \$notTarefaListagem.children().remove();
                        window.x = data.responseJSON.tarefa.listagem;
                        console.log(data.responseJSON);
                        data.responseJSON.tarefa.listagem.forEach(function(item){
                            \$notTarefaListagem.append(templateNotificacao(item));
                        });
                    }else{
                        \$notTarefaContador.text('')
                    }
                }
            });
        }

        function verificarExportacaoAndamento() {
            \$.ajax({
                type: 'POST',
                url: '";
        // line 190
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_verificar_exportacao_ativa");
        echo "',
                complete: function (data) {
                    if (data.responseJSON.length) {
                        \$visExportacao.children().each(function () {
                            \$(this).removeClass('valido').addClass('invalido');
                        });
                        data.responseJSON.forEach(function (a) {
                            if (!\$(\"#e-\" + a.id).length) {
                                \$visExportacao.append(templateProgressoExportacao(a.id, a.curso, a.status));
                            } else {
                                \$(\"#e-\" + a.id).find('.stats-complete').text(a.status + \"%\");
                                \$(\"#e-\" + a.id).find('.progress-bar').attr('style', 'width:' + a.status + \"%;\");
                                \$(\"#e-\" + a.id).find('.progress-bar').attr('aria-valuenow', a.status);
                                \$(\"#e-\" + a.id).find('.sr-only').text(+a.status + \"% completo\");
                                \$(\"#e-\" + a.id).removeClass('invalido').addClass('valido');
                            }
                            \$visExportacao.find('.invalido').remove()
                              clearInterval(intervaloVerificarExportacao);
                            intervaloVerificarExportacao = setInterval(verificarExportacaoAndamento,1000);
                        });
                    } else {
                        \$visExportacao.children().remove();
                        clearInterval(intervaloVerificarExportacao);
                        intervaloVerificarExportacao = setInterval(verificarExportacaoAndamento,5000);
                    }
                }
            });
            existeExportacoes();
        }
        verificarNotificacao();
    //    verificarExportacaoAndamento();
    //    intervaloVerificarExportacao = setInterval(verificarExportacaoAndamento,5000);
    </script>
";
    }

    public function getTemplateName()
    {
        return "::base.admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  506 => 190,  482 => 169,  427 => 118,  424 => 117,  419 => 115,  413 => 110,  409 => 109,  405 => 108,  401 => 107,  397 => 106,  393 => 105,  389 => 104,  385 => 103,  381 => 102,  377 => 101,  373 => 100,  369 => 99,  365 => 98,  361 => 97,  357 => 96,  353 => 95,  349 => 94,  345 => 93,  341 => 92,  337 => 91,  333 => 90,  328 => 89,  325 => 88,  320 => 85,  317 => 84,  313 => 81,  310 => 80,  307 => 79,  303 => 77,  300 => 76,  296 => 70,  293 => 69,  287 => 65,  279 => 71,  277 => 69,  273 => 68,  267 => 65,  264 => 64,  261 => 63,  257 => 61,  254 => 60,  251 => 59,  247 => 57,  244 => 56,  241 => 55,  235 => 48,  231 => 47,  227 => 46,  223 => 45,  219 => 44,  215 => 43,  211 => 42,  207 => 41,  203 => 40,  199 => 39,  195 => 38,  191 => 37,  187 => 36,  183 => 35,  179 => 34,  174 => 33,  171 => 32,  166 => 8,  155 => 240,  151 => 239,  134 => 224,  132 => 117,  130 => 115,  125 => 113,  120 => 112,  118 => 88,  115 => 87,  113 => 84,  109 => 82,  107 => 79,  104 => 78,  101 => 76,  99 => 63,  96 => 62,  94 => 59,  91 => 58,  89 => 55,  80 => 50,  77 => 32,  71 => 13,  63 => 8,  53 => 4,  50 => 2,  48 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "::base.admin.html.twig", "/var/www/narfi/app/Resources/views/base.admin.html.twig");
    }
}
