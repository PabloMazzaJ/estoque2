<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteConfigMoodleBundle:Forms:formMoodleConfig.html.twig */
class __TwigTemplate_38426d5fc2c1b09da9f1fd4fa4f46a912637a7959d3dc2f53801c85dccde8a02 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<form name=\"moodle_config\" method=\"post\" action=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_novo");
        echo "\">
    ";
        // line 2
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
    <div class=\"fade in\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"Nova Tag\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close modal-dismiss\" data-dismiss=\"modal\">
                        <span aria-hidden=\"true\">&times;</span>
                        <span class=\"sr-only\">Fechar</span>
                    </button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Configuração de Ambiente Moodle</h4>
                </div>
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-6\">
                            ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nome", [], "any", false, false, false, 16), 'row');
        echo "
                        </div>
                        <div class=\"col-md-6\">
                            ";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "url", [], "any", false, false, false, 19), 'row');
        echo "
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-4\">
                            ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "servico", [], "any", false, false, false, 24), 'row');
        echo "
                        </div>
                        <div class=\"col-md-4\">
                            ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "usuario", [], "any", false, false, false, 27), 'row');
        echo "
                        </div>
                        <div class=\"col-md-4\">
                            <label>Senha</label>
                            <div class=\"controls\">
                                <input id=\"passwd\" type=\"password\" class=\"form-control\">
                            </div>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "token", [], "any", false, false, false, 38), 'row');
        echo "


                            ";
        // line 41
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "descricao", [], "any", false, false, false, 41), 'row');
        echo "


                        </div>
                    </div>
                    <div class=\"modal-footer\">
                        <div id=\"y\"></div>
                        <button type=\"button\" class=\"btn btn-default modal-dismiss\" data-dismiss=\"modal\">Cancelar
                        </button>
                        <a class=\"btn btn-primary btn-capturar-token\">Capturar Token</a>
                        <button type=\"submit\" class=\"btn btn-success\" data-dismiss=\"modal\">Salvar</button>
                        ";
        // line 53
        echo "                    </div>
                </div>
            </div>
        </div>
        ";
        // line 57
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
        ";
        // line 58
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
        ";
        // line 59
        if ((isset($context["form_delete"]) || array_key_exists("form_delete", $context))) {
            // line 60
            echo "            <div class=\"pull-left\" id=\"x\">
                ";
            // line 61
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form_delete"] ?? null), 'form_start');
            echo "
                <button type=\"submit\" class=\"btn btn-danger\" data-dismiss=\"modal\">Remover</button>
                ";
            // line 63
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form_delete"] ?? null), 'widget');
            echo "
                ";
            // line 64
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form_delete"] ?? null), 'rest');
            echo "
            </div>
        ";
        }
        // line 67
        echo "    </div>
    <script>
        \$('.btn-capturar-token').click(function () {
            \$.ajax({
                type: 'POST',
                url: '";
        // line 72
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_capturar_token");
        echo "',
                data: {
                    usuario: \$('#moodle_config_usuario').val(),
                    senha: \$('#passwd').val(),
                    servico: \$('#moodle_config_servico').val(),
                    url: \$('#moodle_config_url').val()
                },
                complete: function (data) {
                    if(data.responseText != null){
                        \$('#moodle_config_token').val(data.responseText);
                        \$('#moodle_config_valido').val(1)
                    }else{
                        new PNotify({
                            title: 'Atualizar Ofertas',
                            text: data.responseText,
                            type: 'success'
                        });

                        /*new PNotify({
                            type:\"error\",
                            text:\"Parâmetros Inválidos, não conseguiu caputar o token\",
                            title:\"Erro ao capturar o token\"
                        })*/
                    }

                }
            });
        });
        ";
        // line 100
        $this->loadTemplate(":js:_form_validador.js.twig", "NteConfigMoodleBundle:Forms:formMoodleConfig.html.twig", 100)->display($context);
        // line 101
        echo "        \$('#y').html(\$('#x'));
        \$('[data-plugin-selectTwo]').each(function () {
            var \$this = \$(this),
                opts = {};
            var pluginOptions = \$this.data('plugin-options');
            if (pluginOptions)
                opts = pluginOptions;
            \$this.themePluginSelect2(opts);
        });
        \$(\".mfp-wrap\").removeAttr('tabindex').css('overflow-y', 'visible').css('overflow-x', 'visible');
    </script>";
    }

    public function getTemplateName()
    {
        return "NteConfigMoodleBundle:Forms:formMoodleConfig.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 101,  183 => 100,  152 => 72,  145 => 67,  139 => 64,  135 => 63,  130 => 61,  127 => 60,  125 => 59,  121 => 58,  117 => 57,  111 => 53,  97 => 41,  91 => 38,  77 => 27,  71 => 24,  63 => 19,  57 => 16,  40 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteConfigMoodleBundle:Forms:formMoodleConfig.html.twig", "/var/www/narfi/src/Nte/Config/MoodleBundle/Resources/views/Forms/formMoodleConfig.html.twig");
    }
}
