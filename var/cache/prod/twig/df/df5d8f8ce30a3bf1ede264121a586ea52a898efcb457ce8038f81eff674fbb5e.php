<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:Relatorio:index.html.twig */
class __TwigTemplate_d73fcbbe2ade000fa3dd679c45ff3ebfe4c8a3bc6db663cbb1d6222993f26a9a extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoExportadorBundle:Relatorio:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Dashboard";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Painel de Relatórios ";
    }

    // line 4
    public function block_conteudo($context, array $blocks = [])
    {
        // line 5
        echo "    <section class=\"panel text-center\">
        <div class=\" panel-body\">
            <div class=\"row\">
                <div class=\"col-md-3\">
                    <a href=\"";
        // line 9
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_index");
        echo "\">
                        <div class=\"featured-box featured-box-primary featured-box-effect-3\" style=\"height: 168px;\">
                            <div class=\"box-content\">
                                <i class=\"icon-featured fa fa-file-text fa-5x\"></i>
                                <h4>LOGs</h4>
                                <p>Visualza o log do middleware</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:Relatorio:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 9,  61 => 5,  58 => 4,  52 => 3,  46 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:Relatorio:index.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/Relatorio/index.html.twig");
    }
}
