<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_cadastro.html.twig */
class __TwigTemplate_c90748bc8c5bd90ce6972e8dc47a9af373b207ef63ad17593ed49e6cd912ba86 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_cadastro.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Cadastro Pessoa ";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\">Pessoal Externo</a></span></li>
     <li><span><a href=\"";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_mostra");
        echo "\">Pessoa</a></span></li>
     <li><span><a href=\"#\">Cadastro</a></span></li>
 ";
    }

    // line 9
    public function block_conteudo($context, array $blocks = [])
    {
        // line 10
        echo "    <header class=\"panel-heading\">
        <div>
            <h2 class=\"panel-title\">
                <span class=\"va-middle\"><i class=\"fa fa-users\"></i> Cadastro de Pessoa</span>
            </h2>
        </div>
    </header>
    <div class=\"panel-body\">
        ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
          <div class=\"row\">
              <div class=\"form-group col-md-3\">
                  ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nome", [], "any", false, false, false, 21), 'row');
        echo "
              </div>
              <div class=\"form-group col-md-4\">
                  ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "sobrenome", [], "any", false, false, false, 24), 'row');
        echo "
              </div>
              <div class=\"form-group col-md-2\">
                  ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "identidade", [], "any", false, false, false, 27), 'row');
        echo "
              </div>
              <div class=\"form-group col-md-2\">
                  ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "cpf", [], "any", false, false, false, 30), 'row');
        echo "
              </div>

          </div>

        <div class=\"row\">
            <div class=\"form-group col-md-3\">
                ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, false, 37), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "usuarioMoodle", [], "any", false, false, false, 40), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "telefone0", [], "any", false, false, false, 43), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "telefone1", [], "any", false, false, false, 46), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "dataNascimento", [], "any", false, false, false, 49), 'row');
        echo "
            </div>
        </div>

        <div class=\"row\">
            <div class=\"form-group col-md-4\">
                ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoLogradouro", [], "any", false, false, false, 55), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-3\">
                ";
        // line 58
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoNumero", [], "any", false, false, false, 58), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-4\">
                ";
        // line 61
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoBairro", [], "any", false, false, false, 61), 'row');
        echo "
            </div>
        </div>

        <div class=\"row\">
            <div class=\"form-group col-md-4\">
                ";
        // line 67
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoComplemento", [], "any", false, false, false, 67), 'row', ["label" => "Complemento"]);
        echo "
            </div>
            <div class=\"form-group col-md-3\">
                ";
        // line 70
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoCidade", [], "any", false, false, false, 70), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 73
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoUf", [], "any", false, false, false, 73), 'row');
        echo "
            </div>
            <div class=\"form-group col-md-2\">
                ";
        // line 76
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enderecoCep", [], "any", false, false, false, 76), 'row');
        echo "
            </div>
        </div>

        <div class=\"row\">
            <div class=\"form-group col-md-6\">
                <a href=\"";
        // line 82
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_mostra");
        echo "\">
                    <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"expButton\"><i class=\"fa fa-arrow-circle-left\"></i></button>
                </a>
                <button type=\"submit\" value=\"Cadastrar\" class=\"btn btn-primary btn-sm\" id=\"expButton\">Cadastrar</button>
            </div>

            <div class=\"form-group col-md-5\"></div>
        </div>
        ";
        // line 90
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
    </div>




";
    }

    // line 98
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 99
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>
    </script>
";
    }

    // line 104
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 105
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>
        #expButton{
            min-width: 9em;
            max-width: 9em;
        }

        .btn.btn-success {
            margin-top: 1em;
        }

        .row {
            display: flex;
            justify-content: center;
        }



    </style>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_cadastro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  238 => 105,  235 => 104,  226 => 99,  223 => 98,  212 => 90,  201 => 82,  192 => 76,  186 => 73,  180 => 70,  174 => 67,  165 => 61,  159 => 58,  153 => 55,  144 => 49,  138 => 46,  132 => 43,  126 => 40,  120 => 37,  110 => 30,  104 => 27,  98 => 24,  92 => 21,  86 => 18,  76 => 10,  73 => 9,  66 => 6,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_cadastro.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntepessoa/pessoa_cadastro.html.twig");
    }
}
