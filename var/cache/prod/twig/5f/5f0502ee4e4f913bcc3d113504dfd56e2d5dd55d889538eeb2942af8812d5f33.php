<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoBuscadorBundle:Pessoas:perfil-completo.html.twig */
class __TwigTemplate_018eb43ef35275f25004cbbc5d07a0bf242340a7027a1c32fd64293854b8c8ea extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil-completo.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "pessoa", [], "any", false, false, false, 2), "nomeCivil", [], "any", false, false, false, 2), "html", null, true);
        echo " ";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "pessoa", [], "any", false, false, false, 3), "nomeCivil", [], "any", false, false, false, 3), "html", null, true);
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span>Painel de Controle</span></li>
     <li><span>Buscar Pessoas</span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "    ";
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_filtro_pesquisa.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil-completo.html.twig", 9)->display($context);
        // line 10
        echo "    <div id=\"resposta\" class=\"\">
        <div class=\"row\">
            ";
        // line 12
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_foto.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil-completo.html.twig", 12)->display($context);
        // line 13
        echo "            <div class=\"col-lg-9 col-md-8 col-sm-12\">
                <div class=\"row\">
                    ";
        // line 15
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_cadastro.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil-completo.html.twig", 15)->display($context);
        // line 16
        echo "                    ";
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_contato.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil-completo.html.twig", 16)->display($context);
        // line 17
        echo "                </div>
                <div class=\"row\">
                    ";
        // line 19
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_usuario.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil-completo.html.twig", 19)->display($context);
        // line 20
        echo "                </div>
            </div>
        </div>
        <div class=\"row\">
            ";
        // line 24
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_alteracao.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil-completo.html.twig", 24)->display($context);
        // line 25
        echo "            ";
        $this->loadTemplate("NteAplicacaoBuscadorBundle:Pessoas:_turma.html.twig", "NteAplicacaoBuscadorBundle:Pessoas:perfil-completo.html.twig", 25)->display($context);
        // line 26
        echo "        </div>
    </div>
";
    }

    // line 29
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 30
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>
        function buscarPeloDocumento(nro, doc) {
            \$.loader.open();
            \$.ajax({
                type: 'POST',
                url: '";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("buscador_pessoas_pesquisar");
        echo "',
                data: {nro: nro, doc: doc},
                dataType: 'json',
                complete: function (data) {
                    \$('#resposta').html(data.responseText);
                    \$.loader.close();
                }
            });
        }
        \$('#doc').change(function () {
            if (\$(this).find(':selected').data)
                if (\$(this).find(':selected').data('mask') != '1') {
                    \$('#nro').mask(\$(this).find(':selected').data('mask'));
                } else {
                    \$('#nro').unmask();
                }
            \$('#nro').attr('placeholder', \$(this).find(':selected').data('mask'));
        });
        \$(\"#fnome\").change(function () {
            window.location='";
        // line 55
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("buscador_pessoas_perfil");
        echo "/'+ this.value;
        });
        \$('#pesquisar-pessoa').click(function () {
            buscarPeloDocumento(\$('#nro').val(), \$('#doc').val())
        });

        \$(document).ready(function () {
            \$(\"#fnome\").select2({
                minimumInputLength: 3,
                width: \"300px\",
                placeholder: \"Buscar pessoas pelo nome...\",
                ajax: {
                    url: '";
        // line 67
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("buscador_pessoas_filtrar_nome_middleware");
        echo "',
                    dataType: 'json',
                    method: \"POST\",
                    data: function (params) {
                        return {
                            termo: params.term,
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                }
            });
        })

    </script>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoBuscadorBundle:Pessoas:perfil-completo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  165 => 67,  150 => 55,  128 => 36,  118 => 30,  115 => 29,  109 => 26,  106 => 25,  104 => 24,  98 => 20,  96 => 19,  92 => 17,  89 => 16,  87 => 15,  83 => 13,  81 => 12,  77 => 10,  74 => 9,  71 => 8,  65 => 5,  62 => 4,  56 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoBuscadorBundle:Pessoas:perfil-completo.html.twig", "/var/www/narfi/src/Nte/Aplicacao/BuscadorBundle/Resources/views/Pessoas/perfil-completo.html.twig");
    }
}
