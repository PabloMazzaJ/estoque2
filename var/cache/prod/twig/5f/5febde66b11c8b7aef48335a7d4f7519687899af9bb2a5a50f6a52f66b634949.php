<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:Default:index.html.twig */
class __TwigTemplate_801df8abd0afd311973975577dc9860bce39001148fa8cafc90196d13f2caa24 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoExportadorBundle:Default:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Exportador";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Exportador";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span>Aplicação</span></li>
     <li><span>Exportador</span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "<div class=\"col-md-12\">
    <div class=\"panel\">
        <div class=\"panel-body\">
            <ul class=\"notifications pull-left hidden-xs\">
                <li data-toggle=\"tooltip\" data-placement=\"bottom\"
                    title=\"Visualizar fila de exportação\">
                    <a class=\"notification-icon btn-lista-exportacao\">
                        <i class=\"fa fa-bars\"></i>
                    </a>
                </li>
                <li data-toggle=\"tooltip\" data-placement=\"bottom\"
                    title=\"Visualizar ofertas disponveis para exportação\">
                    <a class=\"notification-icon btn-lista-oferta-disponiveis\">
                        <i class=\"fa fa-bullseye text-secondary\"></i>
                    </a>
                </li>

            </ul>
            <ul class=\"notifications pull-right hidden-xs contexto-oferta-menu\">

                <!--
                <li data-toggle=\"tooltip\" data-placement=\"bottom\" class=\"\"
                    title=\"Atualizar Ofertas conforme as definições do filtro\">
                    <a class=\"notification-icon notification-icon btn-atualizar-ofertas\">
                        <i class=\"fa fa-refresh  text-danger\"></i>
                    </a>
                </li>
                -->
                <li data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Configura as regras de exportação\">
                    <a class=\"notification-icon btn-regras-agrupamento\">
                        <i class=\"fa fa-cog\"></i>
                    </a>
                </li>
                <li data-toggle=\"tooltip\" data-placement=\"bottom\"
                    title=\"Gera um relatório com o quantitativo por turma\">
                    <a class=\"notification-icon btn-gerar-relatorio1\">
                        <i class=\"fa fa-list-ol\"></i>
                    </a>
                </li>
                <li data-toggle=\"tooltip\" data-placement=\"bottom\"
                    title=\"Gera uma relatório com todas as pessoas inscritas\">
                    <a class=\"notification-icon btn-gerar-relatorio2\">
                        <i class=\"fa fa-users\"></i>
                    </a>
                </li>
                <li data-toggle=\"tooltip\" data-placement=\"bottom\"
                    title=\"Adiciona o curso selecionado na lista de exportações\">
                    <a class=\"notification-icon btn-simple-ajax\" href=\"";
        // line 56
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportacao_new");
        echo "\">
                        <i class=\"fa fa-paper-plane text-success\"></i>
                    </a>
                </li>
            </ul>
            <ul class=\"notifications visible-xs\">
                <li data-toggle=\"tooltip\" data-placement=\"bottom\"
                    title=\"Visualizar as ofertas disponíveis para exportação\">
                    <a class=\"notification-icon btn-lista-exportacao\">
                        <i class=\"fa fa-bars\"></i>
                    </a>
                </li>
                <li data-toggle=\"tooltip\" data-placement=\"bottom\"
                    title=\"Visualiza cursos que estão na lista de exportações\">
                    <a class=\"notification-icon btn-lista-oferta-disponiveis\">
                        <i class=\"fa fa-bullseye text-secondary\"></i>
                    </a>
                </li>
                <!--
                <li data-toggle=\"tooltip\" data-placement=\"bottom\" class=\"contexto-oferta-menu\"
                    title=\"Atualizar Ofertas conforme as definições do filtro\">
                    <a class=\"notification-icon notification-icon btn-atualizar-ofertas\">
                        <i class=\"fa fa-refresh  text-danger\"></i>
                    </a>
                </li>
                -->
                <li data-toggle=\"tooltip\" data-placement=\"bottom\"  class=\"contexto-oferta-menu\"
                    title=\"Configura as regras de exportação\">
                    <a class=\"notification-icon btn-regras-agrupamento\">
                        <i class=\"fa fa-cog\"></i>
                    </a>
                </li>
                <li data-toggle=\"tooltip\" data-placement=\"bottom\" class=\"contexto-oferta-menu\"
                    title=\"Gera um relatório com o quantitativo por turma\">
                    <a class=\"notification-icon btn-gerar-relatorio1\">
                        <i class=\"fa fa-list-ol\"></i>
                    </a>
                </li>
                <li data-toggle=\"tooltip\" data-placement=\"bottom\" class=\"contexto-oferta-menu\"
                    title=\"Gera uma relatório com pessoas relacionadas por turma\">
                    <a class=\"notification-icon btn-gerar-relatorio2\">
                        <i class=\"fa fa-users\"></i>
                    </a>
                </li>
                <li data-toggle=\"tooltip\" data-placement=\"bottom\" class=\"contexto-oferta-menu\"
                    title=\"Adiciona o curso selecionado na lista de exportações\">
                    <a class=\"notification-icon btn-simple-ajax\" href=\"";
        // line 102
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportacao_new");
        echo "\">
                        <i class=\"fa fa-paper-plane text-success\"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
    <div class=\"col-md-4 col-lg-2 col-xs-12 contexto-oferta-menu contexto-oferta hidden\">
        <div class=\"panel panel-affix\" data-spy=\"affix\">
            <div class=\" panel-body\">
                <div class=\"form-group\">
                    <label for=\"cursoNivel\" class=\"control-label\">Nível do Curso</label>
                    <select id=\"cursoNivel\" data-plugin-selecttwo
                            data-plugin-options='{ \"width\": \"100%\" }'
                            class=\" form-control\" placeholder=\"Selecione\">
                        ";
        // line 118
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cursoNivel"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 119
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "item", [], "any", false, false, false, 119), "html", null, true);
            echo "\"
                                    ";
            // line 120
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 120), "cookies", [], "any", false, false, false, 120), "get", [0 => "exportador_nivel"], "method", false, false, false, 120) == twig_get_attribute($this->env, $this->source, $context["e"], "item", [], "any", false, false, false, 120))) {
                echo "selected";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "descricao", [], "any", false, false, false, 120), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 122
        echo "                    </select>
                </div>
                <div class=\"form-group\">
                    <label for=\"cursoTipo\" class=\"control-label\">Tipo do Curso</label>
                    <select id=\"cursoTipo\" data-plugin-selecttwo
                            data-plugin-options='{ \"width\": \"100%\" }'
                            class=\" form-control\" placeholder=\"Selecione\">
                        ";
        // line 129
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cursoTipo"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 130
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "item", [], "any", false, false, false, 130), "html", null, true);
            echo "\"
                                    ";
            // line 131
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 131), "cookies", [], "any", false, false, false, 131), "get", [0 => "exportador_tipo"], "method", false, false, false, 131) == twig_get_attribute($this->env, $this->source, $context["e"], "item", [], "any", false, false, false, 131))) {
                echo "selected";
            }
            echo " >";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "descricao", [], "any", false, false, false, 131), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 133
        echo "                    </select>
                </div>
                <div class=\"form-group\" class=\"control-label\">
                    <label for=\"cursoPeriodo\">Periodo</label>
                    <select id=\"cursoPeriodo\" data-plugin-selecttwo
                            data-plugin-options='{ \"width\": \"100%\" }'
                            class=\" form-control\"
                            placeholder=\"Selecione\">
                        ";
        // line 141
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cursoPeriodo"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 142
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "item", [], "any", false, false, false, 142), "html", null, true);
            echo "\"
                                    ";
            // line 143
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 143), "cookies", [], "any", false, false, false, 143), "get", [0 => "exportador_periodo"], "method", false, false, false, 143) == twig_get_attribute($this->env, $this->source, $context["e"], "item", [], "any", false, false, false, 143))) {
                echo "selected";
            }
            echo " >";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "descricao", [], "any", false, false, false, 143), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 145
        echo "                    </select>
                </div>
                <div class=\"form-group\">
                    <label for=\"cursoAno\" class=\"control-label\">Ano</label>
                    <select id=\"cursoAno\" data-plugin-selecttwo
                            data-plugin-options='{ \"width\": \"100%\" }'
                            class=\" form-control\" placeholder=\"Selecione\">
                        ";
        // line 152
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cursoAno"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 153
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "item", [], "any", false, false, false, 153), "html", null, true);
            echo "\"
                                    ";
            // line 154
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 154), "cookies", [], "any", false, false, false, 154), "get", [0 => "exportador_ano"], "method", false, false, false, 154) == twig_get_attribute($this->env, $this->source, $context["e"], "item", [], "any", false, false, false, 154))) {
                echo "selected";
            }
            echo " >";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "descricao", [], "any", false, false, false, 154), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 156
        echo "                    </select>
                </div>
                <div class=\"form-group\">
                    <a class=\"btn-buscar-cursos btn btn-primary btn-block\"><i class=\"fa fa-search\"></i> Buscar</a>
                </div>
            </div>
        </div>
    </div>
    <div class=\"col-md-12 col-lg-12 col-xs-12 contexto-geral\">
        <div class=\"panel hidden\" id=\"arvore\">
            <div id=\"resposta\" class=\" panel-body\">
                ";
        // line 167
        $this->loadTemplate("@NteAplicacaoExportador/Info/empty.html.twig", "NteAplicacaoExportadorBundle:Default:index.html.twig", 167)->display($context);
        // line 168
        echo "                <div id=\"raiz-cursos\"></div>
            </div>
        </div>
        <div class=\"\" id=\"relatorio\"></div>
    </div>


";
    }

    // line 176
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 177
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>
        /** Alterna entre as visões   */
        function alterarArvoreRelatorio(t) {
            if (t == 1) {
                \$('#relatorio').removeClass('hidden');
                \$('.contexto-oferta').addClass('hidden');
                //\$('.contexto-oferta-menu').addClass('hidden');
                \$('#arvore').addClass('hidden');
                menuLateral(0);
            } else {
                \$('.panel-affix').affix('checkPosition');
                \$('#relatorio').addClass('hidden');
                \$('.contexto-oferta').removeClass('hidden');
                //\$('.contexto-oferta-menu').removeClass('hidden');
                \$('#arvore').removeClass('hidden');
                menuLateral(1);
                \$(function() {
                    var \$affix = \$(\".panel-affix\"),
                        \$parent = \$affix.parent(),
                        resize = function() { \$affix.width(\$parent.width()); };
                    \$(window).resize(resize);
                    resize();
                });
            }
        }

        function menuLateral(\$t){
            if(\$t){
                \$('.contexto-geral').addClass('col-md-8 col-lg-10').removeClass('col-md-12 col-lg-12 ');
            }else{
                \$('.contexto-geral').removeClass('col-md-8 col-lg-10').addClass('col-md-12 col-lg-12 ');

            }
        }

        /**
         * Gerar Relatorio
         */
        function gerarRelatorio(url) {
            \$.ajax({
                type: 'POST',
                url: url,
                data: {
                    cursos: \$('#raiz-cursos').jstree('get_selected'),
                    cursoNivel: \$('#cursoNivel').val(),
                    cursoTipo: \$('#cursoTipo').val(),
                    cursoPeriodo: \$('#cursoPeriodo').val(),
                    cursoAno: \$('#cursoAno').val()
                },
                complete: function (data) {
                    \$('#relatorio').html(data.responseText);
                }
            });
        }

        /**
         * Busca os Cursos/Turmas/Participantes;
         **/
        function buscarCursos(callback, indice) {
            \$.ajax({
                type: 'POST',
                url: '";
        // line 239
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_aplicacao_exportador_buscar_cursos");
        echo "',
                data: {
                    cursoNivel: \$('#cursoNivel').val(),
                    cursoTipo: \$('#cursoTipo').val(),
                    cursoPeriodo: \$('#cursoPeriodo').val(),
                    cursoAno: \$('#cursoAno').val()
                },
                dataType: 'json',
                complete: function (data) {
                    callback.call(indice, data.responseJSON);
                }
            });
        }
        /**
         * Cria uma estrutura editavel em arvore com as categorias
         * @return Jquery
         */
        function renderizarArvoreCursos() {
            return \$('#raiz-cursos').jstree({
                'core': {
                    'themes': {'responsive': true, \"stripes\": true},
                    \"multiple\": false,
                    'check_callback': true, 'data': function (obj, callback) {
                        buscarCursos(callback, this);
                    }
                },
                'types': {
                    'default': {'icon': 'fa fa-folder'}, 'file': {'icon': 'fa fa-file'}
                },
                'plugins': [\"checkbox\", \"dnd\", \"massload\", \"search\", \"sort\", \"state\", \"types\", \"unique\", \"changed\", \"conditionalselect\"],
                \"ui\": {}
            });
        }
        /**
         * Recolhe todos nodos da arvore
         */
        function recolherNodos() {
            return \$(\"#raiz-cursos\").jstree('close_all');
        }

        /**
         * Expandi todos os nodos da arvore
         */
        function expandirNodos() {
            return \$(\"#treeCheckbox\").jstree('open_all');
        }

        /**
         * Atualioza Arvore
         */
        function atualizarArvore() {
            return \$('#raiz-cursos').jstree(\"refresh\");
        }


        \$('.btn-buscar-cursos').click(function () {
            alterarArvoreRelatorio(0);
            atualizarArvore();
        });
        \$('.btn-lista-oferta-disponiveis').click(function () {
            alterarArvoreRelatorio(0);
            renderizarArvoreCursos();
        });

        \$('.btn-gerar-relatorio1').click(function () {
            alterarArvoreRelatorio(1);
            \$('.contexto-oferta-menu').removeClass('hidden');
            \$('.contexto-geral').addClass('col-md-10  col-sm-8').removeClass('col-md-12');
            \$('#relatorio').html(\" \");
            menuLateral(1);
            gerarRelatorio('";
        // line 309
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_turma_quantitativo");
        echo "');
        });


        \$('.btn-gerar-relatorio2').click(function () {
            alterarArvoreRelatorio(1);
            \$('#relatorio').html(\" \");
            \$('.contexto-oferta-menu').removeClass('hidden');
            menuLateral(1);
            gerarRelatorio('";
        // line 318
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_turma_participante");
        echo "');
        });

        \$('.btn-regras-agrupamento').click(function () {
            alterarArvoreRelatorio(1);
            \$('#relatorio').html(\"<div class='panel-body'></div>\");
            \$('.contexto-oferta-menu').removeClass('hidden');
            menuLateral(1);
            \$.ajax({
                type: 'POST',
                url: '";
        // line 328
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_aplicacao_exportador_regras_index");
        echo "',
                data: {
                    cursos: \$('#raiz-cursos').jstree('get_selected'),
                    cursoNivel: \$('#cursoNivel').val(),
                    cursoTipo: \$('#cursoTipo').val(),
                    cursoPeriodo: \$('#cursoPeriodo').val(),
                    cursoAno: \$('#cursoAno').val()
                },
                complete: function (data) {
                    \$('#relatorio').html(data.responseText);
                }
            });
        });

        \$('.btn-lista-exportacao').click(function () {
            alterarArvoreRelatorio(1);
            \$.ajax({
                type: 'GET',
                url: '";
        // line 346
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportacao_index");
        echo "',
                complete: function (data) {
                    \$('#relatorio').html(data.responseText);
                }
            });
        });

        \$('.btn-atualizar-ofertas').click(function () {

            new PNotify({title: 'Atualizar Ofertas', text: \"Aguarde... Atualizando\", type: 'info'});

            \$.ajax({
                type: 'POST',
                url: '";
        // line 359
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_aplicacao_exportador_atualizar_ofertas");
        echo "',
                data: {
                    cursoNivel: \$('#cursoNivel').val(),
                    cursoTipo: \$('#cursoTipo').val(),
                    cursoPeriodo: \$('#cursoPeriodo').val(),
                    cursoAno: \$('#cursoAno').val()
                },
                complete: function (data) {
                    new PNotify({
                        title: 'Atualizar Ofertas',
                        text: data.responseText,
                        type: 'success'
                    });
                    atualizarArvore();
                }
            });
        });



        \$('.simple-ajax-modal').magnificPopup({
            type: 'ajax',
            modal: true
        });

        function principal() {

            \$.ajax({
                type: 'GET',
                url: '";
        // line 388
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportacao_index");
        echo "',
                dataType: 'json',
                complete: function (data) {

                    \$(\"#relatorio\").html(data.responseText);
                    updateDomJS();
                }
            });
        }
        function updateDomJS() {
            \$(\".btn-simple-ajax\").unbind('click');
            \$(\".btn-cancelar\").unbind('click');

            \$('[data-toggle=\"confirmation\"]').confirmation();

            \$('.btn-cancelar').click(function () {
                principal();
            });

            \$(\".btn-simple-ajax\").click(function (e) {
                e.preventDefault();
                \$.ajax({
                    type: 'GET',
                    url: \$(this).attr('href'),
                    dataType: 'json',
                    complete: function (data) {
                        \$(\"#relatorio\").html(data.responseText);
                        alterarArvoreRelatorio(1);
                        updateDomJS();
                    }
                });
            });
        }
        updateDomJS();
        principal();
    </script>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  563 => 388,  531 => 359,  515 => 346,  494 => 328,  481 => 318,  469 => 309,  396 => 239,  330 => 177,  327 => 176,  316 => 168,  314 => 167,  301 => 156,  289 => 154,  284 => 153,  280 => 152,  271 => 145,  259 => 143,  254 => 142,  250 => 141,  240 => 133,  228 => 131,  223 => 130,  219 => 129,  210 => 122,  198 => 120,  193 => 119,  189 => 118,  170 => 102,  121 => 56,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  54 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:Default:index.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/Default/index.html.twig");
    }
}
