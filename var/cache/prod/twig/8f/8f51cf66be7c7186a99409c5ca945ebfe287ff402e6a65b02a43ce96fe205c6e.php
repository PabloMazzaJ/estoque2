<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoBuscadorBundle:Pessoas:_foto.html.twig */
class __TwigTemplate_ad11395c62c5bd7c0bb5c02ca1f1439ef6d865727708497f580ceb42bfff7c9a extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["img"] = ((((isset($context["img"]) || array_key_exists("img", $context)) &&  !(null === ($context["img"] ?? null)))) ? (("data:image/png;base64, " . ($context["img"] ?? null))) : ("/assets/img/default_user.png"));
        // line 2
        echo "<div class=\"col-lg-3 col-md-4 col-sm-12\">
    <section class=\"panel\">
        <div class=\"panel-body\">
            <div class=\"thumb-info mb-sm\">
                <center>
                    <img src=\"";
        // line 7
        echo twig_escape_filter($this->env, ($context["img"] ?? null), "html", null, true);
        echo "\" class=\"rounded img-responsive\" content=\"\"
                         id='usuario-imagem' alt=\"Imagem do novo usuário\">
                </center>
            </div>
            <h6 class=\"text-muted\">Vinculos</h6>
            <hr class=\"dotted short\">
            <p>
            <ol class=\"text-left\">
                ";
        // line 15
        if ( !(null === twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "vinculos", [], "any", false, false, false, 15))) {
            // line 16
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "vinculos", [], "any", false, false, false, 16));
            foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
                // line 17
                echo "                        <li>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["e"], "tipo", [], "any", false, false, false, 17), "descricao", [], "any", false, false, false, 17), "html", null, true);
                echo "
                            <small> (";
                // line 18
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["e"], "situacao", [], "any", false, false, false, 18), "descricao", [], "any", false, false, false, 18), "html", null, true);
                echo ")</small>
                        </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "                ";
        }
        // line 22
        echo "            </ol>
            </p>
        </div>
    </section>
</div>";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoBuscadorBundle:Pessoas:_foto.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 22,  76 => 21,  67 => 18,  62 => 17,  57 => 16,  55 => 15,  44 => 7,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoBuscadorBundle:Pessoas:_foto.html.twig", "/var/www/narfi/src/Nte/Aplicacao/BuscadorBundle/Resources/views/Pessoas/_foto.html.twig");
    }
}
