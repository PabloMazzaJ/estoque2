<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteConfigMoodleBundle:Default:index.html.twig */
class __TwigTemplate_e15947c4dccd0454c767bd9e859f1b8f5a12888f70dc9c618e5c1562226cf407 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteConfigMoodleBundle:Default:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Configurações - Moodle";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Configurações do Moodle";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "    <li><span>Configurações</span></li>
    <li><span>Moodle</span></li>
";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "    <nav class=\"navbar navbar-default\">
        <div id=\"menu\" class=\"container-fluid\">
            <div class=\"navbar-form navbar-left\">
                <div class=\"form-group\">
                    <ul class=\"notifications\">
                        <li data-toggle=\"tooltip\" data-placement=\"bottom\"
                            title=\"Habilitar todas as configurações selecionadas\">
                            <a class=\"notification-icon btn-ativar-todos\">
                                <i class=\"fa fa fa-check\"></i>
                            </a>
                        </li>
                        <li data-toggle=\"tooltip\" data-placement=\"bottom\"
                            title=\"desabilitar todas as configurações selecionadas\">
                            <a class=\"notification-icon btn-desativar-todos\">
                                <i class=\"fa fa fa-remove\"></i>
                            </a>
                        </li>
                        <li data-toggle=\"tooltip\" data-placement=\"top\"
                            title=\"Remover todas as  configurações selecionadas\">
                            <a class=\"notification-icon btn-remover-todos\"
                               data-btn-ok-label=\"Sim\" data-btn-ok-icon=\"fa fa-trash\"
                               data-btn-ok-class=\"btn btn-danger\" data-btn-cancel-label=\"Não\"
                               data-btn-cancel-icon=\"fa  fa-ban\" data-btn-cancel-class=\"btn btn-default\"
                               data-content=\"Você deseja remover as configurações selecionadas?\" data-title=\"Remover\"
                               data-toggle=\"confirmation\" data-placement=\"bottom\"
                               data-on-cancel=\"notificacaoPadrao\"
                               data-on-confirm=\"removerConfiguracoes\">
                                <i class=\"fa fa-trash\"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class=\"navbar-form navbar-right\">
                <div class=\"form-group\">
                    <ul class=\"notifications\">
                        <li data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Lista todos os comandos do crontab\">
                            <a class=\"notification-icon btn-simple-ajax\" href=\"";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_listar");
        echo "\">
                                <i class=\"fa fa-list-alt \"></i>
                            </a>
                        </li>
                        <li data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Adiciona um novo comando ao crontab\">
                            <a class=\"notification-icon btn-simple-ajax\" href=\"";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_novo");
        echo "\">
                                <i class=\"fa fa-plus text-success\"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div class=\"panel\" id=\"response\">

    </div>

";
    }

    // line 66
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 67
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-confirmation/bootstrap-confirmation.js"), "html", null, true);
        echo "\"></script>
    <script>

        function notificacaoPadrao() {
            new PNotify({title: 'Info', type: 'info', text: 'Nada a ser feito...'});
        }
        function principal() {
            \$.ajax({
                type: 'GET',
                url: \"";
        // line 76
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_listar");
        echo "\",
                dataType: 'json',
                complete: function (data) {
                    \$(\"#response\").html(data.responseText);
                }
            });
            updateDomJS();
        }
        function habilitarConfiguracoes(ids, status) {
            \$.ajax({
                type: 'POST',
                data: {ids: ids, status: status},
                url: \"";
        // line 88
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_habilitar");
        echo "\",
                dataType: 'json',
                complete: function (data) {
                    new PNotify(data.responseJSON);
                    principal();
                }
            });

        }
        function removerConfiguracoes() {
            ids = [];
            \$(':checkbox:checked:not(#jobs) ').each(function () {
                ids.push(this.value);
            });
            if (ids.length > 0) {
                \$.ajax({
                    type: 'POST',
                    data: {ids: ids},
                    url: \"";
        // line 106
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_remover_todos");
        echo "\",
                    dataType: 'json',
                    complete: function (data) {
                        new PNotify(data.responseJSON);
                        principal();
                    }
                });
            } else {
                notificacaoPadrao();
            }
        }

        function updateDomJS() {
            \$(\".btn-simple-ajax\").unbind('click');
            \$(\".btn-cancelar\").unbind('click');
            \$(\"#jobs\").unbind('click');
            \$('[data-toggle=\"confirmation\"]').confirmation();

            \$('.btn-ativar-todos').unbind('click');
            \$('.btn-ativar-todos').click(function () {
                ids = [];
                \$(':checkbox:checked:not(#jobs) ').each(function () {
                    ids.push(this.value);
                });
                if (ids.length > 0) {
                    habilitarConfiguracoes(ids, 1);
                } else {
                    notificacaoPadrao();
                }
            });
            \$('.btn-desativar-todos').unbind('click');
            \$('.btn-desativar-todos').click(function () {
                ids = [];
                \$(':checkbox:checked:not(#jobs) ').each(function () {
                    ids.push(this.value);
                });
                if (ids.length > 0) {
                    habilitarConfiguracoes(ids, 0);
                } else {
                    notificacaoPadrao();
                }
            });
            \$('.btn-remover-todos').unbind('click');
            \$('.btn-remover-todos').click(function () {
                \$(this).confirmation('show');
            });

            \$('.btn-cancelar').click(function () {
                principal();
            });
            \$('#jobs').click(function (event) {
                var status = this.checked;
                \$(':checkbox').each(function () {
                    this.checked = status;
                });
            });
            \$(\".btn-simple-ajax\").click(function (e) {
                e.preventDefault();
                \$.ajax({
                    type: 'GET',
                    url: \$(this).attr('href'),
                    dataType: 'json',
                    complete: function (data) {
                        \$(\"#response\").html(data.responseText);
                        updateDomJS();
                    }
                });
            });
        }

        principal();
    </script>
";
    }

    public function getTemplateName()
    {
        return "NteConfigMoodleBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 106,  169 => 88,  154 => 76,  141 => 67,  138 => 66,  119 => 51,  111 => 46,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  54 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteConfigMoodleBundle:Default:index.html.twig", "/var/www/narfi/src/Nte/Config/MoodleBundle/Resources/views/Default/index.html.twig");
    }
}
