<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:Relatorio:sie_alteracoes.html.twig */
class __TwigTemplate_80c2e336e3ced6d59d7e033f484ea0430e3a12f97be1ddc968978cd70d785227 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoExportadorBundle:Relatorio:sie_alteracoes.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Exportador";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Exportador";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span>Aplicação</span></li>
     <li><span>Exportador</span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "    <div class=\"col-md-4 col-lg-2 col-xs-12 contexto-oferta-menu contexto-oferta\">
        <div class=\"panel panel-affix\" data-spy=\"affix\">
            <div class=\" panel-body\">
                <div class=\"form-group\">
                    <label for=\"cursoTipo\" class=\"control-label\">Tipo de Curso</label>
                    <select id=\"cursoTipo\" data-plugin-selecttwo
                            data-plugin-options='{width: \"100%\"}'
                            class=\" form-control\" placeholder=\"Selecione\">
                        <option value=\"*\">Todos</option>
                        ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cursoTipo"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 19
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "id", [], "any", false, false, false, 19), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "descricao", [], "any", false, false, false, 19), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "                    </select>
                </div>
                <div class=\"form-group\">
                    <label for=\"curso\" class=\"control-label\">Curso</label>
                    <select id=\"curso\" data-plugin-selecttwo
                            data-plugin-options='{width: \"100%\"}'
                            class=\" form-control\" placeholder=\"Selecione\">
                        <option value=\"*\">Todos</option>
                        ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cursos"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 30
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "id", [], "any", false, false, false, 30), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "codigo", [], "any", false, false, false, 30), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "nome", [], "any", false, false, false, 30), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "                    </select>
                </div>
                <div class=\"form-group\">
                    <label for=\"observacao\" class=\"control-label\">Observação</label>
                    <select id=\"observacao\" data-plugin-selecttwo
                            data-plugin-options='{dropdownCssClass : \"\"}'
                            class=\" form-control\" placeholder=\"Selecione\">
                        <option value=\"*\">Todos</option>
                        ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["observacao"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["o"]) {
            // line 41
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["o"], "observacao", [], "any", false, false, false, 41), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["o"], "observacao", [], "any", false, false, false, 41), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['o'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "
                    </select>
                </div>
                <div class=\"form-group\">
                    <label for=\"alteracao\" class=\"control-label\">Tipo de Alteração</label>
                    <select id=\"alteracao\" data-plugin-selecttwo
                            data-plugin-options='{dropdownCssClass : \"\"}'
                            class=\" form-control\" placeholder=\"Selecione\">
                        <option value=\"*\">Todos</option>
                        ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["alteracaoTipo"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 53
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "id", [], "any", false, false, false, 53), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "descricao", [], "any", false, false, false, 53), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "
                    </select>
                </div>

                <div class=\"form-group\">
                    <label for=\"dataInicial\" class=\"control-label\">Data Inicial</label>
                    <input id='dataInicial' type=\"text\" class=\"form-control\"
                           data-date-format=\"yyyy-mm-dd\"
                           data-plugin-options='{\"todayHighlight\":true, \"language\": \"pt-BR\"}'
                           data-plugin-datepicker
                           placeholder=\"";
        // line 65
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "d/m/Y"), "html", null, true);
        echo "\">
                </div>
                <div class=\"form-group\">
                    <label for=\"dataFinal\" class=\"control-label\">Data Final</label>
                    <input id=\"dataFinal\" type=\"text\" class=\"form-control\"
                           data-date-format=\"yyyy-mm-dd\" data-date-end-date=\"0d\"
                           data-plugin-options='{\"todayHighlight\":true,   \"language\": \"pt-BR\"}'
                           data-plugin-datepicker
                           placeholder=\"";
        // line 73
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "d/m/Y"), "html", null, true);
        echo "\">
                </div>
                <div class=\"form-group\">
                    <a class=\"btn-buscar-cursos btn btn-primary btn-block\"><i class=\"fa fa-search\"></i> Buscar</a>
                </div>
            </div>
        </div>
    </div>
    <div class=\"col-md-8 col-lg-10 col-xs-12 contexto-geral\">
        <div class=\"panel-body\" id=\"relatorio\">

        </div>
    </div>
";
    }

    // line 87
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 88
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>

        \$('.btn-buscar-cursos').click(function () {
            \$.ajax({
                type: 'POST',
                url: '";
        // line 94
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_realtorio_sie_alteracao_busca");
        echo "',
                data: {
                    curso: \$('#curso').val(),
                    cursoTipo: \$('#cursoTipo').val(),
                    observacao: \$('#observacao').val(),
                    alteracaoTipo: \$('#alteracao').val(),
                    dataInicial: \$('#dataInicial').val(),
                    dataFinal: \$('#dataFinal').val()
                },
                complete: function (data) {
                    \$('#relatorio').html(data.responseText);
                }
            });
        });


        \$('.simple-ajax-modal').magnificPopup({
            type: 'ajax',
            modal: true
        });

        function principal() {
            \$.ajax({
                type: 'GET',
                url: '";
        // line 118
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_realtorio_sie_alteracao_busca");
        echo "',
                dataType: 'json',
                complete: function (data) {
                    \$(\"#relatorio\").html(data.responseText);
                    updateDomJS();
                }
            });
        }

        function updateDomJS() {
            \$(\".btn-simple-ajax\").unbind('click');
            \$(\".btn-cancelar\").unbind('click');

            \$('[data-toggle=\"confirmation\"]').confirmation();

            \$('.btn-cancelar').click(function () {
                principal();
            });

            \$(\".btn-simple-ajax\").click(function (e) {
                e.preventDefault();
                \$.ajax({
                    type: 'GET',
                    url: \$(this).attr('href'),
                    dataType: 'json',
                    complete: function (data) {
                        \$(\"#relatorio\").html(data.responseText);
                        updateDomJS();
                    }
                });
            });
        }
        updateDomJS();
        principal();
    </script>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:Relatorio:sie_alteracoes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 118,  230 => 94,  220 => 88,  217 => 87,  199 => 73,  188 => 65,  176 => 55,  165 => 53,  161 => 52,  150 => 43,  139 => 41,  135 => 40,  125 => 32,  112 => 30,  108 => 29,  98 => 21,  87 => 19,  83 => 18,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  54 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:Relatorio:sie_alteracoes.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/Relatorio/sie_alteracoes.html.twig");
    }
}
