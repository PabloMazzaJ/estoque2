<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntepessoacontrato:exportarTurmas.html.twig */
class __TwigTemplate_8a3c93f8888eccc41b931eb20b69e457157f6882cac6d08c18413ca856fd3f35 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntepessoacontrato:exportarTurmas.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Pessoas - Contrato ";
    }

    // line 3
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 4
        echo "     <li><span><a href=\"#\">Pessoal Externo</a></span></li>
     <li><span><a href=\"#\">Exportar Turmas</a></span></li>
 ";
    }

    // line 7
    public function block_conteudo($context, array $blocks = [])
    {
        // line 8
        echo "
    ";
        // line 10
        echo "    ";
        // line 11
        echo "    ";
        // line 12
        echo "
    <div class=\"panel panel-primary\" id=\"container-master\" data-codigo-unificado=\"";
        // line 13
        echo twig_escape_filter($this->env, ($context["codigoUnificado"] ?? null), "html", null, true);
        echo "\">
        <div class=\"panel-body\" id=\"container-selects\">
            <div id=\"subcontainer-curso\">
                <label for=\"select-curso\">Polos do curso ";
        // line 16
        echo twig_escape_filter($this->env, ($context["nomeunificado"] ?? null), "html", null, true);
        echo " </label>
                <select class=\"form-control form-control-sm fullwidth\" id=\"select-curso\" name=\"curso\">
                    <option>Curso/Polo</option>
                </select>
            </div>

            <div id=\"subcontainer-oferta\">
                <label for=\"select-periodo\">Oferta</label>
                <select class=\"form-control form-control-sm fullwidth\" id=\"select-oferta\" name=\"oferta\">
                    <option>Oferta</option>
                </select>
            </div>

            <div id=\"subcontainer-turma\">
                <label for=\"btn-turma\" class=\"turma-label\">empty</label>
                <button class=\"btn btn-success fullwidth\" id=\"btn-turma\" name=\"turma\">Add Turmas</button>
            </div>

        </div>
    </div>

    <div id=\"container-global\">

        <div id=\"container-principal\" class=\"panel-body\">

            <div class=\"table pessoasLeftTableContainer\">
                <div class=\"table-pessoas-left\"></div>
            </div>

            <div class=\"headerDivider\"></div>

            <div class=\"table table-disciplinas-container\">
                <div class=\"table-disciplinas\"></div>
            </div>

        </div>

    </div>


";
    }

    // line 58
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 59
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/vcom/vcom.js"), "html", null, true);
        echo "\"></script>
    <script>

        (function () {

            const model = {
                pessoasLeft: [],
                disciplinas: [],
                disciplina: '',
                disciplinaIndex: '',
                turmas: [],
                curso: '',
                oferta: '',
                isFirstTime: true,
                codUnificado: \$('#container-master').data('codigo-unificado'),
                lastMinusBtnId: '',
            };

            const controller = {

                init() {
                    this.buscarCursos();
                    this.inicializarTabelaPessoasLeft();
                    this.inicializarTabelaDisciplinas();
                    view.init();
                },

                setValorModel(nome, valor) {
                    model[nome] = valor;
                },

                layoutConfig() {
                    \$('.vcom-searchbar').addClass(\"form-control\");
                    \$('table').addClass(\"table-striped\");
                    \$('.pessoasLeftTableContainer').find('.vcom-searchbar').attr(\"placeholder\", \"Procurar por nome\");
                    \$('.table-disciplinas-container').find('.vcom-searchbar').attr(\"placeholder\", \"Procurar por disciplina\");
                },

                linkaPessoasTurmas() {
                    console.log('escondendo turmas');
                    \$('.vcom-table__row__action-btns').find(\"button[data-action='unlistTurmas']\").remove();
                },

                eventManager(evt) {
                    switch (evt) {

                        case 'curso':
                            this.setValorModel('curso', view.selectCurso.value);
                            this.setValorModel('oferta', view.selectOferta.value);
                            this.buscarOferta();
                            break;

                        case 'oferta':
                            this.populaDisciplinaTable();
                            break;

                        case 'listTurmas':
                            this.buscarTurmas();
                            break;

                        default:
                            console.log('!');
                    }
                },

                switchActionButton(trindex) {
                    // console.log('index');
                    // console.log(trindex);
                    let plusBtn = \$('.table-disciplinas').find(\"tr[data-index=\" + trindex + \"]\").find(\"button\");
                    let minusBtn = \"<button id=\" + trindex + \" data-action='listTurmas'><i class='fa fa-minus'></i></button>\";

                    // let display = \$(plusBtn).css('display');
                    // console.log(display);

                    // \$(plusBtn).hide();

                    // \$('.table-disciplinas').find(\"tr[data-index=\" + trindex + \"]\")
                    //     .find('.vcom-table__row__action-btns')
                    //     .append(minusBtn);




                    // \$(plusBtn).show();
                    // \$(\"#\" + trindex + \"\").remove();
                    // \$('.table-disciplinas').find(\"tr[data-index=\" + trindex + \"]\").remove();


                },

                appendTurmas() {

                    let disciplinasTable = view.disciplinasTableHtml.querySelector('table');
                    let linhaTr = disciplinasTable.childNodes[1].childNodes[model.disciplinaIndex];
                    let tdConteudo = linhaTr.childNodes[0];
                    let ulTurmasListadas = \$(linhaTr).find('ul');

                    let ulElement = document.createElement(\"ul\");

                    model.turmas.unshift({id: 0, nome: \"Selecionar Todas\"});

                    if (ulTurmasListadas.length === 0) {
                        //mostra se já existe, pois pode ter tomado hide no fluxo..
                        tdConteudo.appendChild(ulElement);
                        model.turmas.forEach(function (turma) {
                            let liElement = document.createElement(\"li\");
                            let span = document.createElement(\"span\");
                            let checkbox = document.createElement(\"input\");
                            checkbox.type = \"checkbox\";
                            checkbox.setAttribute(\"data-turma-id\", turma.id);
                            span.textContent = turma.nome;
                            liElement.appendChild(checkbox);
                            liElement.appendChild(span);
                            //jQuery to try animation sliding down..
                            // \$(liElement).appendTo(ulElement);
                            ulElement.appendChild(liElement);
                        });
                    }
                    else {
                        let ulDisplay = \$(ulTurmasListadas).css('display');
                        if (ulDisplay === \"none\") {
                            ulTurmasListadas.slideDown();
                        } else {
                            ulTurmasListadas.slideUp();
                        }
                    }
                },

                buscarTurmas() {
                    \$.ajax({
                        url: \"";
        // line 190
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_exportacao_buscar_turmas");
        echo "\",
                        dataType: 'json',
                        method: \"POST\",
                        data: {
                            codigoUnificado: model.codUnificado,
                            oferta: model.oferta,
                            disciplina: model.disciplina
                        },
                        success: function (data) {
                            let dataUtil = JSON.parse(data);
                            controller.setValorModel('turmas', dataUtil);
                            controller.appendTurmas();
                        },
                        error: function (data) {
                        }
                    })
                },

                carregarDisciplinasFirstTime() {
                    this.populaDisciplinaTable();
                },

                populaDisciplinaTable() {
                    this.disciplinasTable.removeAll();
                    this.disciplinasTable.setQueryBuilder(() => {
                        return {
                            oferta: model.oferta,
                            codigoUnificado: model.codUnificado,
                            curso: model.curso
                        }
                    });
                    this.disciplinasTable.reset();
                },

                //linha 490 da pra adicionar mais botoes no action button

                imprimeModel() {
                    console.table(model);
                },

                inicializarTabelaPessoasLeft() {
                    this.pessoasLeftTable = vcom.table(
                        /* value */  ['nome', 'papel'],
                        /* title */  ['Nome', 'Papel']
                    )
                        .setModel(model.pessoasLeft)
                        .setChecklist(true)
                        .enableSearch(true)
                        .setEmptyByDefault(false)
                        .setSource('";
        // line 239
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_exportacao_pessoas_left");
        echo "')
                        .setQueryBuilder((term) => {
                            return {
                                search: term,
                            }
                        });
                },

                inicializarTabelaDisciplinas() {
                    this.disciplinasTable = vcom.table(
                        /* value */  ['disciplina'],
                        /* title */  ['Disciplina']
                    )
                        .setModel(model.disciplinas)
                        .enableSearch(true)
                        .setSource(' ";
        // line 254
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_exportacao_table_disciplinas");
        echo "')
                        .setActionButtons([
                            {'action': 'listTurmas', 'innerHTML': '<i class=\"fa fa-plus\"></i>'},
                        ])
                        .setActionCallback((evt) => {

                            let valores = Object.values(evt.valueOf());
                            let index = valores[1];
                            let idDisciplina = valores[2].id;

                            if (evt.action === 'listTurmas') {
                                controller.setValorModel('disciplina', idDisciplina);
                                controller.setValorModel('disciplinaIndex', index);
                                controller.eventManager(evt.action);
                                controller.switchActionButton(index);
                            }
                        })
                        .setQueryBuilder((idcurso) => {
                            return {
                                idcurso: null,
                            }
                        });
                },

                buscarCursos() {
                    \$.ajax({
                        url: \"";
        // line 280
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_exportacao_select_cursos");
        echo "\",
                        dataType: 'json',
                        method: \"POST\",
                        data: {
                            codigoUnificadoKey: model.codUnificado
                        },
                        success: (data) => {
                            let dataUtil = JSON.parse(data);
                            view.inicializarSelectCursos(dataUtil);
                        },
                        error: function (data) {
                        }
                    });
                },

                buscarOferta() {
                    \$.ajax({
                        url: \"";
        // line 297
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_exportacao_select_oferta");
        echo "\",
                        dataType: 'json',
                        method: \"POST\",
                        data: {
                            curso: model.curso,
                            codigoUnificado: model.codUnificado
                        },
                        success: function (data) {
                            let dataUtil = JSON.parse(data);
                            view.inicializarSelectOfertas(dataUtil);
                        },
                        error: function (data) {
                        }
                    })
                },
            };

            const view = {

                init() {
                    this.inicializarReferenciaHtml();
                    this.inicializarEventListeners();
                    this.inicializarTabelas();
                },

                inicializarTabelas() {
                    this.disciplinasTableHtml = controller.disciplinasTable.mount();
                    this.pessoasLeftTableContainer.appendChild(controller.pessoasLeftTable.mount());
                    this.disciplinasTableContainer.appendChild(this.disciplinasTableHtml);
                    controller.layoutConfig();

                    //metodos da table
                    // console.log(controller.disciplinasTable);


                },

                inicializarSelectCursos(list) {
                    list.unshift({id: 0, nome: \"Todos Polos\"});
                    let select = document.getElementById('select-curso');
                    let selectInnerHTML = '';
                    selectInnerHTML = list.reduce((acc, opt) => {
                        return acc += `<option \${false} value='\${opt.id}'> \${opt.nome} </option>`;
                    }, selectInnerHTML);
                    select.innerHTML = selectInnerHTML;
                    controller.setValorModel('curso', view.selectCurso.value);
                    controller.buscarOferta();
                },

                inicializarSelectOfertas(list) {
                    let select = document.getElementById('select-oferta');
                    let selectInnerHTML = '';
                    selectInnerHTML = list.reduceRight((acc, opt) => {
                        return acc += `<option \${false} value='\${opt.id}'> \${opt.oferta} </option>`;
                    }, selectInnerHTML);
                    select.innerHTML = selectInnerHTML;
                    controller.setValorModel('oferta', view.selectOferta.value);

                    if (model.isFirstTime === true) {
                        controller.carregarDisciplinasFirstTime();
                        controller.setValorModel('isFirstTime', false);
                    } else {
                        controller.populaDisciplinaTable();
                    }
                },

                inicializarReferenciaHtml() {
                    // this.btnPesquisar = document.querySelector('.btn-pesquisar');
                    this.pessoasLeftTableContainer = document.querySelector('.table-pessoas-left');
                    this.disciplinasTableContainer = document.querySelector('.table-disciplinas');
                    this.selectCurso = document.querySelector('#select-curso');
                    this.selectOferta = document.querySelector('#select-oferta');
                    this.selectsContainer = document.querySelector('#container-selects');
                    this.btnAddTurmas = document.querySelector('#btn-turma');

                },

                inicializarEventListeners() {

                    this.selectsContainer.addEventListener('change', evt => {
                        controller.setValorModel(evt.target.name, evt.target.value);
                        controller.eventManager(evt.target.name);


                    });

                    // this.btnPesquisar.addEventListener('click', evt => {
                    //     // controller.imprimeModel();
                    //     // //get table data
                    // });

                    this.btnAddTurmas.addEventListener('click', evt => {
                        controller.linkaPessoasTurmas();
                    });

                },
            };
            controller.init();
        }());
    </script>
";
    }

    // line 399
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 400
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 401
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/vcom/vcom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <style>

        .turma-label {
            visibility: hidden;
        }

        input[type='checkbox'] {
            margin-left: 1em;
        }

        span {
            margin-left: 1em;
        }

        ul li {
            cursor: pointer;
            list-style-type: none;
            font-size: 1em;
            display: flex;
            justify-content: start;
            align-items: start;
        }

        li:hover {
            /*background-color: #5cb85c;*/
            -webkit-transition: background-color 1000ms linear;
            -ms-transition: background-color 1000ms linear;
            transition: background-color 1000ms linear;
        }


        ul {
            padding: 0;
            /*height: 0;*/
            overflow: hidden;
        }

        /*.vcom-table-container {*/
        /*overflow:scroll;*/
        /*height:1px;*/
        /*}*/


        .vertical-center {
            display: flex;
            align-items: center;
        }

        #subcontainer-produto {
            display: block;
        }

        #subcontainer-quantidade {
            display: block;
            margin-left: 1em;
        }

        #subcontainer-btn-add {
            display: block;
            margin-left: auto;
            float: right;
        }

        #container-selects {
            display: flex;
            justify-content: flex-start;
        }

        #container-global {
            display: flex;
            flex-direction: column;
            flex-grow: 1;
        }

        .fullwidth {
            width: 100%;
        }

        #container-principal {
            display: flex;
            justify-content: space-between;
        }

        .pessoasLeftTableContainer {
            height: 17em;
            width: 30%;
        }

        .table-disciplinas-container {
            height: auto;
            /*max-height: 120em;*/
            width: 70%;
        }

        .headerDivider {
            border-left: 1px solid #D7D7D7;
            border-right: 1px solid #D7D7D7;
            margin: 1em;
        }

    </style>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntepessoacontrato:exportarTurmas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  500 => 401,  495 => 400,  492 => 399,  387 => 297,  367 => 280,  338 => 254,  320 => 239,  268 => 190,  135 => 60,  130 => 59,  127 => 58,  82 => 16,  76 => 13,  73 => 12,  71 => 11,  69 => 10,  66 => 8,  63 => 7,  57 => 4,  54 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntepessoacontrato:exportarTurmas.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntepessoacontrato/exportarTurmas.html.twig");
    }
}
