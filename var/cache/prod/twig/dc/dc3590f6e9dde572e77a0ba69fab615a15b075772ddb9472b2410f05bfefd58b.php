<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* FOSUserBundle:Resetting:reset_content.html.twig */
class __TwigTemplate_02f68653e6b344c5eb0525d2c1644a8dc64318b482e4e39b5a67c7dbd7de11eb extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'form_row' => [$this, 'block_form_row'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => $this->getTemplateName()], true);
        // line 3
        echo "
";
        // line 4
        $this->displayBlock('form_row', $context, $blocks);
        // line 22
        echo "
";
        // line 23
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_resetting_reset", ["token" => ($context["token"] ?? null)]), "attr" => ["class" => "fos_user_resetting_reset"]]);
        echo "
";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
<div class=\"form-group mb-lg\">
<div class=\"row\">
    <button type=\"submit\" class=\"btn btn-primary btn-block\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("resetting.reset.submit", [], "FOSUserBundle"), "html", null, true);
        echo "</button>
</div>
</div>
";
        // line 30
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
";
    }

    // line 4
    public function block_form_row($context, array $blocks = [])
    {
        // line 5
        echo "    ";
        if (twig_in_filter("text", (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 5), "block_prefixes", [], "any", false, false, false, 5)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[1] ?? null) : null))) {
            // line 6
            echo "    <div class=\"form-group mb-lg\">
        <div class=\"clearfix\">
            ";
            // line 8
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label');
            echo "
            ";
            // line 9
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
        </div>
        <div class=\"input-group input-group-icon\">
            ";
            // line 12
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
            echo "
            <span class=\"input-group-addon\">
                <span class=\"icon icon-lg\">
                    <i class=\"fa fa-lock\"></i>
                </span>
            </span>
        </div>
    </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 12,  82 => 9,  78 => 8,  74 => 6,  71 => 5,  68 => 4,  62 => 30,  56 => 27,  50 => 24,  46 => 23,  43 => 22,  41 => 4,  38 => 3,  36 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "FOSUserBundle:Resetting:reset_content.html.twig", "/var/www/narfi/app/Resources/FOSUserBundle/views/Resetting/reset_content.html.twig");
    }
}
