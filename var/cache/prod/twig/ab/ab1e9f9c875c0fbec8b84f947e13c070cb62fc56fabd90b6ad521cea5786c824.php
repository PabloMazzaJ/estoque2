<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontratotemsieturma:index.html.twig */
class __TwigTemplate_533d95334fc1f87ebfb147f54a7b2bd01eae25800b17ae059a413f3f38acffae extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontratotemsieturma:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_conteudo($context, array $blocks = [])
    {
        // line 4
        echo "    <h1>Ntecontratotemsieturmas list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Valido</th>
                <th>Registrodatacriacao</th>
                <th>Registrodataatualizacao</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["nteContratoTemSieTurmas"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["nteContratoTemSieTurma"]) {
            // line 18
            echo "            <tr>
                <td><a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontratotemsieturma_show", ["id" => twig_get_attribute($this->env, $this->source, $context["nteContratoTemSieTurma"], "id", [], "any", false, false, false, 19)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["nteContratoTemSieTurma"], "id", [], "any", false, false, false, 19), "html", null, true);
            echo "</a></td>
                <td>";
            // line 20
            if (twig_get_attribute($this->env, $this->source, $context["nteContratoTemSieTurma"], "valido", [], "any", false, false, false, 20)) {
                echo "Yes";
            } else {
                echo "No";
            }
            echo "</td>
                <td>";
            // line 21
            if (twig_get_attribute($this->env, $this->source, $context["nteContratoTemSieTurma"], "registroDataCriacao", [], "any", false, false, false, 21)) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["nteContratoTemSieTurma"], "registroDataCriacao", [], "any", false, false, false, 21), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 22
            if (twig_get_attribute($this->env, $this->source, $context["nteContratoTemSieTurma"], "registroDataAtualizacao", [], "any", false, false, false, 22)) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["nteContratoTemSieTurma"], "registroDataAtualizacao", [], "any", false, false, false, 22), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontratotemsieturma_show", ["id" => twig_get_attribute($this->env, $this->source, $context["nteContratoTemSieTurma"], "id", [], "any", false, false, false, 26)]), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontratotemsieturma_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["nteContratoTemSieTurma"], "id", [], "any", false, false, false, 29)]), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['nteContratoTemSieTurma'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontratotemsieturma_new");
        echo "\">Create a new nteContratoTemSieTurma</a>
        </li>
    </ul>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontratotemsieturma:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 40,  116 => 35,  104 => 29,  98 => 26,  89 => 22,  83 => 21,  75 => 20,  69 => 19,  66 => 18,  62 => 17,  47 => 4,  44 => 3,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontratotemsieturma:index.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontratotemsieturma/index.html.twig");
    }
}
