<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_detalhes_vinculo.html.twig */
class __TwigTemplate_657e324c1e3e0a235d2af09d347a848f47655c7dc638bfdd91592600d6b1fa1b extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_detalhes_vinculo.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo "Vínculos Pessoa-Contrato";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Pessoal Externo</a></span></li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Vínculo Pessoa-Contrato</a></span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "
    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"modal\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h4 class=\"modal-title\">Tem certeza que deseja desfazer este vínculo</h4>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Fechar\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <div class=\"modal-body\">
                    <p id=\"conteudoModal\">Atenção: desfazer este vínculo ocasionará na remoção de todas as turmas selecionadas para exportação.</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" id=\"modalcancelar\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancelar</button>
                    <button type=\"button\" id=\"modalremover\" class=\"btn btn-success\" data-dismiss=\"modal\">Remover</button>
                </div>
            </div>
        </div>
    </div>

      <header class=\"panel-heading\">
        <div>
            <h2 class=\"panel-title\">
                <span class=\"va-middle\"><i class=\"fa fa-paste\"></i> Vínculos Pessoa-Contrato</span>
            </h2>
        </div>
    </header>
    <div class=\"panel-body\">
        <table class=\"table table-striped\">
            <tr>
                <th>Pessoa</th>
                <th>Contrato</th>
                <th>Edital</th>
                <th>Situacão do Contrato</th>
                <th>Detalhes do Vínculo</th>
                <th>Desfazer Vínculo</th>
                <th>Vincular Turmas</th>
             </tr>

            ";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["pessoaContrato"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["vinculo"]) {
            // line 50
            echo "                <tr>
                    <td id=\"";
            // line 51
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vinculo"], "idPessoa", [], "any", false, false, false, 51), "nome", [], "any", false, false, false, 51), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vinculo"], "idPessoa", [], "any", false, false, false, 51), "nome", [], "any", false, false, false, 51), "html", null, true);
            echo "</td>
                    <td>";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vinculo"], "idContrato", [], "any", false, false, false, 52), "descricao", [], "any", false, false, false, 52), "html", null, true);
            echo "</td>
                    <td>";
            // line 53
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vinculo"], "idContrato", [], "any", false, false, false, 53), "editalNumero", [], "any", false, false, false, 53), "html", null, true);
            echo "</td>
                    <td>";
            // line 54
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vinculo"], "idContratoSituacao", [], "any", false, false, false, 54), "descricao", [], "any", false, false, false, 54), "html", null, true);
            echo "</td>
                    <td><a id=\"detalhes\" class=\"auxHover\" href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntevinculo_itens_pessoa_contrato_detalhes", ["id" => twig_get_attribute($this->env, $this->source, $context["vinculo"], "id", [], "any", false, false, false, 55)]), "html", null, true);
            echo "\"><i class=\"fa fa-info fa-2x\"></i></a></td>
                    <td><i class=\"fa fa-trash fa-3x\" aria-hidden=\"true\" data-idcontrato=\"";
            // line 56
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vinculo"], "idContrato", [], "any", false, false, false, 56), "id", [], "any", false, false, false, 56), "html", null, true);
            echo "\" data-idpessoa=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vinculo"], "idPessoa", [], "any", false, false, false, 56), "id", [], "any", false, false, false, 56), "html", null, true);
            echo "\"></i></td>
                    <td>
                        ";
            // line 59
            echo "                        ";
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vinculo"], "idContratoSituacao", [], "any", false, false, false, 59), "id", [], "any", false, false, false, 59) == 2)) {
                // line 60
                echo "                            <button type=\"button\" class=\"btn btn-success btn-sm\"><a id=\"vincTurma\" href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_vincular_turmas", ["id" => twig_get_attribute($this->env, $this->source, $context["vinculo"], "id", [], "any", false, false, false, 60)]), "html", null, true);
                echo "\"> Vincular Turmas </a></button>
                        ";
            } else {
                // line 62
                echo "                            <button type=\"button\" class=\"btn btn-danger btn-sm\" ><a id=\"verSituacao\" href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntevinculo_itens_pessoa_contrato_detalhes", ["id" => twig_get_attribute($this->env, $this->source, $context["vinculo"], "id", [], "any", false, false, false, 62)]), "html", null, true);
                echo "\">Ver Situação</a></button>
                        ";
            }
            // line 64
            echo "                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['vinculo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "        </table>
        <a href=\"";
        // line 68
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_vinculos_efetuar_vinculos");
        echo "\" class=\"btn-gerar-relatorio2\">
            <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"expButton\">Criar Vínculo</button>
        </a>
    </div>

";
    }

    // line 75
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 76
        echo "
    ";
        // line 77
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>
        \$('.fa.fa-trash.fa-3x').click(function() {

            \$(\"#modal\").modal();
            const idContratoAux = \$(this).data('idcontrato');
            const idPessoaAux = \$(this).data('idpessoa');
            const element = \$(this);
            
            \$('#modalremover').click(function() {
                 removerPessoaContrato(idPessoaAux, idContratoAux, element);
            });
        });

        function removerPessoaContrato(pessoa, contrato, row) {
            \$.magnificPopup.open({
                items: [
                    {
                        src: \$('<i class=\"fa fa-spinner fa-spin fa-3x fa-fw\" ></i>'),
                        type: 'inline'
                    },
                ],
            });

            \$.ajax({
                url: \"";
        // line 102
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_remover_contrato_pessoa");
        echo "\",
                dataType: 'json',
                method: \"POST\",
                data:{
                    idPessoa: pessoa,
                    idContrato:contrato,
                },
                success: function (a) {
                    new PNotify(
                        {
                            title: \"Vínculo removido com sucesso\",
                            type: \"notice\",
                            before_open: function (PNotify) {
                                PNotify.get().css(get_center_pos(PNotify.get().width()));
                            }
                        }
                    );
                    \$.magnificPopup.close();
                    \$(row).fadeOut(\"slow\", function () {
                        \$(row).closest('tr').remove();
                    });
                }
            });
        }

        function get_center_pos(width, top){
            if (!top){
                top = 30;
                \$('.ui-pnotify').each(function() { top += \$(this).outerHeight() + 20; });
            }
            return {\"top\": top, \"left\": (\$(window).width() / 2) - (width / 2)}
        }

        \$('.fa.fa-trash').hover(function() {
            \$(this).css('cursor','pointer');
            \$(this).attr('title', 'Clique para remover o vínculo Pessoa-Contrato');
        });

        \$('.btn.btn-danger').hover(function() {
            \$(this).attr('title', 'Este contrato possui itens pendentes');
        });

        \$('.auxHover').hover(function() {
            \$(this).attr('title', 'Visualize os detalhes do vínculo');
        });

        \$('#vincTurma').hover(function() {
            \$(this).attr('title', 'Este contrato está com todos os itens ok');
        });

    </script>

";
    }

    // line 156
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 157
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>
        #expButton {
            min-width: 9em;
            max-width: 9em;
        }

        .fa-spinner {
            font-size: 5em;
            position: fixed;
            top: 50%;
            left: 50%;
        }

        .mfp-close {
            visibility: hidden;
        }

        #verSituacao {
            color:#ffffff !important;
            text-decoration:none;
        }

        #vincTurma {
            color:#ffffff !important;
            text-decoration:none;
        }

        #detalhes {
            text-decoration:none;
        }


        li span a:hover{
            text-decoration: none;
        }

        .breadcumbSemLink {
            cursor: no-drop;
        }

        .breadcumbComLink {
            cursor: pointer;
        }

        button a {
            color:#ffffff !important;
            text-decoration:none;
        }

        table th,
        table  {
           text-align: center;
           vertical-align: middle;
        }

    </style>

";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_detalhes_vinculo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  280 => 157,  277 => 156,  220 => 102,  192 => 77,  189 => 76,  186 => 75,  176 => 68,  173 => 67,  165 => 64,  159 => 62,  153 => 60,  150 => 59,  143 => 56,  139 => 55,  135 => 54,  131 => 53,  127 => 52,  121 => 51,  118 => 50,  114 => 49,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_detalhes_vinculo.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontrato/contrato_detalhes_vinculo.html.twig");
    }
}
