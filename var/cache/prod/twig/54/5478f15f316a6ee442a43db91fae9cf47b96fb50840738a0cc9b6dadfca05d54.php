<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontratoitem:item_editar.html.twig */
class __TwigTemplate_8bf578bc34badfd421b1d544ab91beff2806a3407e41330ee6f4a8f0df4f79d0 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontratoitem:item_editar.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo "Edição Item";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\">Pessoal Externo</a></span></li>
     <li><span><a href=\"";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontratoitem_mostra");
        echo "\">Item</a></span></li>
     <li><span><a href=\"#\">Edição</a></span></li>
     <li><span><a href=\"#\">";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["nteContratoItem"] ?? null), "descricao", [], "any", false, false, false, 8), "html", null, true);
        echo "</a></span></li>

 ";
    }

    // line 11
    public function block_conteudo($context, array $blocks = [])
    {
        // line 12
        echo "    <header class=\"panel-heading\">
        <div>
            <h2 class=\"panel-title\">
                <span class=\"va-middle\"><i class=\"fa fa-list\"></i> Edição de Item</span>
                <ul class=\"notifications pull-right hidden-xs contexto-oferta-menu\">
                    <li data-toggle=\"tooltip\" title=\"Retornar para lista de itens\">
                        <a href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontratoitem_mostra");
        echo "\" class=\"btn-gerar-relatorio2\">
                            <i class=\"fa fa-arrow-circle-left\"></i>
                        </a>
                    </li>
                </ul>
            </h2>
        </div>
    </header>
    <div class=\"panel-body\">
        ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
        ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
        <input type=\"submit\" value=\"Cadastrar\" class=\"btn btn-success\" id=\"idSubmit\" />
        ";
        // line 30
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
    </div>




";
    }

    // line 39
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 40
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>
        #idSubmit {
            margin-top: 1em;


        }
        .col-md-3 label {
            width: 80%;
        }
        .col-md-3 input {
            width: 20%;
        }
    </style>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontratoitem:item_editar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 40,  120 => 39,  109 => 30,  104 => 28,  100 => 27,  88 => 18,  80 => 12,  77 => 11,  70 => 8,  65 => 6,  62 => 5,  59 => 4,  54 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontratoitem:item_editar.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontratoitem/item_editar.html.twig");
    }
}
