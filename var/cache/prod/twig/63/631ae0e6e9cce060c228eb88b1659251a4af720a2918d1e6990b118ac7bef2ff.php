<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteUsuarioBundle:Usuario:lista.html.twig */
class __TwigTemplate_b995036776e8ee4dc769f621c9e2c12a781853c05aa1ea6ae39a1d73bd6c2988 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<ul>
    ";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["usuarios"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
            // line 3
            echo "        ";
            if ((twig_get_attribute($this->env, $this->source, $context["u"], "id", [], "any", false, false, false, 3) != twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 3), "id", [], "any", false, false, false, 3))) {
                // line 4
                echo "            <li onclick=\"window.location='";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_perfil", ["id" => twig_get_attribute($this->env, $this->source, $context["u"], "id", [], "any", false, false, false, 4)]), "html", null, true);
                echo "'\">
                <figure class=\"profile-picture\">
                    <img src=\"";
                // line 6
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_imagem_perfil", ["id" => twig_get_attribute($this->env, $this->source, $context["u"], "id", [], "any", false, false, false, 6)]), "html", null, true);
                echo "\"
                         alt=\"";
                // line 7
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "nome", [], "any", false, false, false, 7), "html", null, true);
                echo "\"
                         class=\"img-circle\">
                </figure>
                <div class=\"profile-info\">
                    <span class=\"name\">";
                // line 11
                echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_split_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "nome", [], "any", false, false, false, 11), " ")) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[0] ?? null) : null), "html", null, true);
                echo "</span>
                    <span class=\"title\">
                        ";
                // line 13
                if ((null === twig_get_attribute($this->env, $this->source, $context["u"], "lastLogin", [], "any", false, false, false, 13))) {
                    // line 14
                    echo "                            Nunca acessou o sistema
                        ";
                } else {
                    // line 16
                    echo "                        ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "lastLogin", [], "any", false, false, false, 16), "d/m/Y H:i:s"), "html", null, true);
                    echo "
                        ";
                }
                // line 18
                echo "                    </span>
                </div>
            </li>
        ";
            }
            // line 22
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "</ul>";
    }

    public function getTemplateName()
    {
        return "NteUsuarioBundle:Usuario:lista.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 23,  85 => 22,  79 => 18,  73 => 16,  69 => 14,  67 => 13,  62 => 11,  55 => 7,  51 => 6,  45 => 4,  42 => 3,  38 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteUsuarioBundle:Usuario:lista.html.twig", "/var/www/narfi/src/Nte/UsuarioBundle/Resources/views/Usuario/lista.html.twig");
    }
}
