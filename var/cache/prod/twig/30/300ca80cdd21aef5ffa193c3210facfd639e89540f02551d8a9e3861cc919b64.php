<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoBuscadorBundle:Info:empty.html.twig */
class __TwigTemplate_7baaf584a51d3ab266770a0812461347d6a80af2ef01d7b0a3b2c5f89870b71f extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"panel-body\">
    <div id='info-msg' class=\"text-center\">
        <br>
        <i class=\"fa fa-5x fa-search-minus\"></i><br>
        <h2>Não foi possível encontrar esta pessoa.</h2><br>
        =(
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoBuscadorBundle:Info:empty.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoBuscadorBundle:Info:empty.html.twig", "/var/www/narfi/src/Nte/Aplicacao/BuscadorBundle/Resources/views/Info/empty.html.twig");
    }
}
