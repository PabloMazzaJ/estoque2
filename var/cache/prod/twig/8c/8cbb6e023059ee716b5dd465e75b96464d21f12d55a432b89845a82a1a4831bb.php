<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:exportacao:index.html.twig */
class __TwigTemplate_fc97f23fcff5ff7d7ae7cec9ae7a8c15f37671fcad9a1dfda8cb651d3249bafb extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"panel\">
    <div class=\"panel-heading\">
        <h2 class=\"panel-title\">Fila de exportação</h2>
    </div>
    <div class=\"panel-body\" id=\"relatorio\">
        <table class=\"table table-responsive table-striped \">
            ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["middlewareExportacaos"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["middlewareExportacao"]) {
            // line 8
            echo "                <tr>
                    <td>
                        <b>";
            // line 10
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "ano", [], "any", false, false, false, 10), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "idPeriodo", [], "any", false, false, false, 10), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "nomeCurso", [], "any", false, false, false, 10), "html", null, true);
            echo "</b><br>
                        <small>
                            ";
            // line 12
            if (twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "periodico", [], "any", false, false, false, 12)) {
                // line 13
                echo "                                <span class=\"label label-primary\">Exportação Periódica</span>
                            ";
            } else {
                // line 15
                echo "                                <span class=\"label label-danger\">Exportação Única</span>
                            ";
            }
            // line 16
            echo " -
                            criado por ";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "idUsurio", [], "any", false, false, false, 17), "username", [], "any", false, false, false, 17), "html", null, true);
            echo " em
                            ";
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "registroDataCriacao", [], "any", false, false, false, 18), "d/m/Y"), "html", null, true);
            echo "
                            às
                            ";
            // line 20
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "registroDataCriacao", [], "any", false, false, false, 20), "H:i:s"), "html", null, true);
            echo "
                        </small>
                    </td>
                    <td class=\"text-right\">
                        <b>";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "descricaoStatus", [], "any", false, false, false, 24), "html", null, true);
            echo "</b><br>
                        ";
            // line 25
            if (twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "registroDataAtualizacao", [], "any", false, false, false, 25)) {
                // line 26
                echo "                            <small>
                                ";
                // line 27
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "registroDataAtualizacao", [], "any", false, false, false, 27), "d/m/Y"), "html", null, true);
                echo "
                                às ";
                // line 28
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "registroDataAtualizacao", [], "any", false, false, false, 28), "H:i:s"), "html", null, true);
                echo "
                            </small>
                        ";
            }
            // line 31
            echo "                    </td>
                    <td width=\"10\"  class=\"text-right\">
                        ";
            // line 33
            if ((twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "periodico", [], "any", false, false, false, 33) == 1)) {
                // line 34
                echo "                        <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportacao_edit_periodico", ["id" => twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "id", [], "any", false, false, false, 34)]), "html", null, true);
                echo "\" class=\"btn btn-default btn-sm\">
                            <i class=\"fa fa-toggle-on\"></i>Desativar
                        </a>
                        ";
            } else {
                // line 38
                echo "                        <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportacao_edit_periodico", ["id" => twig_get_attribute($this->env, $this->source, $context["middlewareExportacao"], "id", [], "any", false, false, false, 38)]), "html", null, true);
                echo "\" class=\"btn btn-default btn-sm\">
                            <i class=\"fa fa-toggle-off\"></i>Ativar
                        </a>
                        ";
            }
            // line 42
            echo "
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['middlewareExportacao'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "        </table>
    </div>
</div>
<script>
    updateDomJS();
</script>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:exportacao:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 46,  130 => 42,  122 => 38,  114 => 34,  112 => 33,  108 => 31,  102 => 28,  98 => 27,  95 => 26,  93 => 25,  89 => 24,  82 => 20,  77 => 18,  73 => 17,  70 => 16,  66 => 15,  62 => 13,  60 => 12,  51 => 10,  47 => 8,  43 => 7,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:exportacao:index.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/exportacao/index.html.twig");
    }
}
