<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_detalhes.html.twig */
class __TwigTemplate_8bc50bdd2e4ea52d68b77802dd9fa6ee780faca8afc44e7ede00efe64848d56d extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_detalhes.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Detalhes Contrato ";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Pessoal Externo</a></span></li>
     <li><span><a href=\"";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_index");
        echo "\" class=\"breadcumbComLink\">Contrato</a></span></li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Detalhes</a></span></li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "descricao", [], "any", false, false, false, 8), "html", null, true);
        echo "</a></span></li>
 ";
    }

    // line 10
    public function block_conteudo($context, array $blocks = [])
    {
        // line 11
        echo "    <header class=\"panel-heading\">
        <div>
            <h2 class=\"panel-title\">
                <span class=\"va-middle\"><i class=\"fa fa-paste\"></i> Detalhes do Contrato</span>
            </h2>
        </div>
    </header>
    <div class=\"panel-body flex-class\">
        <div id=\"table-left\">
            <table class=\"table table-striped\" >
                <tr>
                    <th>Contrato</th>
                    <th>N.º Edital</th>
                    <th>Papel</th>
                </tr>

                <tr>
                    <td class=\"align-middle\"> ";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "descricao", [], "any", false, false, false, 28), "html", null, true);
        echo "</td>
                    <td class=\"align-middle\"> ";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "editalNumero", [], "any", false, false, false, 29), "html", null, true);
        echo "</td>
                    <td class=\"align-middle\">";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "idPapel", [], "any", false, false, false, 30), "html", null, true);
        echo "</td>
                </tr>
            </table>
        </div>

        <div id=\"table-middle\">
            <table class=\"table table-striped\">
                <tr>
                    <th>Polos</th>
                </tr>
                ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "idCurso", [], "any", false, false, false, 40));
        foreach ($context['_seq'] as $context["_key"] => $context["curso"]) {
            // line 41
            echo "                    <tr>
                        <td class=\"align-middle\">   ";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["curso"], "nome", [], "any", false, false, false, 42), "html", null, true);
            echo "</td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['curso'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "            </table>
        </div>

        <div id=\"table-right\">
            <table class=\"table table-striped\">
                <tr>
                    <th>Item</th>
                    <th>Obrigatório</th>
                </tr>
                ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["itens"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 55
            echo "                    <tr>
                        <td class=\"align-middle\">
                            ";
            // line 57
            echo twig_escape_filter($this->env, $context["item"], "html", null, true);
            echo "
                        </td>
                        <td class=\"align-middle\">
                            ";
            // line 60
            if (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["obrigatorios"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 60) - 1)] ?? null) : null) == false)) {
                // line 61
                echo "                                Não
                            ";
            } else {
                // line 63
                echo "                                Sim
                            ";
            }
            // line 65
            echo "                        </td>
                    </tr>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "            </table>
        </div>

        <div id=\"table-actions\">
            <table class=\"table table-striped\">
                <tr>
                    <th>Editar</th>
                    <th>Deletar</th>
                </tr>
                <tr>
                    <td class=\"align-middle\">
                      <a href=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_edit", ["id" => twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "id", [], "any", false, false, false, 79)]), "html", null, true);
        echo "\"><i class=\"fa fa-2x fa-edit\" aria-hidden=\"true\"></i></a>
                    </td>
                    <td class=\"align-middle\">
                        <a href=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_deletar_contrato", ["id" => twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "id", [], "any", false, false, false, 82)]), "html", null, true);
        echo "\"><i class=\"fa fa-2x fa-trash\" aria-hidden=\"true\"></i></a>
                    </td>
                </tr>
            </table>
        </div>

    </div>

    <a href=\"";
        // line 90
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_index");
        echo "\" class=\"btn-gerar-relatorio2\">
        <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"expButton\"><i class=\"fa fa-arrow-circle-left\"></i></button>
    </a>
";
    }

    // line 95
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 96
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>
        \$('#editIcon').hover(function() {
            \$(this).attr('title', 'Edite este contrato');
        });
    </script>
";
    }

    // line 105
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 106
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>

        .flex-class {
            display: flex;
        }

        #table-left {
            flex-grow: 1;
        }


        .btn {
            min-width: 9em;
            max-width: 9em;
        }

        #verSitu {
            color:#ffffff !important;
            text-decoration:none;
        }

        table th
        {
            text-align: center;
            vertical-align: middle;
        }

        .align-middle {
            text-align: center;
            vertical-align: middle;
        }


        li span a:hover{
            text-decoration: none;
        }

        .breadcumbSemLink {
            cursor: no-drop;
        }

        .breadcumbComLink {
            cursor: pointer;
        }

    </style>

";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_detalhes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  256 => 106,  253 => 105,  241 => 96,  238 => 95,  230 => 90,  219 => 82,  213 => 79,  200 => 68,  184 => 65,  180 => 63,  176 => 61,  174 => 60,  168 => 57,  164 => 55,  147 => 54,  136 => 45,  127 => 42,  124 => 41,  120 => 40,  107 => 30,  103 => 29,  99 => 28,  80 => 11,  77 => 10,  71 => 8,  66 => 6,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_detalhes.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontrato/contrato_detalhes.html.twig");
    }
}
