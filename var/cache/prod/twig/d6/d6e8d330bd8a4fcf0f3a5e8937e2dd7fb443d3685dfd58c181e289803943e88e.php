<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteUsuarioBundle:Usuario:agenda.html.twig */
class __TwigTemplate_0250d2a28198c3abdb71bad8329fd27e68854046d1f4125ec73f2906c8875add extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
    }

    public function getTemplateName()
    {
        return "NteUsuarioBundle:Usuario:agenda.html.twig";
    }

    public function getDebugInfo()
    {
        return array ();
    }

    public function getSourceContext()
    {
        return new Source("", "NteUsuarioBundle:Usuario:agenda.html.twig", "/var/www/narfi/src/Nte/UsuarioBundle/Resources/views/Usuario/agenda.html.twig");
    }
}
