<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteUsuarioBundle:Usuario:Usuario.html_1.twig */
class __TwigTemplate_dc189fd923b1da7f07d0d6c0bc399cf3d382a97ed00c9303156ff1607d61dc40 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'menuLateral' => [$this, 'block_menuLateral'],
            'conteudo' => [$this, 'block_conteudo'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("::menu.html.twig", "NteUsuarioBundle:Usuario:Usuario.html_1.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_menuLateral($context, array $blocks = [])
    {
    }

    // line 5
    public function block_conteudo($context, array $blocks = [])
    {
        // line 6
        echo "    <div class=\"col-lg-12\">
        <div id=\"example\">
            <div id=\"grid\"></div>
            <div id=\"definirSenha\"></div>
            
        </div>
    </div>
";
    }

    // line 15
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 16
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/kendo/styles/kendo.common.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/kendo/styles/kendo.blueopal.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
";
    }

    // line 20
    public function block_javascripts($context, array $blocks = [])
    {
        // line 21
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

    <script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/kendo/js/kendo.all.min.js"), "html", null, true);
        echo "\" ></script>
    <script src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/kendo/js/cultures/kendo.culture.pt-BR.min.js"), "html", null, true);
        echo "\" ></script>


    <script type=\"text/x-kendo-template\" id=\"template\">
        <div class=\"toolbar\">
        <label class=\"category-label\" for=\"category\">Filtrar Usuários por Grupo</label>
        <input type=\"search\" id=\"category\" style=\"width: 150px\"/>
        </div>
    </script>
    <script id=\"popup-editor\" type=\"text/x-kendo-template\">    </script>
    <script type=\"text/x-kendo-template\" id=\"template_senha\">
        <div id=\"details-container\">
            <form id=\"definir-senha-formulario\" name=\"definir-senha-formulario\"  method=\"get\">
                    <label for=\"password\" >Password</label> <input id=\"password\" name=\"password\" type=\"password\"   />
                        <span class=\"k-invalid-msg\" data-for=\"password\"></span><br />
                    <label for=\"password-confirm\" >Confirm Password</label>
                    <input id=\"password-confirm\" name=\"password-confirm\" type=\"password\" />
                        <span class=\"k-invalid-msg\" data-for=\"password-confirm\"></span><br />
                    <input id=\"register-submit\" name=\"register-submit\" type=\"submit\" value=\"Register\" />
            </form>
        </div>
    </script>


    <script >
var wnd, templateSenha;

function definirNovaSenha(e) {
    e.preventDefault();
    var dataItem = this.dataItem(\$(e.currentTarget).closest(\"tr\"));
    wnd.content(templateSenha(dataItem));
    wnd.center().open();
}
/*
function rolesDisplay(data) {
    var res = [];
    \$.each(data.Cities, function (idx, elem) {
        res.push(grupos.get(elem).text);
    });
    return res.join(\", \");
}
*/
var grupos = new kendo.data.DataSource({
    
        data:[
            {id: \"ROLE_ADMIN\",\"text\": \"Super Administrador\"}, 
            {id: \"ROLE_AGENDA_ADMIN\",\"text\": \"Agenda admin\"}, 
            {id: \"ROLE_AGENDA_USER\",  \"text\": \"Agenda usuario\"}, 
            {id: \"ROLE_AGENDA\",  \"text\": \"Agenda visão\"}, 
            {id: \"ROLE_CIP\",  \"text\": \"CIP\"}, 
            {id: \"ROLE_CAD\",\"text\": \"CAD\"}, 
            {id: \"ROLE_FIN\",\"text\": \"Financeiro\"}, 
            {id: \"ROLE_ESTOQUE\",\"text\": \"Estoque\"}, 
            {id: \"ROLE_TECNICO\",\"text\": \"Tecnico\"}, 
            {id: \"ROLE_ADMINISTRATIVO\",\"text\": \"Administrativo\"},
            {id: \"ROLE_SUPER_ADMIN\",\"text\": \"root\"}
        ],
    schema: {
        model: {
            id    :\"id\",
            fields: {
                text: {type: \"string\"}
            }
        }
    }
});

wnd = \$(\"#definirSenha\").kendoWindow({
    title: \"Definir nova senha\",
    modal: true,
    visible: false,
    resizable: false,
    width: 400
}).data(\"kendoWindow\");

templateSenha = kendo.template(\$(\"#template_senha\").html());
function rolesEditor(container, options) {
   \$(\"<select multiple='multiple' data-bind='value : grupos'/>\").appendTo(container).kendoMultiSelect({
        dataTextField : \"text\",
        dataValueField: \"id\",
        dataSource    : grupos
    });
}

function exibeCor(container, options) {
    

    if(!(options.model['cor']))options.model['cor']=\"#ffffff\";
    \$(\"<input type='color' data-bind='value:\" + options.field + \"' />\")
                .appendTo(container)
                .kendoColorPicker({
                buttons: true

            });
         
        };

function rolesDisplay(data) {
  var res = [];
    \$.each(data.roles, function (idx, elem) {
        grupos.fetch(function() {  res.push(grupos.get(elem).text);});
   
    });
        
    return res.join(\", \");  
}




\$(document).ready(function () {
    
    \$(\"#definir-senha-formulario\").kendoValidator({
              rules: {
                  verifyPasswords: function(input){
                     var ret = true;
                             if (input.is(\"[name=password-confirm]\")) {
                                 ret = input.val() === \$(\"#password\").val();
                             }
                             return ret;
                  }
              },
              messages: {
                  verifyPasswords: \"A senha não confere!!\"
              }
          });

    var dataSource = new kendo.data.DataSource({
   
         transport: {
         read: {
         url: \"";
        // line 155
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_2_get_usuarios");
        echo "\",
         dataType: \"json\",
         contentType: \"application/json; charset=utf-8\"
         },
         update: {
         url: \"";
        // line 160
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_2_post_usuario");
        echo "\",
         type: \"POST\",
         dataType: \"json\"
         },
         create: {
         url: \"";
        // line 165
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_2_post_usuario");
        echo "\",
         type: \"POST\",
         dataType: \"json\"
         },
         destroy: {
         url: \"";
        // line 170
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_2_delete_usuario");
        echo "\",
         type: \"DELETE\",
         dataType: \"json\"
         },
         parameterMap: function(options, operation) {
         if (operation !== \"read\" && options.models) {
         return {models: kendo.stringify(options.models)};
         }
         }
         },
        batch: true,
        pageSize: 20,
        schema: {
            model: {
                id: \"id\",
                fields: {
                    id: {
                        field: \"id\",
                        type: \"number\",
                        editable: false
                    },
                    nome: {
                        validation: {
                            required: true
                        }
                    },
                    username: {
                        validation: {
                            required: true,
                            min: 1
                        }
                    },
                    roles: {validation: {
                            required: true
                        }},
                    cor:{type:\"string\" },
                    email:{type:\"string\" },
                    last_login:{type:\"string\", editable: false },
                
                }
            }
        }
    });
    \$(\"#grid\").kendoGrid({
        dataSource: dataSource,
        sortable: {
            mode: \"single\",
            allowUnsort: false
        },
        pageable: true,
        resizable: true,
        columnMenu: true,
        height: 550,
        toolbar: [{
            name: \"create\",
            text: \"Adicionar Novo\"
        }],
        filterable: {
            extra: false,
            messages: {
                info: \"Filtrar por: \",
                selectValue: \"Selecione um grupo\",
                filter: \"Aplicar\",
                clear: \"Limpar\"
            },
            operators: {
                string: {
                    startswith: \"Iniciando com\",
                    eq: \"Equivalante\",
                    neq: \"Não é equivalente\"
                }
            }
        },
        columns: [
        {field: \"id\",title: \"ID\",width: \"56px\", hidden:true}, 
        {field: \"nome\",  title: \"Nome\"        }, 
        {field: \"username\",title: \"Usuário\"},
        {field: \"roles\",title: \"Grupos\",template: rolesDisplay,editor: rolesEditor }, 
        {field: \"email\",title: \"E-Mail\", hidden: true},
        {field: \"cor\",title: \"Cor de exibição\", hidden: true,editor: exibeCor, template: \"<span style='display: inline-block; width: 100%; height: 100%; background-color: #= cor #'></span>\" },
       {field: \"senha\",title: \"Senha\", hidden: true},
     
{
            command: [{
                name: \"edit\",
                text: \"Editar\"
            }, {
                name: \"destroy\",
                text: \"Remover\"
            }],
            title: \"Opções\",
            width: \"200px\"
        }],
        editable: {
            mode: \"popup\",
            // template: kendo.template(\$(\"#popup-editor\").html())
        },
        columnMenu: {
            messages: {
                sortAscending: \"Ordenar (a-Z)\",
                sortDescending: \"Ordenar (Z-a)\",
                filter: \"Aplicar Filtro\",
                columns: \"Escolha as colunas\",
            }
        },
        pageable: {
            messages: {
                display: \"{0} - {1} de {2} Usuários\", //{0} is the index of the first record on the page, {1} - index of the last record on the page, {2} is the total amount of records
                empty: \"Não existe nenhum usuário cadastrado\",
                page: \"Pagina\",
                of: \"de {0}\", //{0} is total amount of pages
                itemsPerPage: \"Itens por página\",
                first: \"Primeira página\",
                previous: \"Última Página\",
                next: \"Próxima\",
                last: \"Anterior\",
                refresh: \"Atualizar\"
            }
        }
    });
    /*
    var dropDown = grid.find(\"#category\").kendoDropDownList({
        dataTextField: \"CategoryName\",
        dataValueField: \"CategoryID\",
        autoBind: false,
        optionLabel: \"All\",
        dataSource: {
            type: \"odata\",
            severFiltering: true,
            transport: {
                read: \"http://demos.telerik.com/kendo-ui/service/Northwind.svc/Categories\"
            }
        },
        change: function () {
            var value = this.value();
            if (value) {
                grid.data(\"kendoGrid\").dataSource.filter({
                    field: \"CategoryID\",
                    operator: \"eq\",
                    value: parseInt(value)
                });
            } else {
                grid.data(\"kendoGrid\").dataSource.filter({});
            }
        }
    });
    */
});

    </script>";
    }

    public function getTemplateName()
    {
        return "NteUsuarioBundle:Usuario:Usuario.html_1.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 170,  246 => 165,  238 => 160,  230 => 155,  96 => 24,  92 => 23,  86 => 21,  83 => 20,  77 => 18,  73 => 17,  69 => 16,  66 => 15,  55 => 6,  52 => 5,  47 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteUsuarioBundle:Usuario:Usuario.html_1.twig", "/var/www/narfi/src/Nte/UsuarioBundle/Resources/views/Usuario/Usuario.html_1.twig");
    }
}
