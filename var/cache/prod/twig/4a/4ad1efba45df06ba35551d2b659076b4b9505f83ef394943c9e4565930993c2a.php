<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :js:_form_validador.js.twig */
class __TwigTemplate_f6f623dc7d894b9e6fc9d287de7335a4ede79158fde9c677db100a3498529cf5 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if ( !(isset($context["tipo"]) || array_key_exists("tipo", $context))) {
            // line 2
            $context["tipo"] = 1;
        }
        // line 4
        echo "
(function (\$) {
    'use strict';
    \$(\"form[name='";
        // line 7
        echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 7)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["id"] ?? null) : null), "js", null, true);
        echo "']\").validate({
        submitHandler: function (form) {
            var \$form = \$(form);
            var \$submitButton = \$(this.submitButton);
            console.log('teste.html.twig')
            \$.ajax({
                type: 'POST',
                url: \$form.attr('action'),
                data: \$form.serialize(),
                dataType: 'json',
                complete: function (data) {
                    if (typeof data.responseJSON === 'object') {
                        if (data.responseJSON.response == 'success') {
                            ";
        // line 20
        if ((($context["tipo"] ?? null) == 1)) {
            // line 21
            echo "                            \$.magnificPopup.close();
                            ";
        } else {
            // line 23
            echo "                            principal();
                            ";
        }
        // line 25
        echo "                            \$form.find('.form-control')
                                .val('')
                                .blur()
                                .parent()
                                .removeClass('has-success')
                                .removeClass('has-error')
                                .find('label.error')
                                .remove();

                        }
                        new PNotify({
                            title: data.responseJSON.title,
                            text: data.responseJSON.text,
                            type: data.responseJSON.response
                        });
                    }
                    ";
        // line 41
        if ((($context["tipo"] ?? null) == 1)) {
            // line 42
            echo "                    \$.magnificPopup.close();
                    if (\$('#grid').length) {
                        \$('#grid').data('kendoGrid').dataSource.read();
                    }
                    ";
        } else {
            // line 47
            echo "                    principal();
                    ";
        }
        // line 49
        echo "                }
            });
            return false;
        }
    });

    \$(\"form[name='form']\").validate({
        submitHandler: function (form) {
            var \$form = \$(form);
            \$.ajax({
                type: 'POST',
                url: \$form.attr('action'),
                data: \$form.serialize(),
                dataType: 'json',
                complete: function (data) {
                    if (typeof data.responseJSON === 'object') {
                        if (data.responseJSON.response == 'success') {
                            ";
        // line 66
        if ((($context["tipo"] ?? null) == 1)) {
            // line 67
            echo "                            \$.magnificPopup.close();
                            ";
        } else {
            // line 69
            echo "                            principal();
                            ";
        }
        // line 71
        echo "                            \$form.find('.form-control')
                                .val('')
                                .blur()
                                .parent()
                                .removeClass('has-success')
                                .removeClass('has-error')
                                .find('label.error')
                                .remove();
                        }

                        new PNotify({
                            title: data.responseJSON.title,
                            text: data.responseJSON.text,
                            type: data.responseJSON.response
                        });
                        return;
                    }
                    ";
        // line 88
        if ((($context["tipo"] ?? null) == 1)) {
            // line 89
            echo "                    \$.magnificPopup.close();
                    if (\$('#grid').length) {
                        \$('#grid').data('kendoGrid').dataSource.read();
                    }
                    ";
        } else {
            // line 94
            echo "                    principal();
                    ";
        }
        // line 96
        echo "                }


            });
            return false;
        }
    });
}).apply(this, [jQuery]);
";
    }

    public function getTemplateName()
    {
        return ":js:_form_validador.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 96,  159 => 94,  152 => 89,  150 => 88,  131 => 71,  127 => 69,  123 => 67,  121 => 66,  102 => 49,  98 => 47,  91 => 42,  89 => 41,  71 => 25,  67 => 23,  63 => 21,  61 => 20,  45 => 7,  40 => 4,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":js:_form_validador.js.twig", "/var/www/narfi/app/Resources/views/js/_form_validador.js.twig");
    }
}
