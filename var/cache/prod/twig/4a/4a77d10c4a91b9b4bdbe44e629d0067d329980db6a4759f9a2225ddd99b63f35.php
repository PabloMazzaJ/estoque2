<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_detalhes_itens_vinculo.html.twig */
class __TwigTemplate_338478162d98ba74e86b947fbba32a65bfe30e1c23a1af73be6f37c2b1c62e65 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_detalhes_itens_vinculo.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Detalhes Contrato ";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Detalhes do Vínculo Pessoa-Contrato";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Pessoal Externo</a></span></li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Vínculo Pessoa-Contrato</a></span></li>
     <li><span><a href=\"";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntevinculo_pessoa_contrato");
        echo "\">Detalhes Vínculo</a></span></li>
 ";
    }

    // line 9
    public function block_conteudo($context, array $blocks = [])
    {
        // line 10
        echo "    <div class=\"modal fade\" id=\"modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Selecione a situação do contrato</h4>
                </div>
                <div class=\"modal-body\">
                    <div class=\"funkyradio\">
                        <div class=\"funkyradio-success\">
                            <input type=\"radio\" name=\"radio\" id=\"radio-contrato-aceito\" />
                            <label for=\"radio-contrato-aceito\">Contrato Aceito</label>
                        </div>
                        <div class=\"funkyradio-warning\">
                            <input type=\"radio\" name=\"radio\" id=\"radio-contrato-analise\" />
                            <label for=\"radio-contrato-analise\">Contrato Sob Análise</label>
                        </div>
                        <div class=\"funkyradio-danger\">
                            <input type=\"radio\" name=\"radio\" id=\"radio-contrato-rejeitado\" />
                            <label for=\"radio-contrato-rejeitado\">Contrato Rejeitado</label>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <button type=\"button\" class=\"btn btn-primary\" id=\"modal-confirmar\">Salvar</button>
                </div>
            </div>
        </div>
    </div>

    <header class=\"panel-heading\">
        <h2 class=\"panel-title\">
            <span class=\"va-middle\"><i class=\"fa fa-paste\"></i> Vínculo Pessoa-Contrato</span>
        </h2>
    </header>
    <div class=\"panel-body\">
        <table class=\"table table-striped\" id=\"pessoaContratoTable\" data-pessoacontrato=\"";
        // line 47
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "id", [], "any", false, false, false, 47), "html", null, true);
        echo "\">
            <tr>
                <td class=\"\">Nome: ";
        // line 49
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "idpessoa", [], "any", false, false, false, 49), "nome", [], "any", false, false, false, 49), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "idPessoa", [], "any", false, false, false, 49), "sobrenome", [], "any", false, false, false, 49), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <td class=\"\">E-mail: ";
        // line 52
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "idPessoa", [], "any", false, false, false, 52), "email", [], "any", false, false, false, 52), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <td class=\"\">Cpf: ";
        // line 55
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "idPessoa", [], "any", false, false, false, 55), "cpf", [], "any", false, false, false, 55), "html", null, true);
        echo "
                </td>
            </tr>
            <tr>
                <td class=\"\">Contrato: ";
        // line 59
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "idContrato", [], "any", false, false, false, 59), "descricao", [], "any", false, false, false, 59), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <td class=\"\">Edital: ";
        // line 62
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "idContrato", [], "any", false, false, false, 62), "editalNumero", [], "any", false, false, false, 62), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <td class=\"\">Papel: ";
        // line 65
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "idContrato", [], "any", false, false, false, 65), "idPapel", [], "any", false, false, false, 65), "html", null, true);
        echo "</td>
            </tr>
        </table>
        ";
        // line 68
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 69
            echo "            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_edit", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "idPessoa", [], "any", false, false, false, 69), "id", [], "any", false, false, false, 69)]), "html", null, true);
            echo "\" class=\"btn-gerar-relatorio2\">
                <button type=\"button\"  class=\"btn btn-primary btn-sm\"><a id=\"expButton\" href=\"";
            // line 70
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_edit", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "idPessoa", [], "any", false, false, false, 70), "id", [], "any", false, false, false, 70)]), "html", null, true);
            echo "\">Editar Pessoa</a></button>
            </a>

            <a href=\"";
            // line 73
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_edit", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "idContrato", [], "any", false, false, false, 73), "id", [], "any", false, false, false, 73)]), "html", null, true);
            echo "\" class=\"btn-gerar-relatorio2\">
                <button type=\"button\"  class=\"btn btn-primary btn-sm\"><a id=\"expButton\" href=\"";
            // line 74
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_edit", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "idContrato", [], "any", false, false, false, 74), "id", [], "any", false, false, false, 74)]), "html", null, true);
            echo "\">Editar Contrato</a></button>
            </a>
        ";
        }
        // line 77
        echo "
    </div>

    <header class=\"panel-heading\">
        <h2 class=\"panel-title\">
            <span class=\"va-middle\"><i class=\"fa fa-paste\"></i> Itens </span>
        </h2>
    </header>

    <div class=\"panel-body\">
        <table class=\"table table-striped\">
            <tr>
                <td class=\"col-md-2 font-weight-bold\">Item</td>
                <td class=\"col-md-1 font-weight-bold\">Obrigatório</td>
                <td class=\"font-weight-bold\">Situação</td>
                <td class=\"col-md-6 font-weight-bold\">Observações</td>
                <td class=\"font-weight-bold\">Data Update </td>
            </tr>
            ";
        // line 95
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["pessoaContratoRequisito"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 96
            echo "            <tr>
                <td>";
            // line 97
            echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["arrayItens"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 97) - 1)] ?? null) : null), "html", null, true);
            echo "</td>
                <td>
                    ";
            // line 99
            if (twig_test_empty(($context["arrayObrigatorio"] ?? null))) {
                // line 100
                echo "                        <p></p>
                    ";
            } else {
                // line 102
                echo "                        ";
                if (((($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["arrayObrigatorio"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 102) - 1)] ?? null) : null) == false)) {
                    // line 103
                    echo "                            <p>Não</p>
                        ";
                } else {
                    // line 105
                    echo "                            <p>Sim</p>
                        ";
                }
                // line 107
                echo "                    ";
            }
            // line 108
            echo "
                </td>
                <td>";
            // line 110
            echo twig_escape_filter($this->env, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["arraySituacao"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 110) - 1)] ?? null) : null), "html", null, true);
            echo "</td>
                <td>";
            // line 111
            echo twig_escape_filter($this->env, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["arrayObservacao"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 111) - 1)] ?? null) : null), "html", null, true);
            echo "</td>
                <td>";
            // line 112
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = ($context["arrayDataAtualizacao"] ?? null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4[(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 112) - 1)] ?? null) : null), "d-m-Y H:i:s"), "html", null, true);
            echo "</td>
            </tr>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 115
        echo "        </table>
        ";
        // line 116
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_GERENCIA_ITENS")) {
            // line 117
            echo "            <a id=\"expButton\" href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_situacao_itens", ["id" => twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "id", [], "any", false, false, false, 117)]), "html", null, true);
            echo "\">
                <button type=\"button\"  class=\"btn btn-primary btn-sm\">Editar Itens</button>
            </a>
        ";
        }
        // line 121
        echo "    </div>

    ";
        // line 124
        echo "
    ";
        // line 125
        if ((($context["turmasExistem"] ?? null) == 1)) {
            // line 126
            echo "
    <header class=\"panel-heading\">
        <h2 class=\"panel-title\">
            <span class=\"va-middle\"><i class=\"fa fa-paste\"></i> Situação Turmas Exportação</span>
        </h2>
    </header>
    <div class=\"panel-body\">
        <table class=\"table table-striped\" id=\"pessoaContratoTable\" data-pessoacontrato=\"";
            // line 133
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "id", [], "any", false, false, false, 133), "html", null, true);
            echo "\">
            <tr>
                <td>Turma</td>
                <td>Valido</td>
            </tr>

            ";
            // line 139
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["retornoNotificacao"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["turma"]) {
                // line 140
                echo "            <tr>
                <td>";
                // line 141
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["turma"], "nome", [], "any", false, false, false, 141), "html", null, true);
                echo "</td>
                <td>
                    ";
                // line 143
                if ((twig_get_attribute($this->env, $this->source, $context["turma"], "valido", [], "any", false, false, false, 143) == 1)) {
                    // line 144
                    echo "                        Sim
                    ";
                } else {
                    // line 146
                    echo "                        Não
                    ";
                }
                // line 148
                echo "                </td>
            </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['turma'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 151
            echo "        </table>

        ";
            // line 153
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 154
                echo "            <a id=\"expButton\" href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_validar_turmas_exportacao", ["id" => twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "id", [], "any", false, false, false, 154)]), "html", null, true);
                echo "\">
                <button type=\"button\" class=\"btn btn-primary btn-sm\">Validar Turmas</button>
            </a>
        ";
            }
            // line 158
            echo "    </div>

    ";
        }
        // line 161
        echo "    ";
        // line 162
        echo "
    <header class=\"panel-heading\">
        <h2 class=\"panel-title\">
            <span class=\"va-middle\"><i class=\"fa fa-paste\"></i> Situação do Contrato </span>
        </h2>
    </header>
    <div class=\"panel-body\">
        <table class=\"table table-striped\">
            <tr>
                <th class=\"font-weight-bold centerText th-same-size\" id=\"th-contrato-rejeitado\">Contrato Rejeitado</th>
                <th class=\"font-weight-bold centerText th-same-size\" id=\"th-contrato-sob-analise\">Contrato Sob Análise</th>
                <th class=\"font-weight-bold centerText th-same-size\" id=\"th-contrato-aceito\">Contrato Aceito</th>
            </tr>
            <tr>
                <td class=\"centerText\" id=\"td-contrato-rejeitado\"><i class=\"fa fa-times\"></i></td>
                <td class=\"centerText\" id=\"td-contrato-sob-analise\"><i class=\"fa fa-exclamation\"></i></td>
                <td class=\"centerText\" id=\"td-contrato-aceito\"><i class=\"fa fa-check\"></i></td>
            </tr>
        </table>

        <div class=\"progress progress-striped light active m-md\">
            <div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuemin=\"0\" aria-valuemax=\"100\" >
                Progresso ";
        // line 184
        echo twig_escape_filter($this->env, ($context["valorMedia"] ?? null), "html", null, true);
        echo "%
            </div>
        </div>
        <a href=\"";
        // line 187
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntevinculo_pessoa_contrato");
        echo "\" class=\"btn-gerar-relatorio2\">
            <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"expButton\"><i class=\"fa fa-arrow-circle-left fa-sm\"></i></button>
        </a>
        ";
        // line 190
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_GERENCIA_ITENS")) {
            // line 191
            echo "                    ";
            // line 192
            echo "
        ";
            // line 193
            if ((($context["valorMedia"] ?? null) == 100)) {
                // line 194
                echo "            <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_vincular_turmas", ["id" => twig_get_attribute($this->env, $this->source, ($context["pessoaContrato"] ?? null), "id", [], "any", false, false, false, 194)]), "html", null, true);
                echo "\">
                <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"btnExp\">Exportar</button>
            </a>

        ";
            }
            // line 199
            echo "            ";
            // line 200
            echo "                ";
            // line 201
            echo "            ";
            // line 202
            echo "        ";
        }
        // line 203
        echo "    </div>

";
    }

    // line 207
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 208
        echo "
    ";
        // line 209
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>
        \$(function() {
            verificaSituacaoContrato();
            \$('.progress-bar.progress-bar-success').css('width', ";
        // line 213
        echo twig_escape_filter($this->env, ($context["valorMedia"] ?? null), "html", null, true);
        echo "+'%').attr('aria-valuenow', ";
        echo twig_escape_filter($this->env, ($context["valorMedia"] ?? null), "html", null, true);
        echo ");

            document.getElementById('btn-situacao-modal').addEventListener('click', function () {
                \$(\"#modal\").modal();
                verificaSituacaoContrato();
            });
        });

        function verificaSituacaoContrato()
        {
            \$.ajax({
                url: \"";
        // line 224
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_verifica_contrato_traffic_lights");
        echo "\",
                dataType: 'json',
                method: \"POST\",
                data:{
                    ntePessoaContratoKey: \$('#pessoaContratoTable').data('pessoacontrato')
                },
                success: function (data) {
                    let opcoes = data;
                    console.log('ntePessoaContrato id: ' + \$('#pessoaContratoTable').data('pessoacontrato'));
                    for (let i = 0; i < opcoes.length; i++) {
                        let object = opcoes[i];
                        for (let propriedade in object) {
                            if (propriedade === 'id')
                            {
                                if (object[propriedade] == 1) {
                                    console.log('Contrato Sob Análise');
                                    \$('#td-contrato-sob-analise').addClass('contratoSobAnaliseBackground');
                                    \$('.fa.fa-check').hide();
                                    \$('.fa.fa-times').hide();
                                    \$('.fa.fa-exclamation').show();
                                    \$( \"#radio-contrato-analise\" ).prop( \"checked\", true );

                                } else if (object[propriedade] == 2) {
                                    console.log('Contrato Aceito');
                                    \$('#td-contrato-aceito').addClass('contratoAceitoBackground');
                                    \$('.fa.fa-exclamation').hide();
                                    \$('.fa.fa-times').hide();
                                    \$('.fa.fa-check').show();
                                    \$( \"#radio-contrato-aceito\" ).prop( \"checked\", true );

                                } else if (object[propriedade] == 3) {
                                    console.log('Contrato Rejeitado');
                                    \$('#td-contrato-rejeitado').addClass('contratoRejeitadoBackground');
                                    \$('.fa.fa-exclamation').hide();
                                    \$('.fa.fa-check').hide();
                                    \$('.fa.fa-times').show();
                                    \$( \"#radio-contrato-rejeitado\" ).prop( \"checked\", true );

                                }
                            }
                        }
                    }

                },
                error: function (data) {

                }
            });
        }

    </script>
";
    }

    // line 277
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 278
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>

        .fa-times {
            color: white;
        }

        .fa-exclamation {
            color: red;
        }

        .fa-check {
            color: white;
        }

        .funkyradio div {
            clear: both;
            /*margin: 0 50px;*/
            overflow: hidden;
        }
        .funkyradio label {
            /*min-width: 400px;*/
            width: 100%;
            border-radius: 3px;
            border: 1px solid #D1D3D4;
            font-weight: normal;
        }

        .funkyradio input[type=\"radio\"]:empty, .funkyradio input[type=\"checkbox\"]:empty {
            display: none;
        }
        .funkyradio input[type=\"radio\"]:empty ~ label, .funkyradio input[type=\"checkbox\"]:empty ~ label {
            position: relative;
            line-height: 2.5em;
            text-indent: 3.25em;

            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .funkyradio input[type=\"radio\"]:empty ~ label:before, .funkyradio input[type=\"checkbox\"]:empty ~ label:before {
            position: absolute;
            display: block;
            top: 0;
            bottom: 0;
            left: 0;
            content:'';
            width: 2.5em;
            background: #D1D3D4;
            border-radius: 3px 0 0 3px;
        }
        .funkyradio input[type=\"radio\"]:hover:not(:checked) ~ label:before, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label:before {
            content:'\\2714';
            text-indent: .9em;
            color: #C2C2C2;
        }
        .funkyradio input[type=\"radio\"]:hover:not(:checked) ~ label, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label {
            color: #888;
        }
        .funkyradio input[type=\"radio\"]:checked ~ label:before, .funkyradio input[type=\"checkbox\"]:checked ~ label:before {
            content:'\\2714';
            text-indent: .9em;
            color: #333;
            background-color: #ccc;
        }
        .funkyradio input[type=\"radio\"]:checked ~ label, .funkyradio input[type=\"checkbox\"]:checked ~ label {
            color: #777;
        }
        .funkyradio input[type=\"radio\"]:focus ~ label:before, .funkyradio input[type=\"checkbox\"]:focus ~ label:before {
            box-shadow: 0 0 0 3px #999;
        }
        .funkyradio-default input[type=\"radio\"]:checked ~ label:before, .funkyradio-default input[type=\"checkbox\"]:checked ~ label:before {
            color: #333;
            background-color: #ccc;
        }
        .funkyradio-primary input[type=\"radio\"]:checked ~ label:before, .funkyradio-primary input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #337ab7;
        }
        .funkyradio-success input[type=\"radio\"]:checked ~ label:before, .funkyradio-success input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #5cb85c;
        }
        .funkyradio-danger input[type=\"radio\"]:checked ~ label:before, .funkyradio-danger input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #d9534f;
        }
        .funkyradio-warning input[type=\"radio\"]:checked ~ label:before, .funkyradio-warning input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #f0ad4e;
        }
        .funkyradio-info input[type=\"radio\"]:checked ~ label:before, .funkyradio-info input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #5bc0de;
        }

        #btn-situacao-modal {
            color:#ffffff !important;
            text-decoration:none;
        }



        .contratoAceitoBackground {
            background-color: #27e833;
        }

        .contratoRejeitadoBackground {
            background-color: #fb122f ;
        }

        .contratoSobAnaliseBackground {
            background-color: #fad201;
        }

        .th-same-size {
            width: 33.33%;
        }

        .centerText {
            text-align: center;
        }

        .btn {
           min-width: 9em;
           max-width: 9em;
        }

        li {
            list-style-type:none;
        }

        h4 {
            font-weight: bold;
        }

        .font-weight-bold {
            font-weight: bold;
        }

        li span a:hover{
            text-decoration: none;
        }

        #expButton {
            color:#ffffff !important;
            text-decoration:none;
        }

        .breadcumbSemLink {
            cursor: no-drop;
        }

        .breadcumbComLink {
            cursor: pointer;
        }
    </style>

";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_detalhes_itens_vinculo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  525 => 278,  522 => 277,  466 => 224,  450 => 213,  443 => 209,  440 => 208,  437 => 207,  431 => 203,  428 => 202,  426 => 201,  424 => 200,  422 => 199,  413 => 194,  411 => 193,  408 => 192,  406 => 191,  404 => 190,  398 => 187,  392 => 184,  368 => 162,  366 => 161,  361 => 158,  353 => 154,  351 => 153,  347 => 151,  339 => 148,  335 => 146,  331 => 144,  329 => 143,  324 => 141,  321 => 140,  317 => 139,  308 => 133,  299 => 126,  297 => 125,  294 => 124,  290 => 121,  282 => 117,  280 => 116,  277 => 115,  260 => 112,  256 => 111,  252 => 110,  248 => 108,  245 => 107,  241 => 105,  237 => 103,  234 => 102,  230 => 100,  228 => 99,  223 => 97,  220 => 96,  203 => 95,  183 => 77,  177 => 74,  173 => 73,  167 => 70,  162 => 69,  160 => 68,  154 => 65,  148 => 62,  142 => 59,  135 => 55,  129 => 52,  121 => 49,  116 => 47,  77 => 10,  74 => 9,  68 => 7,  64 => 5,  61 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_detalhes_itens_vinculo.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontrato/contrato_detalhes_itens_vinculo.html.twig");
    }
}
