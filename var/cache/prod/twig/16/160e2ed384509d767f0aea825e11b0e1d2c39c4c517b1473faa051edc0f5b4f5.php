<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontratoitem:item_mostra.html.twig */
class __TwigTemplate_81fc0e3d443a14d344a0611207cd01d4997ea05a8105428f9b978f603fc25dfe extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontratoitem:item_mostra.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo "Item";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\">Pessoal Externo</a></span></li>
     <li><span><a href=\"#\">Item</a></span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "    <header class=\"panel-heading\">
        <div>
            <h2 class=\"panel-title\">
                <span class=\"va-middle\"><i class=\"fa fa-list\"></i> Itens</span>
                <ul class=\"notifications pull-right hidden-xs contexto-oferta-menu\">
                    <li data-toggle=\"tooltip\" title=\"Adicionar novo item\">
                        <a href=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontratoitem_new");
        echo "\" class=\"btn-gerar-relatorio2\">
                            <i class=\"fa fa-plus-circle\"></i>
                        </a>
                    </li>
                </ul>
            </h2>
        </div>
    </header>
    <div class=\"panel-body\">
    <table class=\"table table-striped\">
        <thead>
            <tr>
                <th>Descricao</th>
                <th>Valor</th>
                <th>Editar Item</th>
            </tr>
        </thead>
        <tbody>

        ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["nteContratoItems"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["nteContratoItem"]) {
            // line 35
            echo "            <tr>

                <td>";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["nteContratoItem"], "descricao", [], "any", false, false, false, 37), "html", null, true);
            echo "</td>
                <td>";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["nteContratoItem"], "valor", [], "any", false, false, false, 38), "html", null, true);
            echo "</td>
                <td>
                        <li>
                            <a href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontratoitem_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["nteContratoItem"], "id", [], "any", false, false, false, 41)]), "html", null, true);
            echo "\">Editar</a>
                        </li>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['nteContratoItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "        </tbody>
    </table>
    </div>




";
    }

    // line 55
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 56
        echo "
    ";
        // line 57
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>


    </script>

";
    }

    // line 65
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 66
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>

        li {
            list-style-type:none;
        }

        table th,
        table  {
            text-align: center;
            vertical-align: middle;
        }

    </style>

";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontratoitem:item_mostra.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 66,  159 => 65,  148 => 57,  145 => 56,  142 => 55,  131 => 46,  120 => 41,  114 => 38,  110 => 37,  106 => 35,  102 => 34,  80 => 15,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontratoitem:item_mostra.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontratoitem/item_mostra.html.twig");
    }
}
