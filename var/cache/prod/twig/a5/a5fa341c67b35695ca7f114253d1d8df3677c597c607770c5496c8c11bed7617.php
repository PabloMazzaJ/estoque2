<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_tela_pos_lista_gerada_vinculacao_direta.html.twig */
class __TwigTemplate_cbf2176d96cca36e87f390fc2643dce0bafa8d1683848395e9d20f4a3462763a extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_tela_pos_lista_gerada_vinculacao_direta.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Detalhes Contrato ";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Pessoal Externo</a></span></li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Contrato</a></span></li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Vincular</a></span></li>
 ";
    }

    // line 9
    public function block_conteudo($context, array $blocks = [])
    {
        // line 10
        echo "    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"modal\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Fechar\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <h4 class=\"modal-title\" style=\"text-align: center\">Alterar todas pessoas </h4>
                </div>
                <div class=\"modal-body\">
                    <div class=\"funkyradio check-inputs\">
                        <div class=\"grupo-modal\">
                            <div class=\"funkyradio-success\">
                                <input type=\"radio\" name=\"adicionar\" id=\"add-all-itens\" class=\"radioBtnClassAdicionar\"/>
                                <label for=\"add-all-itens\">Adicionar todas pessoas</label>
                            </div>
                            <div class=\"funkyradio-danger\">
                                <input type=\"radio\" name=\"adicionar\" id=\"remover-all-itens\" class=\"radioBtnClassAdicionar\"/>
                                <label for=\"remover-all-itens\">Remover todas pessoas</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" id=\"modalcancelar\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancelar</button>
                    <button type=\"button\" id=\"modal-confirmar\" class=\"btn btn-success\" data-dismiss=\"modal\">Confirmar</button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"modal-vinculos\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Fechar\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <h4 class=\"modal-title\" style=\"text-align: center\">Alterar todas pessoas </h4>
                </div>
                <div class=\"modal-body\">
                   <p style=\"text-align: center\">Deseja visualizar todos os vínculos? (vocè será redirecionado para outra tela.) </p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" id=\"modalcancelar\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancelar</button>
                    <button type=\"button\" id=\"modal-confirmar-ver-vinculos\" class=\"btn btn-success\" data-dismiss=\"modal\">Confirmar</button>
                </div>
            </div>
        </div>
    </div>

    <header class=\"panel-heading center\">
        <div>
            <h2 class=\"panel-title\">
                <span class=\"va-middle\">Lista de Pessoas</span>
                <i class=\"fa fa-cog fa-lg breadcumbComLink\" aria-hidden=\"true\" style=\"float: right\" title=\"Clique aqui para configurar todos itens da lista\"></i>
            </h2>
        </div>
    </header>
    <div class=\"panel-body center\">
        <table class=\"table table-striped\" id=\"myTable\" data-id-contrato=\"";
        // line 70
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "id", [], "any", false, false, false, 70), "html", null, true);
        echo "\">
            <tr>
                <th class=\"col-md-1 \">Pessoas</th>
                <th class=\"col-md-1\">Cpf</th>
                <th class=\"col-md-1\">Selecionar</th>
            </tr>

            ";
        // line 77
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["pessoas"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["pessoa"]) {
            // line 78
            echo "                <tr>
                    <td style=\"vertical-align: middle\" data-pessoa-id=\"";
            // line 79
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pessoa"], "id", [], "any", false, false, false, 79), "html", null, true);
            echo "\">
                        ";
            // line 80
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pessoa"], "nome", [], "any", false, false, false, 80), "html", null, true);
            echo "
                    </td>
                    <td style=\"vertical-align: middle\">
                        ";
            // line 83
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pessoa"], "cpf", [], "any", false, false, false, 83), "html", null, true);
            echo "
                    </td>
                    <td style=\"vertical-align: middle\">
                        <label class=\"switch\">
                            <input type=\"checkbox\" class=\"switch-adicionar success\" >
                            <span class=\"slider round\"></span>
                        </label>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pessoa'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 93
        echo "        </table>
        <a href=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_contrato_detalhes_criacao", ["id" => twig_get_attribute($this->env, $this->source, ($context["contrato"] ?? null), "id", [], "any", false, false, false, 94)]), "html", null, true);
        echo "\" class=\"btn-gerar-relatorio2\">
            <div style=\"float: left\" title=\"Clique aqui para realizar alterações na lista\">
                <button type=\"button\" class=\"btn btn-primary btn-sm btn-strict\" id=\"buttonRetornar\"><i class=\"fa fa-arrow-circle-left\"></i></button>
            </div>
        </a>

        <div style=\"float: right; padding-left: 1px\" title=\"Clique aqui para visualizar todos vínculos do seu curso\">
            <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"ver-vinculos\">Ver Vínculos</button>
        </div>

        <div style=\"float: right\" title=\"Clique aqui para vincular os selecionados ao contrato\">
            <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"vincular-btn\">Vincular Selecionados</button>
        </div>
    </div>

";
    }

    // line 110
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 111
        echo "
    ";
        // line 112
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "

    <script>
        \$(function ()
        {
            modalConfirmarVerVinculos();
            verVinculos();
            windowConfig();
            openCogModal();
            modalConfirmarCog();
            concluirVinculacaoDireta();
        });

        function modalConfirmarVerVinculos()
        {
            \$('#modal-confirmar-ver-vinculos').click(function() {
                window.document.location = 'http://vm16.nte.ufsm.br/app_dev.php/externo/ntecontrato/detalhesvinculo';
            });
        }

        function verVinculos()
        {
            \$('#ver-vinculos').click(function() {
                \$('#modal-vinculos').modal();
            });
        }

        function windowConfig()
        {
            \$(window).resize(function () {
                \$(\".ui-pnotify\").each(function () {
                    \$(this).css(get_center_pos(\$(this).width(), \$(this).position().top))
                });
            });
        }

        function get_center_pos(width, top)
        {
            if (!top)
            {
                top = 30;
                \$('.ui-pnotify').each(function() { top += \$(this).outerHeight() + 20; });
            }
            return {
                \"top\": top, \"left\": (\$(window).width() / 2) - (width / 2)
            }
        }

        function notificaUsuario(titulo, texto, tipo)
        {
            new PNotify(
                {
                    title: titulo,
                    text: texto,
                    type: tipo,
                    delay: 3000,
                    before_open: function (PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    }
                }
            );
        }

        function webServiceConcluirVinculacaoDireta(arrayIdPessoas, idContrato)
        {
            console.log(arrayIdPessoas);
            console.log(idContrato);

            \$.ajax({
               url: \"";
        // line 181
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_criar_vinculo_bulk");
        echo "\",
                dataType: 'json',
                method: 'POST',
                data: {
                   arrayIdPessoasKey: arrayIdPessoas,
                   idContratoKey: idContrato
                },
                success: function (data) {

                    let dataArray = data;

                    for (let i = 0; i < dataArray.length; i++) {

                        let pessoa = dataArray[i].pessoa;
                        let contrato = dataArray[i].contrato;
                        let existia = dataArray[i].existia;

                        if (existia == true) {
                            let texto =  pessoa + ' foi vinculado(a) ao contrato ' + contrato + ' com sucesso.';
                            notificaUsuario('', texto, 'success');
                        } else {
                            let texto =  pessoa + ' já possui um vínculo com este contrato.';
                            notificaUsuario('', texto, 'warning');
                        }
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }

        function concluirVinculacaoDireta()
        {
            \$('#vincular-btn').click(function(){

                let arrayIdPessoas = [];
                let idContrato = \$('table').data('id-contrato');

                let oTable = document.getElementById('myTable');
                let rowLength = oTable.rows.length;

                //loops through rows
                for (let i = 0; i < rowLength; i++)
                {
                    //gets cells of current row
                    let oCells = oTable.rows.item(i).cells;

                    //gets amount of cells of current row
                    let cellLength = oCells.length;

                    //loops through each cell in current row
                    for(let j = 0; j < cellLength; j++)
                    {
                        if (j === 2)
                        {
                            let cellChildren = oCells.item(j).children[0];
                            let switchValue = \$(cellChildren).find('input').is(':checked') ? 1 : 0;
                                //todo pegar id's das pessoas e não o nome.
                            if (switchValue === 1)
                            {
                                let cellPessoa = oCells.item(j-2);
                                let pessoaId = \$(cellPessoa).data('pessoa-id');
                                arrayIdPessoas.push(pessoaId);
                            }
                        }
                    }
                }

                webServiceConcluirVinculacaoDireta(arrayIdPessoas, idContrato);
            });
        }

        function modalConfirmarCog()
        {
            \$('#modal-confirmar').click(function() {

                if (\$(\"input[type='radio'].radioBtnClassAdicionar\").is(':checked'))
                {
                    let resultAdd = \$(\"input[name='adicionar'].radioBtnClassAdicionar:checked\").attr('id');

                    if (resultAdd == 'add-all-itens')
                    {
                        \$('table td:nth-child(3) .switch-adicionar').prop('checked', true);
                        console.log('add all itens selecionado');
                    } else if (resultAdd == 'remover-all-itens')
                    {
                        console.log('remover all itens');
                        \$('table td:nth-child(3) .switch-adicionar').prop('checked', false);
                    }
                }
            });
        }

        function openCogModal()
        {
            \$('.fa.fa-cog.fa-lg').click( function()
            {
                \$(\"#modal\").modal();
            });
        }

    </script>
";
    }

    // line 286
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 287
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>
        .modal-header {
            border-bottom: 0 none;
        }

        .modal-footer {
            border-top: 0 none;
        }

        .grupo-modal {
            display: flex;
            justify-content: space-between;
        }

        .grupo-modal > div {
            width: calc(50% - 2px);
        }

        .funkyradio div {
            clear: both;
            /*margin: 0 50px;*/
            overflow: hidden;
        }
        .funkyradio label {
            /*min-width: 400px;*/
            width: 100%;
            border-radius: 3px;
            border: 1px solid #D1D3D4;
            font-weight: normal;
        }
        .funkyradio input[type=\"radio\"]:empty, .funkyradio input[type=\"checkbox\"]:empty {
            display: none;
        }
        .funkyradio input[type=\"radio\"]:empty ~ label, .funkyradio input[type=\"checkbox\"]:empty ~ label {
            position: relative;
            line-height: 2.5em;
            text-indent: 3.25em;

            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .funkyradio input[type=\"radio\"]:empty ~ label:before, .funkyradio input[type=\"checkbox\"]:empty ~ label:before {
            position: absolute;
            display: block;
            top: 0;
            bottom: 0;
            left: 0;
            content:'';
            width: 2.5em;
            background: #D1D3D4;
            border-radius: 3px 0 0 3px;
        }
        .funkyradio input[type=\"radio\"]:hover:not(:checked) ~ label:before, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label:before {
            content:'\\2714';
            text-indent: .9em;
            color: #C2C2C2;
        }
        .funkyradio input[type=\"radio\"]:hover:not(:checked) ~ label, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label {
            color: #888;
        }
        .funkyradio input[type=\"radio\"]:checked ~ label:before, .funkyradio input[type=\"checkbox\"]:checked ~ label:before {
            content:'\\2714';
            text-indent: .9em;
            color: #333;
            background-color: #ccc;
        }
        .funkyradio-danger input[type=\"radio\"]:checked ~ label:before, .funkyradio input[type=\"checkbox\"]:checked ~ label:before {
            content:'\\03a7';
            text-indent: .9em;
            color: #333;
            background-color: #ccc;
        }
        .funkyradio-danger input[type=\"radio\"]:hover:not(:checked) ~ label:before, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label:before {
            content:'\\03a7';
            text-indent: .9em;
            color: #C2C2C2;
        }
        .funkyradio-warning input[type=\"radio\"]:hover:not(:checked) ~ label:before, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label:before {
            content:'\\2713';
            text-indent: .9em;
            color: #C2C2C2;
            font-weight: bold;
        }

        .funkyradio-warning input[type=\"radio\"]:checked ~ label:before, .funkyradio input[type=\"checkbox\"]:checked ~ label:before {
            content: '\\2713';
            text-indent: .9em;
            color: #333;
            background-color: #ccc;
            font-weight: bold;
        }
        .funkyradio input[type=\"radio\"]:checked ~ label, .funkyradio input[type=\"checkbox\"]:checked ~ label {
            color: #777;
        }
        .funkyradio input[type=\"radio\"]:focus ~ label:before, .funkyradio input[type=\"checkbox\"]:focus ~ label:before {
            box-shadow: 0 0 0 3px #999;
        }
        .funkyradio-default input[type=\"radio\"]:checked ~ label:before, .funkyradio-default input[type=\"checkbox\"]:checked ~ label:before {
            color: #333;
            background-color: #ccc;
        }
        .funkyradio-primary input[type=\"radio\"]:checked ~ label:before, .funkyradio-primary input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #337ab7;
        }
        .funkyradio-success input[type=\"radio\"]:checked ~ label:before, .funkyradio-success input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #5cb85c;
        }
        .funkyradio-danger input[type=\"radio\"]:checked ~ label:before, .funkyradio-danger input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #d9534f;
        }
        .funkyradio-warning input[type=\"radio\"]:checked ~ label:before, .funkyradio-warning input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #f0ad4e;
        }
        .funkyradio-info input[type=\"radio\"]:checked ~ label:before, .funkyradio-info input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #5bc0de;
        }

        table {
            cursor: default;
        }

        .x-mark-obrigatorio {
            color: #b61124;
        }

        .check-obrigatorio {
            font-size: larger;
            color: #00e158;
        }

        table th {
            text-align: center;
        }

        .center {
            margin: auto;
            width: 60%;
        }

        .btn-strict {
            min-width: 9em;
            max-width: 9em;
        }

        .breadcumbSemLink { cursor: no-drop; }

        .breadcumbComLink { cursor: pointer; }

        .switch
        {
            position: relative;
            display: inline-block;
            width: 50px;
            height: 24px;
        }

        /* Hide default HTML checkbox */
        .switch input {display:none;}

        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: \"\";
            height: 16px;
            width: 16px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input.default:checked + .slider {
            background-color: #444;
        }
        input.primary:checked + .slider {
            background-color: #2196F3;
        }
        input.success:checked + .slider {
            background-color: #8bc34a;
        }
        input.info:checked + .slider {
            background-color: #3de0f5;
        }
        input.warning:checked + .slider {
            background-color: #FFC107;
        }
        input.danger:checked + .slider {
            background-color: #f44336;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(16px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

    </style>

";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_tela_pos_lista_gerada_vinculacao_direta.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  390 => 287,  387 => 286,  279 => 181,  207 => 112,  204 => 111,  201 => 110,  181 => 94,  178 => 93,  162 => 83,  156 => 80,  152 => 79,  149 => 78,  145 => 77,  135 => 70,  73 => 10,  70 => 9,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_tela_pos_lista_gerada_vinculacao_direta.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontrato/contrato_tela_pos_lista_gerada_vinculacao_direta.html.twig");
    }
}
