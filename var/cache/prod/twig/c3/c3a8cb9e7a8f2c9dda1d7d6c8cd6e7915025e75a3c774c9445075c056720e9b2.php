<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteConfigMoodleBundle:Default:tabela.html.twig */
class __TwigTemplate_1efc857865ffb42f65f246807b86c5974ed1a7288e11bc63c92291174e53cd06 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<section class=\"panel\">
    <div class=\" panel-body\">
        <table class=\"table table-striped \">
            <thead>
            <tr>
                <th width=\"10\">
                    <div class=\"checkbox-custom checkbox-default\">
                        <input type=\"checkbox\" id=\"configs\" data-toggle=\"tooltip\" title=\"Selecionar todos\"
                               data-placement=\"top\">
                        <label> </label>
                    </div>
                </th>
                <th width=\"10\"></th>
                <th>Ambiente</th>
                <th>Acesso</th>
                <th class=\"text-right\"></th>
            </tr>
            </thead>
            <tbody>
            ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["m"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["conf"]) {
            // line 21
            echo "                <tr>
                    <td>
                        <div class=\"checkbox-custom checkbox-default\">
                            <input type=\"checkbox\" name=\"configs\" id=\"moodle-";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "id", [], "any", false, false, false, 24), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "id", [], "any", false, false, false, 24), "html", null, true);
            echo "\">
                            <label> </label>
                        </div>
                    </td>
                    <td>";
            // line 28
            if ((twig_get_attribute($this->env, $this->source, $context["conf"], "valido", [], "any", false, false, false, 28) == 1)) {
                // line 29
                echo "                            <i class=\"fa fa-check-circle-o text-success\" aria-hidden=\"true\"></i>
                        ";
            } else {
                // line 31
                echo "                            <i class=\"fa fa-times-circle-o text-danger\" aria-hidden=\"true\"></i>
                        ";
            }
            // line 33
            echo "                    </td>
                    <td>
                        <a href=\"";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "url", [], "any", false, false, false, 35), "html", null, true);
            echo "\" target=\"_blank\"><b>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "nome", [], "any", false, false, false, 35), "html", null, true);
            echo "</b></a><br>
                        <small>";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "descricao", [], "any", false, false, false, 36), "html", null, true);
            echo "</small>
                    </td>
                    <td>
                        ";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "servico", [], "any", false, false, false, 39), "html", null, true);
            echo " / ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "usuario", [], "any", false, false, false, 39), "html", null, true);
            echo " <br> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "token", [], "any", false, false, false, 39), "html", null, true);
            echo "
                    </td>
                    <td>
                        <a title=\"Configura os parâmetros da configuração moodle\"
                           class=\"btn btn-default btn-sm btn-simple-ajax pull-right\"
                           href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_editar", ["id" => twig_get_attribute($this->env, $this->source, $context["conf"], "id", [], "any", false, false, false, 44)]), "html", null, true);
            echo "\"
                           data-toggle=\"tooltip\"
                           data-placement=\"top\">
                            <i class=\"fa fa-sliders\"></i>
                        </a>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['conf'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "            </tbody>
        </table>
    </div>
</section>
<script>
    updateDomJS();
</script>";
    }

    public function getTemplateName()
    {
        return "NteConfigMoodleBundle:Default:tabela.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 52,  112 => 44,  100 => 39,  94 => 36,  88 => 35,  84 => 33,  80 => 31,  76 => 29,  74 => 28,  65 => 24,  60 => 21,  56 => 20,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteConfigMoodleBundle:Default:tabela.html.twig", "/var/www/narfi/src/Nte/Config/MoodleBundle/Resources/views/Default/tabela.html.twig");
    }
}
