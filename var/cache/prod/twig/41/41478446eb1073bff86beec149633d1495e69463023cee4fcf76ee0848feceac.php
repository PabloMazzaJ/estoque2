<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteUsuarioBundle:Usuario:usuarioTest.html.twig */
class __TwigTemplate_2141bdf2296258a547a3d2548b5aca2402d0fab496c18c2c4ce1536ffce109ff extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteUsuarioBundle:Usuario:usuarioTest.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Administração de Usuários";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Administração Usuários";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_index");
        echo "\"> Usuários </a></span></li>
 ";
    }

    // line 7
    public function block_conteudo($context, array $blocks = [])
    {
        // line 8
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["lastlogin"] ?? null), "Y-m-d"), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "NteUsuarioBundle:Usuario:usuarioTest.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  69 => 7,  62 => 5,  59 => 4,  53 => 3,  47 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteUsuarioBundle:Usuario:usuarioTest.html.twig", "/var/www/narfi/src/Nte/UsuarioBundle/Resources/views/Usuario/usuarioTest.html.twig");
    }
}
