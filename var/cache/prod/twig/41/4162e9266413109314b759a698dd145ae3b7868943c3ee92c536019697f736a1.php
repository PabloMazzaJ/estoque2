<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteUsuarioBundle:Usuario:perfil.html.twig */
class __TwigTemplate_792dd8d429724bb67e1e3f0f767ffd24b50b4f93ffce6bebdb9dfe96c7a36d78 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteUsuarioBundle:Usuario:perfil.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Perfil";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span>Usuários</span></li>
     <li><span>";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "nome", [], "any", false, false, false, 6), "html", null, true);
        echo "</span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "    ";
        $context["roles"] = twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "roles2", [], "any", false, false, false, 9);
        // line 10
        echo "    <div class=\"row\">
        <div class=\"col-md-4 col-lg-3\">
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <div class=\"thumb-info mb-md\">
                        <img src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_imagem_perfil", ["id" => twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "id", [], "any", false, false, false, 15)]), "html", null, true);
        echo "\"
                             class=\"rounded img-responsive\"
                             alt=\"John Doe\">
                        <div class=\"thumb-info-title\">
                        <span class=\"thumb-info-inner\">
                            ";
        // line 20
        echo twig_escape_filter($this->env, (((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "nome", [], "any", false, false, false, 20)) > 50)) ? ((twig_slice($this->env, twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "nome", [], "any", false, false, false, 20), 0, 50) . "...")) : (twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "nome", [], "any", false, false, false, 20))), "html", null, true);
        echo "
                        </span>
                            <span class=\"thumb-info-type\">";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "highestRoleName", [], "any", false, false, false, 22), "html", null, true);
        echo "</span>
                        </div>
                    </div>
                    <!--
                    <div class=\"widget-toggle-expand mb-md\">
                        <div class=\"widget-header\">
                            <h6>Progressão em suas tarefas</h6>
                            <div class=\"widget-toggle\">+</div>
                        </div>
                        <div class=\"widget-content-collapsed\">
                            <div class=\"progress progress-xs light\">
                                <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\"
                                     aria-valuemax=\"100\" style=\"width: 60%;\">
                                    60%
                                </div>
                            </div>
                        </div>
                        <div class=\"widget-content-expanded\">
                        </div>
                    </div>
                    -->
                    <hr class=\"dotted short\">
                    <ul class=\"list-unstyled\">
                        <li><i class=\"fa fa-user\"></i> ";
        // line 45
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "nome", [], "any", false, false, false, 45), "html", null, true);
        echo "</li>
                        <li><i class=\"fa fa-envelope\"></i> ";
        // line 46
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "email", [], "any", false, false, false, 46), "html", null, true);
        echo " </li>
                        <li>
                            ";
        // line 48
        if ((twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "telefone", [], "any", false, false, false, 48) == null)) {
            // line 49
            echo "                                <i class=\"fa fa-phone\"></i>      Não informado
                            ";
        } else {
            // line 51
            echo "                                <i class=\"fa fa-phone\"></i>  ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "telefone", [], "any", false, false, false, 51), "html", null, true);
            echo "
                            ";
        }
        // line 53
        echo "                        </li>
                        <li><i class=\"fa fa-whatsapp\"></i>
                            ";
        // line 55
        if ((twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "socialWhatsapp", [], "any", false, false, false, 55) == null)) {
            // line 56
            echo "                                Não informado
                            ";
        } else {
            // line 58
            echo "                                ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "socialWhatsapp", [], "any", false, false, false, 58), "html", null, true);
            echo "
                            ";
        }
        // line 59
        echo "</li>
                        <li><i class=\"fa fa-lock\"></i> Permissões
                            <p>
                            <ol>
                                ";
        // line 63
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "roles", [], "any", false, false, false, 63));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 64
            echo "                                    <li>";
            echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["roles"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[$context["e"]] ?? null) : null), "html", null, true);
            echo "</li>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "                            </ol>
                        </li>
                        <li></li>
                        <li></li>
                    </ul>
                    <hr class=\"dotted short\">
                    <h6 class=\"text-muted\"><i class=\"fa fa-clock-o\"></i> Último Acesso</h6>
                    <p>
                        ";
        // line 74
        if ((twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "lastLogin", [], "any", false, false, false, 74) == null)) {
            // line 75
            echo "                            Nunca acessou o sistema
                        ";
        } else {
            // line 77
            echo "                            ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "lastLogin", [], "any", false, false, false, 77), "d/m/Y H:i"), "html", null, true);
            echo "
                        ";
        }
        // line 79
        echo "                    </p>
                    <hr class=\"dotted short\">
                    <div class=\"social-icons-list\">
                        <a rel=\"tooltip\" data-placement=\"bottom\" href=\"http://";
        // line 82
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "socialWhatsapp", [], "any", false, false, false, 82), "html", null, true);
        echo "\"
                           data-original-title=\"Whatsapp \"><i class=\"fa fa-whatsapp\"></i><span>Whatsapp</span></a>
                        <a rel=\"tooltip\" data-placement=\"bottom\" href=\"mailto://";
        // line 84
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "email", [], "any", false, false, false, 84), "html", null, true);
        echo "\"
                           data-original-title=\"E-Mail\"><i class=\"fa fa-envelope\"></i><span>E-Mail</span></a>
                    </div>

                </div>
            </section>
        </div>
        <div class=\"col-md-8 col-lg-6\">
            <div class=\"panel-body\">
                ";
        // line 93
        if (twig_length_filter($this->env, ($context["frigaAvaliacoes"] ?? null))) {
            // line 94
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["frigaAvaliacoes"] ?? null));
            foreach ($context['_seq'] as $context["key"] => $context["frigaAvalicao"]) {
                // line 95
                echo "                    <section id=\"";
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\">
                        <table class=\"table\">
                            <thead>
                            <tr>
                                <th colspan=\"4\" class=\"text-primary\"><i class=\"fa fa-map\"></i> Polo ";
                // line 99
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "</th>
                            </tr>
                            <tr class=\"hidden-xs\">
                                <th>N° Inscrição</th>
                                <th>Nome</th>
                                <th class=\"text-center\">Situação</th>
                                <th class=\"text-center\">Pontuação</th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
                // line 109
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["frigaAvalicao"]);
                foreach ($context['_seq'] as $context["_key"] => $context["frigaPessoa"]) {
                    // line 110
                    echo "                                <tr class=\"visible-xs\">
                                    <td class=\"text-uppercase\"><a href=\"";
                    // line 111
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("frigaavaliacao_candidato_perfil", ["id" => twig_get_attribute($this->env, $this->source, $context["frigaPessoa"], "id", [], "any", false, false, false, 111)]), "html", null, true);
                    echo "\"><b>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["frigaPessoa"], "nome", [], "any", false, false, false, 111), "html", null, true);
                    echo "</b></a><br>
                                        ";
                    // line 112
                    if ((twig_get_attribute($this->env, $this->source, $context["frigaPessoa"], "idSituacao", [], "any", false, false, false, 112) ==  -10)) {
                        // line 113
                        echo "                                            <small class=\"\"><i class=\"fa fa-user-times\"></i>
                                                Desclassificado
                                            </small>
                                        ";
                    } elseif ((twig_get_attribute($this->env, $this->source,                     // line 116
$context["frigaPessoa"], "idSituacao", [], "any", false, false, false, 116) ==  -1)) {
                        // line 117
                        echo "                                            <small class=\"\">
                                                <i class=\"fa fa-minus-circle\"></i>
                                                Não Homologado
                                            </small>
                                        ";
                    } elseif ((twig_get_attribute($this->env, $this->source,                     // line 121
$context["frigaPessoa"], "idSituacao", [], "any", false, false, false, 121) == 0)) {
                        // line 122
                        echo "                                            <small class=\"\">
                                                <i class=\"fa fa-times\"></i>
                                                Não avaliado
                                            </small>
                                        ";
                    } elseif ((twig_get_attribute($this->env, $this->source,                     // line 126
$context["frigaPessoa"], "idSituacao", [], "any", false, false, false, 126) == 1)) {
                        // line 127
                        echo "                                            <small class=\"\">
                                                <i class=\"fa fa-user-plus\"></i>
                                                Homologado
                                            </small>
                                        ";
                    } elseif ((twig_get_attribute($this->env, $this->source,                     // line 131
$context["frigaPessoa"], "idSituacao", [], "any", false, false, false, 131) == 2)) {
                        // line 132
                        echo "                                            <small class=\"\">
                                                <i class=\"fa fa-user-plus\"></i>
                                                Não Entrevistado
                                            </small>
                                        ";
                    } elseif ((twig_get_attribute($this->env, $this->source,                     // line 136
$context["frigaPessoa"], "idSituacao", [], "any", false, false, false, 136) == 3)) {
                        // line 137
                        echo "                                            <small class=\"\">
                                                <i class=\"fa fa-user-plus\"></i>
                                                Classificado
                                            </small>
                                        ";
                    }
                    // line 142
                    echo "                                        <small><i class=\"fa fa-trophy\"></i> ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["frigaPessoa"], "pontuacaoTotal", [], "any", false, false, false, 142), "html", null, true);
                    echo " Pontos</small>
                                    </td>
                                </tr>
                                <tr class=\"hidden-xs\">
                                    <td width=\"10\">";
                    // line 146
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["frigaPessoa"], "uuid", [], "any", false, false, false, 146), "html", null, true);
                    echo "</td>
                                    <td><a href=\"";
                    // line 147
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("frigaavaliacao_candidato_perfil", ["id" => twig_get_attribute($this->env, $this->source, $context["frigaPessoa"], "id", [], "any", false, false, false, 147)]), "html", null, true);
                    echo "\"><b>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["frigaPessoa"], "nome", [], "any", false, false, false, 147), "html", null, true);
                    echo "</b></a></td>
                                    <td width=\"10\" class=\"text-center\">
                                        ";
                    // line 149
                    if ((twig_get_attribute($this->env, $this->source, $context["frigaPessoa"], "idSituacao", [], "any", false, false, false, 149) ==  -10)) {
                        // line 150
                        echo "                                            <small class=\"label label-danger\"><i class=\"fa fa-user-times\"></i>
                                                Desclassificado
                                            </small>
                                        ";
                    } elseif ((twig_get_attribute($this->env, $this->source,                     // line 153
$context["frigaPessoa"], "idSituacao", [], "any", false, false, false, 153) ==  -1)) {
                        // line 154
                        echo "                                            <small class=\"label label-danger\">
                                                <i class=\"fa fa-minus-circle\"></i>
                                                Não Homologado
                                            </small>
                                        ";
                    } elseif ((twig_get_attribute($this->env, $this->source,                     // line 158
$context["frigaPessoa"], "idSituacao", [], "any", false, false, false, 158) == 0)) {
                        // line 159
                        echo "                                            <small class=\"badge\">
                                                <i class=\"fa fa-times\"></i>
                                                Não avaliado
                                            </small>
                                        ";
                    } elseif ((twig_get_attribute($this->env, $this->source,                     // line 163
$context["frigaPessoa"], "idSituacao", [], "any", false, false, false, 163) == 1)) {
                        // line 164
                        echo "                                            <small class=\"label label-primary\">
                                                <i class=\"fa fa-user-plus\"></i>
                                                Homologado
                                            </small>
                                        ";
                    } elseif ((twig_get_attribute($this->env, $this->source,                     // line 168
$context["frigaPessoa"], "idSituacao", [], "any", false, false, false, 168) == 2)) {
                        // line 169
                        echo "                                            <small class=\"label label-primary\">
                                                <i class=\"fa fa-user-plus\"></i>
                                                Não Entrevistado
                                            </small>
                                        ";
                    } elseif ((twig_get_attribute($this->env, $this->source,                     // line 173
$context["frigaPessoa"], "idSituacao", [], "any", false, false, false, 173) == 3)) {
                        // line 174
                        echo "                                            <small class=\"label label-success\">
                                                <i class=\"fa fa-user-plus\"></i>
                                                Classificado
                                            </small>
                                        ";
                    }
                    // line 179
                    echo "                                    </td>
                                    <td width=\"10\" class=\"text-center\">
                                        ";
                    // line 181
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["frigaPessoa"], "pontuacaoTotal", [], "any", false, false, false, 181), "html", null, true);
                    echo "
                                    </td>
                                </tr>
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['frigaPessoa'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 185
                echo "                            </tbody>
                        </table>
                    </section>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['frigaAvalicao'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 189
            echo "                ";
        } else {
            // line 190
            echo "                    <section class=\"body-error error-inside\">
                        <div class=\"center-error\">
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    <div class=\"main-error mb-xlg\">
                                        <h2 class=\"error-code text-dark text-center text-weight-semibold m-none\">0 <i class=\"fa fa-trophy text-primary\"></i></h2>
                                        <p class=\"error-explanation text-center text-primary\">
                                            <br>Nenhum <b> candidato</b> para este usuário avaliar.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                ";
        }
        // line 205
        echo "            </div>
        </div>
        <div class=\"col-md-12 col-lg-3\">
            ";
        // line 208
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN_USER")) {
            // line 209
            echo "
                        <a class=\"btn btn-block btn-default\" href=\"";
            // line 210
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_editar", ["id" => twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "id", [], "any", false, false, false, 210)]), "html", null, true);
            echo "\">
                            <i class=\"fa fa-edit\"></i> Editar
                        </a>
            ";
        }
        // line 214
        echo "            <ul class=\"simple-card-list mb-xlg\">
                <li class=\"primary\">
                    <h3>";
        // line 216
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["avaliador"] ?? null), "candidatos", [], "any", false, false, false, 216), "html", null, true);
        echo "</h3>
                    <p>Candidatos</p>
                </li>
                <li class=\"bg-secondary\">
                    <h3>";
        // line 220
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["avaliador"] ?? null), "avaliados", [], "any", false, false, false, 220), "html", null, true);
        echo "
                        de ";
        // line 221
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["avaliador"] ?? null), "candidatos", [], "any", false, false, false, 221), "html", null, true);
        echo "</h3>
                    <p>Homologações</p>
                </li>
                <li class=\"bg-tertiary\">
                    <h3>";
        // line 225
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["avaliador"] ?? null), "entrevistados", [], "any", false, false, false, 225), "html", null, true);
        echo "</h3>
                    <p>Entrevistas</p>
                </li>
                <li class=\"bg-quartenary\">
                    <h3>  ";
        // line 229
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["avaliador"] ?? null), "recursos", [], "any", false, false, false, 229), "html", null, true);
        echo "</h3>
                    <p>Recursos</p>
                </li>
            </ul>
            <h4 class=\"mb-md\"><b>Polos Vinculados</b></h4>
            <ul class=\"simple-bullet-list mb-xlg\">
                ";
        // line 235
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "polos", [], "any", false, false, false, 235)) == 0)) {
            // line 236
            echo "                    <li class=\"red\">
                        <span class=\"title\">Sem vinculo</span>
                        <span class=\"description truncate\">Este usuário não está vinculado a nenhuma cidade</span>
                    </li>
                ";
        } else {
            // line 241
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "polos", [], "any", false, false, false, 241));
            foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
                // line 242
                echo "                        <li></li>
                        <li class=\"blue\">
                            <span class=\"title\">";
                // line 244
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "descricao", [], "any", false, false, false, 244), "html", null, true);
                echo "</span>
                            <span class=\"description truncate\">
                             ";
                // line 246
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["e"], "idUsuario", [], "any", false, false, false, 246));
                foreach ($context['_seq'] as $context["_key"] => $context["up"]) {
                    // line 247
                    echo "                                 ";
                    if ((twig_get_attribute($this->env, $this->source, $context["up"], "nome", [], "any", false, false, false, 247) != twig_get_attribute($this->env, $this->source, ($context["usuario"] ?? null), "nome", [], "any", false, false, false, 247))) {
                        // line 248
                        echo "                                 <a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_perfil", ["id" => twig_get_attribute($this->env, $this->source, $context["up"], "id", [], "any", false, false, false, 248)]), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["up"], "nome", [], "any", false, false, false, 248), "html", null, true);
                        echo "</a>
                                 ";
                    }
                    // line 250
                    echo "                             ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['up'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 251
                echo "                        </span>
                        </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 254
            echo "                ";
        }
        // line 255
        echo "            </ul>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "NteUsuarioBundle:Usuario:perfil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  525 => 255,  522 => 254,  514 => 251,  508 => 250,  500 => 248,  497 => 247,  493 => 246,  488 => 244,  484 => 242,  479 => 241,  472 => 236,  470 => 235,  461 => 229,  454 => 225,  447 => 221,  443 => 220,  436 => 216,  432 => 214,  425 => 210,  422 => 209,  420 => 208,  415 => 205,  398 => 190,  395 => 189,  386 => 185,  376 => 181,  372 => 179,  365 => 174,  363 => 173,  357 => 169,  355 => 168,  349 => 164,  347 => 163,  341 => 159,  339 => 158,  333 => 154,  331 => 153,  326 => 150,  324 => 149,  317 => 147,  313 => 146,  305 => 142,  298 => 137,  296 => 136,  290 => 132,  288 => 131,  282 => 127,  280 => 126,  274 => 122,  272 => 121,  266 => 117,  264 => 116,  259 => 113,  257 => 112,  251 => 111,  248 => 110,  244 => 109,  231 => 99,  223 => 95,  218 => 94,  216 => 93,  204 => 84,  199 => 82,  194 => 79,  188 => 77,  184 => 75,  182 => 74,  172 => 66,  163 => 64,  159 => 63,  153 => 59,  147 => 58,  143 => 56,  141 => 55,  137 => 53,  131 => 51,  127 => 49,  125 => 48,  120 => 46,  116 => 45,  90 => 22,  85 => 20,  77 => 15,  70 => 10,  67 => 9,  64 => 8,  58 => 6,  55 => 5,  52 => 4,  46 => 3,  36 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteUsuarioBundle:Usuario:perfil.html.twig", "/var/www/narfi/src/Nte/UsuarioBundle/Resources/views/Usuario/perfil.html.twig");
    }
}
