<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteConfigMoodleBundle:Default:form.html.twig */
class __TwigTemplate_19fbe52864cdaa28ef124b1d2b053ac682d56371d0a77e1ad345aa053660c817 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
<header class=\"panel-heading\">
    <div class=\"panel-actions\">
        <a href=\"#\" class=\"panel-action panel-action-toggle\" data-panel-toggle></a>
        <a href=\"#\" class=\"panel-action panel-action-dismiss\" data-panel-dismiss></a>
    </div>
    <h2 class=\"panel-title\">";
        // line 7
        echo twig_escape_filter($this->env, ($context["titulo"] ?? null), "html", null, true);
        echo "</h2>
</header>
<div class=\"panel-body\">

    <div class=\"row\">
        <div class=\"col-md-6\">
            ";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nome", [], "any", false, false, false, 13), 'row');
        echo "
        </div>
        <div class=\"col-md-6\">
            ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "url", [], "any", false, false, false, 16), 'row');
        echo "
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-4\">
            ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "servico", [], "any", false, false, false, 21), 'row');
        echo "
        </div>
        <div class=\"col-md-4\">
            ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "usuario", [], "any", false, false, false, 24), 'row');
        echo "
        </div>
        <div class=\"col-md-4\">
            <label>Senha</label>
            <div class=\"controls\">
                <input id=\"passwd\" type=\"password\" class=\" senha form-control\" placeholder=\"***********\" value=\"webservicenteuab2017\">
            </div>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            ";
        // line 35
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "token", [], "any", false, false, false, 35), 'row');
        echo "
            ";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "descricao", [], "any", false, false, false, 36), 'row');
        echo "
            ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "valido", [], "any", false, false, false, 37), 'row');
        echo "
        </div>
    </div>
</div>
<div class=\"panel-footer text-right\">
    ";
        // line 42
        if ((isset($context["delete_form"]) || array_key_exists("delete_form", $context))) {
            // line 43
            echo "        <div class=\"pull-left\" id=\"y\"></div>
    ";
        }
        // line 45
        echo "    <div class=\"btn-group\">
        ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "capturatoken", [], "any", false, false, false, 46), 'row');
        echo "
        ";
        // line 47
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "salvar", [], "any", false, false, false, 47), 'row');
        echo "
    </div>

</div>
";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
";
        // line 52
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "

";
        // line 54
        if ((isset($context["delete_form"]) || array_key_exists("delete_form", $context))) {
            // line 55
            echo "    <div id=\"x\">
        ";
            // line 56
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["delete_form"] ?? null), 'form');
            echo "
    </div>
";
        }
        // line 59
        echo "<style>
    table tr td:nth-child(2) {
        background: #f3f3f3;
        text-align: center;
        font-weight: bold;;
    }

    .checkbox-custom.checkbox-default {
        display: table-cell;
        float: left;
        margin-right: 15px;
    }
</style>
<script>
    ";
        // line 73
        $this->loadTemplate(":js:_form_validador.js.twig", "NteConfigMoodleBundle:Default:form.html.twig", 73)->display(twig_array_merge($context, ["tipo" => 0]));
        // line 74
        echo "    \$('#y').html(\$('#x'));
    \$('.btn-capturar-token').unbind('click');
    \$('.btn-capturar-token').click(function () {
        \$.ajax({
            type: 'POST',
            url: '";
        // line 79
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_capturar_token");
        echo "',
            data: {
                usuario: \$(\"#config_usuario\").val(),
                senha: \$('.senha').val(),
                servico: \$('#config_servico').val(),
                url: \$('#config_url').val()
            },
            complete: function (data) {
                if(typeof (data.responseJSON.error) != 'undefined'){
                    new PNotify({
                        title:\"Erro na captura do Token\",
                        text: data.responseJSON.error,
                        type:\"error\"
                    });
                }else{
                    if(typeof (data.responseJSON.token) != 'undefined'){
                        new PNotify({
                            title:\"Token capturado\",
                            text: \"O token (\" + data.responseJSON.token + \") foi capturado com sucesso!\",
                            type:\"success\"
                        });
                        \$('#config_token').val(data.responseJSON.token);
                    }
                }
            }
        });
    });


</script>";
    }

    public function getTemplateName()
    {
        return "NteConfigMoodleBundle:Default:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 79,  161 => 74,  159 => 73,  143 => 59,  137 => 56,  134 => 55,  132 => 54,  127 => 52,  123 => 51,  116 => 47,  112 => 46,  109 => 45,  105 => 43,  103 => 42,  95 => 37,  91 => 36,  87 => 35,  73 => 24,  67 => 21,  59 => 16,  53 => 13,  44 => 7,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteConfigMoodleBundle:Default:form.html.twig", "/var/www/narfi/src/Nte/Config/MoodleBundle/Resources/views/Default/form.html.twig");
    }
}
