<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAdminPainelBundle:pablo:pabloeditarpessoa.html.twig */
class __TwigTemplate_12031d7536d53e519d93cdc8983dbcea4620af2ef3f671fed61812678ab21e3f extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAdminPainelBundle:pablo:pabloeditarpessoa.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Dashboard";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Painel ";
    }

    // line 5
    public function block_conteudo($context, array $blocks = [])
    {
        // line 6
        echo "
    <div class=\"row\">

        <div class=\"col-md-8 col-lg-6\">
            <div class=\"tabs\">
                <ul class=\"nav nav-tabs tabs-primary\">
                    <li class='active'>
                        <a href=\"#editar\"  data-toggle=\"tab\">Editar ";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoa"] ?? null), "nome", [], "any", false, false, false, 13), "html", null, true);
        echo "</a>
                    </li>
                </ul>
                <div class=\"tab-content\">
                    <div id=\"editar\" class=\"tab-pane active\">
                        ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
                        <h4 class=\"mb-xlg\">Informações pessoais</h4>
                        <fieldset>
                            ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "Nome", [], "any", false, false, false, 21), 'row');
        echo "
                            ";
        // line 22
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "Email", [], "any", false, false, false, 22), 'row');
        echo "
                            ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "Cpf", [], "any", false, false, false, 23), 'row');
        echo "
                            ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "Tipo", [], "any", false, false, false, 24), 'row');
        echo "

                        </fieldset>
                        <hr class=\"dotted tall\">
                        <div class=\"panel-footer\">
                            <div class=\"row\">
                                <div class=\"col-md-9 col-md-offset-3\">

                                </div>
                            </div>
                        </div>
                        ";
        // line 35
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "NteAdminPainelBundle:pablo:pabloeditarpessoa.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 35,  96 => 24,  92 => 23,  88 => 22,  84 => 21,  78 => 18,  70 => 13,  61 => 6,  58 => 5,  52 => 3,  46 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAdminPainelBundle:pablo:pabloeditarpessoa.html.twig", "/var/www/narfi/src/Nte/Admin/PainelBundle/Resources/views/pablo/pabloeditarpessoa.html.twig");
    }
}
