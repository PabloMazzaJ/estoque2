<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* AlmoxarifadoBundle:Default:index.html.twig */
class __TwigTemplate_42512b480e13024afa15b2141f6981011f8450014d2bef1154d8a5f6a1d07e47 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "almoxerifado home page!";
    }

    public function getTemplateName()
    {
        return "AlmoxarifadoBundle:Default:index.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "AlmoxarifadoBundle:Default:index.html.twig", "/var/www/narfi/src/Nte/Aplicacao/AlmoxarifadoBundle/Resources/views/Default/index.html.twig");
    }
}
