<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoBuscadorBundle:Pessoas:_contato.html.twig */
class __TwigTemplate_f75b120858ba21c711f636e47586c0a0002ab65933543bc2db7cae20bce700c9 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"col-sm-12 col-md-6\">
    <section class=\"panel\">
        <header class=\"panel-heading\">
            <h2 class=\"panel-title\">
                <span class=\"va-middle\">Informações de contato</span>
            </h2>
        </header>
        <div class=\"panel-body\">
            <div class=\"content\">
                <table class=\"table table-responsive\">
                    <tr>
                        <th width=\"50%\">E-Mail</th>
                        <td>
                            <ul class=\"list-unstyled\">";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "enderecos", [], "any", false, false, false, 14));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 15
            echo "                                    <li>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "email", [], "any", false, false, false, 15), "html", null, true);
            echo "</li>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "</ul>
                        </td>
                    </tr>
                    <tr>
                        <th width=\"50%\">Telefone</th>
                        <td>
                            <ul class=\"list-unstyled\">";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "enderecos", [], "any", false, false, false, 21));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 22
            echo "                                    <li>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "telefoneCelular", [], "any", false, false, false, 22), "html", null, true);
            echo "</li>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "</ul>
                        </td>
                    </tr>
                    <tr>
                        <th width=\"50%\">Cidade</th>
                        <td>
                            <ul class=\"list-unstyled\">
                                ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "enderecos", [], "any", false, false, false, 29));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 30
            echo "                                    <li>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["e"], "cidade", [], "any", false, false, false, 30), "nome", [], "any", false, false, false, 30), "html", null, true);
            echo "</li>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th width=\"50%\">Estado</th>
                        <td>
                            <ul class=\"list-unstyled\">";
        // line 37
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "enderecos", [], "any", false, false, false, 37));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 38
            echo "                                    <li>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["e"], "estado", [], "any", false, false, false, 38), "descricao", [], "any", false, false, false, 38), "html", null, true);
            echo "</li>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "</ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
</div>";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoBuscadorBundle:Pessoas:_contato.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 38,  109 => 37,  101 => 31,  93 => 30,  89 => 29,  73 => 22,  69 => 21,  54 => 15,  50 => 14,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoBuscadorBundle:Pessoas:_contato.html.twig", "/var/www/narfi/src/Nte/Aplicacao/BuscadorBundle/Resources/views/Pessoas/_contato.html.twig");
    }
}
