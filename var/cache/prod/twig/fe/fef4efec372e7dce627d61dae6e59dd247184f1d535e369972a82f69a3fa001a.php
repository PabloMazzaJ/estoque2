<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAdminPainelBundle:Default:index.html.twig */
class __TwigTemplate_af401c51a7949e0aa178e84f666f523fb83f6cdbca5ced3e817b599acf234388 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'conteudo' => [$this, 'block_conteudo'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts3' => [$this, 'block_javascripts3'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAdminPainelBundle:Default:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Dashboard";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Painel ";
    }

    // line 4
    public function block_conteudo($context, array $blocks = [])
    {
        // line 5
        echo "
    <div class=\"col-md-6 col-lg-6 col-xl-6\">
        <div class=\"row\">
            <div class=\"col-md-4 col-lg-4 col-xl-4\">
                <section class=\"panel panel-featured-left panel-featured-primary\">
                    <div class=\"panel-body\">
                        <div class=\"widget-summary widget-summary-sm\">
                            <div class=\"widget-summary-col widget-summary-col-icon\">
                                <div class=\"summary-icon bg-primary\">
                                    <i class=\"fa fa-graduation-cap\"></i>
                                </div>
                            </div>
                            <div class=\"widget-summary-col\">
                                <div class=\"summary\">
                                    <h4 class=\"title\">Cursos</h4>
                                    <div class=\"info\">
                                        <strong class=\"amount\">";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dados"] ?? null), "curso", [], "any", false, false, false, 21), "html", null, true);
        echo "</strong>
                                        ";
        // line 22
        if ((twig_get_attribute($this->env, $this->source, ($context["dados"] ?? null), "cursoNovo", [], "any", false, false, false, 22) > 0)) {
            // line 23
            echo "                                            <span class=\"text-primary\">(";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dados"] ?? null), "cursoNovo", [], "any", false, false, false, 23), "html", null, true);
            echo " Novos)</span>
                                        ";
        }
        // line 25
        echo "                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class=\"col-md-4 col-lg-4 col-xl-4\">
                <section class=\"panel panel-featured-left panel-featured-primary\">
                    <div class=\"panel-body\">
                        <div class=\"widget-summary widget-summary-sm\">
                            <div class=\"widget-summary-col widget-summary-col-icon\">
                                <div class=\"summary-icon bg-primary\">
                                    <i class=\"fa fa-th-large\"></i>
                                </div>
                            </div>
                            <div class=\"widget-summary-col\">
                                <div class=\"summary\">
                                    <h4 class=\"title\">Disciplinas</h4>
                                    <div class=\"info\">
                                        <strong class=\"amount\">";
        // line 45
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dados"] ?? null), "disciplina", [], "any", false, false, false, 45), "html", null, true);
        echo "</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class=\"col-md-4 col-lg-4 col-xl-4\">
                <section class=\"panel panel-featured-left panel-featured-primary\">
                    <div class=\"panel-body\">
                        <div class=\"widget-summary widget-summary-sm\">
                            <div class=\"widget-summary-col widget-summary-col-icon\">
                                <div class=\"summary-icon bg-primary\">
                                    <i class=\"fa fa-th\"></i>
                                </div>
                            </div>
                            <div class=\"widget-summary-col\">
                                <div class=\"summary\">
                                    <h4 class=\"title\">Turmas</h4>
                                    <div class=\"info\">
                                        <strong class=\"amount\">";
        // line 66
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dados"] ?? null), "turma", [], "any", false, false, false, 66), "html", null, true);
        echo "</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class=\"col-md-4 col-lg-4 col-xl-4\">
                <section class=\"panel panel-featured-left panel-featured-secondary\">
                    <div class=\"panel-body\">
                        <div class=\"widget-summary widget-summary-sm\">
                            <div class=\"widget-summary-col widget-summary-col-icon\">
                                <div class=\"summary-icon bg-secondary\">
                                    <i class=\"fa fa-user\"></i>
                                </div>
                            </div>
                            <div class=\"widget-summary-col\">
                                <div class=\"summary\">
                                    <h4 class=\"title\">Usuários</h4>
                                    <div class=\"info\">
                                        <strong class=\"amount\">";
        // line 88
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dados"] ?? null), "usuario", [], "any", false, false, false, 88), "html", null, true);
        echo "</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class=\"col-md-4 col-lg-4 col-xl-4\">
                <section class=\"panel panel-featured-left panel-featured-tertiary\">
                    <div class=\"panel-body\">
                        <div class=\"widget-summary widget-summary-sm\">
                            <div class=\"widget-summary-col widget-summary-col-icon\">
                                <div class=\"summary-icon bg-tertiary\">
                                    <i class=\"fa fa-user-plus\"></i>
                                </div>
                            </div>
                            <div class=\"widget-summary-col\">
                                <div class=\"summary\">
                                    <h4 class=\"title\">Matriculas</h4>
                                    <div class=\"info\">
                                        <strong class=\"amount\">";
        // line 109
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dados"] ?? null), "inscricao", [], "any", false, false, false, 109), "html", null, true);
        echo "</strong>
                                        ";
        // line 110
        if ((twig_get_attribute($this->env, $this->source, ($context["dados"] ?? null), "inscricaoNovo", [], "any", false, false, false, 110) > 0)) {
            // line 111
            echo "                                            <span class=\"text-primary\">(";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dados"] ?? null), "inscricaoNovo", [], "any", false, false, false, 111), "html", null, true);
            echo " Novos)</span>
                                        ";
        }
        // line 113
        echo "                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class=\"col-md-4 col-lg-4 col-xl-4\">
                <section class=\"panel panel-featured-left panel-featured-quartenary\">
                    <div class=\"panel-body\">
                        <div class=\"widget-summary widget-summary-sm\">
                            <div class=\"widget-summary-col widget-summary-col-icon\">
                                <div class=\"summary-icon bg-quartenary\">
                                    <i class=\"fa fa-user-times\"></i>
                                </div>
                            </div>
                            <div class=\"widget-summary-col\">
                                <div class=\"summary\">
                                    <h4 class=\"title\">Desmatriculas</h4>
                                    <div class=\"info\">
                                        <strong class=\"amount\">";
        // line 133
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dados"] ?? null), "desinscricao", [], "any", false, false, false, 133), "html", null, true);
        echo "</strong>
                                        ";
        // line 134
        if ((twig_get_attribute($this->env, $this->source, ($context["dados"] ?? null), "desinscricaoNovo", [], "any", false, false, false, 134) > 0)) {
            // line 135
            echo "                                            <span class=\"text-primary\">(";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dados"] ?? null), "desinscricaoNovo", [], "any", false, false, false, 135), "html", null, true);
            echo " Novos)</span>
                                        ";
        }
        // line 137
        echo "                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class=\"col-md-6 col-lg-6 col-xl-6\">
        <section class=\"panel\">
            <div class=\" panel-body text-center\">
                <div class=\"row\">
                    <div class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12 \">
                        <a href=\"";
        // line 151
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_aplicacao_exportador_homepage");
        echo "\">
                            <div class=\"featured-box featured-box-primary featured-box-effect-3\" style=\"height: 168px;\">
                                <div class=\"box-content\">
                                    <i class=\"icon-featured fa fa-cloud-download fa-5x\"></i>
                                    <h4>Exportar Cursos</h4>
                                    <p>Exporta um curso inteiro para moodle</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12\">
                        <a href=\"";
        // line 162
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("buscador_pessoas_index");
        echo "\">
                            <div class=\"featured-box featured-box-primary featured-box-effect-3\" style=\"height: 168px;\">
                                <div class=\"box-content\">
                                    <i class=\"icon-featured fa fa-users fa-5x\"></i>
                                    <h4>Buscar Pessoas</h4>
                                    <p>Busca pessoas cadastradas no sie</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12\">
                        <a href=\"";
        // line 173
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_index");
        echo "\">
                            <div class=\"featured-box featured-box-primary featured-box-effect-3\">
                                <div class=\"box-content\">
                                    <i class=\"icon-featured fa fa-bar-chart fa-5x\"></i>
                                    <h4>Relatórios</h4>
                                    <p>Visualiza relatório e estátisticas do sistema</p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12\">
                        <a href=\"";
        // line 185
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_base_homepage");
        echo "\">
                            <div class=\"featured-box featured-box-primary featured-box-effect-3\">
                                <div class=\"box-content\">
                                    <i class=\"icon-featured fa fa-cogs fa-5x\"></i>
                                    <h4>Configurações</h4>
                                    <p>Gerencia as configurações do sistema</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class=\"col-md-6 col-lg-6 col-xl-6\">
        <section class=\"panel\">
            <header class=\"panel-heading\">
                <h2 class=\"panel-title\">
                    <span class=\"va-middle\">Últimas tentativas de  exportação</span>
                </h2>
            </header>
            <div class=\"panel-body\">
                <table class=\"table table-condensed table-striped table-bordered\">
                    <tr>
                        <th>Tentativa</th>
                        <th>Nome do Curso</th>
                    </tr>
                    ";
        // line 213
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rel3"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 214
            echo "                        <tr>
                            <td>
                                <a href=\"";
            // line 216
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "idExportacao", [], "any", false, false, false, 216), "id", [], "any", false, false, false, 216), "uuid" => twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 216)]), "html", null, true);
            echo "\">
                                    ";
            // line 217
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "uuid", [], "any", false, false, false, 217), "d/m/Y H:i:s"), "html", null, true);
            echo "
                                </a>
                            </td>
                            <td>
                                ";
            // line 221
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "idExportacao", [], "any", false, false, false, 221), "status", [], "any", false, false, false, 221) == 99)) {
                // line 222
                echo "                                    <div class=\"label label-success\">Ok</div>
                                ";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 223
$context["r"], "idExportacao", [], "any", false, false, false, 223), "status", [], "any", false, false, false, 223) > 100)) {
                // line 224
                echo "                                    <div class=\"label label-danger\">Erro</div>
                                ";
            } else {
                // line 226
                echo "                                    <div class=\"label label-primary\">Exportando</div>
                                ";
            }
            // line 228
            echo "
                                ";
            // line 229
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "idExportacao", [], "any", false, false, false, 229), "nomeCurso", [], "any", false, false, false, 229), "html", null, true);
            echo "
                            </td>
                        </tr>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 234
        echo "                </table>
            </div>
        </section>
    </div>
    <div class=\"col-md-6\">
        <section class=\"panel\">
            <header class=\"panel-heading\">
                <h2 class=\"panel-title\">Alterações de matriculas no SIE </h2>
            </header>
            <div class=\"panel-body\">
                <div id=\"graficoAltaracaoSie\" class=\"ct-chart ct-perfect-fourth ct-golden-section\"></div>
            </div>
        </section>
    </div>


";
    }

    // line 251
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 252
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 254
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 255
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script src=\"";
        // line 256
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/highcharts/highcharts.js"), "html", null, true);
        echo "\"></script>
    <script>
        Highcharts.chart('graficoAltaracaoSie', {
            chart: {type: 'area'},
            title: { text: '' },
            xAxis: {
                categories: ";
        // line 262
        echo json_encode(($context["gLabels"] ?? null));
        echo ",
                tickmarkPlacement: 'on',
                title: { enabled: false}
            },
            yAxis: {
                title: {
                    text: 'Quantitativo de Alterações'
                },
                labels: {
                   /* formatter: function () {
                        return this.value / 1000;
                    }

                    */
                }
            },
            tooltip: {
                split: true,
              //  valueSuffix: ' Alterações'
            },
            plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '#666666',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#666666'
                    }
                }
            },
            series: ";
        // line 293
        echo json_encode(($context["gSeries"] ?? null));
        echo "
        });

        Highcharts.chart('graficoMatriculas', {
            chart: {type: 'area'},
            title: { text: '' },
            xAxis: {
                categories: ";
        // line 300
        echo json_encode(($context["gMLabels"] ?? null));
        echo ",
                tickmarkPlacement: 'on',
                title: { enabled: false}
            },
            yAxis: {
                title: {
                    text: 'Quantitativo de Alterações'
                },
                labels: {
                    /* formatter: function () {
                         return this.value / 1000;
                     }

                     */
                }
            },
            tooltip: {
                split: true,
                //  valueSuffix: ' Alterações'
            },
            plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '#666666',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#666666'
                    }
                }
            },
            series: ";
        // line 331
        echo json_encode(($context["gMSeries"] ?? null));
        echo "
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "NteAdminPainelBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  497 => 331,  463 => 300,  453 => 293,  419 => 262,  410 => 256,  405 => 255,  402 => 254,  395 => 252,  392 => 251,  372 => 234,  361 => 229,  358 => 228,  354 => 226,  350 => 224,  348 => 223,  345 => 222,  343 => 221,  336 => 217,  332 => 216,  328 => 214,  324 => 213,  293 => 185,  278 => 173,  264 => 162,  250 => 151,  234 => 137,  228 => 135,  226 => 134,  222 => 133,  200 => 113,  194 => 111,  192 => 110,  188 => 109,  164 => 88,  139 => 66,  115 => 45,  93 => 25,  87 => 23,  85 => 22,  81 => 21,  63 => 5,  60 => 4,  54 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAdminPainelBundle:Default:index.html.twig", "/var/www/narfi/src/Nte/Admin/PainelBundle/Resources/views/Default/index.html.twig");
    }
}
