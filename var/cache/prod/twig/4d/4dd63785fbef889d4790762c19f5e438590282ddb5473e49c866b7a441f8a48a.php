<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:Relatorio:turmasParticipantes.html.twig */
class __TwigTemplate_4af96fcbe4eecea229d87039cdd44f4e74b3d841e4e75da1a0dadf59dff27222 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rel"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 2
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "disciplina", [], "any", false, false, false, 2), "turma", [], "any", false, false, false, 2));
            foreach ($context['_seq'] as $context["_key"] => $context["turma"]) {
                // line 3
                echo "        <div class=\"col-md-6\">
            <div class=\"panel\">
                <header class=\"panel-heading\">
                    <h2 class=\"panel-title\">";
                // line 6
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["turma"], "disciplina", [], "any", false, false, false, 6), "html", null, true);
                echo "</h2>
                    <p class=\"panel-subtitle\">";
                // line 7
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["turma"], "codigo", [], "any", false, false, false, 7), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "curso", [], "any", false, false, false, 7), "html", null, true);
                echo "</p>
                </header>
                <div class=\"panel-body\">
                    <table class=\"table table-bordered table-striped\">
                        <tr>
                            <th width=\"40\">Papel</th>
                            <th width=\"55\">Matricula</th>
                            <th>Nome</th>
                        </tr>
                        ";
                // line 16
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["turma"], "participante", [], "any", false, false, false, 16));
                foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                    // line 17
                    echo "                            <tr ";
                    if (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["p"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["valido"] ?? null) : null) == 0)) {
                        echo " class=\"customizado-vermelho1\" ";
                    }
                    echo ">
                                <td>";
                    // line 18
                    echo twig_escape_filter($this->env, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["p"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["papel"] ?? null) : null), "html", null, true);
                    echo "</td>
                                <td>";
                    // line 19
                    echo twig_escape_filter($this->env, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["p"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["matricula"] ?? null) : null), "html", null, true);
                    echo "</td>
                                <td>";
                    // line 20
                    echo twig_escape_filter($this->env, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["p"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["nomeCivil"] ?? null) : null), "html", null, true);
                    echo "</td>
                            </tr>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 23
                echo "                    </table>
                </div>
            </div>
        </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['turma'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:Relatorio:turmasParticipantes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 23,  86 => 20,  82 => 19,  78 => 18,  71 => 17,  67 => 16,  53 => 7,  49 => 6,  44 => 3,  39 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:Relatorio:turmasParticipantes.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/Relatorio/turmasParticipantes.html.twig");
    }
}
