<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_cria_vinculo.html.twig */
class __TwigTemplate_4375ba7f6f95e21d12ea2a2f48538b38957142ec35dbe4c1dd053aee7d88184d extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_cria_vinculo.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Pessoas - Contrato ";
    }

    // line 3
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 4
        echo "     <li><span><a href=\"#\">Pessoal Externo</a></span></li>
     <li><span><a href=\"#\">Pessoas - Contrato</a></span></li>
 ";
    }

    // line 7
    public function block_conteudo($context, array $blocks = [])
    {
        // line 8
        echo "
    <div id=\"container-principal\" class=\"panel-body flexbox\">

        <div id=\"container-esquerda\">
            <header class=\"panel-heading center\">
                <h2 class=\"panel-title\">
                    <span class=\"va-middle\">Dados do Vínculo</span>
                </h2>
            </header>

            <div id=\"container-inputs\">

                <label for=\"contrato-input\" class=\"text-weight-bold\">Edital</label>
                <input name=\"contrato-input\" class=\"contrato-input-class\">
                <label for=\"pessoa-input\" class=\"text-weight-bold\">Pessoa</label>
                <input name=\"pessoa-input\" class=\"pessoa-input-class\">

                <div id=\"container-left-buttons\">
                    <button id=\"adicionar\" class=\"btn btn-primary\">Adicionar</button>
                    <button id=\"cancelar\" class=\"btn btn-secondary\">Cancelar</button>
                </div>

            </div>

        </div>

        <div class=\"headerDivider\"></div>

        <div id=\"container-direita\">
            <header class=\"panel-heading center\">
                <h2 class=\"panel-title\">
                    <span class=\"va-middle\">Pessoas a serem vinculadas</span>
                </h2>
            </header>
            <table class=\"table table-striped\">
                <tr class=\"table-container\"></tr>
            </table>
            <button id=\"btn-send-data\" class=\"btn btn-primary\" style=\"float: right\">Vincular</button>
        </div>

    </div>

";
    }

    // line 52
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 53
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/vcom.js"), "html", null, true);
        echo "\"></script>
    <script>

        function get_center_pos(width, top) {
            if (!top) {
                top = 30;
                \$('.ui-pnotify').each(function () {
                    top += \$(this).outerHeight() + 20;
                });
            }
            return {\"top\": top, \"left\": (\$(window).width() / 2) - (width / 2)}
        }

        let myTable = {};
        let btnSendTableData = document.getElementById('btn-send-data');

        let formValues = {
            edital: '',
            editalId: '',
            pessoaId: '',
            nome: ''
        };

        let contratoAutocomplete = {};
        let pessoaAutocomplete = {};

        \$(document).ready(function () {

            initTable();
            buscarPessoas();
            buscarContratos();

            const btnAdicionar = document.getElementById('adicionar');

            btnAdicionar.addEventListener('click', evt => {
                console.log(formValues);
                myTable.add(Object.assign({}, formValues));
                formValues = {
                    edital: formValues.edital,
                    editalId: formValues.editalId,
                    pessoaId: '',
                    nome: ''
                };
                //reseta inputs.
                // contratoAutocomplete.reset();
                pessoaAutocomplete.reset();
            });

            btnSendTableData.addEventListener('click', evt => {
                let tableData = myTable.getModel();
                console.log(myTable.getModel());
                vincularPessoaContrato(tableData)
            });

            \$(window).resize(function () {
                \$(\".ui-pnotify\").each(function () {
                    \$(this).css(get_center_pos(\$(this).width(), \$(this).position().top))
                });
            });
        });

        function vincularPessoaContrato(data) {
            \$.ajax({
                url: \"";
        // line 117
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_vincular_pessoa_contrato_table_data");
        echo "\",
                dataType: 'json',
                method: \"POST\",
                data: {
                    dataKey: data
                },
                success: function (a) {
                    console.log('retorno func');
                    console.log(a);
                },
                error: function (a) {
                }
            });
        }

        function initTable() {

            let container = document.querySelector('.table-container');

            myTable = vcom.table(
                /*value*/      ['edital', 'nome',],
                /*title*/      ['Edital', 'Pessoa',]
            ).setModel([])  //  configura qual o array que será usado para popular a tabela
                .setActionButtons([    //  configura os botões em todas as linhas
                    {'action': 'remover', 'innerHTML': '<i class=\"fa fa-trash\"></i>'}
                ])
                .setActionCallback((evt) => {
                    // função para lidar com qualquer ação que aconteça na tabela
                    console.log(evt);
                    if (evt.action === 'remover') myTable.remove(evt.index);
                })
                .setChecklist(true);

            // myTable.add({editalId: 99, edital: 'coisa', pessoaId: 99, nome: 'pablo'});

            const myTableHTML = myTable.mount();

            container.appendChild(myTableHTML); //\tadiciona a tabela em algum container
        }

        function buscarContratos() {

            let elemento = document.querySelector('.contrato-input-class');
            contratoAutocomplete = vcom.autocomplete(elemento)

                .setSource('";
        // line 162
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_busca_contratos");
        echo "')
                // .setInitialValue('')
                .setPlaceholder('Selecione um Edital')
                .setQueryBuilder(term => {
                    return {editalNumero: term}
                })
                .setResponseHandler(item => {
                    return [item['id'], item['editalNumero']];
                })
                .setOnValueChange(value => {
                    formValues['edital'] = value[2]['editalNumero'];
                    formValues['editalId'] = value[2]['id'];
                })
                .mount();
        }

        function buscarPessoas() {

            let elemento = document.querySelector('.pessoa-input-class');
            pessoaAutocomplete = vcom.autocomplete(elemento)
                .setSource('";
        // line 182
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_busca_pessoas");
        echo "')
                // .setInitialValue('')
                .setPlaceholder('Selecione uma Pessoa')
                .setQueryBuilder(term => {
                    return {nome: term, valido: 1}
                })
                .setResponseHandler(item => {
                    return [item['id'], item['nome']];
                })
                .setOnValueChange(value => {
                    formValues['nome'] = value[2]['nome'];
                    formValues['pessoaId'] = value[2]['id'];
                })
                .mount();

        }

        function notificarUsuarioPnotify(title, text, type) {
            new PNotify(
                {
                    title: title,
                    text: text,
                    type: type,
                    before_open: function (PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    }
                }
            );
        }

    </script>
";
    }

    // line 215
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 216
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/vcom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <style>

        #container-left-buttons {
            margin-top: 1em;
        }

        .headerDivider {
            border-left: 1px solid #D7D7D7;
            border-right: 1px solid #D7D7D7;
            margin: 1em;

        }

        #container-principal {
            display: flex;
        }

        #container-esquerda {
            height: 40vw;
            width: 25%;
        }

        #container-direita {
            height: 40vw;
            width: 75%;
        }

        th {
            text-align: center;
        }

        td {
            text-align: center;
        }


        .mfp-close {
            visibility: hidden;
        }

        #inputPlaceholder {
            width: 50%;
            height: 6em;
            margin-left: 7em;
        }


    </style>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_cria_vinculo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  301 => 217,  296 => 216,  293 => 215,  257 => 182,  234 => 162,  186 => 117,  120 => 54,  115 => 53,  112 => 52,  66 => 8,  63 => 7,  57 => 4,  54 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_cria_vinculo.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntepessoa/pessoa_cria_vinculo.html.twig");
    }
}
