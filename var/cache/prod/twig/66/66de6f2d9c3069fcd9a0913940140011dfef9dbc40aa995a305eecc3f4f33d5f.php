<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ::menu.admin2.html.twig */
class __TwigTemplate_eea200b53614bf0917ccaa5e6ad99d2949a3783fc421140fc899d2f4600e0875 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<aside id=\"sidebar-right\" class=\"sidebar-right\">
    <div class=\"nano\">
        <div class=\"nano-content\">
            <a href=\"#\" class=\"mobile-close visible-xs\">
                Collapse <i class=\"fa fa-chevron-right\"></i>
            </a>

            <div class=\"sidebar-right-wrapper\">
<!--
                <div class=\"sidebar-widget widget-calendar\">
                    <h6>Meus Agendamentos</h6>

                    <div data-plugin-datepicker data-plugin-skin=\"dark\"></div>
                    ";
        // line 14
        echo "{ render(controller('HinkelmannAgendaBundle:Pagina:meusAgendamentos'))}}

                </div>
-->

                <div class=\"sidebar-widget widget-friends\">
                    <h6>Usuários</h6>

                    ";
        // line 23
        echo "
                </div>

            </div>
        </div>
    </div>
</aside>

";
    }

    public function getTemplateName()
    {
        return "::menu.admin2.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  60 => 23,  50 => 14,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "::menu.admin2.html.twig", "/var/www/narfi/app/Resources/views/menu.admin2.html.twig");
    }
}
