<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* AlmoxarifadoBundle:Requisicao:importa-csv-produtos.html.twig */
class __TwigTemplate_b7da037d5747afae36557e826364a2f2b57135fdb884c0a774e89b14ea5b7ab8 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "AlmoxarifadoBundle:Requisicao:importa-csv-produtos.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
    }

    // line 3
    public function block_conteudo($context, array $blocks = [])
    {
        // line 4
        echo "
    carregar csv

";
    }

    // line 9
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 10
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>

        (function () {

            const model = {
                produtos: [],
            };

            const controller = {

                init() {
                    view.init();
                },

                setValorModel(nome, valor) {
                    model[nome] = valor;
                },

                imprimeModel() {
                    console.table(model);
                },
            };

            const view = {

                init() {
                    this.inicializarReferenciaHtml();
                    this.inicializarEventListeners();
                },

                inicializarReferenciaHtml() {

                },

                inicializarEventListeners() {


                },
            };
            controller.init();
        }());


    </script>
";
    }

    // line 57
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 58
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <style>




    </style>
";
    }

    public function getTemplateName()
    {
        return "AlmoxarifadoBundle:Requisicao:importa-csv-produtos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 58,  116 => 57,  65 => 10,  62 => 9,  55 => 4,  52 => 3,  47 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "AlmoxarifadoBundle:Requisicao:importa-csv-produtos.html.twig", "/var/www/narfi/src/Nte/Aplicacao/AlmoxarifadoBundle/Resources/views/Requisicao/importa-csv-produtos.html.twig");
    }
}
