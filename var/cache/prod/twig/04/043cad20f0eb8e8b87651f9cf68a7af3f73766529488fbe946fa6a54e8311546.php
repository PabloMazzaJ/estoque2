<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_mostra.html.twig */
class __TwigTemplate_9acdc0f823b02b1d0ce2d9ab39b40157890cbfab0abc5f2916d282258319d3f9 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_mostra.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo "Pessoas";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"semUnderline\">Pessoal Externo</a></span></li>
     <li><span><a href=\"#\">Pessoas</a></span></li>
  ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "

    <header class=\"panel-heading\">
        <div>
            <h2 class=\"panel-title\">
                <span class=\"va-middle\"><i class=\"fa fa-users\"></i> Pessoas</span>
            </h2>
        </div>
    </header>
    <div class=\"panel-body\">
        <table class=\"table table-striped\">
            <tr>
                <th class=\"col-md-2\">Nome</th>
                <th class=\"col-md-2\">Sobrenome</th>
                <th class=\"col-md-1\">RG</th>
                <th class=\"col-md-1\">Cpf</th>
                <th class=\"col-md-2\">E-mail</th>
                <th class=\"col-md-1\">Telefone</th>
                <th class=\"col-md-1\">Celular</th>
                <th class=\"col-md-1\">Vínculos</th>
                <th class=\"col-md-1\">Endereço</th>
                <th class=\"col-md-1\">Editar</th>
            </tr>
            ";
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["pessoas"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["pessoa"]) {
            // line 33
            echo "                <tr>
                    <td>";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pessoa"], "nome", [], "any", false, false, false, 34), "html", null, true);
            echo "</td>
                    <td>";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pessoa"], "sobrenome", [], "any", false, false, false, 35), "html", null, true);
            echo "</td>
                    <td>";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pessoa"], "identidade", [], "any", false, false, false, 36), "html", null, true);
            echo "</td>
                    <td>";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pessoa"], "cpf", [], "any", false, false, false, 37), "html", null, true);
            echo "</td>
                    <td>";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pessoa"], "email", [], "any", false, false, false, 38), "html", null, true);
            echo "</td>
                    <td>";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pessoa"], "telefone0", [], "any", false, false, false, 39), "html", null, true);
            echo "</td>
                    <td>";
            // line 40
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pessoa"], "telefone1", [], "any", false, false, false, 40), "html", null, true);
            echo "</td>
                    <td><a href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_tem_vinculo", ["id" => twig_get_attribute($this->env, $this->source, $context["pessoa"], "id", [], "any", false, false, false, 41)]), "html", null, true);
            echo "\"> <i class=\"fa fa-2x fa-adjust vinculo-title\" aria-hidden=\"true\"></i> </a></td>
                    <td><a href=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_mostra_detalhes", ["id" => twig_get_attribute($this->env, $this->source, $context["pessoa"], "id", [], "any", false, false, false, 42)]), "html", null, true);
            echo "\"> <i class=\"fa fa-2x fa-map endereco-title\" aria-hidden=\"true\"></i> </a></td>
                    <td><a href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["pessoa"], "id", [], "any", false, false, false, 43)]), "html", null, true);
            echo "\"><i class=\"fa fa-2x fa-edit edit-title\" aria-hidden=\"true\"></i></a></td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pessoa'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "        </table>

            <a href=\"";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_new");
        echo "\" class=\"btn-gerar-relatorio2\">
                <button title=\"Adicionar Pessoa\" type=\"button\" class=\"btn btn-primary btn-sm\" id=\"expButton\"><i class=\"fa fa-user-plus\"></i></button>
            </a>

    </div>
";
    }

    // line 55
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 56
        echo "
    ";
        // line 57
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>
        \$('.vinculo-title').hover(function() {
            \$(this).attr('title', 'Visualizar os vínculos desta pessoa');
        });

        \$('.edit-title').hover(function() {
            \$(this).attr('title', 'Editar os dados desta pessoa');
        });

        \$('.endereco-title').hover(function() {
            \$(this).attr('title', 'Visualizar o endereço desta pessoa');
        });

        \$('.delete-title').hover(function() {
            \$(this).attr('title', 'Deletar esta pessoa');
        });


    </script>

";
    }

    // line 80
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 81
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>
        .btn {
            min-width: 9em;
            max-width: 9em;
        }

        #detalhes {
            text-decoration:none;
        }

        table th,
        table  {
            text-align: center;
            vertical-align: middle;
        }


    </style>

";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_mostra.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 81,  195 => 80,  169 => 57,  166 => 56,  163 => 55,  153 => 48,  149 => 46,  140 => 43,  136 => 42,  132 => 41,  128 => 40,  124 => 39,  120 => 38,  116 => 37,  112 => 36,  108 => 35,  104 => 34,  101 => 33,  97 => 32,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_mostra.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntepessoa/pessoa_mostra.html.twig");
    }
}
