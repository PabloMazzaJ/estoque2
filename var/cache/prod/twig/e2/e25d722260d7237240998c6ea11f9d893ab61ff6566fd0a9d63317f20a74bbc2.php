<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteUsuarioBundle:Usuario:novo.html.twig */
class __TwigTemplate_ef93fab9ead2cc04c5be15b0645b99962637d2815501885169daff0556a4e6a8 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'javascripts' => [$this, 'block_javascripts'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 44
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => ":forms:form_row.html.twig"], true);
        // line 2
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteUsuarioBundle:Usuario:novo.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Usuário ";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span>Novo Usuários</span></li>
 ";
    }

    // line 7
    public function block_javascripts($context, array $blocks = [])
    {
        // line 8
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/dropzone/dropzone.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/summernote/summernote.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-validation/jquery.validate.js"), "html", null, true);
        echo "\"></script>
    <script>
        Dropzone.options.uploadFoto = {
            dictDefaultMessage: \"Arraste a imagem aqui, para upload\",
            init: function () {
                console.log('inicializando...');
                this.on(\"success\", function (file, responseText) {
                    \$(\"#usuario-imagem\").attr('src', responseText);
                    \$(\"#";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "img", [], "any", false, false, false, 20), "vars", [], "any", false, false, false, 20), "id", [], "any", false, false, false, 20), "html", null, true);
        echo "\").val(responseText);
                    this.removeAllFiles();
                });
            }
        };
        \$(\"#form-user\").validate({
            rules: {
                password: \"required\",
                password_again: {
                    equalTo: \"#password\"
                }
            }
        });
    </script>
";
    }

    // line 35
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 36
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/dropzone/basic.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/dropzone/dropzone.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/summernote/summernote.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/summernote/summernote-bs3.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\"
          href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css"), "html", null, true);
        echo "\">
";
    }

    // line 45
    public function block_conteudo($context, array $blocks = [])
    {
        // line 46
        echo "    <div class=\"row\">
        <div class=\"col-md-4 col-lg-3\">
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <div class=\"thumb-info mb-md\">
                        <form action=\"";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_uploadimagem");
        echo "\" method=\"POST\"
                              class=\"dropzone dz-square\" id=\"upload-foto\">
                            <img src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "img", [], "any", false, false, false, 53), "vars", [], "any", false, false, false, 53), "value", [], "any", false, false, false, 53)), "html", null, true);
        echo "\" class=\"rounded img-responsive\"
                                 id='usuario-imagem' alt=\"Imagem do novo usuário\">
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <div class=\"col-md-8 col-lg-6\">
            <div class=\"tabs\">
                <ul class=\"nav nav-tabs tabs-primary\">
                    <li class=\"active\">
                        <a href=\"#formulario\" data-toggle=\"tab\">Novo usuário</a>
                    </li>
                </ul>
                <div class=\"tab-content\">
                    <div id=\"formulario\" class=\"tab-pane active\">
                        ";
        // line 69
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
                        <h4 class=\"mb-xlg\">Infomrações pessoais</h4>
                        <fieldset>
                            ";
        // line 72
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nome", [], "any", false, false, false, 72), 'row');
        echo "
                            ";
        // line 73
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "username", [], "any", false, false, false, 73), 'row');
        echo "
                            ";
        // line 74
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, false, 74), 'row');
        echo "
                            <div class=\"form-group\">
                                ";
        // line 76
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "cor", [], "any", false, false, false, 76), 'label');
        echo "
                                <div class=\"col-md-8\">
                                    <div class=\"input-group color\" data-plugin-colorpicker>
                                        <span class=\"input-group-addon\"><i></i></span>
                                        ";
        // line 80
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "cor", [], "any", false, false, false, 80), 'widget');
        echo "
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <hr class=\"dotted tall\">
                        <h4 class=\"mb-xlg\">Senha</h4>
                        <fieldset class=\"mb-xl\"> ";
        // line 87
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "</fieldset>
                        <div class=\"panel-footer\">
                            <div class=\"row\">
                                <div class=\"col-md-9 col-md-offset-3\">
                                    <button type=\"submit\" class=\"btn btn-primary\">Enviar</button>
                                    <button type=\"reset\" class=\"btn btn-default\">Resetar</button>
                                </div>
                            </div>
                        </div>
                        ";
        // line 96
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "

                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "NteUsuarioBundle:Usuario:novo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  228 => 96,  216 => 87,  206 => 80,  199 => 76,  194 => 74,  190 => 73,  186 => 72,  180 => 69,  161 => 53,  156 => 51,  149 => 46,  146 => 45,  140 => 42,  135 => 40,  131 => 39,  127 => 38,  123 => 37,  118 => 36,  115 => 35,  96 => 20,  85 => 12,  81 => 11,  77 => 10,  73 => 9,  68 => 8,  65 => 7,  60 => 5,  57 => 4,  51 => 3,  46 => 2,  44 => 44,  38 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteUsuarioBundle:Usuario:novo.html.twig", "/var/www/narfi/src/Nte/UsuarioBundle/Resources/views/Usuario/novo.html.twig");
    }
}
