<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_cadastro_parte_1.html.twig */
class __TwigTemplate_710fbcf086252c5475d847add4d628abd85aece0ee8e761b49fd8e42db7302f7 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_cadastro_parte_1.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo "Cadastro Contrato";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Pessoal Externo</a></span></li>
     <li><span><a href=\"";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_index");
        echo "\" class=\"breadcumbComLink\">Criar Contrato</a></span></li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Cadastro</a></span></li>
 ";
    }

    // line 9
    public function block_conteudo($context, array $blocks = [])
    {
        // line 10
        echo "    <header class=\"panel-heading center\" style=\"width: 80%\">
        <h2 class=\"panel-title\">
            <span class=\"va-middle\"><i class=\"fa fa-paste\"></i> Cadastro de Contrato</span>
        </h2>
    </header>

    <div class=\"panel-body center\" style=\"width: 80%\">
        ";
        // line 17
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
            <div class=\"row\">
                <div class=\"form-group col-md-4\">
                    ";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "descricao", [], "any", false, false, false, 20), 'row');
        echo "
                </div>
                <div class=\"form-group col-md-4\">
                    ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "editalNumero", [], "any", false, false, false, 23), 'row');
        echo "
                </div>
                <div class=\"form-group col-md-2\">
                    ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "idPapel", [], "any", false, false, false, 26), 'row');
        echo "
                </div>
            </div>

        ";
        // line 31
        echo "            ";
        // line 32
        echo "                ";
        // line 33
        echo "            ";
        // line 34
        echo "            ";
        // line 35
        echo "                ";
        // line 36
        echo "            ";
        // line 37
        echo "            ";
        // line 38
        echo "                ";
        // line 39
        echo "            ";
        // line 40
        echo "            ";
        // line 41
        echo "                ";
        // line 42
        echo "            ";
        // line 43
        echo "        ";
        // line 44
        echo "
        <div class=\"row\">
            <div class=\"form-group col-md-10\">
                ";
        // line 47
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "idCurso", [], "any", false, false, false, 47), 'row', ["attr" => ["height" => "300px"]]);
        echo "
            </div>
        </div>

        ";
        // line 52
        echo "        <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_index");
        echo "\">
            <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"expButton\"><i class=\"fa fa-arrow-circle-left\"></i></button>
        </a>
        <a href=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_cadastro_contrato_parte_dois", ["id" => twig_get_attribute($this->env, $this->source, ($context["nteContrato"] ?? null), "id", [], "any", false, false, false, 55)]), "html", null, true);
        echo "\">
            <button type=\"submit\" class=\"btn btn-primary btn-sm\" id=\"expButton\">Cadastrar</button>
        </a>
    </div>

    ";
        // line 60
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
";
    }

    // line 63
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 64
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>

        \$(function() {
            \$('#nteContrato_Enviar').hide();
        });

    </script>
";
    }

    // line 74
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 75
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>

        section {
            height: 100em;
        }

        #nteContrato_idCurso{
            height: 20em;
        }

        #expButton {
            float: left;
            margin-top: 1em;
            min-width: 9em;
            max-width: 9em;
            margin-left: 4px;
        }

        .btn.btn-success {
            margin-top: 1em;
        }

        .select-center-text {

            text-align: center;
        }

        .center {
            margin: auto;
            width: 50%;
        }

        .row {
            display: flex;
            justify-content: center;
        }

        .breadcumbSemLink {
            cursor: no-drop;
        }

        .breadcumbComLink {
            cursor: pointer;
        }

    </style>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_cadastro_parte_1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 75,  186 => 74,  172 => 64,  169 => 63,  163 => 60,  155 => 55,  148 => 52,  141 => 47,  136 => 44,  134 => 43,  132 => 42,  130 => 41,  128 => 40,  126 => 39,  124 => 38,  122 => 37,  120 => 36,  118 => 35,  116 => 34,  114 => 33,  112 => 32,  110 => 31,  103 => 26,  97 => 23,  91 => 20,  85 => 17,  76 => 10,  73 => 9,  66 => 6,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_cadastro_parte_1.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontrato/contrato_cadastro_parte_1.html.twig");
    }
}
