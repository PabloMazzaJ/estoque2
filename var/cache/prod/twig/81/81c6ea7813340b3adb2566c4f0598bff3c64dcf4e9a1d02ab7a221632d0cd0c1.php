<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_gerenciar_situacao_itens.html.twig */
class __TwigTemplate_a60c08e955a24c5e41a7633d068f532042b1d4fdcef2b2564f55ac88e679fb2c extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_gerenciar_situacao_itens.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Detalhes Contrato ";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Pessoal Externo</a></span></li>
     <li><span><a href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntevinculo_itens_pessoa_contrato_detalhes", ["id" => twig_get_attribute($this->env, $this->source, ($context["ntePessoaContrato"] ?? null), "id", [], "any", false, false, false, 6)]), "html", null, true);
        echo "\"
                  class=\"breadcumbComLink\">Vínculo - ";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["ntePessoaContrato"] ?? null), "idpessoa", [], "any", false, false, false, 7), "nome", [], "any", false, false, false, 7), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["ntePessoaContrato"] ?? null), "idContrato", [], "any", false, false, false, 7), "descricao", [], "any", false, false, false, 7), "html", null, true);
        echo "</a></span>
     </li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Situação Itens</a></span></li>
 ";
    }

    // line 11
    public function block_conteudo($context, array $blocks = [])
    {
        // line 12
        echo "    <div class=\"modal fade\" id=\"modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">×</span>
                    </button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Selecione uma situação para aplicar a todos os itens to
                        contrato
                    </h4>
                </div>
                <div class=\"modal-body\">
                    <div class=\"funkyradio check-inputs\">
                        <div class=\"funkyradio-success\">
                            <input type=\"radio\" name=\"radio\" id=\"radio-contrato-aceito\"/>
                            <label for=\"radio-contrato-aceito\">Aceitar</label>
                        </div>
                        <div class=\"funkyradio-warning\">
                            <input type=\"radio\" name=\"radio\" id=\"radio-contrato-analise\"/>
                            <label for=\"radio-contrato-analise\">Sob análise</label>
                        </div>
                        <div class=\"funkyradio-danger\">
                            <input type=\"radio\" name=\"radio\" id=\"radio-contrato-rejeitado\"/>
                            <label for=\"radio-contrato-rejeitado\">Rejeitar</label>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <button type=\"button\" class=\"btn btn-primary\" id=\"modal-confirmar\" data-dismiss=\"modal\">Salvar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"display-flex\">
        <div class=\"panel-body display-flex\" style=\"flex-direction: column; width: 80%\">
            <header class=\"panel-heading\" style=\"width: 100%\">
                <div>
                    <h2 class=\"panel-title\">
                        <span class=\"va-middle\"><i class=\"fa fa-paste\"></i> Gerenciar Itens</span>
                    </h2>
                </div>
            </header>

            <table class=\"table table-striped\" data-pessoacontrato=\"";
        // line 58
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["ntePessoaContrato"] ?? null), "id", [], "any", false, false, false, 58), "html", null, true);
        echo "\"
                   style=\"width: 100%\" id=\"tabela-itens\">
                <tr>
                    <th class=\"col-md-2\">Itens</th>
                    <th class=\"col-md-1\">Situação do Item</th>
                    <th class=\"col-md-1\">Observações</th>
                    <th class=\"col-md-2\">Entregue Por / Feito Por</th>
                    <th class=\"col-md-1\">Data da Entrega</th>
                    <th class=\"col-md-1\"></th>
                </tr>

                ";
        // line 69
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["ntePessoaContratoRequisito"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["cadaRequisito"]) {
            // line 70
            echo "                    <tr>
                        <td style=\"vertical-align: middle\">
                            ";
            // line 72
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["cadaRequisito"], "idContratoItem", [], "any", false, false, false, 72), "descricao", [], "any", false, false, false, 72), "html", null, true);
            echo "
                        </td>
                        <td style=\"vertical-align: middle\">
                            <select data-idrequisito=\"";
            // line 75
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cadaRequisito"], "id", [], "any", false, false, false, 75), "html", null, true);
            echo "\" class=\"select-situacao\">
                                <option>";
            // line 76
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["cadaRequisito"], "idContratoSituacao", [], "any", false, false, false, 76), "descricao", [], "any", false, false, false, 76), "html", null, true);
            echo "</option>
                            </select>
                        </td>
                        <td style=\"vertical-align: middle\">
                            <button type=\"button\" class=\"btn btn-primary btn-sm obs-clss\"
                                    data-idrequisito-obs=\"";
            // line 81
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cadaRequisito"], "id", [], "any", false, false, false, 81), "html", null, true);
            echo "\">Observação
                            </button>
                            <input type=\"hidden\" id=\"hidden-input\">
                        </td>
                        <td style=\"vertical-align: middle\">
                            <input type=\"text\" name=\"entregueinput\" class=\"form-control\"
                                   value=\"";
            // line 87
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cadaRequisito"], "entreguepor", [], "any", false, false, false, 87), "html", null, true);
            echo "\">
                        </td>
                        <td style=\"vertical-align: middle\">
                            ";
            // line 90
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cadaRequisito"], "registroDataAtualizacao", [], "any", false, false, false, 90), "Y-m-d"), "html", null, true);
            echo "
                        </td>
                        <td style=\"vertical-align: middle\">
                            <button type=\"button\" class=\"btn btn-primary btn-sm btn-salvar\"
                                    data-idrequisitobutton=\"";
            // line 94
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cadaRequisito"], "id", [], "any", false, false, false, 94), "html", null, true);
            echo "\">Salvar Item
                            </button>
                        </td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cadaRequisito'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 99
        echo "            </table>
            <div class=\"display-flex\">
                <a href=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntevinculo_itens_pessoa_contrato_detalhes", ["id" => twig_get_attribute($this->env, $this->source, ($context["ntePessoaContrato"] ?? null), "id", [], "any", false, false, false, 101)]), "html", null, true);
        echo "\"
                   class=\"btn-gerar-relatorio2\">
                    <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"expButton\"><i
                                class=\"fa fa-arrow-circle-left\"></i></button>
                </a>
                <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"alterarTodas\" style=\"margin-left: 1px\">Alterar
                    Todas
                </button>
            </div>
        </div>";
        // line 114
        echo "<div class=\"panel-body\" style=\"display: flex; flex-direction: column; width: 20%; justify-content: space-between\">
            <div>
                <header class=\"panel-heading\" style=\"width: 100%\">
                    <div>
                        <h2 class=\"panel-title\">
                            <span class=\"va-middle\"><span class=\"aux-vis\">x</span></span>
                        </h2>
                    </div>
                </header>
                <table class=\"table table-striped\">
                    <tr>
                        <th>Função</th>
                    </tr>
                    <tr>
                        <td style=\"vertical-align: middle\">
                            <select class=\"select-funcao\" id=\"select-funcao\">
                                <option>PF1 - Professor Formador 1</option>
                                <option>PF2 - Professor Formador 2</option>
                                <option>PC1 - Professor Conteudista 1</option>
                                <option>PC2 - Professor Conteudista 2</option>
                                <option>Tutor</option>
                                <option>Coordenador de Curso 1</option>
                                <option>Coordenador de Tutoria 1</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>

            <div class=\"btn-bottom\" style=\"display: flex;\" >
                <button type=\"button\" class=\"btn btn-primary btn-sm\" style=\"align-self: flex-end\" id=\"funcao-button\" >
                    Salvar Função
                </button>
            </div>

        </div>
    </div>
";
    }

    // line 153
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 154
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script src=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/vcom/vcom.js"), "html", null, true);
        echo "\"></script>
    <script>

        \$(function () {

            modalManagement();
            populaSelect();
            inicializarFuncaoSelect();
            openObsBtn();
            pnotifyConfig();
            selectFuncaoConfig();
            funcaoBtn();

            \$('.btn.btn-primary.btn-sm.btn-salvar').click(function () {
                let observacao = \$(this).parent().parent().find('#hidden-input').val();
                let contratoRequisitoId = \$(this).data('idrequisitobutton');
                let entreguePor = \$(this).parent().parent().find(\"input[name=entregueinput]\").val();
                let situacaoString = \$(this).parent().parent().find(\"option:selected\").text();


                alteraObservacao(contratoRequisitoId, observacao, entreguePor);
                alteraSituacao(contratoRequisitoId, situacaoString);
                verificaSituacaoContrato();

                notificaUsuario('', 'Dados salvos com sucesso!', 'success')
            });
        });

        function funcaoBtn()
        {
            \$('#funcao-button').click(function() {

                let funcao = \$('.select-funcao').find(\"option:selected\").text();

                \$.ajax({
                    url: \"";
        // line 190
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_inserir_funcao_pessoa_contrato_requisitos");
        echo "\",
                    dataType: 'json',
                    method: \"POST\",
                    data: {
                        pessoaContratoIdKey: \$('#tabela-itens').data('pessoacontrato'),
                        funcaoKey: funcao
                    },
                    success: function (data) {
                        notificaUsuario('', 'Dados salvos com sucesso!', 'success')
                    },
                    error: function (data) {
                    }
                });

            });
        }

        function selectFuncaoConfig() {
            \$('.select-funcao').select2({
                width: \"15vw\",
            });
        }

        function get_center_pos(width, top) {
            if (!top) {
                top = 30;
                \$('.ui-pnotify').each(function () {
                    top += \$(this).outerHeight() + 20;
                });
            }
            return {
                \"top\": top, \"left\": (\$(window).width() / 2) - (width / 2)
            }
        }

        function pnotifyConfig() {
            \$(window).resize(function () {
                \$(\".ui-pnotify\").each(function () {
                    \$(this).css(get_center_pos(\$(this).width(), \$(this).position().top))
                });
            });
        }

        function notificaUsuario(titulo, texto, tipo) {
            new PNotify(
                {
                    title: false,
                    text: texto,
                    min_height: \"65px\",
                    type: tipo,
                    delay: 3000,
                    before_open: function (PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    }
                }
            );
        }

        function openObsBtn() {
            \$('.btn.btn-primary.btn-sm.obs-clss').click(function () {
                let button = \$(this);
                let contratoRequisitoId = button.data('idrequisito-obs');
                buscarObsInitialValue(contratoRequisitoId, button);
            });
        }

        function buscarObsInitialValue(ntePessoaContratoRequisitoId, button) {
            let obs;
            console.log('ntePessoaContratoRequisitoId');
            console.log(ntePessoaContratoRequisitoId);
            \$.ajax({
                url: \"";
        // line 261
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_buscar_contrato_requisito_obs");
        echo "\",
                dataType: 'json',
                method: \"POST\",
                data: {
                    ntePessoaContratoRequisitoIdKey: ntePessoaContratoRequisitoId
                },
                success: function (data) {
                    obs = data;
                    console.log(data);
                    vcom.modalForm(
                        (values, closemodal) => {
                            let sibling = button.siblings()[0];
                            sibling.value = values.observacoes;
                            closemodal();
                        }, [
                            {
                                name: 'observacoes',
                                type: 'textarea',
                                label: 'Digite a observação: ',
                                initialValue: obs,
                                placeholder: 'escreva aqui'
                            }
                        ]);
                },
                error: function (data) {
                }
            });

            return obs;
        }


        function inicializarFuncaoSelect() {
            \$('.js-example-basic-single').select2();
        }

        function modalManagement() {
            \$('#alterarTodas').click(function () {
                \$(\"#modal\").modal();
            });

            \$('#modal-confirmar').click(function () {
                let inputId = \$(\".check-inputs input[type='radio']:checked\").attr('id');
                alterarTodosItens(inputId);
            })
        }

        function alterarTodosItens(inputId) {
            // \$('.select-situacao').select2().trigger('change');

            \$.ajax({
                url: \"";
        // line 312
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessaoa_externa_api_altera_todos_itens");
        echo "\",
                dataType: 'json',
                method: \"POST\",
                data: {
                    situacaoKey: inputId,
                    ntePessoaContratoKey: \$('table').data('pessoacontrato')
                },
                success: function (data) {
                    location.reload();
                },
                error: function (data) {
                }
            });
        }

        function populaSelect() {
            \$('.select-situacao').select2({
                width: \"8vw\",
                ajax: {
                    url: '";
        // line 331
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_buscar_situacao");
        echo "',
                    dataType: 'json',
                    method: \"POST\",
                    data: function (params) {
                        return {};
                    },
                    processResults: function (data) {
                        return {
                            results: data,
                        };
                    },
                }
            });
        }

        function verificaSituacaoContrato() {
            \$.ajax({
                url: \"";
        // line 348
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_verifica_situacao_contrato");
        echo "\",
                dataType: 'json',
                method: \"POST\",
                data: {
                    ntePessoaContratoKey: \$('table').data('pessoacontrato'),
                },
                success: function (a) {
                    console.log('success');
                },
                error: function (a) {
                }
            });
        }

        //altera observacao e entregue por
        function alteraObservacao(ntePessoaContratoRequisitoId, observacaoString, entreguePor) {
            \$.ajax({
                url: \"";
        // line 365
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_altera_observacao");
        echo "\",
                dataType: 'json',
                method: \"POST\",
                data: {
                    requisitoKey: ntePessoaContratoRequisitoId,
                    observacaoKey: observacaoString,
                    entreguePorKey: entreguePor
                },
                success: function (a) {
                    console.log('success');
                    if (a.result) {
                        console.log('result');
                    }
                },
                error: function (a) {
                    console.log('success');
                }
            });
        }

        function alteraSituacao(ntePessoaContratoRequisitoId, situacaoString) {
            \$.ajax({
                url: \"";
        // line 387
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_altera_situacao");
        echo "\",
                dataType: 'json',
                method: \"POST\",
                data: {
                    requisitoKey: ntePessoaContratoRequisitoId,
                    situacaoStringKey: situacaoString,
                },
                success: function (a) {
                },
                error: function (a) {
                }
            });
        }

    </script>
";
    }

    // line 404
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 405
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 406
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/vcom/vcom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <style>

        .btn-bottom {
            display: flex;
            flex-direction: column;
        }
        
        .panel-body::after,
        .panel-body::before {
            display: none;
        }

        .aux-vis {
            visibility: hidden
        }

        .display-flex {
            display: flex;
        }

        input {
            text-align: center;
        }

        .vcom-modal-popup {
            max-width: 60vw;
            width: 60vw;
        }

        .vcom-modal-popup form {
            width: 80%;
        }

        .vcom-modal-btnContainer {
            padding-top: 1em;
        }

        .vcom-modal-popup textarea {
            height: 10vw;
        }

        [class^='select2'] {
            border-radius: 4px !important;
        }

        .fa-times {
            color: white;
        }

        .fa-exclamation {
            color: red;
        }

        .fa-check {
            color: white;
        }

        .funkyradio div {
            clear: both;
            /*margin: 0 50px;*/
            overflow: hidden;
        }

        .funkyradio label {
            /*min-width: 400px;*/
            width: 100%;
            border-radius: 3px;
            border: 1px solid #D1D3D4;
            font-weight: normal;
        }

        .funkyradio input[type=\"radio\"]:empty, .funkyradio input[type=\"checkbox\"]:empty {
            display: none;
        }

        .funkyradio input[type=\"radio\"]:empty ~ label, .funkyradio input[type=\"checkbox\"]:empty ~ label {
            position: relative;
            line-height: 2.5em;
            text-indent: 3.25em;

            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .funkyradio input[type=\"radio\"]:empty ~ label:before, .funkyradio input[type=\"checkbox\"]:empty ~ label:before {
            position: absolute;
            display: block;
            top: 0;
            bottom: 0;
            left: 0;
            content: '';
            width: 2.5em;
            background: #D1D3D4;
            border-radius: 3px 0 0 3px;
        }

        .funkyradio input[type=\"radio\"]:hover:not(:checked) ~ label:before, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label:before {
            content: '\\2714';
            text-indent: .9em;
            color: #C2C2C2;
        }

        .funkyradio input[type=\"radio\"]:hover:not(:checked) ~ label, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label {
            color: #888;
        }

        .funkyradio input[type=\"radio\"]:checked ~ label:before, .funkyradio input[type=\"checkbox\"]:checked ~ label:before {
            content: '\\2714';
            text-indent: .9em;
            color: #333;
            background-color: #ccc;
        }

        .funkyradio-danger input[type=\"radio\"]:checked ~ label:before, .funkyradio input[type=\"checkbox\"]:checked ~ label:before {
            content: '\\03a7';
            text-indent: .9em;
            color: #333;
            background-color: #ccc;
        }

        .funkyradio-danger input[type=\"radio\"]:hover:not(:checked) ~ label:before, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label:before {
            content: '\\03a7';
            text-indent: .9em;
            color: #C2C2C2;
        }

        .funkyradio-warning input[type=\"radio\"]:hover:not(:checked) ~ label:before, .funkyradio input[type=\"checkbox\"]:hover:not(:checked) ~ label:before {
            content: '!';
            text-indent: 1.1em;
            color: #C2C2C2;
            font-weight: bold;

        }

        .funkyradio-warning input[type=\"radio\"]:checked ~ label:before, .funkyradio input[type=\"checkbox\"]:checked ~ label:before {
            content: '!';
            text-indent: 1.1em;
            color: #333;
            background-color: #ccc;
            font-weight: bold;

        }

        .funkyradio input[type=\"radio\"]:checked ~ label, .funkyradio input[type=\"checkbox\"]:checked ~ label {
            color: #777;
        }

        .funkyradio input[type=\"radio\"]:focus ~ label:before, .funkyradio input[type=\"checkbox\"]:focus ~ label:before {
            box-shadow: 0 0 0 3px #999;
        }

        .funkyradio-default input[type=\"radio\"]:checked ~ label:before, .funkyradio-default input[type=\"checkbox\"]:checked ~ label:before {
            color: #333;
            background-color: #ccc;
        }

        .funkyradio-primary input[type=\"radio\"]:checked ~ label:before, .funkyradio-primary input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #337ab7;
        }

        .funkyradio-success input[type=\"radio\"]:checked ~ label:before, .funkyradio-success input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #5cb85c;
        }

        .funkyradio-danger input[type=\"radio\"]:checked ~ label:before, .funkyradio-danger input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #d9534f;
        }

        .funkyradio-warning input[type=\"radio\"]:checked ~ label:before, .funkyradio-warning input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #f0ad4e;
        }

        .funkyradio-info input[type=\"radio\"]:checked ~ label:before, .funkyradio-info input[type=\"checkbox\"]:checked ~ label:before {
            color: #fff;
            background-color: #5bc0de;
        }

        .btn {
            min-width: 9em;
            max-width: 9em;
        }

        tr td {
            text-align: center;
        }

        textarea {
            resize: none;
        }

        table th, td {
            text-align: center;
            vertical-align: middle;
        }

        li span a:hover {
            text-decoration: none;
        }

        .breadcumbSemLink {
            cursor: no-drop;
        }

        .breadcumbComLink {
            cursor: pointer;
        }
    </style>

";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_gerenciar_situacao_itens.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  550 => 406,  545 => 405,  542 => 404,  522 => 387,  497 => 365,  477 => 348,  457 => 331,  435 => 312,  381 => 261,  307 => 190,  269 => 155,  264 => 154,  261 => 153,  220 => 114,  208 => 101,  204 => 99,  193 => 94,  186 => 90,  180 => 87,  171 => 81,  163 => 76,  159 => 75,  153 => 72,  149 => 70,  145 => 69,  131 => 58,  83 => 12,  80 => 11,  70 => 7,  66 => 6,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_gerenciar_situacao_itens.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontrato/contrato_gerenciar_situacao_itens.html.twig");
    }
}
