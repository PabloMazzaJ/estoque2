<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntepessoacontrato:efetuar_vinculos.hml.twig */
class __TwigTemplate_1992cada3ea973cfdf8ee060f1f173c71842f0faeb5249220c6795dc8db3a3de extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntepessoacontrato:efetuar_vinculos.hml.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Pessoas - Contrato ";
    }

    // line 3
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 4
        echo "     <li><span><a href=\"#\">Pessoal Externo</a></span></li>
     <li><span><a href=\"#\">Pessoas - Contrato</a></span></li>
 ";
    }

    // line 7
    public function block_conteudo($context, array $blocks = [])
    {
        // line 8
        echo "
    <div id=\"container-principal\" class=\"panel-body flexbox\">

        <div id=\"container-esquerda\" class=\"auxx\">
            <header class=\"panel-heading center\">
                <h2 class=\"panel-title\">
                    <span class=\"va-middle\">Editais</span>
                </h2>
            </header>

            <table class=\"table table-striped\" >
                <tr class=\"table-container-editais\"></tr>
            </table>
        </div>

        <div class=\"headerDivider\"></div>

        <div id=\"container-direita\">
            <header class=\"panel-heading center\">
                <h2 class=\"panel-title\">
                    <span class=\"va-middle\">Pessoas</span>
                </h2>
            </header>

            <table class=\"table table-striped\" id=\"outside-container-table-pessoas\">
                <tr class=\"table-container-pessoas\"></tr>
            </table>

        </div>

        <div class=\"headerDivider\"></div>

        <div id=\"container-final\">
            <header class=\"panel-heading center\">
                <h2 class=\"panel-title\">
                    <span class=\"va-middle\">Vínculos</span>
                </h2>
            </header>

            <table class=\"table table-striped\">
                <tr class=\"table-container-vinculos\"></tr>
            </table>
        </div>


        ";
        // line 54
        echo "
        <div id=\"container-btn\" class=\"panel-body\">
            <a href=\"";
        // line 56
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntevinculo_pessoa_contrato");
        echo "\">
                <button class=\"btn btn-primary btn-acoes\" id=\"ver-vinculos-btn\" title=\"Clique aqui para visualizar os vínculos\"><i class=\"fas fa-eye\"></i></button>
            </a>
            <button class=\"btn btn-primary btn-acoes\" id=\"vincula-btn\" title=\"Clique aqui para criar os vínculos\"><i class=\"fas fa-plus-circle\"></i></button>
            <button class=\"btn btn-primary btn-acoes\" id=\"clear-table-btn\" title=\"Clique aqui para limpar os vínculos\"><i class=\"fas fa-eraser\"></i></button>
            <button class=\"btn btn-success btn-acoes\" id=\"confirma-btn\" title=\"Clique aqui para registrar os vínculos\"><i class=\"fas fa-check-circle\"></i></button>
        </div>


    </div>

";
    }

    // line 69
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 70
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/vcom/vcom.js"), "html", null, true);
        echo "\"></script>

    <script src=\"https://kit.fontawesome.com/1f09ac1f48.js\"></script>
    <script>

        function get_center_pos(width, top) {
            if (!top) {
                top = 30;
                \$('.ui-pnotify').each(function () {
                    top += \$(this).outerHeight() + 20;
                });
            }
            return {\"top\": top, \"left\": (\$(window).width() / 2) - (width / 2)}
        }

        let pessoasTable = {};
        let vinculosTable = {};
        let editaistable = {};

        function criarVinculo(element, indexEdital, editaisSelecionados) {
            let pessoasSelecionadas = pessoasTable.getSelectedItems();

            pessoasSelecionadas.forEach(function(elemento, indexPessoa) {
                console.log(editaisSelecionados[indexEdital].item.id);
                console.log(editaisSelecionados[indexEdital].item.editalNumero);
                vinculosTable.add({
                    editalId: editaisSelecionados[indexEdital].item.id,
                    edital: editaisSelecionados[indexEdital].item.editalNumero,
                    pessoaId: pessoasSelecionadas[indexPessoa].item.id,
                    pessoa: pessoasSelecionadas[indexPessoa].item.nome
                });
            });
        }

        let btnAddToVinculosTable = document.getElementById('vincula-btn');
        function relacionarPessoaEditalToVinculos() {
            btnAddToVinculosTable.addEventListener(\"click\", function() {
                let editaisSelecionados = editaistable.getSelectedItems();
                editaisSelecionados.forEach(criarVinculo);
            });
        }

        let btnClearVinculosTable = document.getElementById('clear-table-btn');
        function clearVinculosTable() {
            btnClearVinculosTable.addEventListener(\"click\", function() {
                vinculosTable.removeAll();
            });
        }

        let btnConfirmaVinculos = document.getElementById('confirma-btn');
        function aceitarVinculos() {
            btnConfirmaVinculos.addEventListener(\"click\", function() {
                \$.ajax({
                    url: \"";
        // line 124
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_aceitar_vinculo");
        echo "\",
                    dataType: 'json',
                    method: \"POST\",
                    data: {
                        vinculosData: vinculosTable.getSelectedItems(),
                    },
                    success: function (data) {

                        if (data === true) {
                            notificarUsuarioPnotify('Vínculos criados com sucesso.', '', 'success');
                        }

                    },
                    error: function (data) {
                    }
                });
            })
        }

        \$(document).ready(function () {

            initTableEditais();
            initTablePessoas();
            initTableVinculos();
            searchBarStyle();
            clearVinculosTable();
            relacionarPessoaEditalToVinculos();
            aceitarVinculos();

            \$(window).resize(function () {
                \$(\".ui-pnotify\").each(function () {
                    \$(this).css(get_center_pos(\$(this).width(), \$(this).position().top))
                });
            });
        });

        function searchBarStyle() {
            \$('#container-esquerda').find('.vcom-searchbar').attr(\"placeholder\", \"Procurar por nome\");
            \$('#container-direita').find('.vcom-searchbar').attr(\"placeholder\", \"Procurar por edital\");
        }

        function initTablePessoas() {

            let container = document.querySelector('.table-container-pessoas');

            pessoasTable = vcom.table(
                /*value*/      ['nome', 'cpf'],
                /*title*/      ['Pessoa', 'cpf']
            ).setModel([])
                .setActionButtons([
                    // {'action': 'remover', 'innerHTML': '<i class=\"fa fa-trash\"></i>'}
                ])
                .setActionCallback((evt) => {
                    // if (evt.action === 'remover') pessoasTable.remove(evt.index);
                })
                .setChecklist(true)
                .enableSearch(true)
                .setSource('";
        // line 181
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_busca_pessoas");
        echo "')
                .setQueryBuilder((term) => {
                    return {
                        search: term,
                    }
                })
            ;

            const myTableHTML = pessoasTable.mount();
            container.appendChild(myTableHTML);
        }

        function initTableEditais() {
            let container = document.querySelector('.table-container-editais');

            editaistable = vcom.table(
                /*value*/      [ 'editalNumero'],
                /*title*/      ['Edital']
            ).setChecklist(true)
             .enableSearch(true)
             .setSource('";
        // line 201
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_busca_contratos");
        echo "')
             .setQueryBuilder((term) => {
                return {
                    search : term,
                }
            })
            ;

            const myTableHTML = editaistable.mount();
            container.appendChild(myTableHTML); //\tadiciona a tabela em algum container
        }

        function initTableVinculos() {
            let container = document.querySelector('.table-container-vinculos');

            vinculosTable = vcom.table(
                /*value*/      [ 'edital', 'pessoa'],
                /*title*/      ['Edital', 'Pessoa']
            ).setActionButtons([
                {'action': 'remover', 'innerHTML': '<i class=\"fa fa-trash\"></i>'}
            ])
            .setActionCallback((evt) => {
                if (evt.action === 'remover') vinculosTable.remove(evt.index);

            })
            .setChecklist(true)
            // .enableSearch(true)
            ;

            const myTableHTML = vinculosTable.mount();
            container.appendChild(myTableHTML); //\tadiciona a tabela em algum container
        }

        function notificarUsuarioPnotify(title, text, type)
        {
            new PNotify(
                {
                    title: title,
                    text: text,
                    type: type,
                    before_open: function (PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    }
                }
            );
        }

    </script>
";
    }

    // line 251
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 252
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 253
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/vcom/vcom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <style>

        td input {
            cursor: pointer;
        }

        .vcom-searchbar {
            border-radius: 3px;
            text-align: center;
        }

        #container-left-buttons {
            margin-top: 1em;
        }

        .headerDivider {
            border-left: 1px solid #D7D7D7;
            border-right: 1px solid #D7D7D7;
            margin: 1em;
        }

        #container-principal {
            display: flex;
            margin-right: 3em;
        }

        #container-esquerda {
            width: 30%;
        }

        #container-direita {
            width: 35%;
        }

        #container-final {
            width: 35%;
        }

        #container-btn {
            position: fixed;
            right: 0;
            top: 45%;
            width: 5em;
            margin-top: -3em;
        }

        th {
            text-align: center;
        }

        td {
            text-align: center;
        }

        .btn-acoes {
            margin-top: 1em;
        }

    </style>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntepessoacontrato:efetuar_vinculos.hml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  341 => 253,  336 => 252,  333 => 251,  280 => 201,  257 => 181,  197 => 124,  141 => 71,  136 => 70,  133 => 69,  117 => 56,  113 => 54,  66 => 8,  63 => 7,  57 => 4,  54 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntepessoacontrato:efetuar_vinculos.hml.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntepessoacontrato/efetuar_vinculos.hml.twig");
    }
}
