<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :widgets:video-player.html.twig */
class __TwigTemplate_deedbefd7f5cfc90b8a2d12545f2537e6aecfa383bd3bf30d36d619530f75bc0 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"embed-responsive embed-responsive-16by9\">
    <img itemprop=\"image\"  class=\"hidden\"  src=\"/repositorio/";
        // line 2
        echo twig_escape_filter($this->env, ($context["poster"] ?? null), "html", null, true);
        echo "/capa.png\"/>
    <video id=\"player\" controls preload=\"auto\" width=\"100%\" height=\"100%\"
           style=\"width:100%;height:100%;\" poster=\"/repositorio/";
        // line 4
        echo twig_escape_filter($this->env, ($context["poster"] ?? null), "html", null, true);
        echo "/capa.png\"  data-setup='{\"example_option\":true}'>
        <source src=\"/repositorio/";
        // line 5
        echo twig_escape_filter($this->env, ($context["recurso"] ?? null), "html", null, true);
        echo "/video.mp4\" type='video/mp4'  data-plugin-type=\"native\" data-quality=\"HD\"/>
        <source src=\"/repositorio/";
        // line 6
        echo twig_escape_filter($this->env, ($context["recurso"] ?? null), "html", null, true);
        echo "/720p.mp4\"  type='video/mp4'  data-plugin-type=\"native\" data-quality=\"720p\"/>
        <source src=\"/repositorio/";
        // line 7
        echo twig_escape_filter($this->env, ($context["recurso"] ?? null), "html", null, true);
        echo "/480p.mp4\"  type='video/mp4'  data-plugin-type=\"native\" data-quality=\"480p\"/>
        <source src=\"/repositorio/";
        // line 8
        echo twig_escape_filter($this->env, ($context["recurso"] ?? null), "html", null, true);
        echo "/360p.mp4\"  type='video/mp4'  data-plugin-type=\"native\" data-quality=\"360p\"/>
        ";
        // line 9
        if ((isset($context["legendas"]) || array_key_exists("legendas", $context))) {
            // line 10
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["legendas"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
                // line 11
                echo "                ";
                if ((twig_get_attribute($this->env, $this->source, $context["l"], "publicado", [], "any", false, false, false, 11) == 1)) {
                    // line 12
                    echo "                    <track kind=\"subtitles\" src=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("legenda_visualizar_str", ["id" => twig_get_attribute($this->env, $this->source, $context["l"], "id", [], "any", false, false, false, 12)]), "html", null, true);
                    echo "\" srclang=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["l"], "idioma", [], "any", false, false, false, 12), "html", null, true);
                    echo "\"/>
                ";
                }
                // line 14
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 15
            echo "        ";
        }
        // line 16
        echo "        <track kind=\"metadata\" class=\"time-rail-thumbnails\" src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("video_gerar_vtt", ["id" => twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["catalogoVideo"] ?? null), "dataCriacao", [], "any", false, false, false, 16), "U")]), "html", null, true);
        echo "\" srclang=\"None\" />
        <object width=\"320\" height=\"240\" type=\"application/x-shockwave-flash\" data=\"/";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/mediaelement/build/flashmediaelement.swf"), "html", null, true);
        echo "\">
            <param name=\"movie\" value=\"/";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/mediaelement/build/flashmediaelement.swf"), "html", null, true);
        echo "\" />
            <param name=\"flashvars\" value=\"controls=true&file=/repositorio/";
        // line 19
        echo twig_escape_filter($this->env, ($context["recurso"] ?? null), "html", null, true);
        echo "/video.mp4\" />
            <img src=\"/repositorio/";
        // line 20
        echo twig_escape_filter($this->env, ($context["poster"] ?? null), "html", null, true);
        echo "/capa.png\" width=\"320\" height=\"240\" title=\"Recurso não suportado\" />
        </object>
    </video>
</div>";
    }

    public function getTemplateName()
    {
        return ":widgets:video-player.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 20,  103 => 19,  99 => 18,  95 => 17,  90 => 16,  87 => 15,  81 => 14,  73 => 12,  70 => 11,  65 => 10,  63 => 9,  59 => 8,  55 => 7,  51 => 6,  47 => 5,  43 => 4,  38 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":widgets:video-player.html.twig", "/var/www/narfi/app/Resources/views/widgets/video-player.html.twig");
    }
}
