<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteConfigBaseBundle:Crontab:tabela.html.twig */
class __TwigTemplate_0eca8549232bfd135a100f863bb5e88fb3ac4d16b197f91578c4d6dd7b4065a3 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"panel-body\">
    <table class=\"table table-responsive\">
        <tr>
            <th width=\"10\">
                <div class=\"checkbox-custom checkbox-default\">
                    <input type=\"checkbox\" id=\"jobs\" data-toggle=\"tooltip\" title=\"Selecionar todos\"
                           data-placement=\"top\">
                    <label> </label>
                </div>
            </th>
            <th width=\"10\"></th>
            <th>Tarefa</th>
            <th class=\"text-center\">Minuto</th>
            <th class=\"text-center\">Hora</th>
            <th class=\"text-center\">Dia</th>
            <th class=\"text-center\">Mês</th>
            <th class=\"text-center\">Semana</th>
            <th class=\"\">Última Execução</th>
            <th class=\"text-right\">Configurar</th>
        </tr>
        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cronjobs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["job"]) {
            // line 22
            echo "            ";
            $context["schedule"] = twig_split_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "schedule", [], "any", false, false, false, 22), " ");
            // line 23
            echo "            <tr>
                <td>
                    <div class=\"checkbox-custom checkbox-default\">
                        <input type=\"checkbox\" name=\"jobs\" id=\"job-";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "id", [], "any", false, false, false, 26), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "id", [], "any", false, false, false, 26), "html", null, true);
            echo "\">
                        <label> </label>
                    </div>
                </td>
                <td>
                    ";
            // line 31
            if ((twig_get_attribute($this->env, $this->source, $context["job"], "enabled", [], "any", false, false, false, 31) == 1)) {
                // line 32
                echo "                        <i class=\"fa fa-check-circle-o text-success\" aria-hidden=\"true\"></i>
                    ";
            } else {
                // line 34
                echo "                        <i class=\"fa fa-times-circle-o text-danger\" aria-hidden=\"true\"></i>
                    ";
            }
            // line 36
            echo "                </td>
                <td>
                    <b>";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "name", [], "any", false, false, false, 38), "html", null, true);
            echo "</b><br>
                    <small>";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "description", [], "any", false, false, false, 39), "html", null, true);
            echo "</small>
                </td>
                <td class=\"text-center\">";
            // line 41
            echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["schedule"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[0] ?? null) : null), "html", null, true);
            echo " </td>
                <td class=\"text-center\">";
            // line 42
            echo twig_escape_filter($this->env, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["schedule"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[1] ?? null) : null), "html", null, true);
            echo " </td>
                <td class=\"text-center\">";
            // line 43
            echo twig_escape_filter($this->env, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["schedule"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[2] ?? null) : null), "html", null, true);
            echo " </td>
                <td class=\"text-center\">";
            // line 44
            echo twig_escape_filter($this->env, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["schedule"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[3] ?? null) : null), "html", null, true);
            echo " </td>
                <td class=\"text-center\">";
            // line 45
            echo twig_escape_filter($this->env, (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = ($context["schedule"] ?? null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4[4] ?? null) : null), "html", null, true);
            echo " </td>
                <td>
                    ";
            // line 47
            if ((twig_get_attribute($this->env, $this->source, $context["job"], "ultimaExecucao", [], "any", false, false, false, 47) === 0)) {
                // line 48
                echo "                        Nunca
                    ";
            } else {
                // line 50
                echo "                    ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "ultimaExecucao", [], "any", false, false, false, 50), "d/m/Y H:i:s"), "html", null, true);
                echo "
                    ";
            }
            // line 52
            echo "                </td>

                <td class=\"\">
                    <a title=\"Configura os parâmetros do crontab\"
                       class=\"btn btn-default btn-sm btn-simple-ajax pull-right\"
                       href=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_crontab_editar", ["id" => twig_get_attribute($this->env, $this->source, $context["job"], "id", [], "any", false, false, false, 57)]), "html", null, true);
            echo "\"
                       data-toggle=\"tooltip\"
                       data-placement=\"top\">
                        <i class=\"fa fa-sliders\"></i>
                    </a>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['job'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "    </table>
</div>
<script>
    updateDomJS();
</script>";
    }

    public function getTemplateName()
    {
        return "NteConfigBaseBundle:Crontab:tabela.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 65,  142 => 57,  135 => 52,  129 => 50,  125 => 48,  123 => 47,  118 => 45,  114 => 44,  110 => 43,  106 => 42,  102 => 41,  97 => 39,  93 => 38,  89 => 36,  85 => 34,  81 => 32,  79 => 31,  69 => 26,  64 => 23,  61 => 22,  57 => 21,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteConfigBaseBundle:Crontab:tabela.html.twig", "/var/www/narfi/src/Nte/Config/BaseBundle/Resources/views/Crontab/tabela.html.twig");
    }
}
