<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_pessoas_vinculadas.html.twig */
class __TwigTemplate_9b681e623710bd7447045ddda2b3ff61353187d69865447dfcf3450d465d9841 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_pessoas_vinculadas.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo "Contratos";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Pessoal Externo</a></span></li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Edital</a></span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "<div class=\"flex-container\">
    <header class=\"panel-heading theader\">
        <h2 class=\"panel-title\"> Pessoas vinculadas ao edital ";
        // line 11
        echo twig_escape_filter($this->env, ($context["edital"] ?? null), "html", null, true);
        echo " </h2>
    </header>
    <div class=\"panel-body tpbody\">
        <table class=\"table table-striped\">
            <tr>
                <th>Pessoa</th>
                <th>Papel</th>
            </tr>
            ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["membros"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
            // line 20
            echo "                <tr>
                    <td> ";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "membro", [], "any", false, false, false, 21), "html", null, true);
            echo " </td>
                    <td> ";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "papel", [], "any", false, false, false, 22), "html", null, true);
            echo "</td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "        </table>
</div>

";
    }

    // line 30
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 31
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>
        \$('td a i').hover(function () {
            \$(this).attr('title', 'Clique para visualizar os detalhes deste contrato');
        });
    </script>
";
    }

    // line 39
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 40
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <style>
        .flex-container{
            display: flex;
            align-items: center;
            flex-direction: column;
        }

        .theader {
            text-align: center;
            width: 60%;

        }

        .tpbody {
            width: 60%;
        }

        th {
            text-align: center;
        }

        td {
            text-align: center;
        }


    </style>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_pessoas_vinculadas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 40,  129 => 39,  117 => 31,  114 => 30,  107 => 25,  98 => 22,  94 => 21,  91 => 20,  87 => 19,  76 => 11,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_pessoas_vinculadas.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontrato/contrato_pessoas_vinculadas.html.twig");
    }
}
