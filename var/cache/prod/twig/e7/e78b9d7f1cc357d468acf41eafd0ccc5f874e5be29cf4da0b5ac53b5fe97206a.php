<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ::forms.html.twig */
class __TwigTemplate_36a3ef33d422c3d7d4097ac3bc0b9adfdc3a466ace4634760f88dc1d898af75d extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'form_row' => [$this, 'block_form_row'],
            'form_label' => [$this, 'block_form_label'],
            'form_widget_simple' => [$this, 'block_form_widget_simple'],
            'widget_attributes' => [$this, 'block_widget_attributes'],
            'button_attributes' => [$this, 'block_button_attributes'],
            'form_start' => [$this, 'block_form_start'],
            'button_row' => [$this, 'block_button_row'],
            'button_widget' => [$this, 'block_button_widget'],
            'submit_widget' => [$this, 'block_submit_widget'],
            'reset_widget' => [$this, 'block_reset_widget'],
            'choice_widget_collapsed' => [$this, 'block_choice_widget_collapsed'],
            'checkbox_widget' => [$this, 'block_checkbox_widget'],
            'radio_widget' => [$this, 'block_radio_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $this->displayBlock('form_row', $context, $blocks);
        // line 20
        echo "
";
        // line 21
        $this->displayBlock('form_label', $context, $blocks);
        // line 37
        echo "
 ";
        // line 38
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 44
        echo "

";
        // line 46
        $this->displayBlock('widget_attributes', $context, $blocks);
        // line 68
        echo "
";
        // line 69
        $this->displayBlock('button_attributes', $context, $blocks);
        // line 84
        echo "
";
        // line 85
        $this->displayBlock('form_start', $context, $blocks);
        // line 101
        echo "
 ";
        // line 102
        $this->displayBlock('button_row', $context, $blocks);
        // line 122
        echo "

";
        // line 124
        $this->displayBlock('button_widget', $context, $blocks);
        // line 132
        echo "
";
        // line 133
        $this->displayBlock('submit_widget', $context, $blocks);
        // line 139
        echo "
";
        // line 140
        $this->displayBlock('reset_widget', $context, $blocks);
        // line 146
        echo "

    ";
        // line 148
        $this->displayBlock('choice_widget_collapsed', $context, $blocks);
        // line 175
        echo "
";
        // line 176
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 182
        echo "
";
        // line 183
        $this->displayBlock('radio_widget', $context, $blocks);
        // line 188
        echo "


";
    }

    // line 1
    public function block_form_row($context, array $blocks = [])
    {
        // line 2
        echo "    ";
        ob_start();
        // line 3
        echo "        ";
        if (twig_in_filter("checkbox", (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 3), "block_prefixes", [], "any", false, false, false, 3)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[1] ?? null) : null))) {
            // line 4
            echo "            <div class=\"checkbox-custom checkbox-default\">
                ";
            // line 5
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
            echo " ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label');
            echo "
            </div>
        ";
        } else {
            // line 8
            echo "            <div class=\" form-group\">
                ";
            // line 9
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label');
            echo "

                <div class=\"controls\">
                    ";
            // line 12
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
            echo "
                    ";
            // line 13
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
                </div>
            </div>
        ";
        }
        // line 17
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 18
        echo "
";
    }

    // line 21
    public function block_form_label($context, array $blocks = [])
    {
        // line 22
        ob_start();
        // line 23
        echo "    ";
        if ((($context["label"] ?? null) != false)) {
            // line 24
            echo "        ";
            if ( !($context["compound"] ?? null)) {
                // line 25
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["for" => ($context["id"] ?? null)]);
                // line 26
                echo "        ";
            }
            // line 27
            echo "        ";
            if (($context["required"] ?? null)) {
                // line 28
                echo "           ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["class" => twig_trim_filter((((twig_get_attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", true, true, false, 28)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", false, false, false, 28), "")) : ("")) . " control-label"))]);
                // line 29
                echo "        ";
            }
            // line 30
            echo "        ";
            if (twig_test_empty(($context["label"] ?? null))) {
                // line 31
                echo "            ";
                $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize(($context["name"] ?? null));
                // line 32
                echo "        ";
            }
            // line 33
            echo "        <label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? null));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), [], ($context["translation_domain"] ?? null)), "html", null, true);
            echo "</label>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 38
    public function block_form_widget_simple($context, array $blocks = [])
    {
        // line 39
        echo "     ";
        ob_start();
        // line 40
        echo "         ";
        $context["type"] = (((isset($context["type"]) || array_key_exists("type", $context))) ? (_twig_default_filter(($context["type"] ?? null), "text")) : ("text"));
        // line 41
        echo "         <input type=\"";
        echo twig_escape_filter($this->env, ($context["type"] ?? null), "html", null, true);
        echo "\"  ";
        if (!twig_in_filter(($context["type"] ?? null), [0 => "hidden"])) {
            echo "class=\"form-control\" ";
        }
        echo " ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " ";
        if ( !twig_test_empty(($context["value"] ?? null))) {
            echo "value=\"";
            echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
            echo "\" ";
        }
        echo "/>
     ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 43
        echo " ";
    }

    // line 46
    public function block_widget_attributes($context, array $blocks = [])
    {
        // line 47
        echo "    ";
        ob_start();
        // line 48
        echo "        id=\"";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, ($context["full_name"] ?? null), "html", null, true);
        echo "\"
      ";
        // line 52
        if (($context["disabled"] ?? null)) {
            echo " disabled=\"disabled\"";
        }
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["attr"] ?? null));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            // line 56
            echo " ";
            // line 57
            if (twig_in_filter($context["attrname"], [0 => "placeholder", 1 => "title"])) {
                // line 58
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["attrvalue"], [], ($context["translation_domain"] ?? null)), "html", null, true);
                echo "\"";
            } elseif ((            // line 59
$context["attrvalue"] == true)) {
                // line 60
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "\"";
            } elseif ((            // line 61
$context["attrvalue"] != false)) {
                // line 62
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 69
    public function block_button_attributes($context, array $blocks = [])
    {
        // line 70
        echo "    ";
        ob_start();
        // line 71
        echo "        id=\"";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, ($context["full_name"] ?? null), "html", null, true);
        echo "\"";
        if (($context["disabled"] ?? null)) {
            echo " disabled=\"disabled\"";
        }
        // line 72
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["attr"] ?? null));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            // line 73
            echo " ";
            // line 74
            if (twig_in_filter($context["attrname"], [0 => "placeholder", 1 => "title"])) {
                // line 75
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["attrvalue"], [], ($context["translation_domain"] ?? null)), "html", null, true);
                echo "\"";
            } elseif ((            // line 76
$context["attrvalue"] == true)) {
                // line 77
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "\"";
            } elseif ((            // line 78
$context["attrvalue"] != false)) {
                // line 79
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 85
    public function block_form_start($context, array $blocks = [])
    {
        // line 86
        echo "    ";
        ob_start();
        // line 87
        echo "        ";
        $context["method"] = twig_upper_filter($this->env, ($context["method"] ?? null));
        // line 88
        echo "        ";
        if (twig_in_filter(($context["method"] ?? null), [0 => "GET", 1 => "POST"])) {
            // line 89
            echo "            ";
            $context["form_method"] = ($context["method"] ?? null);
            // line 90
            echo "        ";
        } else {
            // line 91
            echo "            ";
            $context["form_method"] = "POST";
            // line 92
            echo "        ";
        }
        // line 93
        echo "

        <form name=\"";
        // line 95
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 95), "name", [], "any", false, false, false, 95), "html", null, true);
        echo "\" method=\"";
        echo twig_escape_filter($this->env, twig_lower_filter($this->env, ($context["form_method"] ?? null)), "html", null, true);
        echo "\" action=\"";
        echo twig_escape_filter($this->env, ($context["action"] ?? null), "html", null, true);
        echo "\"";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["attr"] ?? null));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        if (($context["multipart"] ?? null)) {
            echo " enctype=\"multipart/form-data\"";
        }
        echo ">
        ";
        // line 96
        if ((($context["form_method"] ?? null) != ($context["method"] ?? null))) {
            // line 97
            echo "            <input type=\"hidden\" name=\"_method\" value=\"";
            echo twig_escape_filter($this->env, ($context["method"] ?? null), "html", null, true);
            echo "\" />
        ";
        }
        // line 99
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 102
    public function block_button_row($context, array $blocks = [])
    {
        // line 103
        echo "     ";
        ob_start();
        // line 104
        echo "         ";
        // line 112
        echo "         ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
         ";
        // line 120
        echo "     ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 121
        echo " ";
    }

    // line 124
    public function block_button_widget($context, array $blocks = [])
    {
        // line 125
        echo "    ";
        ob_start();
        // line 126
        echo "        ";
        if (twig_test_empty(($context["label"] ?? null))) {
            // line 127
            echo "            ";
            $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize(($context["name"] ?? null));
            // line 128
            echo "        ";
        }
        // line 129
        echo "        <button type=\"";
        echo twig_escape_filter($this->env, (((isset($context["type"]) || array_key_exists("type", $context))) ? (_twig_default_filter(($context["type"] ?? null), "button")) : ("button")), "html", null, true);
        echo "\" ";
        $this->displayBlock("button_attributes", $context, $blocks);
        echo ">";
        echo ($context["label"] ?? null);
        echo "</button>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 133
    public function block_submit_widget($context, array $blocks = [])
    {
        // line 134
        echo "    ";
        ob_start();
        // line 135
        echo "        ";
        $context["type"] = (((isset($context["type"]) || array_key_exists("type", $context))) ? (_twig_default_filter(($context["type"] ?? null), "submit")) : ("submit"));
        // line 136
        echo "        ";
        $this->displayBlock("button_widget", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 140
    public function block_reset_widget($context, array $blocks = [])
    {
        // line 141
        echo "    ";
        ob_start();
        // line 142
        echo "        ";
        $context["type"] = (((isset($context["type"]) || array_key_exists("type", $context))) ? (_twig_default_filter(($context["type"] ?? null), "reset")) : ("reset"));
        // line 143
        echo "        ";
        $this->displayBlock("button_widget", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 148
    public function block_choice_widget_collapsed($context, array $blocks = [])
    {
        // line 149
        echo "        ";
        ob_start();
        // line 150
        echo "            ";
        // line 160
        echo "
            <select class='form-control' ";
        // line 161
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (($context["multiple"] ?? null)) {
            echo " multiple=\"multiple\"";
        }
        echo ">

                ";
        // line 163
        if ((twig_length_filter($this->env, ($context["preferred_choices"] ?? null)) > 0)) {
            // line 164
            echo "                    ";
            $context["options"] = ($context["preferred_choices"] ?? null);
            // line 165
            echo "                    ";
            $this->displayBlock("choice_widget_options", $context, $blocks);
            echo "
                    ";
            // line 166
            if (((twig_length_filter($this->env, ($context["choices"] ?? null)) > 0) &&  !(null === ($context["separator"] ?? null)))) {
                // line 167
                echo "                        <option disabled=\"disabled\">";
                echo twig_escape_filter($this->env, ($context["separator"] ?? null), "html", null, true);
                echo "</option>
                    ";
            }
            // line 169
            echo "                ";
        }
        // line 170
        echo "                ";
        $context["options"] = ($context["choices"] ?? null);
        // line 171
        echo "                ";
        $this->displayBlock("choice_widget_options", $context, $blocks);
        echo "
            </select>
        ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 174
        echo "    ";
    }

    // line 176
    public function block_checkbox_widget($context, array $blocks = [])
    {
        // line 177
        echo "    ";
        ob_start();
        // line 178
        echo "        <input type=\"checkbox\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if ((isset($context["value"]) || array_key_exists("value", $context))) {
            echo " value=\"";
            echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
            echo "\"";
        }
        if (($context["checked"] ?? null)) {
            echo " checked=\"checked\"";
        }
        echo " />

    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 183
    public function block_radio_widget($context, array $blocks = [])
    {
        // line 184
        echo "    ";
        ob_start();
        // line 185
        echo "        <input type=\"radio\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if ((isset($context["value"]) || array_key_exists("value", $context))) {
            echo " value=\"";
            echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
            echo "\"";
        }
        if (($context["checked"] ?? null)) {
            echo " checked=\"checked\"";
        }
        echo " />
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "::forms.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  587 => 185,  584 => 184,  581 => 183,  564 => 178,  561 => 177,  558 => 176,  554 => 174,  547 => 171,  544 => 170,  541 => 169,  535 => 167,  533 => 166,  528 => 165,  525 => 164,  523 => 163,  515 => 161,  512 => 160,  510 => 150,  507 => 149,  504 => 148,  496 => 143,  493 => 142,  490 => 141,  487 => 140,  479 => 136,  476 => 135,  473 => 134,  470 => 133,  458 => 129,  455 => 128,  452 => 127,  449 => 126,  446 => 125,  443 => 124,  439 => 121,  436 => 120,  431 => 112,  429 => 104,  426 => 103,  423 => 102,  418 => 99,  412 => 97,  410 => 96,  386 => 95,  382 => 93,  379 => 92,  376 => 91,  373 => 90,  370 => 89,  367 => 88,  364 => 87,  361 => 86,  358 => 85,  345 => 79,  343 => 78,  338 => 77,  336 => 76,  331 => 75,  329 => 74,  327 => 73,  323 => 72,  314 => 71,  311 => 70,  308 => 69,  295 => 62,  293 => 61,  288 => 60,  286 => 59,  281 => 58,  279 => 57,  277 => 56,  273 => 54,  269 => 52,  262 => 48,  259 => 47,  256 => 46,  252 => 43,  234 => 41,  231 => 40,  228 => 39,  225 => 38,  203 => 33,  200 => 32,  197 => 31,  194 => 30,  191 => 29,  188 => 28,  185 => 27,  182 => 26,  179 => 25,  176 => 24,  173 => 23,  171 => 22,  168 => 21,  163 => 18,  160 => 17,  153 => 13,  149 => 12,  143 => 9,  140 => 8,  132 => 5,  129 => 4,  126 => 3,  123 => 2,  120 => 1,  113 => 188,  111 => 183,  108 => 182,  106 => 176,  103 => 175,  101 => 148,  97 => 146,  95 => 140,  92 => 139,  90 => 133,  87 => 132,  85 => 124,  81 => 122,  79 => 102,  76 => 101,  74 => 85,  71 => 84,  69 => 69,  66 => 68,  64 => 46,  60 => 44,  58 => 38,  55 => 37,  53 => 21,  50 => 20,  48 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "::forms.html.twig", "/var/www/narfi/app/Resources/views/forms.html.twig");
    }
}
