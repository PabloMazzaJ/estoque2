<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_edit.html.twig */
class __TwigTemplate_96bea2268fb831df76f598d6c2b3151dfef18b82ae7887622f5a9f157817f1bc extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_edit.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo "Cadastro Contrato";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Pessoal Externo</a></span></li>
     <li><span><a href=\"";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_index");
        echo "\" class=\"breadcumbComLink\">Contrato</a></span></li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Cadastro</a></span></li>
 ";
    }

    // line 9
    public function block_conteudo($context, array $blocks = [])
    {
        // line 10
        echo "    <header class=\"panel-heading\">
        <h2 class=\"panel-title\">
            <span class=\"va-middle\"><i class=\"fa fa-paste\"></i> Cadastro de Contrato</span>
        </h2>
    </header>
    <div class=\"panel-body\">
        ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
        <div class=\"col-md-4\">
            ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "descricao", [], "any", false, false, false, 18), 'row');
        echo "
            ";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "editalNumero", [], "any", false, false, false, 19), 'row');
        echo "
            ";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "idCurso", [], "any", false, false, false, 20), 'row', ["attr" => ["id" => "cursoClass"]]);
        echo "
            ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "idPapel", [], "any", false, false, false, 21), 'row');
        echo "
            <div>
                <a href=\"";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntecontrato_index");
        echo "\" class=\"btn-gerar-relatorio2\">
                    <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"expButton\"><i class=\"fa fa-arrow-circle-left\"></i></button>
                </a>
                <button type=\"submit\" class=\"btn btn-primary btn-sm\" id=\"expButton\">Editar</button>
            </div>
        </div>
    </div>
    ";
        // line 30
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "

";
    }

    // line 34
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 35
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>

        \$(function() {
            \$('#nteContrato_Enviar').hide();
        });

    </script>
";
    }

    // line 45
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 46
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>

        .btn {
            min-width: 9em;
            max-width: 9em;
        }

        #nteContrato_idCurso{
            height: 11em;
        }

        tbody tr th {
            text-align: center;
        }

        table th,
        table  {
            text-align: left;
            vertical-align: middle;
        }

        #aux {
            margin-left: 2em;
        }

        .btn.btn-success {
            margin-top: 1em;
        }

        .col-md-1 label {
            width: 80%;
        }
        .col-md-1 input {
            width: 20%;
        }
        .col-md-2 label {
            width: 90%;
        }
        .col-md-2 input {
            width: 10%;
        }

        li span a:hover{
            text-decoration: none;
        }

        #nteContrato_contratoItem > input {
            clear: left;
        }

        .breadcumbSemLink {
            cursor: no-drop;
        }

        .breadcumbComLink {
            cursor: pointer;
        }
    </style>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 46,  140 => 45,  126 => 35,  123 => 34,  116 => 30,  106 => 23,  101 => 21,  97 => 20,  93 => 19,  89 => 18,  84 => 16,  76 => 10,  73 => 9,  66 => 6,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_edit.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontrato/contrato_edit.html.twig");
    }
}
