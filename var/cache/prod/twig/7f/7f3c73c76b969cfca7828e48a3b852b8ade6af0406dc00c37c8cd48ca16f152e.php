<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* FOSUserBundle:Security:form.html.twig */
class __TwigTemplate_d3ddedb6e0f6999d861beaf3d0ebe4f7aed4b352959211119fff61aee9919987 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"featured-boxes\">
                <div class=\"row\">
                    ";
        // line 7
        if (($context["error"] ?? null)) {
            // line 8
            echo "                        <div class=\"alert alert-warning\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, ($context["error"] ?? null), "messageKey", [], "any", false, false, false, 8), twig_get_attribute($this->env, $this->source, ($context["error"] ?? null), "messageData", [], "any", false, false, false, 8), "security"), "html", null, true);
            echo "  </div>
                    ";
        }
        // line 10
        echo "                    <div class=\"col-md-6 col-md-offset-3 col-sm-12\">
                        <div class=\"featured-box featured-box-primary align-left mt-xlg\" style=\"height: 327px;\">
                            <div class=\"box-content\">
                                <h4 class=\"heading-primary text-uppercase mb-md\">Acesso Administrativo</h4>

                                <form action=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_security_check");
        echo "\" method=\"post\" role=\"form\">
                                    <div class=\"row\">
                                        <div class=\"form-group\">
                                            <div class=\"col-md-12\">
                                                <label>";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security.login.username", [], "FOSUserBundle"), "html", null, true);
        echo "</label>
                                                <input type=\"text\" value=\"\" class=\"form-control input-lg \" id=\"username\"
                                                       name=\"_username\" value=\"";
        // line 21
        echo twig_escape_filter($this->env, ($context["last_username"] ?? null), "html", null, true);
        echo "\"
                                                       placeholder=\"Informe seu Usuário\">
                                            </div>
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"form-group\">
                                            <div class=\"col-md-12\">
                                                <label>";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security.login.password", [], "FOSUserBundle"), "html", null, true);
        echo "</label>
                                                <input type=\"password\" id=\"password\" name=\"_password\"
                                                       placeholder=\"*************\" class=\"form-control input-lg\">
                                            </div>
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-md-5\">
                                            <span class=\"remember-box checkbox\">
                                                <label for=\"remember_me\">
                                                    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\"
                                                           checked>";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security.login.remember_me", [], "FOSUserBundle"), "html", null, true);
        echo " </label>
                                            </span>
                                        </div>
                                        <div class=\"col-md-2\">
                                            <input type=\"submit\" id=\"_submit\" name=\"_submit\"
                                                   value=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security.login.submit", [], "FOSUserBundle"), "html", null, true);
        echo "\"
                                                   class=\"btn btn-primary  mb-xl\"
                                                   data-loading-text=\"Loading...\">
                                        </div>
                                    </div>
                                    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
        echo "\"/>
                                </form>
                            </div>
                        </div>
                    </div>
                    ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 55), "flashBag", [], "any", false, false, false, 55), "all", [], "any", false, false, false, 55));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 56
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 57
                echo "                            <div class=\"";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\">
                                ";
                // line 58
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["message"], [], "FOSUserBundle"), "html", null, true);
                echo "
                            </div>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 61
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "                </div>

            </div>
        </div>
    </div>

</div>";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 62,  141 => 61,  132 => 58,  127 => 57,  122 => 56,  118 => 55,  110 => 50,  102 => 45,  94 => 40,  80 => 29,  69 => 21,  64 => 19,  57 => 15,  50 => 10,  44 => 8,  42 => 7,  35 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "FOSUserBundle:Security:form.html.twig", "/var/www/narfi/app/Resources/FOSUserBundle/views/Security/form.html.twig");
    }
}
