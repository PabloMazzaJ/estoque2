<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_validar_turmas.html.twig */
class __TwigTemplate_af48e06c6a5ab01377149e7f93c46e1bb50f5a185ce12f7cc536aa13ca197f8b extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_validar_turmas.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Detalhes Contrato ";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Pessoal Externo</a></span></li>
     <li><span><a href=\"#\" class=\"breadcumbSemLink\">Situação Turmas</a></span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "    <header class=\"panel-heading center\" style=\"width: 70%\">
        <div>
            <h2 class=\"panel-title\" >
                <span class=\"va-middle\"><i class=\"fa fa-paste\"></i> Gerenciar Validade das Turmas</span>
            </h2>
        </div>
    </header>
    <div>
        <div class=\"panel-body center\" style=\"width: 70%\">
            <table class=\"table table-striped\" data-pessoa-contrato=\"";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["ntePessoaContrato"] ?? null), "id", [], "any", false, false, false, 18), "html", null, true);
        echo "\">
                <tr>
                    <th class=\"col-md-2\">Turma</th>
                    <th class=\"col-md-2\">Validade</th>
                </tr>
                ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["turmasValidacao"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["turma"]) {
            // line 24
            echo "                    <tr>
                        <td>
                            ";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["turma"], "nome", [], "any", false, false, false, 26), "html", null, true);
            echo "
                        </td>
                        <td>
                            <select class=\"form-control\" data-nte-contrato-tem-sie-turma-id=\"";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["turma"], "id", [], "any", false, false, false, 29), "html", null, true);
            echo "\" data-turma-nome=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["turma"], "nome", [], "any", false, false, false, 29), "html", null, true);
            echo "\" style=\"width: 70%\">
                                <option selected=\"selected\">
                                    ";
            // line 31
            if ((twig_get_attribute($this->env, $this->source, $context["turma"], "valido", [], "any", false, false, false, 31) == 1)) {
                // line 32
                echo "                                        Turma Válida
                                    ";
            } else {
                // line 34
                echo "                                        Turma Inválida
                                    ";
            }
            // line 36
            echo "                                </option>

                                ";
            // line 38
            if ((twig_get_attribute($this->env, $this->source, $context["turma"], "valido", [], "any", false, false, false, 38) == 1)) {
                // line 39
                echo "                                    <option>Turma Inválida</option>
                                ";
            } else {
                // line 41
                echo "                                    <option>Turma Válida</option>
                                ";
            }
            // line 43
            echo "
                            </select>
                            <i class=\"fa fa-spinner fa-spin fa-2x fa-fw\"></i>
                        </td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['turma'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "            </table>

            <a href=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntevinculo_itens_pessoa_contrato_detalhes", ["id" => twig_get_attribute($this->env, $this->source, ($context["ntePessoaContrato"] ?? null), "id", [], "any", false, false, false, 51)]), "html", null, true);
        echo "\" class=\"btn-gerar-relatorio2 left\">
                <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"expButton\"><i class=\"fa fa-arrow-circle-left\"></i></button>
            </a>

        </div>
    </div>
";
    }

    // line 59
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 60
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>

        function get_center_pos(width, top)
        {
            if (!top)
            {
                top = 30;
                \$('.ui-pnotify').each(function() { top += \$(this).outerHeight() + 20; });
            }
            return {
                \"top\": top, \"left\": (\$(window).width() / 2) - (width / 2)
            }
        }

        function notificaUsuario(titulo, texto, tipo)
        {
            new PNotify(
                {
                    title: titulo,
                    text: texto,
                    type: tipo,
                    delay: 3000,
                    before_open: function (PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    }
                }
            );
        }

        \$(function ()
        {
            \$(window).resize(function () {
                \$(\".ui-pnotify\").each(function () {
                    \$(this).css(get_center_pos(\$(this).width(), \$(this).position().top))
                });
            });

            hideLoadingSpinners();
            selectConfig();
        });

        function hideLoadingSpinners()
        {
            \$('td i').animate({opacity:0});
        }

        function selectConfig()
        {
            \$('select').select2({
                width: 'resolve',
                minimumResultsForSearch: -1
            }).on('change', function () {
                \$(this).siblings('i').animate({opacity:1}).animate({opacity:0});
                let valido = \$(this).val();
                let nteContratoTemSieTurmaId = \$(this).data('nte-contrato-tem-sie-turma-id');
                let nomeTurma = \$(this).data('turma-nome');

                validarTurma(nteContratoTemSieTurmaId, valido, nomeTurma);
            });
        }


        function validarTurma(nteContratoTemSieTurmaId, valido, nomeTurma)
        {
            \$.ajax({
               url: \"";
        // line 126
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pessoa_externa_api_valida_turma");
        echo "\",
               dataType: 'json',
                method: 'POST',
                data: {
                    nteContratoTemSieTurmaIdKey: nteContratoTemSieTurmaId,
                    validoKey: valido
                },
                success: function (data)
                {
                    if (valido == 'Turma Válida') {
                        notificaUsuario('' , \"A turma \" + nomeTurma + \" agora possui o status de Válida. \", 'success');
                    } else {
                        notificaUsuario('' , \"A turma \" + nomeTurma + \" agora possui o status de Inválida. \", 'danger');
                    }
                },
                error: function (data) {
                }
            });
        }

    </script>
";
    }

    // line 149
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 150
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>

        .left {
            margin: auto;
            float: left;
        }

        .center {
            margin: auto;
            width: 50%;
        }


        .btn {
            min-width: 9em;
            max-width: 9em;
        }

        tr td { text-align: center; }

        textarea { resize: none; }

        table th, td { text-align: center;
            vertical-align: middle; }

        li span a:hover{ text-decoration: none; }

        .breadcumbSemLink { cursor: no-drop; }

        .breadcumbComLink { cursor: pointer; }

    </style>

";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_validar_turmas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  264 => 150,  261 => 149,  235 => 126,  165 => 60,  162 => 59,  151 => 51,  147 => 49,  136 => 43,  132 => 41,  128 => 39,  126 => 38,  122 => 36,  118 => 34,  114 => 32,  112 => 31,  105 => 29,  99 => 26,  95 => 24,  91 => 23,  83 => 18,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntecontrato:contrato_validar_turmas.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntecontrato/contrato_validar_turmas.html.twig");
    }
}
