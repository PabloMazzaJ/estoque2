<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :widgets:_form-busca.html.twig */
class __TwigTemplate_20d1791d3e3f2f283debc0d00abe9b97143d947dd4c10a46f30cc88b1e963f59 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<form action=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_tube_catalogo_homepage");
        echo "\" method=\"POST\" role=\"search\"
      class=\"search \">
    <div class=\"input-group input-search\">
        <input type=\"text\" class=\"form-control texto2\"
               name=\"texto\"
               id=\"texto\"
               value=\"";
        // line 7
        (((isset($context["texto"]) || array_key_exists("texto", $context))) ? (print (twig_escape_filter($this->env, ($context["texto"] ?? null), "html", null, true))) : (print ("")));
        echo "\"
               placeholder=\"Pesquise um tema\">
                    <span class=\"input-group-btn\">
                        <button aria-label=\"Botão pesquisar tema\" class=\"btn btn-default\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
                    </span>
    </div>
</form>";
    }

    public function getTemplateName()
    {
        return ":widgets:_form-busca.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 7,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":widgets:_form-busca.html.twig", "/var/www/narfi/app/Resources/views/widgets/_form-busca.html.twig");
    }
}
