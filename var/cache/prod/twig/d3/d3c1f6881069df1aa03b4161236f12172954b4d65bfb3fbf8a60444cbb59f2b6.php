<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteUsuarioBundle:Usuario:Usuario.html.twig */
class __TwigTemplate_000bd19317d9f917cc6b4f1ddb6245325db2d6c81d3e9b54d0bb6fde8d211e7c extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteUsuarioBundle:Usuario:Usuario.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Administração de Usuários";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Administração Usuários";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_index");
        echo "\"> Usuários </a></span></li>
 ";
    }

    // line 7
    public function block_conteudo($context, array $blocks = [])
    {
        // line 8
        echo "    <div class=\"col-md-3 col-lg-2 col-sm-12\">
        <section class=\"panel\">
            <div class=\"panel-body\">
                <form class=\"\" action=\"#\">
                    <div class=\"form-group\">
                        <label class=\"control-label\">Usuário ou E-Mail </label>
                        <input class=\"form-control\">
                    </div>
                    <div class=\"form-group\">
                        ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range("A", "Z"));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 18
            echo "                            <a class=\"#\"> ";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo " </a>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "                    </div>
                    <div class=\"form-group\">
                        <button class=\"btn btn-primary btn-block\"><i class=\"fa fa-search\"></i> Procurar</button>
                    </div>
                </form>
            </div>
        </section>
        ";
        // line 27
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN_USER")) {
            // line 28
            echo "        <section class=\"panel\">
            <div class=\"panel-body\">
                <a href=\"";
            // line 30
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_novo");
            echo "\" class=\"btn btn-default btn-block\">
                    <i class=\"fa fa-2x fa-user-plus\"></i><br>
                    Adicionar Usuários
                </a>
                <a href=\"";
            // line 34
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_novo");
            echo "\" class=\"btn btn-default btn-block\">
                    <i class=\"fa fa-2x fa-upload\"></i><br>
                    Importar Usuários
                </a>
            </div>
        </section>
        ";
        }
        // line 41
        echo "    </div>
    <div class=\"col-md-9 col-lg-10 col-sm-12\">
        <section class=\"panel\">
            <header class=\"panel-heading\">
                <h2 class=\"panel-title\">
                    <span class=\"va-middle\"> <i class=\"fa fa-users\"> </i> Lista de usuários </span>
                </h2>
            </header>
            <div class=\"panel-body\">
                <table class=\"table table-responsive table-condensed\">
                    ";
        // line 51
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["usuarios"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
            // line 52
            echo "                        <tr>
                            <td width=\"64\">
                                <div class=\"image rounded \">
                                    <a title=\"clique aqui para visualizar o usuário de ";
            // line 55
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "nome", [], "any", false, false, false, 55), "html", null, true);
            echo "\"
                                       href=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_perfil", ["id" => twig_get_attribute($this->env, $this->source, $context["u"], "id", [], "any", false, false, false, 56)]), "html", null, true);
            echo "\">
                                        <img width=\"64\"
                                             src=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_imagem_perfil", ["id" => twig_get_attribute($this->env, $this->source, $context["u"], "id", [], "any", false, false, false, 58)]), "html", null, true);
            echo "\"
                                             alt=\"";
            // line 59
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "nome", [], "any", false, false, false, 59), "html", null, true);
            echo "\" class=\"img-circle img-responsive\">
                                    </a>
                                </div>
                            </td>
                            <td>
                                <a title=\"clique aqui para visualizar o usuário de ";
            // line 64
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "nome", [], "any", false, false, false, 64), "html", null, true);
            echo "\"
                                   href=\"";
            // line 65
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_perfil", ["id" => twig_get_attribute($this->env, $this->source, $context["u"], "id", [], "any", false, false, false, 65)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "nome", [], "any", false, false, false, 65), "html", null, true);
            echo "</a><br>
                                ";
            // line 66
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "email", [], "any", false, false, false, 66), "html", null, true);
            echo "
                            </td>
                            <td class=\"text-right\">
                                ";
            // line 69
            if ((twig_get_attribute($this->env, $this->source, $context["u"], "lastLogin", [], "any", false, false, false, 69) != null)) {
                // line 70
                echo "                                Último acesso em <br>
                                <small>";
                // line 71
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "lastLogin", [], "any", false, false, false, 71), "d/m/Y H:i:s"), "html", null, true);
                echo "</small>
                                ";
            } else {
                // line 73
                echo "                                Nunca acessou o sistema
                                ";
            }
            // line 75
            echo "                            </td>
                            ";
            // line 76
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN_USER")) {
                // line 77
                echo "                            <td width=\"130\">
                                <div class=\"btn-group btn-group-justified\">
                                    <a class=\"btn btn-default\"
                                       title=\"Clique aqui para ativar/desativar o usuário  ";
                // line 80
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "username", [], "any", false, false, false, 80), "html", null, true);
                echo "\"
                                       href=\"";
                // line 81
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_ativar", ["id" => twig_get_attribute($this->env, $this->source, $context["u"], "id", [], "any", false, false, false, 81)]), "html", null, true);
                echo "\">
                                        ";
                // line 82
                if ((twig_get_attribute($this->env, $this->source, $context["u"], "enabled", [], "any", false, false, false, 82) == 1)) {
                    // line 83
                    echo "                                        <i class=\"fa fa-toggle-on text-primary\"></i>
                                        ";
                } else {
                    // line 85
                    echo "                                        <i class=\"fa fa-toggle-off \"></i>
                                        ";
                }
                // line 87
                echo "                                    </a>
                                    <a class=\"btn btn-default\"
                                       title=\"Clique aqui para trocar a senha do usuário ";
                // line 89
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "username", [], "any", false, false, false, 89), "html", null, true);
                echo "\"
                                       href=\"";
                // line 90
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_editar", ["id" => twig_get_attribute($this->env, $this->source, $context["u"], "id", [], "any", false, false, false, 90)]), "html", null, true);
                echo "\">
                                        <i class=\"fa fa-lock\"></i>
                                    </a>
                                    <a class=\"btn btn-default\"
                                       title=\"Clique aqui para editar o usuário ";
                // line 94
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "username", [], "any", false, false, false, 94), "html", null, true);
                echo "\"
                                       href=\"";
                // line 95
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_editar", ["id" => twig_get_attribute($this->env, $this->source, $context["u"], "id", [], "any", false, false, false, 95)]), "html", null, true);
                echo "\">
                                        <i class=\"fa fa-edit\"></i>
                                    </a>
                                </div>
                            </td>
                            ";
            }
            // line 101
            echo "                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "                </table>
            </div>
        </section>
    </div>
";
    }

    public function getTemplateName()
    {
        return "NteUsuarioBundle:Usuario:Usuario.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  263 => 103,  256 => 101,  247 => 95,  243 => 94,  236 => 90,  232 => 89,  228 => 87,  224 => 85,  220 => 83,  218 => 82,  214 => 81,  210 => 80,  205 => 77,  203 => 76,  200 => 75,  196 => 73,  191 => 71,  188 => 70,  186 => 69,  180 => 66,  174 => 65,  170 => 64,  162 => 59,  158 => 58,  153 => 56,  149 => 55,  144 => 52,  140 => 51,  128 => 41,  118 => 34,  111 => 30,  107 => 28,  105 => 27,  96 => 20,  87 => 18,  83 => 17,  72 => 8,  69 => 7,  62 => 5,  59 => 4,  53 => 3,  47 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteUsuarioBundle:Usuario:Usuario.html.twig", "/var/www/narfi/src/Nte/UsuarioBundle/Resources/views/Usuario/Usuario.html.twig");
    }
}
