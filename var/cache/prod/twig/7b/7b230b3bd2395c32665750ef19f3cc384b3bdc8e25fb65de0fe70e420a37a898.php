<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:Relatorio:turmas.html.twig */
class __TwigTemplate_999fa0024bee2f46d97ceec9426378963382ecf091db6b912af53ab4c12db2af extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"panel-body\">
    <table class=\"table table-striped\">
        <tr>
            <th>Curso</th>
            <th>Disciplina</th>
            <th>Turma</th>
            <th>Inclusão</th>
            <th>Prof</th>
            <th>Alun</th>
            <th>Doc.</th>
        </tr>
        ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rel"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 13
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "disciplina", [], "any", false, false, false, 13), "turma", [], "any", false, false, false, 13));
            foreach ($context['_seq'] as $context["_key"] => $context["turma"]) {
                // line 14
                echo "
                <tr ";
                // line 15
                if ((twig_get_attribute($this->env, $this->source, $context["turma"], "valido", [], "any", false, false, false, 15) == 0)) {
                    echo " class=\"customizado-vermelho1\" ";
                }
                echo ">
                    <td>";
                // line 16
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "curso", [], "any", false, false, false, 16), "html", null, true);
                echo "</td>
                    <td>";
                // line 17
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["turma"], "disciplina", [], "any", false, false, false, 17), "html", null, true);
                echo "</td>

                    <td>

                        <a target=\"_blank\"
                           href=\"";
                // line 22
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_aplicacao_buscador_cursos_disciplina_turmas", ["turma" => twig_get_attribute($this->env, $this->source, $context["turma"], "id", [], "any", false, false, false, 22)]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["turma"], "codigo", [], "any", false, false, false, 22), "html", null, true);
                echo "</a>

                    </td>
                    <td>";
                // line 25
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["turma"], "dataCriacao", [], "any", false, false, false, 25), "d/m/Y H:i"), "html", null, true);
                echo "</td>
                    <td>";
                // line 26
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["turma"], "qtdProfessor", [], "any", false, false, false, 26), "html", null, true);
                echo "</td>
                    <td>";
                // line 27
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["turma"], "qtdAluno", [], "any", false, false, false, 27), "html", null, true);
                echo "</td>
                    <td>";
                // line 28
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["turma"], "qtdDoc", [], "any", false, false, false, 28), "html", null, true);
                echo "</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['turma'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 31
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "    </table>
</div>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:Relatorio:turmas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 32,  107 => 31,  98 => 28,  94 => 27,  90 => 26,  86 => 25,  78 => 22,  70 => 17,  66 => 16,  60 => 15,  57 => 14,  52 => 13,  48 => 12,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:Relatorio:turmas.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/Relatorio/turmas.html.twig");
    }
}
