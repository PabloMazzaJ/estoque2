<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteConfigMoodleBundle:nteconfigmoodle:index.html.twig */
class __TwigTemplate_eb1af406bbd07e191ccc703b8914650ab43e7fbfa06dbe603bfa82744e99a125 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteConfigMoodleBundle:nteconfigmoodle:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Configurações - Moodle";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Configurações";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "    <li><span>Painel de Configurações</span></li>
    <li><span>Moodle</span></li>
";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "    <section class=\"panel\">
        <div class=\"panel-heading\">
            <div class=\"text-left col-md-6\">
                <a href=\"";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_config_moodle_novo");
        echo "\" class=\"btn btn-default btn-sm simple-ajax-modal \"><i
                            class=\"fa fa-minus\"></i> Remover Ambeinte</a>
            </div>
            <div class=\"text-right col-md-6\">
                <a href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nteconfigmoodle_new");
        echo "\" class=\"btn btn-default btn-sm \"><i
                            class=\"fa fa-plus\"></i> Configurar Ambeinte</a>
            </div>
        </div>
        <div class=\" panel-body\">
            <table class=\"table table-striped \">
                <thead>
                <tr>
                    <th><input type=\"checkbox\" class=\"seleciona-tudo-table\"></th>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Serviço</th>
                    <th>Usuário</th>
                    <th>Token</th>
                    <th class=\"text-center\">Válido</th>
                    <th></th>
                </tr>
                </thead>

                ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["nteConfigMoodles"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["conf"]) {
            // line 36
            echo "                    <tr>
                        <td><input type=\"checkbox\" name=\"config\" value=\"";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "id", [], "any", false, false, false, 37), "html", null, true);
            echo "\"></td>
                        <td>";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "nome", [], "any", false, false, false, 38), "html", null, true);
            echo "</td>
                        <td>";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "descricao", [], "any", false, false, false, 39), "html", null, true);
            echo "</td>
                        <td>";
            // line 40
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "servico", [], "any", false, false, false, 40), "html", null, true);
            echo "</td>
                        <td>";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "usuario", [], "any", false, false, false, 41), "html", null, true);
            echo "</td>
                        <td>";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["conf"], "token", [], "any", false, false, false, 42), "html", null, true);
            echo "</td>
                        <td class=\"text-center\">
                            <i class=\"fa fa-";
            // line 44
            echo ((twig_get_attribute($this->env, $this->source, $context["conf"], "valido", [], "any", false, false, false, 44)) ? ("check-circle text-success") : ("minus-circle text-danger"));
            echo "\"></i>
                        </td>
                        <td>
                            <ul>
                                <li>
                                    <a href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nteconfigmoodle_show", ["id" => twig_get_attribute($this->env, $this->source, $context["conf"], "id", [], "any", false, false, false, 49)]), "html", null, true);
            echo "\">show</a>
                                </li>
                                <li>
                                    <a href=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nteconfigmoodle_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["conf"], "id", [], "any", false, false, false, 52)]), "html", null, true);
            echo "\">edit</a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['conf'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "            </table>
        </div>
    </section>
";
    }

    // line 62
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 63
        echo "    ";
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>
        \$('.simple-ajax-modal').magnificPopup({
            type: 'ajax',
            modal: true
        });
        jQuery(document).on('click', '.simple-ajax-action', function (e) {
            e.preventDefault();
            \$.ajax({
                type: 'GET',
                url: \$(this).attr('href'),
                dataType: 'json',
                complete: function (data) {
                    /*    new PNotify({
                     title: data.responseJSON.title,
                     text: data.responseJSON.text,
                     type: data.responseJSON.response,
                     //  addclass: data.responseJSON.response != \"success\"? 'notification-danger':null
                     });
                     */
                }
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "NteConfigMoodleBundle:nteconfigmoodle:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 63,  171 => 62,  164 => 58,  152 => 52,  146 => 49,  138 => 44,  133 => 42,  129 => 41,  125 => 40,  121 => 39,  117 => 38,  113 => 37,  110 => 36,  106 => 35,  84 => 16,  77 => 12,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  54 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteConfigMoodleBundle:nteconfigmoodle:index.html.twig", "/var/www/narfi/src/Nte/Config/MoodleBundle/Resources/views/nteconfigmoodle/index.html.twig");
    }
}
