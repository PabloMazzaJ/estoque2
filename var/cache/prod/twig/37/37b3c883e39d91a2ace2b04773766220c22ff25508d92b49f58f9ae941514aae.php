<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoBuscadorBundle:Pessoas:_cadastro.html.twig */
class __TwigTemplate_931bc32559777b8f081c7d009aa5444ab1bc01df4368b83d871afe2428db12af extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"col-sm-12 col-md-6\">
    <section class=\"panel\">
        <header class=\"panel-heading\">
            <h2 class=\"panel-title\">
                <span class=\"va-middle\">Informações de cadastro</span>
            </h2>
        </header>
        <div class=\"panel-body\">
            <div class=\"content\">
                <table class=\" table table-responsive\">
                    <tr>
                        <th width=\"50%\">Nome Civil</th>
                        <td>";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "pessoa", [], "any", false, false, false, 13), "nomeCivil", [], "any", false, false, false, 13), "html", null, true);
        echo "</td>
                    </tr>
                    ";
        // line 15
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "pessoa", [], "any", false, false, false, 15), "nome", [], "any", false, false, false, 15) != twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "pessoa", [], "any", false, false, false, 15), "nomeCivil", [], "any", false, false, false, 15))) {
            // line 16
            echo "                        <tr>
                            <th>Nome Social</th>
                            <td>";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "pessoa", [], "any", false, false, false, 18), "nome", [], "any", false, false, false, 18), "html", null, true);
            echo "</td>
                        </tr>
                    ";
        }
        // line 21
        echo "                    ";
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "deficiencias", [], "any", false, false, false, 21))) {
            // line 22
            echo "                        <tr>
                            <th>Deficiência</th>
                            <td>
                                <ul class=\"list-unstyled\">
                                    ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "deficiencias", [], "any", false, false, false, 26));
            foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
                // line 27
                echo "                                        <li>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "descricao", [], "any", false, false, false, 27), "html", null, true);
                echo "</li>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "                                </ul>
                            </td>
                        </tr>
                    ";
        }
        // line 33
        echo "
                    ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "documentos", [], "any", false, false, false, 34));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 35
            echo "                        <tr class=\"form-group\">
                            <th>";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "descricao", [], "any", false, false, false, 36), "html", null, true);
            echo "</th>
                            <td>";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["e"], "numero", [], "any", false, false, false, 37), "html", null, true);
            echo "</td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "                </table>
            </div>
        </div>
    </section>
</div>";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoBuscadorBundle:Pessoas:_cadastro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 40,  108 => 37,  104 => 36,  101 => 35,  97 => 34,  94 => 33,  88 => 29,  79 => 27,  75 => 26,  69 => 22,  66 => 21,  60 => 18,  56 => 16,  54 => 15,  49 => 13,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoBuscadorBundle:Pessoas:_cadastro.html.twig", "/var/www/narfi/src/Nte/Aplicacao/BuscadorBundle/Resources/views/Pessoas/_cadastro.html.twig");
    }
}
