<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAdminPainelBundle:Checagem:index-tarefa.html.twig */
class __TwigTemplate_b3b7f4f984fd5fdc588182efcbcfba29b27dac3c8a784097cd77e97d5776d2e8 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts' => [$this, 'block_javascripts'],
            'javascripts3' => [$this, 'block_javascripts3'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAdminPainelBundle:Checagem:index-tarefa.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " P";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Painel ";
    }

    // line 4
    public function block_conteudo($context, array $blocks = [])
    {
        // line 5
        echo "    <div class=\"col-md-6 col-lg-6 col-xl-6\">
        <div class=\"panel\">
            <div class=\"panel-body\">
                <form class=\"form-horizontal form-bordered\" action=\"#\">
                    ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["itens"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 10
            echo "                        <div class=\"form-group\">
                            <label class=\"col-md-6 col-sm-3 control-label\">";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "titulo", [], "any", false, false, false, 11), "html", null, true);
            echo "</label>
                            <div class=\"col-md-6\">
                                <a href=\"";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "url", [], "any", false, false, false, 13), "html", null, true);
            echo "\" class=\"btn \">
                                    ";
            // line 14
            if ((twig_get_attribute($this->env, $this->source, $context["item"], "check", [], "any", false, false, false, 14) == 0)) {
                // line 15
                echo "                                        <i class=\"fa fa-check-circle text-success\"></i> OK
                                    ";
            } else {
                // line 17
                echo "                                        <i class=\"fa fa-circle text-danger\"></i> Corrigir Pendências
                                    ";
            }
            // line 19
            echo "                                </a>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "                </form>
            </div>
        </div>
    </div>
    <div class=\"col-md-6 col-lg-6 col-xl-6\">
        <div class=\"panel\">
            <div class=\"panel-body\">
                <meter min=\"0\" max=\"100\" value=\"";
        // line 30
        echo twig_escape_filter($this->env, ($context["stats"] ?? null), "html", null, true);
        echo "\" id=\"meterDark\"></meter>
            </div>
            ";
        // line 32
        echo twig_escape_filter($this->env, twig_length_filter($this->env, ($context["itens"] ?? null)), "html", null, true);
        echo "
        </div>
    </div>
";
    }

    // line 36
    public function block_javascripts($context, array $blocks = [])
    {
        // line 37
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/snap.svg/snap.svg.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/liquid-meter/liquid.meter.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 41
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 42
        echo "    <script>
        /** Liquid Meter Dark */
        \$('#meterDark').liquidMeter({
            shape: 'square',
            color: '#0088CC',
            background: '#272A31',
            stroke: '#33363F',
            fontSize: '24px',
            fontWeight: '600',
            textColor: '#FFFFFF',
            liquidOpacity: 0.9,
            liquidPalette: ['#0088CC'],
            speed: 3000,
            animate: !\$.browser.mobile
        });

        if (typeof(\$.browser) != 'undefined') {
            if(\$.browser.mobile) {
                \$('#meter, #meterDark').liquidMeter('color', '#0088cc');
            }
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "NteAdminPainelBundle:Checagem:index-tarefa.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 42,  144 => 41,  138 => 39,  134 => 38,  129 => 37,  126 => 36,  118 => 32,  113 => 30,  104 => 23,  95 => 19,  91 => 17,  87 => 15,  85 => 14,  81 => 13,  76 => 11,  73 => 10,  69 => 9,  63 => 5,  60 => 4,  54 => 3,  48 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAdminPainelBundle:Checagem:index-tarefa.html.twig", "/var/www/narfi/src/Nte/Admin/PainelBundle/Resources/views/Checagem/index-tarefa.html.twig");
    }
}
