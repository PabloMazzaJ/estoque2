<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoBuscadorBundle:Pessoas:_filtro_pesquisa.html.twig */
class __TwigTemplate_830bce702ac3f31f19c28fb5acc426304b58b4b4019a347bfccca524e2a4dfb9 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<nav class=\"navbar navbar-default\">
    <div id=\"menu\" class=\"container-fluid\">
        <div class=\"navbar-form navbar-left\">
            <div class=\"form-group\">
                <select id=\"fnome\" data-plugin-selecttwo class=\" form-control\" style=\"width: 50%\"></select>
            </div>
        </div>
        <div class=\"navbar-form navbar-right\">
            <div class=\"form-group\">
                <select id=\"doc\" data-plugin-selecttwo class=\" form-control\">
                    <option data-mask='1' value=\"LOGIN\"> LOGIN</option>
                    <option data-mask='1' value=\"MatrAluno\"> Aluno Especial</option>
                    <option data-mask='000.000.000-00' value=\"CPF\"> Cadastro de Pessoas Físicas</option>
                    <option data-mask='00.000.000/0000-00' value=\"CNPJ\"> Cadastro Nacional de Pessoas Jurídicas
                    </option>
                    <!--
                    <option data-mask='0' value=\"RG\"> Carteira de Identidade</option>
                        <option data-mask='00' value=\"PASSAPORTE\"> Passaporte</option>
                        <option data-mask='0' value=\"CNH\"> Carteira Nacional de Habilitação</option>
                        <option data-mask='000000000000-0000-0000' value=\"TE\"> Título de Eleitor</option>
                        <option data-mask='0' value=\"CN\"> Certidão de Nascimento</option>
                        <option data-mask='0' value=\"CC\"> Certidão de Casamento</option>
                        <option data-mask='0' value=\"CT\"> Carteira de Trabalho</option>
                      -->
                </select>
            </div>
            <div class=\"form-group\">
                <div class=\"input-group\">
                    <input type=\"text\" class=\"form-control\" id=\"nro\" placeholder=\"Informe alguma coisa para buscar\">
                    <a id=\"pesquisar-pessoa\" class=\"input-group-addon btn btn-sm btn-success\">
                        <i class=\"fa fa-search\"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoBuscadorBundle:Pessoas:_filtro_pesquisa.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoBuscadorBundle:Pessoas:_filtro_pesquisa.html.twig", "/var/www/narfi/src/Nte/Aplicacao/BuscadorBundle/Resources/views/Pessoas/_filtro_pesquisa.html.twig");
    }
}
