<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:Relatorio:log-detalhe.html.twig */
class __TwigTemplate_1b8733f3958b42b4f8eff582c4d27a75e17ec3b1f5abb182f0cfca2db7ba176f extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoExportadorBundle:Relatorio:log-detalhe.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Log Resumido - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "nomeCurso", [], "any", false, false, false, 2), "html", null, true);
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "nomeCurso", [], "any", false, false, false, 3), "html", null, true);
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span>Relatório</span></li>
     <li><span>Log</span></li>
     <li><span>";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["me"] ?? null), "nomeCurso", [], "any", false, false, false, 7), "html", null, true);
        echo "</span></li>
 ";
    }

    // line 9
    public function block_conteudo($context, array $blocks = [])
    {
        // line 10
        echo "    <section class=\"panel \" id=\"relatorio\">
        <div class=\"panel-body\">
            <table class=\"table table-condensed table-striped table-bordered\">
                <tr>
                    <th>Data</th>
                    <th>Hora</th>
                    <th>Ambiente</th>
                    <th>Componente</th>
                    <th>Operação</th>
                    <th>Mensagem</th>
                </tr>

                ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rel"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 23
            echo "                    <tr>
                        <td>";
            // line 24
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "registroDataCriacao", [], "any", false, false, false, 24), "d/m/Y"), "html", null, true);
            echo "</td>
                        <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "registroDataCriacao", [], "any", false, false, false, 25), "H:i:s"), "html", null, true);
            echo "</td>
                        <td><a title=\"";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "idExportacao", [], "any", false, false, false, 26), "idMoodle", [], "any", false, false, false, 26), "descricao", [], "any", false, false, false, 26), "html", null, true);
            echo "\" href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "idExportacao", [], "any", false, false, false, 26), "idMoodle", [], "any", false, false, false, 26), "url", [], "any", false, false, false, 26), "html", null, true);
            echo "\">
                            ";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "idExportacao", [], "any", false, false, false, 27), "idMoodle", [], "any", false, false, false, 27), "nome", [], "any", false, false, false, 27), "html", null, true);
            echo "
                            </a>
                        </td>
                        <td>";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "descricaoTipoLog", [], "any", false, false, false, 30), "html", null, true);
            echo "</td>
                        <td>";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "descricaoOperacao1", [], "any", false, false, false, 31), "html", null, true);
            echo "</td>
                        <td>";
            // line 32
            echo twig_get_attribute($this->env, $this->source, $context["r"], "mensagem", [], "any", false, false, false, 32);
            echo "</td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "            </table>
        </div>
    </section>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:Relatorio:log-detalhe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 35,  125 => 32,  121 => 31,  117 => 30,  111 => 27,  105 => 26,  101 => 25,  97 => 24,  94 => 23,  90 => 22,  76 => 10,  73 => 9,  67 => 7,  63 => 5,  60 => 4,  54 => 3,  47 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:Relatorio:log-detalhe.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/Relatorio/log-detalhe.html.twig");
    }
}
