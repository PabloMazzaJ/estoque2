<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:exportacao:new.html.twig */
class __TwigTemplate_956c5ecb94d575776a3fe4da788a0da7d440c543a7c551516301d24fabcbf807 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
<header class=\"panel-heading\">
    <div class=\"panel-actions\">
        <a href=\"#\" class=\"panel-action panel-action-toggle\" data-panel-toggle></a>
        <a href=\"#\" class=\"panel-action panel-action-dismiss\" data-panel-dismiss></a>
    </div>
    <h2 class=\"panel-title\">";
        // line 7
        echo twig_escape_filter($this->env, ($context["titulo"] ?? null), "html", null, true);
        echo "</h2>
</header>
<div class=\"panel-body\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            ";
        // line 12
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "idMoodle", [], "any", false, false, false, 12), 'row');
        echo "
        </div>
        <div class=\"col-md-12\">
            ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "periodico", [], "any", false, false, false, 15), 'row');
        echo "
        </div>
    </div>
</div>
<div class=\"panel-footer text-right\">
    ";
        // line 20
        if ((isset($context["delete_form"]) || array_key_exists("delete_form", $context))) {
            // line 21
            echo "        <div class=\"pull-left\" id=\"y\"></div>
    ";
        }
        // line 23
        echo "    <div class=\"btn-group\">
        ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "salvar", [], "any", false, false, false, 24), 'row');
        echo "
    </div>
</div>
";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
";
        // line 28
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "

";
        // line 30
        if ((isset($context["delete_form"]) || array_key_exists("delete_form", $context))) {
            // line 31
            echo "    <div id=\"x\">
        ";
            // line 32
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["delete_form"] ?? null), 'form');
            echo "
    </div>
";
        }
        // line 35
        echo "<style>
    table tr td:nth-child(2) {
        background: #f3f3f3;
        text-align: center;
        font-weight: bold;;
    }
    .checkbox-custom.checkbox-default {
        display: table-cell;
        float: left;
        margin-right: 15px;
    }
</style>
<script>
    ";
        // line 48
        $this->loadTemplate(":js:_form_validador.js.twig", "NteAplicacaoExportadorBundle:exportacao:new.html.twig", 48)->display(twig_array_merge($context, ["tipo" => 0]));
        // line 49
        echo "    \$('#y').html(\$('#x'));
    \$('#nte_aplicacao_exportadorbundle_middlewareexportacao_nomeCurso').val(\$('#raiz-cursos').jstree('get_selected'));
    \$('#nte_aplicacao_exportadorbundle_middlewareexportacao_idNivel').val(\$('#cursoNivel').val());
    \$('#nte_aplicacao_exportadorbundle_middlewareexportacao_idTipo').val(\$('#cursoTipo').val());
    \$('#nte_aplicacao_exportadorbundle_middlewareexportacao_idPeriodo').val(\$('#cursoPeriodo').val());
    \$('#nte_aplicacao_exportadorbundle_middlewareexportacao_ano').val(\$('#cursoAno').val());
</script>
";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:exportacao:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 49,  116 => 48,  101 => 35,  95 => 32,  92 => 31,  90 => 30,  85 => 28,  81 => 27,  75 => 24,  72 => 23,  68 => 21,  66 => 20,  58 => 15,  52 => 12,  44 => 7,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:exportacao:new.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/exportacao/new.html.twig");
    }
}
