<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ::topo.admin.html.twig */
class __TwigTemplate_2c681be97f1cd18e11605fded3c4bad79802ebd4e21c99dc47d61a5025b0a0d4 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        echo "
<header class=\"header\">
    <div class=\"logo-container\">
        <a href=\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_admin_painel_homepage");
        echo "\" class=\"logo\">
            <img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/img/marca_apenas_sigla.svg"), "html", null, true);
        echo "\" height=\"40\">
        </a>

        <div class=\"visible-xs toggle-sidebar-left\" data-toggle-class=\"sidebar-left-opened\" data-target=\"html\"
             data-fire-event=\"sidebar-left-opened\">
            <i class=\"fa fa-bars\" aria-label=\"Toggle sidebar\"></i>
        </div>
    </div>
    <div class=\"header-right\">
        <span class=\"separator\"></span>
        <ul class=\"notifications\">
            <li>
                <a href=\"#\" class=\"dropdown-toggle notification-icon\" data-toggle=\"dropdown\">
                    <i class=\"fa fa-tasks\"></i>
                    <span class=\"badge navbar-notification-tasks-number\"></span>
                </a>
                <div class=\"dropdown-menu notification-menu large\">
                    <div class=\"notification-title\">
                        <span class=\"pull-right label label-default navbar-notification-tasks-number\">0</span>
                        Pendências
                    </div>
                    <div class=\"content\">
                        <ul id='navbar-notification-tasks-list'>
                            <li>Parabéns, não existe nenhuma pendência!</li>
                        </ul>
                        <hr/>
                        <div class=\"text-right\">
                            <a href=\"";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_admin_checagem_tarefa");
        echo "\" class=\"view-more\">Ver todos</a>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <a href=\"#\" class=\"dropdown-toggle notification-icon\" data-toggle=\"dropdown\">
                    <i class=\"fa fa-envelope\"></i>
                    <span class=\"badge\"></span>
                </a>

                <div class=\"dropdown-menu notification-menu\">
                    <div class=\"notification-title\">
                        <span class=\"pull-right label label-default\">0</span>
                        Mensagens
                    </div>
                    <div class=\"content\">
                        <ul>
                            <li>Módulo inativo</li>
                        </ul>
                        <hr/>
                        <div class=\"text-right\">
                            <a href=\"#\" class=\"view-more\">Ver todos</a>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <a href=\"#\" class=\"dropdown-toggle notification-icon\" data-toggle=\"dropdown\">
                    <i class=\"fa fa-bell\"></i>
                </a>
                <div class=\"dropdown-menu notification-menu\">
                    <div class=\"notification-title\">
                        <span class=\"pull-right label label-default\">0</span>
                        Alertas
                    </div>
                    <div class=\"content\">
                        <ul>
                            <li>Módulo inativo</li>
                        </ul>
                        <hr/>
                        <div class=\"text-right\">
                            <a href=\"#\" class=\"view-more\">Ver todos</a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <span class=\"separator\"></span>

        <div id=\"userbox\" class=\"userbox\">
            <a href=\"#\" data-toggle=\"dropdown\">
                <figure class=\"profile-picture\">
                    <img src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(($context["imgUser"] ?? null)), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 86), "username", [], "any", false, false, false, 86), "html", null, true);
        echo "\" class=\"img-circle\"
                         data-lock-picture=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(($context["imgUser"] ?? null)), "html", null, true);
        echo "\"/>
                </figure>
                <div class=\"profile-info\" data-lock-name=\"";
        // line 89
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 89), "username", [], "any", false, false, false, 89), "html", null, true);
        echo "\"
                     data-lock-email=\"";
        // line 90
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 90), "email", [], "any", false, false, false, 90), "html", null, true);
        echo "\">
                    <span class=\"name\">";
        // line 91
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 91), "username", [], "any", false, false, false, 91), "html", null, true);
        echo "</span>
                    <span class=\"role\">Usuário</span>
                </div>
                <i class=\"fa custom-caret\"></i>
            </a>

            <div class=\"dropdown-menu\">
                <ul class=\"list-unstyled\">
                    <li class=\"divider\"></li>
                    <li>
                        <a role=\"menuitem\" tabindex=\"-1\" href=\"";
        // line 101
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nte_usuario_perfil");
        echo "\">
                            <i class=\"fa fa-user\"></i> Meu Perfil
                        </a>
                    </li><!--
                    <li>
                        <a role=\"menuitem\" tabindex=\"-1\" href=\"##\" data-lock-screen=\"true\">
                            <i class=\"fa fa-lock\"></i> Bloquear Tela
                        </a>
                    </li>-->
                    <li>
                        <a role=\"menuitem\" tabindex=\"-1\" href=\"";
        // line 111
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_security_logout");
        echo "\">
                            <i class=\"fa fa-power-off\"></i> Sair
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "::topo.admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 111,  162 => 101,  149 => 91,  145 => 90,  141 => 89,  136 => 87,  130 => 86,  74 => 33,  44 => 6,  40 => 5,  35 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "::topo.admin.html.twig", "/var/www/narfi/app/Resources/views/topo.admin.html.twig");
    }
}
