<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoExportadorBundle:Relatorio:log.html.twig */
class __TwigTemplate_bca72143d9bd7c2c8cf9c3b30dab88c200de0e5fdb7cb171514f672c1b23af69 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoExportadorBundle:Relatorio:log.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo " Log";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
        echo "Log";
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span>Relatório</span></li>
     <li><span>Log</span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "    <div class=\"col-md-7 col-sm-12\">
        <section class=\"panel \" id=\"relatorio\">
            <header class=\"panel-heading\">
                <h2 class=\"panel-title\">
                    <span class=\"va-middle\">Log resumido</span>
                </h2>
            </header>
            <div class=\"panel-body\">
                <table class=\"table table-condensed table-striped table-bordered\">
                    <tr>
                        <th>Oferta</th>
                        <th>Nome do Curso</th>
                        <th>última atualização</th>
                    </tr>
                    ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rel"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 24
            echo "                        <tr>
                            <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "descricaoPeriodoOferta", [], "any", false, false, false, 25), "html", null, true);
            echo "</td>
                            <td><a href=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_resumo", ["me" => twig_get_attribute($this->env, $this->source, $context["r"], "id", [], "any", false, false, false, 26)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "nomeCurso", [], "any", false, false, false, 26), "html", null, true);
            echo "</a></td>
                            <td class=\"center\"><b>";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "descricaoStatus", [], "any", false, false, false, 27), "html", null, true);
            echo "</b><br><small>";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "registroDataAtualizacao", [], "any", false, false, false, 27), "d/m/Y"), "html", null, true);
            echo " às ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "registroDataAtualizacao", [], "any", false, false, false, 27), "H:i:s"), "html", null, true);
            echo "</small></td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "                </table>
            </div>
        </section>
    </div>
    <div class=\"col-md-5 col-sm-12\">
    <section class=\"panel \" id=\"relatorio\">
        <header class=\"panel-heading\">
            <h2 class=\"panel-title\">
                <span class=\"va-middle\">Log  completo com as últimas tentativas</span>
            </h2>
        </header>
        <div class=\"panel-body\">
            <table class=\"table table-condensed table-striped table-bordered\">
                <tr>
                    <th>Tentativa</th>
                    <th>Nome do Curso</th>
                </tr>
                ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rel1"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 48
            echo "                <tr>
                    <td>
                        <a href=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("exportador_relatorio_log_detalhe", ["me" => (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["r"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["id"] ?? null) : null), "uuid" => (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["r"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["uuid"] ?? null) : null)]), "html", null, true);
            echo "\">
                            ";
            // line 51
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["r"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["uuid"] ?? null) : null), "d/m/Y H:i:s"), "html", null, true);
            echo "
                        </a>
                    </td>
                    <td>";
            // line 54
            echo twig_escape_filter($this->env, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["r"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["nomeCurso"] ?? null) : null), "html", null, true);
            echo "</td>
                </tr>

                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "            </table>
        </div>
    </section>
    </div>

";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoExportadorBundle:Relatorio:log.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 58,  154 => 54,  148 => 51,  144 => 50,  140 => 48,  136 => 47,  117 => 30,  104 => 27,  98 => 26,  94 => 25,  91 => 24,  87 => 23,  71 => 9,  68 => 8,  62 => 5,  59 => 4,  53 => 3,  47 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoExportadorBundle:Relatorio:log.html.twig", "/var/www/narfi/src/Nte/Aplicacao/ExportadorBundle/Resources/views/Relatorio/log.html.twig");
    }
}
