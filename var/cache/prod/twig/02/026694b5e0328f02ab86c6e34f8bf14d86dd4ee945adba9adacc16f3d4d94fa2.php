<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteConfigBaseBundle:Crontab:form.html.twig */
class __TwigTemplate_4dfd4285fbe64e1fd0bfe03d13eb28d7127eedbf1bd3c01ec7a2e875ed10f6d6 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
  ";
        // line 2
        $context["schedule"] = twig_split_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["cronJob"] ?? null), "schedule", [], "any", false, false, false, 2), " ");
        // line 3
        echo "<header class=\"panel-heading\">
    <div class=\"panel-actions\">
        <a href=\"#\" class=\"panel-action panel-action-toggle\" data-panel-toggle></a>
        <a href=\"#\" class=\"panel-action panel-action-dismiss\" data-panel-dismiss></a>
    </div>
    <h2 class=\"panel-title\">";
        // line 8
        echo twig_escape_filter($this->env, ($context["titulo"] ?? null), "html", null, true);
        echo "</h2>
</header>
<div class=\"panel-body\">

    <div class=\"row\">
        <div class=\"col-md-12\">
            <table class=\"table table-responsive\">
                <tr>
                    ";
        // line 16
        $context["sMinuto"] = twig_split_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["schedule"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[0] ?? null) : null), "/");
        // line 17
        echo "                    ";
        $context["sHora"] = twig_split_filter($this->env, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["schedule"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[1] ?? null) : null), "/");
        // line 18
        echo "                    ";
        $context["sMesDia"] = twig_split_filter($this->env, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["schedule"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[2] ?? null) : null), "/");
        // line 19
        echo "                    ";
        $context["sMes"] = twig_split_filter($this->env, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["schedule"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[3] ?? null) : null), "/");
        // line 20
        echo "                    ";
        $context["sSemana"] = twig_split_filter($this->env, (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = ($context["schedule"] ?? null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4[4] ?? null) : null), ",");
        // line 21
        echo "
                    <td width=\"120\">Minuto</td>
                    <td width=\"20\"><input type=\"checkbox\"  ";
        // line 23
        if (((($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = ($context["sMinuto"] ?? null)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666[0] ?? null) : null) == "*")) {
            echo " checked=\"true\" ";
        }
        echo " id=\"cron-minuto-checkbox\"/></td>
                    <td width=\"80\"><input type=\"text\" class=\"form-control scheduler-inputs\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = ($context["schedule"] ?? null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e[0] ?? null) : null), "html", null, true);
        echo "\"
                                          id=\"cron-minuto-input\"></td>
                    <td>
                        <div class=\"m-md slider-primary\" data-plugin-slider
                             data-plugin-options='{ \"value\": ";
        // line 28
        if ((((($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = ($context["sMinuto"] ?? null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52[0] ?? null) : null) == "*") && twig_get_attribute($this->env, $this->source, ($context["sMinuto"] ?? null), 1, [], "array", true, true, false, 28))) {
            echo " ";
            echo twig_escape_filter($this->env, (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = ($context["sMinuto"] ?? null)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136[1] ?? null) : null), "html", null, true);
        } elseif (((($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = ($context["sMinuto"] ?? null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386[0] ?? null) : null) != "*")) {
            echo twig_escape_filter($this->env, (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = ($context["sMinuto"] ?? null)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9[0] ?? null) : null), "html", null, true);
        } else {
            echo " -1 ";
        }
        echo ", \"range\": \"min\", \"max\": 59,\"min\":-1 }'
                             data-plugin-slider-checkbox=\"#cron-minuto-checkbox\"
                             data-plugin-slider-output=\"#cron-minuto-input\">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Hora</td>
                    <td class=\"\"><input type=\"checkbox\" ";
        // line 36
        if (((($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = ($context["sHora"] ?? null)) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae[0] ?? null) : null) == "*")) {
            echo " checked=\"true\" ";
        }
        echo " id=\"cron-hora-checkbox\"/></td>
                    <td><input type=\"text\" class=\"form-control scheduler-inputs\" value=\"";
        // line 37
        echo twig_escape_filter($this->env, (($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = ($context["schedule"] ?? null)) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f[1] ?? null) : null), "html", null, true);
        echo "\"
                               id=\"cron-hora-input\"></td>
                    <td>
                        <div class=\"m-md slider-primary\" data-plugin-slider
                             data-plugin-options='{ \"value\": ";
        // line 41
        if ((((($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = ($context["sHora"] ?? null)) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40[0] ?? null) : null) == "*") && twig_get_attribute($this->env, $this->source, ($context["sHora"] ?? null), 1, [], "array", true, true, false, 41))) {
            echo " ";
            echo twig_escape_filter($this->env, (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = ($context["sHora"] ?? null)) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f[1] ?? null) : null), "html", null, true);
        } elseif (((($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = ($context["sHora"] ?? null)) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760[0] ?? null) : null) != "*")) {
            echo twig_escape_filter($this->env, (($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = ($context["sHora"] ?? null)) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce[0] ?? null) : null), "html", null, true);
            echo " ";
        } else {
            echo "-1 ";
        }
        echo ", \"range\": \"min\", \"max\": 23, \"min\":-1 }'
                             data-plugin-slider-checkbox=\"#cron-hora-checkbox\"
                             data-plugin-slider-output=\"#cron-hora-input\">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Dia do Mês</td>
                    <td><input type=\"checkbox\" ";
        // line 49
        if (((($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = ($context["sMesDia"] ?? null)) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b[0] ?? null) : null) == "*")) {
            echo " checked=\"true\" ";
        }
        echo " id=\"cron-mes-dia-checkbox\"/></td>
                    <td><input type=\"text\" class=\"form-control scheduler-inputs\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = ($context["schedule"] ?? null)) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c[2] ?? null) : null), "html", null, true);
        echo "\"
                               id=\"cron-mes-dia-input\"></td>
                    <td>
                        <div class=\"m-lg slider-primary\" data-plugin-slider
                             data-plugin-options='{ \"value\": ";
        // line 54
        if ((((($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = ($context["sMesDia"] ?? null)) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972[0] ?? null) : null) == "*") && twig_get_attribute($this->env, $this->source, ($context["sMesDia"] ?? null), 1, [], "array", true, true, false, 54))) {
            echo " ";
            echo twig_escape_filter($this->env, (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = ($context["sMesDia"] ?? null)) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216[1] ?? null) : null), "html", null, true);
            echo " ";
        } elseif (((($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = ($context["sMesDia"] ?? null)) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0[0] ?? null) : null) != "*")) {
            echo twig_escape_filter($this->env, (($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = ($context["sMesDia"] ?? null)) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c[0] ?? null) : null), "html", null, true);
        } else {
            echo "0";
        }
        echo ", \"range\": \"min\", \"max\": 31,\"min\":0 }'
                             data-plugin-slider-checkbox=\"#cron-mes-dia-checkbox\"
                             data-plugin-slider-output=\"#cron-mes-dia-input\">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Mês</td>
                    <td><input type=\"checkbox\" ";
        // line 62
        if (((($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = ($context["sMes"] ?? null)) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f[0] ?? null) : null) == "*")) {
            echo " checked=\"true\" ";
        }
        echo " id=\"cron-mes-checkbox\"/></td>
                    <td><input type=\"text\" class=\"form-control scheduler-inputs\" value=\"";
        // line 63
        echo twig_escape_filter($this->env, (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = ($context["schedule"] ?? null)) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc[3] ?? null) : null), "html", null, true);
        echo "\"
                               id=\"cron-mes-input\"></td>
                    <td>
                        <div class=\"m-md slider-primary\" data-plugin-slider
                             data-plugin-options='{ \"value\": ";
        // line 67
        if ((((($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = ($context["sMes"] ?? null)) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55[0] ?? null) : null) == "*") && twig_get_attribute($this->env, $this->source, ($context["sMes"] ?? null), 1, [], "array", true, true, false, 67))) {
            echo " ";
            echo twig_escape_filter($this->env, (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = ($context["sMes"] ?? null)) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba[1] ?? null) : null), "html", null, true);
            echo " ";
        } elseif (((($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = ($context["sMes"] ?? null)) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78[0] ?? null) : null) != "*")) {
            echo twig_escape_filter($this->env, (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = ($context["sMes"] ?? null)) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de[0] ?? null) : null), "html", null, true);
        } else {
            echo "0";
        }
        echo ", \"range\": \"min\", \"max\": 12, \"min\":0 }'
                             data-plugin-slider-checkbox=\"#cron-mes-checkbox\"
                             data-plugin-slider-output=\"#cron-mes-input\">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Dia da Semana</td>
                    <td><input type=\"checkbox\" ";
        // line 75
        if (((($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = ($context["sSemana"] ?? null)) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828[0] ?? null) : null) == "*")) {
            echo " checked=\"true\" ";
        }
        echo " class=\"checkbox-semana-dia\"/></td>
                    <td>
                        <input type=\"text\" class=\"form-control scheduler-inputs\" value=\"";
        // line 77
        echo twig_escape_filter($this->env, (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = ($context["schedule"] ?? null)) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd[4] ?? null) : null), "html", null, true);
        echo "\"
                               id=\"cron-semana-dia\">
                    </td>
                    <td>
                        <div class=\"checkbox-custom checkbox-default\">
                            <input type=\"checkbox\" name=\"semana-dia\" id=\"semana=dia0\" value=\"0\"
                                    ";
        // line 83
        if (((($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = ($context["sSemana"] ?? null)) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6[0] ?? null) : null) == "*")) {
            echo " disabled=\"true\"";
        }
        // line 84
        echo "                                    ";
        if ((((($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = ($context["sSemana"] ?? null)) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855[0] ?? null) : null) != "*") && twig_in_filter(0, ($context["sSemana"] ?? null)))) {
            echo " checked=\"true\"";
        }
        // line 85
        echo "                                   class=\"semana-dia\">
                            <label> Dom</label>
                        </div>
                        <div class=\"checkbox-custom checkbox-default\">
                            <input type=\"checkbox\" name=\"semana-dia\" id=\"semana=dia1\" value=\"1\"
                                    ";
        // line 90
        if (((($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = ($context["sSemana"] ?? null)) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b[0] ?? null) : null) == "*")) {
            echo " disabled=\"true\" ";
        }
        // line 91
        echo "                                    ";
        if ((((($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f = ($context["sSemana"] ?? null)) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f[0] ?? null) : null) != "*") && twig_in_filter(1, ($context["sSemana"] ?? null)))) {
            echo " checked=\"true\"";
        }
        // line 92
        echo "                                   class=\"semana-dia\">
                            <label> Seg</label>
                        </div>
                        <div class=\"checkbox-custom checkbox-default\">
                            <input type=\"checkbox\" name=\"semana-dia\" id=\"semana=dia2\" value=\"2\"
                                    ";
        // line 97
        if (((($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = ($context["sSemana"] ?? null)) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0[0] ?? null) : null) == "*")) {
            echo " disabled=\"true\" ";
        }
        // line 98
        echo "                                    ";
        if ((((($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = ($context["sSemana"] ?? null)) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55[0] ?? null) : null) != "*") && twig_in_filter(2, ($context["sSemana"] ?? null)))) {
            echo " checked=\"true\"";
        }
        // line 99
        echo "                                   class=\"semana-dia\">
                            <label> Ter</label>
                        </div>
                        <div class=\"checkbox-custom checkbox-default\">
                            <input type=\"checkbox\" name=\"semana-dia\" id=\"semana=dia3\" value=\"3\"
                                    ";
        // line 104
        if (((($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a = ($context["sSemana"] ?? null)) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a[0] ?? null) : null) == "*")) {
            echo " disabled=\"true\" ";
        }
        // line 105
        echo "                                    ";
        if ((((($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = ($context["sSemana"] ?? null)) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88[0] ?? null) : null) != "*") && twig_in_filter(3, ($context["sSemana"] ?? null)))) {
            echo " checked=\"true\"";
        }
        // line 106
        echo "                                   class=\"semana-dia\">
                            <label> Qua</label>
                        </div>
                        <div class=\"checkbox-custom checkbox-default\">
                            <input type=\"checkbox\" name=\"semana-dia\" id=\"semana=dia4\" value=\"4\"
                                    ";
        // line 111
        if (((($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 = ($context["sSemana"] ?? null)) && is_array($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758) || $__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 instanceof ArrayAccess ? ($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758[0] ?? null) : null) == "*")) {
            echo " disabled=\"true\" ";
        }
        // line 112
        echo "                                    ";
        if ((((($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 = ($context["sSemana"] ?? null)) && is_array($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35) || $__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 instanceof ArrayAccess ? ($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35[0] ?? null) : null) != "*") && twig_in_filter(4, ($context["sSemana"] ?? null)))) {
            echo " checked=\"true\"";
        }
        // line 113
        echo "                                   class=\"semana-dia\">
                            <label> Qui</label>
                        </div>
                        <div class=\"checkbox-custom checkbox-default\">
                            <input type=\"checkbox\" name=\"semana-dia\" id=\"semana=dia5\" value=\"5\"
                                    ";
        // line 118
        if (((($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b = ($context["sSemana"] ?? null)) && is_array($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b) || $__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b instanceof ArrayAccess ? ($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b[0] ?? null) : null) == "*")) {
            echo " disabled=\"true\" ";
        }
        // line 119
        echo "                                    ";
        if ((((($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae = ($context["sSemana"] ?? null)) && is_array($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae) || $__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae instanceof ArrayAccess ? ($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae[0] ?? null) : null) != "*") && twig_in_filter(5, ($context["sSemana"] ?? null)))) {
            echo " checked=\"true\"";
        }
        // line 120
        echo "                                   class=\"semana-dia\">
                            <label> Seg</label>
                        </div>
                        <div class=\"checkbox-custom checkbox-default\">
                            <input type=\"checkbox\" name=\"semana-dia\" id=\"semana=dia6\" value=\"6\"
                                    ";
        // line 125
        if (((($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 = ($context["sSemana"] ?? null)) && is_array($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54) || $__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 instanceof ArrayAccess ? ($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54[0] ?? null) : null) == "*")) {
            echo " disabled=\"true\" ";
        }
        // line 126
        echo "                                    ";
        if ((((($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f = ($context["sSemana"] ?? null)) && is_array($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f) || $__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f instanceof ArrayAccess ? ($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f[0] ?? null) : null) != "*") && twig_in_filter(6, ($context["sSemana"] ?? null)))) {
            echo " checked=\"true\"";
        }
        // line 127
        echo "                                   class=\"semana-dia\">
                            <label> Sab</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Todos</td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class=\"col-md-6\">
            ";
        // line 141
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 141), 'row');
        echo "
        </div>
        <div class=\"col-md-6\">
            ";
        // line 144
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 144), 'row');
        echo "
        </div>
        <div class=\"col-md-12\">
            ";
        // line 147
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "command", [], "any", false, false, false, 147), 'row');
        echo "
        </div>
        <div class=\"col-md-4\">
            ";
        // line 150
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "enabled", [], "any", false, false, false, 150), 'row');
        echo "
        </div>

    </div>
</div>
<div class=\"panel-footer text-right\">
    ";
        // line 156
        if ((isset($context["delete_form"]) || array_key_exists("delete_form", $context))) {
            // line 157
            echo "    <div class=\"pull-left\" id=\"y\"></div>
    ";
        }
        // line 159
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "salvar", [], "any", false, false, false, 159), 'row');
        echo "
</div>
";
        // line 161
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
";
        // line 162
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "

";
        // line 164
        if ((isset($context["delete_form"]) || array_key_exists("delete_form", $context))) {
            // line 165
            echo "    <div id=\"x\">
        ";
            // line 166
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["delete_form"] ?? null), 'form');
            echo "
    </div>
";
        }
        // line 169
        echo "<style>
    table tr td:nth-child(2) {
        background: #f3f3f3;
        text-align: center;
        font-weight: bold;;
    }

    .checkbox-custom.checkbox-default {
        display: table-cell;
        float: left;
        margin-right: 15px;
    }
</style>
<script>
    ";
        // line 183
        $this->loadTemplate(":js:_form_validador.js.twig", "NteConfigBaseBundle:Crontab:form.html.twig", 183)->display(twig_array_merge($context, ["tipo" => 0]));
        // line 184
        echo "    \$('#y').html(\$('#x'));


    \$(\".checkbox-semana-dia\").unbind('click');

    \$(\".checkbox-semana-dia\").click(function () {
        var status = this.checked;
        \$(\".semana-dia\").each(function () {
            this.disabled = status;
            this.checked = !status;
        });
        if (this.checked) {
            \$(\"#cron-semana-dia\").val(\"*\");
        }
    });

    \$('.scheduler-crontab-input').change(function () {
        atualizarValorScheduler();
    });

    \$(\".slider-primary\").each(function () {
        \$(this).slider({
            range: \$(this).data('plugin-options').range,
            min: \$(this).data('plugin-options').min,
            max: \$(this).data('plugin-options').max,
            value: \$(this).data('plugin-options').value,
            slide: function (event, ui) {
                if (ui.value == \$(this).data('plugin-options').min) {
                    ui.value = \"*\";
                }
                ckb = \$(this).data('plugin-slider-checkbox');
                if (ui.value != \"*\") {
                    if (\$(ckb)[0].checked) {
                        valor = '*/' + ui.value;
                    } else {
                        valor = ui.value;
                    }
                } else {
                    valor = ui.value;
                }
                \$(\$(this).data('plugin-slider-output')).val(valor);
                atualizarValorScheduler();
            }
        });
    });
    \$(\".semana-dia\").click(function (e) {
        status = \"\";
        \$(\".semana-dia\").each(function () {
            if (this.checked) {
                if (status !== \"\") {
                    status += \",\"
                }
                status += this.value
            }
        });
        if (status.length == 13) {
            status = \"*\";
        }
        \$(\"#cron-semana-dia\").val(status);
        atualizarValorScheduler();

    });
    function atualizarValorScheduler() {
        valor = \$(\"#cron-minuto-input\").val() + \" \"
            + \$(\"#cron-hora-input\").val() + \" \"
            + \$(\"#cron-mes-dia-input\").val() + \" \"
            + \$(\"#cron-mes-input\").val() + \" \"
            + \$(\"#cron-semana-dia\").val();
        \$(\".scheduler-crontab-input\").val(valor);
        console.log(valor);
    }

</script>";
    }

    public function getTemplateName()
    {
        return "NteConfigBaseBundle:Crontab:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  418 => 184,  416 => 183,  400 => 169,  394 => 166,  391 => 165,  389 => 164,  384 => 162,  380 => 161,  374 => 159,  370 => 157,  368 => 156,  359 => 150,  353 => 147,  347 => 144,  341 => 141,  325 => 127,  320 => 126,  316 => 125,  309 => 120,  304 => 119,  300 => 118,  293 => 113,  288 => 112,  284 => 111,  277 => 106,  272 => 105,  268 => 104,  261 => 99,  256 => 98,  252 => 97,  245 => 92,  240 => 91,  236 => 90,  229 => 85,  224 => 84,  220 => 83,  211 => 77,  204 => 75,  185 => 67,  178 => 63,  172 => 62,  153 => 54,  146 => 50,  140 => 49,  121 => 41,  114 => 37,  108 => 36,  90 => 28,  83 => 24,  77 => 23,  73 => 21,  70 => 20,  67 => 19,  64 => 18,  61 => 17,  59 => 16,  48 => 8,  41 => 3,  39 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteConfigBaseBundle:Crontab:form.html.twig", "/var/www/narfi/src/Nte/Config/BaseBundle/Resources/views/Crontab/form.html.twig");
    }
}
