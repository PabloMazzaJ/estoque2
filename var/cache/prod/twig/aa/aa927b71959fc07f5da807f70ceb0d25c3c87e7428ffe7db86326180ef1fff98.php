<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_tem_vinculos.html.twig */
class __TwigTemplate_95b868ca98c6b9b6ab3b2bc33df66a43e7e8d64cad0e1a55f659d6a464cfce1b extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'titulo' => [$this, 'block_titulo'],
            'breadcumbsTitulo' => [$this, 'block_breadcumbsTitulo'],
            'breadcumbsItens' => [$this, 'block_breadcumbsItens'],
            'conteudo' => [$this, 'block_conteudo'],
            'javascripts3' => [$this, 'block_javascripts3'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("base.admin.html.twig", "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_tem_vinculos.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = [])
    {
        echo "Pessoas";
    }

    // line 3
    public function block_breadcumbsTitulo($context, array $blocks = [])
    {
    }

    // line 4
    public function block_breadcumbsItens($context, array $blocks = [])
    {
        // line 5
        echo "     <li><span><a href=\"#\" class=\"semUnderline\">Pessoal Externo</a></span></li>
     <li><span><a href=\"#\">Pessoas</a></span></li>
 ";
    }

    // line 8
    public function block_conteudo($context, array $blocks = [])
    {
        // line 9
        echo "    <header class=\"panel-heading\">
        <div>
            <h2 class=\"panel-title\">
                <span class=\"va-middle\"><i class=\"fa fa-users\"></i> Vínculos de ";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["pessoa"] ?? null), "nome", [], "any", false, false, false, 12), "html", null, true);
        echo "</span>
            </h2>
        </div>
    </header>
    <div class=\"panel-body\">
        <table class=\"table table-striped\">
            <tr>
                <th class=\"col-md-1\">#</th>
                <th class=\"col-md-2\">Curso / Polo</th>
                <th class=\"col-md-2\">Edital</th>
                <th class=\"col-md-2\">Situação</th>
            </tr>
            ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["vinculos"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["vinculo"]) {
            // line 25
            echo "                <tr>
                    <td>";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 26), "html", null, true);
            echo "</td>
                    <td> ";
            // line 27
            echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["cursos"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 27) - 1)] ?? null) : null), "html", null, true);
            echo " </td>
                    <td>";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vinculo"], "idContrato", [], "any", false, false, false, 28), "editalNumero", [], "any", false, false, false, 28), "html", null, true);
            echo "</td>
                    ";
            // line 29
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vinculo"], "idContratoSituacao", [], "any", false, false, false, 29), "descricao", [], "any", false, false, false, 29) == "Contrato Aceito")) {
                // line 30
                echo "                        <td>Aceito</td>
                    ";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 31
$context["vinculo"], "idContratoSituacao", [], "any", false, false, false, 31), "descricao", [], "any", false, false, false, 31) == "Contrato Sob Análise")) {
                // line 32
                echo "                        <td>Sob Análise</td>
                    ";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 33
$context["vinculo"], "idContratoSituacao", [], "any", false, false, false, 33), "descricao", [], "any", false, false, false, 33) == "Contrato Rejeitado")) {
                // line 34
                echo "                        <td>Rejeitado</td>
                    ";
            }
            // line 36
            echo "                </tr>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['vinculo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "        </table>

        <a href=\"";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ntepessoa_mostra");
        echo "\" class=\"btn-gerar-relatorio2\">
            <button type=\"button\" class=\"btn btn-primary btn-sm\" id=\"expButton\"><i class=\"fa fa-arrow-circle-left\"></i></button>
        </a>

    </div>
";
    }

    // line 47
    public function block_javascripts3($context, array $blocks = [])
    {
        // line 48
        echo "
    ";
        // line 49
        $this->displayParentBlock("javascripts3", $context, $blocks);
        echo "
    <script>


    </script>

";
    }

    // line 57
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 58
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>
        .btn {
            min-width: 9em;
            max-width: 9em;
        }

        table th,
        table {
            text-align: center;
            vertical-align: middle;
        }
    </style>

";
    }

    public function getTemplateName()
    {
        return "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_tem_vinculos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 58,  186 => 57,  175 => 49,  172 => 48,  169 => 47,  159 => 40,  155 => 38,  140 => 36,  136 => 34,  134 => 33,  131 => 32,  129 => 31,  126 => 30,  124 => 29,  120 => 28,  116 => 27,  112 => 26,  109 => 25,  92 => 24,  77 => 12,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  55 => 3,  49 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "NteAplicacaoPessoalExternoBundle:ntepessoa:pessoa_tem_vinculos.html.twig", "/var/www/narfi/src/Nte/Aplicacao/PessoalExternoBundle/Resources/views/ntepessoa/pessoa_tem_vinculos.html.twig");
    }
}
