<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = [];
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_wdt']), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not__profiler_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_search_results']), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler']), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_router']), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception']), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception_css']), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_twig_error_test']), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        elseif (0 === strpos($pathinfo, '/almoxarifado')) {
            if (0 === strpos($pathinfo, '/almoxarifado/almoxapi')) {
                // almox_persiste_requisicao
                if ('/almoxarifado/almoxapi/persist-requisicao' === $pathinfo) {
                    $ret = array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\ServicosController::persitirRequisicaoAction',  '_route' => 'almox_persiste_requisicao',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_almox_persiste_requisicao;
                    }

                    return $ret;
                }
                not_almox_persiste_requisicao:

                // almox_lista_produtos
                if ('/almoxarifado/almoxapi/lista-produtos' === $pathinfo) {
                    $ret = array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\ServicosController::listarProdutosAction',  '_route' => 'almox_lista_produtos',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_almox_lista_produtos;
                    }

                    return $ret;
                }
                not_almox_lista_produtos:

                // almox_remover_ultimo_pedido
                if ('/almoxarifado/almoxapi/ultimo-pedido' === $pathinfo) {
                    $ret = array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\ServicosController::cancelarRequisicaoAction',  '_route' => 'almox_remover_ultimo_pedido',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_almox_remover_ultimo_pedido;
                    }

                    return $ret;
                }
                not_almox_remover_ultimo_pedido:

                // almox_criar_interacao
                if ('/almoxarifado/almoxapi/interacao-obs' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\ServicosController::iniciarInteracaoAction',  '_route' => 'almox_criar_interacao',);
                }

                // almox_enviar_avaliacao
                if ('/almoxarifado/almoxapi/avaliacao' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\ServicosController::enviarAvaliacaoAction',  '_route' => 'almox_enviar_avaliacao',);
                }

                // almox_motivo_rejeicao_parcial
                if ('/almoxarifado/almoxapi/rejectparcial' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\ServicosController::adicionarMotivoRejeicaoParcialAction',  '_route' => 'almox_motivo_rejeicao_parcial',);
                }

                // almox_verifica_situacao_itens
                if ('/almoxarifado/almoxapi/verifica-situacao-item' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\ServicosController::verificarSituacaoItensAction',  '_route' => 'almox_verifica_situacao_itens',);
                }

                // almox_motivo_rejeicao_um_item
                if ('/almoxarifado/almoxapi/busca-motivo-rejeicao' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\ServicosController::buscaMotivoRejeicaoUmItemAction',  '_route' => 'almox_motivo_rejeicao_um_item',);
                }

            }

            // nova_requisicao
            if ('/almoxarifado/nova-requisicao' === $pathinfo) {
                return array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\RequisicaoController::criarRequisicaoAction',  '_route' => 'nova_requisicao',);
            }

            // lista_tabela_requisitada
            if (0 === strpos($pathinfo, '/almoxarifado/requisicao/lista-requisitada') && preg_match('#^/almoxarifado/requisicao/lista\\-requisitada/(?P<requisicao>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'lista_tabela_requisitada']), array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\RequisicaoController::mostrarListaProdutosRequisitadosAction',));
            }

            // importar_csv_produtos
            if ('/almoxarifado/produtos/import' === $pathinfo) {
                return array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\RequisicaoController::importarCsvProdutosAction',  '_route' => 'importar_csv_produtos',);
            }

            // detalhes_pedido
            if (0 === strpos($pathinfo, '/almoxarifado/pedido') && preg_match('#^/almoxarifado/pedido/(?P<pedido>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'detalhes_pedido']), array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\RequisicaoController::mostrarDetalhesPedidoAction',));
            }

            // lista_pedidos_administrativo
            if ('/almoxarifado/lista-pedidos-adm' === $pathinfo) {
                return array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\RequisicaoController::listarPedidosAdministrativoAction',  '_route' => 'lista_pedidos_administrativo',);
            }

            // lista_pedidos_coordenador
            if ('/almoxarifado/lista-pedidos-coord' === $pathinfo) {
                return array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\RequisicaoController::listarPedidosDoSolicitanteAction',  '_route' => 'lista_pedidos_coordenador',);
            }

            // detalhes_pedido_coordenador
            if (0 === strpos($pathinfo, '/almoxarifado/meu-pedido') && preg_match('#^/almoxarifado/meu\\-pedido/(?P<pedido>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'detalhes_pedido_coordenador']), array (  '_controller' => 'Nte\\Aplicacao\\AlmoxarifadoBundle\\Controller\\RequisicaoController::mostrarMeusPedidosDetalhesAction',));
            }

        }

        elseif (0 === strpos($pathinfo, '/externo')) {
            if (0 === strpos($pathinfo, '/externo/nte')) {
                if (0 === strpos($pathinfo, '/externo/ntepessoa')) {
                    // ntepessoa_new
                    if ('/externo/ntepessoa/new' === $pathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePessoaController::newAction',  '_route' => 'ntepessoa_new',);
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_ntepessoa_new;
                        }

                        return $ret;
                    }
                    not_ntepessoa_new:

                    // ntepessoa_edit
                    if (0 === strpos($pathinfo, '/externo/ntepessoa/editarpessoa') && preg_match('#^/externo/ntepessoa/editarpessoa/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ntepessoa_edit']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePessoaController::editAction',));
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_ntepessoa_edit;
                        }

                        return $ret;
                    }
                    not_ntepessoa_edit:

                    // ntepessoa_delete
                    if (0 === strpos($pathinfo, '/externo/ntepessoa/deletar') && preg_match('#^/externo/ntepessoa/deletar/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ntepessoa_delete']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePessoaController::deleteAction',));
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_ntepessoa_delete;
                        }

                        return $ret;
                    }
                    not_ntepessoa_delete:

                    // ntepessoa_vincula_pessoa_contrato
                    if ('/externo/ntepessoa/vinculapessoacontrato' === $pathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePessoaController::vinculaAction',  '_route' => 'ntepessoa_vincula_pessoa_contrato',);
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_ntepessoa_vincula_pessoa_contrato;
                        }

                        return $ret;
                    }
                    not_ntepessoa_vincula_pessoa_contrato:

                    // ntepessoa_mostra
                    if ('/externo/ntepessoa' === $trimmedPathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePessoaController::mostraAction',  '_route' => 'ntepessoa_mostra',);
                        if ('/' === substr($pathinfo, -1)) {
                            // no-op
                        } elseif ('GET' !== $canonicalMethod) {
                            goto not_ntepessoa_mostra;
                        } else {
                            return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ntepessoa_mostra'));
                        }

                        return $ret;
                    }
                    not_ntepessoa_mostra:

                    // ntepessoa_mostra_detalhes
                    if (0 === strpos($pathinfo, '/externo/ntepessoa/mostra') && preg_match('#^/externo/ntepessoa/mostra/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ntepessoa_mostra_detalhes']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePessoaController::mostraEnderecoAction',));
                        if (!in_array($canonicalMethod, ['GET'])) {
                            $allow = array_merge($allow, ['GET']);
                            goto not_ntepessoa_mostra_detalhes;
                        }

                        return $ret;
                    }
                    not_ntepessoa_mostra_detalhes:

                    // pessoa_testando
                    if ('/externo/ntepessoa/test' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePessoaController::testaAction',  '_route' => 'pessoa_testando',);
                    }

                    // ntepessoa_tem_vinculo
                    if (0 === strpos($pathinfo, '/externo/ntepessoa/pessoa/vinculos') && preg_match('#^/externo/ntepessoa/pessoa/vinculos/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ntepessoa_tem_vinculo']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePessoaController::pessoaTemVinculoAction',));
                        if (!in_array($canonicalMethod, ['GET'])) {
                            $allow = array_merge($allow, ['GET']);
                            goto not_ntepessoa_tem_vinculo;
                        }

                        return $ret;
                    }
                    not_ntepessoa_tem_vinculo:

                }

                elseif (0 === strpos($pathinfo, '/externo/ntepapel')) {
                    // ntepapel_index
                    if ('/externo/ntepapel' === $trimmedPathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePapelController::indexAction',  '_route' => 'ntepapel_index',);
                        if ('/' === substr($pathinfo, -1)) {
                            // no-op
                        } elseif ('GET' !== $canonicalMethod) {
                            goto not_ntepapel_index;
                        } else {
                            return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ntepapel_index'));
                        }

                        if (!in_array($canonicalMethod, ['GET'])) {
                            $allow = array_merge($allow, ['GET']);
                            goto not_ntepapel_index;
                        }

                        return $ret;
                    }
                    not_ntepapel_index:

                    // ntepapel_show
                    if (preg_match('#^/externo/ntepapel/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ntepapel_show']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePapelController::showAction',));
                        if (!in_array($canonicalMethod, ['GET'])) {
                            $allow = array_merge($allow, ['GET']);
                            goto not_ntepapel_show;
                        }

                        return $ret;
                    }
                    not_ntepapel_show:

                    // ntepapel_new
                    if ('/externo/ntepapel/new' === $pathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePapelController::newAction',  '_route' => 'ntepapel_new',);
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_ntepapel_new;
                        }

                        return $ret;
                    }
                    not_ntepapel_new:

                    // ntepapel_edit
                    if (preg_match('#^/externo/ntepapel/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ntepapel_edit']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePapelController::editAction',));
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_ntepapel_edit;
                        }

                        return $ret;
                    }
                    not_ntepapel_edit:

                    // ntepapel_delete
                    if (preg_match('#^/externo/ntepapel/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ntepapel_delete']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePapelController::deleteAction',));
                        if (!in_array($requestMethod, ['DELETE'])) {
                            $allow = array_merge($allow, ['DELETE']);
                            goto not_ntepapel_delete;
                        }

                        return $ret;
                    }
                    not_ntepapel_delete:

                }

                elseif (0 === strpos($pathinfo, '/externo/ntecontrato')) {
                    // ntecontrato_index
                    if ('/externo/ntecontrato' === $trimmedPathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::indexAction',  '_route' => 'ntecontrato_index',);
                        if ('/' === substr($pathinfo, -1)) {
                            // no-op
                        } elseif ('GET' !== $canonicalMethod) {
                            goto not_ntecontrato_index;
                        } else {
                            return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ntecontrato_index'));
                        }

                        if (!in_array($canonicalMethod, ['GET'])) {
                            $allow = array_merge($allow, ['GET']);
                            goto not_ntecontrato_index;
                        }

                        return $ret;
                    }
                    not_ntecontrato_index:

                    // ntecontrato_detalhes
                    if (preg_match('#^/externo/ntecontrato/(?P<id>[^/]++)/contratodetalhe$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ntecontrato_detalhes']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::detalhesContratoAction',));
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_ntecontrato_detalhes;
                        }

                        return $ret;
                    }
                    not_ntecontrato_detalhes:

                    // nte_contrato_membros
                    if (0 === strpos($pathinfo, '/externo/ntecontrato/membros') && preg_match('#^/externo/ntecontrato/membros(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_contrato_membros']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::contratoMembrosAction',  'id' => NULL,));
                        if (!in_array($canonicalMethod, ['GET'])) {
                            $allow = array_merge($allow, ['GET']);
                            goto not_nte_contrato_membros;
                        }

                        return $ret;
                    }
                    not_nte_contrato_membros:

                    if (0 === strpos($pathinfo, '/externo/ntecontrato/de')) {
                        // ntevinculo_pessoa_contrato
                        if ('/externo/ntecontrato/detalhesvinculo' === $pathinfo) {
                            $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::detalhesVinculoPessoaContratoAction',  '_route' => 'ntevinculo_pessoa_contrato',);
                            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                $allow = array_merge($allow, ['GET', 'POST']);
                                goto not_ntevinculo_pessoa_contrato;
                            }

                            return $ret;
                        }
                        not_ntevinculo_pessoa_contrato:

                        // ntevinculo_itens_pessoa_contrato_detalhes
                        if (0 === strpos($pathinfo, '/externo/ntecontrato/detalhesitensvinculo') && preg_match('#^/externo/ntecontrato/detalhesitensvinculo/(?P<id>\\d+)/?$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ntevinculo_itens_pessoa_contrato_detalhes']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::detalhesItensVinculoPessoaContratoAction',  'id' => NULL,));
                            if ('/' === substr($pathinfo, -1)) {
                                // no-op
                            } elseif ('GET' !== $canonicalMethod) {
                                goto not_ntevinculo_itens_pessoa_contrato_detalhes;
                            } else {
                                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ntevinculo_itens_pessoa_contrato_detalhes'));
                            }

                            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                $allow = array_merge($allow, ['GET', 'POST']);
                                goto not_ntevinculo_itens_pessoa_contrato_detalhes;
                            }

                            return $ret;
                        }
                        not_ntevinculo_itens_pessoa_contrato_detalhes:

                        // nte_deletar_contrato
                        if (0 === strpos($pathinfo, '/externo/ntecontrato/deletarcontrato') && preg_match('#^/externo/ntecontrato/deletarcontrato/(?P<id>\\d+)$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_deletar_contrato']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::deleteContratoAction',));
                            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                $allow = array_merge($allow, ['GET', 'POST']);
                                goto not_nte_deletar_contrato;
                            }

                            return $ret;
                        }
                        not_nte_deletar_contrato:

                    }

                    // ntecontrato_edit
                    if (0 === strpos($pathinfo, '/externo/ntecontrato/editarcontrato') && preg_match('#^/externo/ntecontrato/editarcontrato/(?P<id>\\d+)$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ntecontrato_edit']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::editContratoAction',));
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_ntecontrato_edit;
                        }

                        return $ret;
                    }
                    not_ntecontrato_edit:

                    // nte_situacao_itens
                    if (0 === strpos($pathinfo, '/externo/ntecontrato/situacao/itens') && preg_match('#^/externo/ntecontrato/situacao/itens/(?P<id>\\d+)$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_situacao_itens']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::editSituacaoAction',));
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_nte_situacao_itens;
                        }

                        return $ret;
                    }
                    not_nte_situacao_itens:

                    if (0 === strpos($pathinfo, '/externo/ntecontrato/v')) {
                        // nte_validar_turmas_exportacao
                        if (0 === strpos($pathinfo, '/externo/ntecontrato/validacao/turmas') && preg_match('#^/externo/ntecontrato/validacao/turmas/(?P<id>\\d+)$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_validar_turmas_exportacao']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::validarTurmasAction',));
                            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                $allow = array_merge($allow, ['GET', 'POST']);
                                goto not_nte_validar_turmas_exportacao;
                            }

                            return $ret;
                        }
                        not_nte_validar_turmas_exportacao:

                        // nte_vincular_turmas
                        if (0 === strpos($pathinfo, '/externo/ntecontrato/vincularturmas') && preg_match('#^/externo/ntecontrato/vincularturmas/(?P<id>\\d+)$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_vincular_turmas']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::vincularTurmasAction',));
                            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                $allow = array_merge($allow, ['GET', 'POST']);
                                goto not_nte_vincular_turmas;
                            }

                            return $ret;
                        }
                        not_nte_vincular_turmas:

                        // nte_modificando_vincular_turmas
                        if (0 === strpos($pathinfo, '/externo/ntecontrato/vincularmodificando') && preg_match('#^/externo/ntecontrato/vincularmodificando/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_modificando_vincular_turmas']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::vincularTurmasModificandoAction',));
                            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                $allow = array_merge($allow, ['GET', 'POST']);
                                goto not_nte_modificando_vincular_turmas;
                            }

                            return $ret;
                        }
                        not_nte_modificando_vincular_turmas:

                    }

                    elseif (0 === strpos($pathinfo, '/externo/ntecontrato/c')) {
                        // nte_cadastro_contrato_parte_um
                        if ('/externo/ntecontrato/cadastro/parte/um' === $pathinfo) {
                            $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::cadastroContratoUmAction',  '_route' => 'nte_cadastro_contrato_parte_um',);
                            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                $allow = array_merge($allow, ['GET', 'POST']);
                                goto not_nte_cadastro_contrato_parte_um;
                            }

                            return $ret;
                        }
                        not_nte_cadastro_contrato_parte_um:

                        // nte_cadastro_contrato_parte_dois
                        if (0 === strpos($pathinfo, '/externo/ntecontrato/cadastro/parte/dois') && preg_match('#^/externo/ntecontrato/cadastro/parte/dois(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_cadastro_contrato_parte_dois']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::cadastroContratoDoisAction',  'id' => NULL,));
                            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                $allow = array_merge($allow, ['GET', 'POST']);
                                goto not_nte_cadastro_contrato_parte_dois;
                            }

                            return $ret;
                        }
                        not_nte_cadastro_contrato_parte_dois:

                        // nte_contrato_detalhes_criacao
                        if (0 === strpos($pathinfo, '/externo/ntecontrato/contrato/detalhescriacao') && preg_match('#^/externo/ntecontrato/contrato/detalhescriacao(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_contrato_detalhes_criacao']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::contratoDetalhesCriacaoAction',  'id' => NULL,));
                            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                $allow = array_merge($allow, ['GET', 'POST']);
                                goto not_nte_contrato_detalhes_criacao;
                            }

                            return $ret;
                        }
                        not_nte_contrato_detalhes_criacao:

                        // nte_contrato_vinculo_direto_pos_lista_termos
                        if (0 === strpos($pathinfo, '/externo/ntecontrato/contrato/vinculo/direto') && preg_match('#^/externo/ntecontrato/contrato/vinculo/direto(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_contrato_vinculo_direto_pos_lista_termos']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoController::vinculoDiretoPosListaTermosAction',  'id' => NULL,));
                            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                $allow = array_merge($allow, ['GET', 'POST']);
                                goto not_nte_contrato_vinculo_direto_pos_lista_termos;
                            }

                            return $ret;
                        }
                        not_nte_contrato_vinculo_direto_pos_lista_termos:

                    }

                }

                elseif (0 === strpos($pathinfo, '/externo/nteitem')) {
                    // ntecontratoitem_new
                    if ('/externo/nteitem/new' === $pathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoItemController::newAction',  '_route' => 'ntecontratoitem_new',);
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_ntecontratoitem_new;
                        }

                        return $ret;
                    }
                    not_ntecontratoitem_new:

                    // ntecontratoitem_mostra
                    if ('/externo/nteitem' === $trimmedPathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoItemController::mostraAction',  '_route' => 'ntecontratoitem_mostra',);
                        if ('/' === substr($pathinfo, -1)) {
                            // no-op
                        } elseif ('GET' !== $canonicalMethod) {
                            goto not_ntecontratoitem_mostra;
                        } else {
                            return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ntecontratoitem_mostra'));
                        }

                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_ntecontratoitem_mostra;
                        }

                        return $ret;
                    }
                    not_ntecontratoitem_mostra:

                    // ntecontratoitem_edit
                    if (0 === strpos($pathinfo, '/externo/nteitem/edit') && preg_match('#^/externo/nteitem/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'ntecontratoitem_edit']), array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NteContratoItemController::editAction',));
                    }

                }

            }

            // nte_vinculos_efetuar_vinculos
            if ('/externo/vinculos/vincular' === $pathinfo) {
                return array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePessoaContratoController::efetuarVinculoAction',  '_route' => 'nte_vinculos_efetuar_vinculos',);
            }

            // nte_vinculos_exportar_turmas
            if ('/externo/vinculos/exportar' === $pathinfo) {
                $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\NtePessoaContratoController::exportarTurmasAction',  '_route' => 'nte_vinculos_exportar_turmas',);
                if (!in_array($canonicalMethod, ['POST', 'GET'])) {
                    $allow = array_merge($allow, ['POST', 'GET']);
                    goto not_nte_vinculos_exportar_turmas;
                }

                return $ret;
            }
            not_nte_vinculos_exportar_turmas:

            if (0 === strpos($pathinfo, '/externo/api')) {
                if (0 === strpos($pathinfo, '/externo/api/buscar')) {
                    // pessoa_externa_api_buscar_contrato
                    if ('/externo/api/buscar/contrato' === $pathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::buscarContratoAction',  '_route' => 'pessoa_externa_api_buscar_contrato',);
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_pessoa_externa_api_buscar_contrato;
                        }

                        return $ret;
                    }
                    not_pessoa_externa_api_buscar_contrato:

                    // pessoa_externa_api_buscar_situacao
                    if ('/externo/api/buscar/situacao' === $pathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::buscarSituacaoAction',  '_route' => 'pessoa_externa_api_buscar_situacao',);
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_pessoa_externa_api_buscar_situacao;
                        }

                        return $ret;
                    }
                    not_pessoa_externa_api_buscar_situacao:

                    // pessoa_externa_api_periodo
                    if ('/externo/api/buscar/periodo' === $pathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::buscaPeriodoAction',  '_route' => 'pessoa_externa_api_periodo',);
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_pessoa_externa_api_periodo;
                        }

                        return $ret;
                    }
                    not_pessoa_externa_api_periodo:

                    // pessoa_externa_api_ano
                    if ('/externo/api/buscar/ano' === $pathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::buscarAnoAction',  '_route' => 'pessoa_externa_api_ano',);
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_pessoa_externa_api_ano;
                        }

                        return $ret;
                    }
                    not_pessoa_externa_api_ano:

                    // pessoa_externa_api_turmas
                    if ('/externo/api/buscar/turmas' === $pathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::buscarTurmaAction',  '_route' => 'pessoa_externa_api_turmas',);
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_pessoa_externa_api_turmas;
                        }

                        return $ret;
                    }
                    not_pessoa_externa_api_turmas:

                }

                elseif (0 === strpos($pathinfo, '/externo/api/v')) {
                    if (0 === strpos($pathinfo, '/externo/api/vincular/contrato/pessoa')) {
                        // pessoa_externa_api_atribuir_contrato_pessoa
                        if ('/externo/api/vincular/contrato/pessoa' === $pathinfo) {
                            $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::criarVinculoPessoaContratoAction',  '_route' => 'pessoa_externa_api_atribuir_contrato_pessoa',);
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_pessoa_externa_api_atribuir_contrato_pessoa;
                            }

                            return $ret;
                        }
                        not_pessoa_externa_api_atribuir_contrato_pessoa:

                        // pessoa_externa_api_criar_vinculo_bulk
                        if ('/externo/api/vincular/contrato/pessoas' === $pathinfo) {
                            $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::criarVinculoPessoaContratoBulkAction',  '_route' => 'pessoa_externa_api_criar_vinculo_bulk',);
                            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                $allow = array_merge($allow, ['GET', 'POST']);
                                goto not_pessoa_externa_api_criar_vinculo_bulk;
                            }

                            return $ret;
                        }
                        not_pessoa_externa_api_criar_vinculo_bulk:

                    }

                    // api_aceitar_vinculo
                    if ('/externo/api/vincular/ntepessoacontrato' === $pathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::vincularNtePessoaContratoAction',  '_route' => 'api_aceitar_vinculo',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_api_aceitar_vinculo;
                        }

                        return $ret;
                    }
                    not_api_aceitar_vinculo:

                    if (0 === strpos($pathinfo, '/externo/api/verifica')) {
                        // pessoa_externa_api_situacao
                        if ('/externo/api/verificar/contrato/pessoa' === $pathinfo) {
                            $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::gerenciaSituacaoAction',  '_route' => 'pessoa_externa_api_situacao',);
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_pessoa_externa_api_situacao;
                            }

                            return $ret;
                        }
                        not_pessoa_externa_api_situacao:

                        // pessoa_externa_api_verifica_vinculo
                        if ('/externo/api/verificar/vinculo' === $pathinfo) {
                            $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::verificaVinculoAction',  '_route' => 'pessoa_externa_api_verifica_vinculo',);
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_pessoa_externa_api_verifica_vinculo;
                            }

                            return $ret;
                        }
                        not_pessoa_externa_api_verifica_vinculo:

                        // pessoa_externa_api_verifica_turmas_exportadas
                        if ('/externo/api/verifica/vinculo/turmasxp' === $trimmedPathinfo) {
                            $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::verificaTurmasXpAction',  '_route' => 'pessoa_externa_api_verifica_turmas_exportadas',);
                            if ('/' === substr($pathinfo, -1)) {
                                // no-op
                            } elseif ('GET' !== $canonicalMethod) {
                                goto not_pessoa_externa_api_verifica_turmas_exportadas;
                            } else {
                                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'pessoa_externa_api_verifica_turmas_exportadas'));
                            }

                            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                $allow = array_merge($allow, ['GET', 'POST']);
                                goto not_pessoa_externa_api_verifica_turmas_exportadas;
                            }

                            return $ret;
                        }
                        not_pessoa_externa_api_verifica_turmas_exportadas:

                        // pessoa_externa_api_verifica_situacao_contrato
                        if ('/externo/api/verifica/situacao/contrato' === $pathinfo) {
                            $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::verificarSituacaoContratoAction',  '_route' => 'pessoa_externa_api_verifica_situacao_contrato',);
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_pessoa_externa_api_verifica_situacao_contrato;
                            }

                            return $ret;
                        }
                        not_pessoa_externa_api_verifica_situacao_contrato:

                    }

                }

                // pessoa_externa_api_remover_contrato_pessoa
                if ('/externo/api/remover/contrato/pessoa' === $pathinfo) {
                    $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::removerPessoaContratoAction',  '_route' => 'pessoa_externa_api_remover_contrato_pessoa',);
                    if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                        $allow = array_merge($allow, ['GET', 'POST']);
                        goto not_pessoa_externa_api_remover_contrato_pessoa;
                    }

                    return $ret;
                }
                not_pessoa_externa_api_remover_contrato_pessoa:

                // pessoa_externa_api_remove_turma_exportacao
                if ('/externo/api/remove/turma/exportacao' === $pathinfo) {
                    $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::removeTurmaExportacaoAction',  '_route' => 'pessoa_externa_api_remove_turma_exportacao',);
                    if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                        $allow = array_merge($allow, ['GET', 'POST']);
                        goto not_pessoa_externa_api_remove_turma_exportacao;
                    }

                    return $ret;
                }
                not_pessoa_externa_api_remove_turma_exportacao:

                if (0 === strpos($pathinfo, '/externo/api/a')) {
                    // pessoa_externa_api_altera_situacao
                    if ('/externo/api/alterar/situacao' === $pathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::alterarSituacaoAction',  '_route' => 'pessoa_externa_api_altera_situacao',);
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_pessoa_externa_api_altera_situacao;
                        }

                        return $ret;
                    }
                    not_pessoa_externa_api_altera_situacao:

                    // pessoa_externa_api_altera_observacao
                    if ('/externo/api/alterar/observacao' === $pathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::alterarObservacaoAction',  '_route' => 'pessoa_externa_api_altera_observacao',);
                        if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                            $allow = array_merge($allow, ['GET', 'POST']);
                            goto not_pessoa_externa_api_altera_observacao;
                        }

                        return $ret;
                    }
                    not_pessoa_externa_api_altera_observacao:

                    if (0 === strpos($pathinfo, '/externo/api/api')) {
                        if (0 === strpos($pathinfo, '/externo/api/api/verifica/contrato')) {
                            // pessoa_externa_api_verifica_contrato_traffic_lights
                            if ('/externo/api/api/verifica/contrato' === $pathinfo) {
                                $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::verificarSituacaoContratoTrafficLightsAction',  '_route' => 'pessoa_externa_api_verifica_contrato_traffic_lights',);
                                if (!in_array($requestMethod, ['POST'])) {
                                    $allow = array_merge($allow, ['POST']);
                                    goto not_pessoa_externa_api_verifica_contrato_traffic_lights;
                                }

                                return $ret;
                            }
                            not_pessoa_externa_api_verifica_contrato_traffic_lights:

                            // pessoa_externa_api_verifica_configuracao_contrato
                            if ('/externo/api/api/verifica/contratoconfig' === $pathinfo) {
                                $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::verificarContratoConfigAction',  '_route' => 'pessoa_externa_api_verifica_configuracao_contrato',);
                                if (!in_array($requestMethod, ['POST'])) {
                                    $allow = array_merge($allow, ['POST']);
                                    goto not_pessoa_externa_api_verifica_configuracao_contrato;
                                }

                                return $ret;
                            }
                            not_pessoa_externa_api_verifica_configuracao_contrato:

                        }

                        // pessoa_externa_api_valida_turma
                        if ('/externo/api/api/valida/turma' === $pathinfo) {
                            $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::alterarValidacaoTurmasAction',  '_route' => 'pessoa_externa_api_valida_turma',);
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_pessoa_externa_api_valida_turma;
                            }

                            return $ret;
                        }
                        not_pessoa_externa_api_valida_turma:

                        if (0 === strpos($pathinfo, '/externo/api/api/s')) {
                            // pessaoa_externa_api_altera_todos_itens
                            if ('/externo/api/api/situacao/altera/todas' === $pathinfo) {
                                $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::alterarTodosItensSituacaoAction',  '_route' => 'pessaoa_externa_api_altera_todos_itens',);
                                if (!in_array($requestMethod, ['POST'])) {
                                    $allow = array_merge($allow, ['POST']);
                                    goto not_pessaoa_externa_api_altera_todos_itens;
                                }

                                return $ret;
                            }
                            not_pessaoa_externa_api_altera_todos_itens:

                            // api_exportacao_select_cursos
                            if ('/externo/api/api/select/cursos' === $pathinfo) {
                                $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::buscarCursosSelectExportacaoAction',  '_route' => 'api_exportacao_select_cursos',);
                                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                    $allow = array_merge($allow, ['GET', 'POST']);
                                    goto not_api_exportacao_select_cursos;
                                }

                                return $ret;
                            }
                            not_api_exportacao_select_cursos:

                            // api_exportacao_select_oferta
                            if ('/externo/api/api/select/oferta' === $pathinfo) {
                                $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::ofertaSelectExportacaoAction',  '_route' => 'api_exportacao_select_oferta',);
                                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                    $allow = array_merge($allow, ['GET', 'POST']);
                                    goto not_api_exportacao_select_oferta;
                                }

                                return $ret;
                            }
                            not_api_exportacao_select_oferta:

                        }

                        // pessoa_externa_api_insere_configuracao_contrato
                        if ('/externo/api/api/insere/contratoconfig' === $pathinfo) {
                            $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::inserirContratoConfigAction',  '_route' => 'pessoa_externa_api_insere_configuracao_contrato',);
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_pessoa_externa_api_insere_configuracao_contrato;
                            }

                            return $ret;
                        }
                        not_pessoa_externa_api_insere_configuracao_contrato:

                        // pessoa_externa_api_inserir_funcao_pessoa_contrato_requisitos
                        if ('/externo/api/api/inserir/funcao' === $pathinfo) {
                            $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::inserirFuncaoPessoaContratoRequisitoAction',  '_route' => 'pessoa_externa_api_inserir_funcao_pessoa_contrato_requisitos',);
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_pessoa_externa_api_inserir_funcao_pessoa_contrato_requisitos;
                            }

                            return $ret;
                        }
                        not_pessoa_externa_api_inserir_funcao_pessoa_contrato_requisitos:

                        if (0 === strpos($pathinfo, '/externo/api/api/busca')) {
                            // pessoa_externa_api_buscar_contrato_requisito_obs
                            if ('/externo/api/api/busca/observacao' === $pathinfo) {
                                $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::buscarObservacaoContratoRequisitoAction',  '_route' => 'pessoa_externa_api_buscar_contrato_requisito_obs',);
                                if (!in_array($requestMethod, ['POST'])) {
                                    $allow = array_merge($allow, ['POST']);
                                    goto not_pessoa_externa_api_buscar_contrato_requisito_obs;
                                }

                                return $ret;
                            }
                            not_pessoa_externa_api_buscar_contrato_requisito_obs:

                            // api_busca_pessoas
                            if ('/externo/api/api/busca/pessoas' === $pathinfo) {
                                $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::buscarPessoasWebServiceAction',  '_route' => 'api_busca_pessoas',);
                                if (!in_array($canonicalMethod, ['GET'])) {
                                    $allow = array_merge($allow, ['GET']);
                                    goto not_api_busca_pessoas;
                                }

                                return $ret;
                            }
                            not_api_busca_pessoas:

                            // api_busca_contratos
                            if ('/externo/api/api/busca/contratos' === $pathinfo) {
                                $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::buscarContratosWebServiceAction',  '_route' => 'api_busca_contratos',);
                                if (!in_array($canonicalMethod, ['GET'])) {
                                    $allow = array_merge($allow, ['GET']);
                                    goto not_api_busca_contratos;
                                }

                                return $ret;
                            }
                            not_api_busca_contratos:

                        }

                        elseif (0 === strpos($pathinfo, '/externo/api/api/t')) {
                            // api_exportacao_pessoas_left
                            if ('/externo/api/api/table-source/pessoas-left' === $pathinfo) {
                                $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::buscarPessoasLeftTableSourceAction',  '_route' => 'api_exportacao_pessoas_left',);
                                if (!in_array($canonicalMethod, ['GET'])) {
                                    $allow = array_merge($allow, ['GET']);
                                    goto not_api_exportacao_pessoas_left;
                                }

                                return $ret;
                            }
                            not_api_exportacao_pessoas_left:

                            // api_exportacao_table_disciplinas
                            if ('/externo/api/api/table-source/disciplinas' === $pathinfo) {
                                $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::buscarTableDisciplinasSourceAction',  '_route' => 'api_exportacao_table_disciplinas',);
                                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                    $allow = array_merge($allow, ['GET', 'POST']);
                                    goto not_api_exportacao_table_disciplinas;
                                }

                                return $ret;
                            }
                            not_api_exportacao_table_disciplinas:

                            // api_exportacao_buscar_turmas
                            if ('/externo/api/api/turmas' === $pathinfo) {
                                $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::buscarTurmasExportacaoAction',  '_route' => 'api_exportacao_buscar_turmas',);
                                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                                    $allow = array_merge($allow, ['GET', 'POST']);
                                    goto not_api_exportacao_buscar_turmas;
                                }

                                return $ret;
                            }
                            not_api_exportacao_buscar_turmas:

                        }

                    }

                }

                // pessoa_externa_api_insere_vinculo_tem_turma
                if ('/externo/api/inserir/vinculo/turmas' === $pathinfo) {
                    $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\ApiController::inserirContratoTemSieTurmaAction',  '_route' => 'pessoa_externa_api_insere_vinculo_tem_turma',);
                    if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                        $allow = array_merge($allow, ['GET', 'POST']);
                        goto not_pessoa_externa_api_insere_vinculo_tem_turma;
                    }

                    return $ret;
                }
                not_pessoa_externa_api_insere_vinculo_tem_turma:

            }

        }

        elseif (0 === strpos($pathinfo, '/exportador')) {
            if (0 === strpos($pathinfo, '/exportador/exportacao')) {
                // exportacao_index
                if ('/exportador/exportacao' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\MiddlewareExportacaoController::indexAction',  '_route' => 'exportacao_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_exportacao_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'exportacao_index'));
                    }

                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_exportacao_index;
                    }

                    return $ret;
                }
                not_exportacao_index:

                // exportacao_show
                if (preg_match('#^/exportador/exportacao/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'exportacao_show']), array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\MiddlewareExportacaoController::showAction',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_exportacao_show;
                    }

                    return $ret;
                }
                not_exportacao_show:

                // exportacao_new
                if ('/exportador/exportacao/new' === $pathinfo) {
                    $ret = array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\MiddlewareExportacaoController::newAction',  '_route' => 'exportacao_new',);
                    if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                        $allow = array_merge($allow, ['GET', 'POST']);
                        goto not_exportacao_new;
                    }

                    return $ret;
                }
                not_exportacao_new:

                // exportacao_edit
                if (preg_match('#^/exportador/exportacao/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'exportacao_edit']), array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\MiddlewareExportacaoController::editAction',));
                    if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                        $allow = array_merge($allow, ['GET', 'POST']);
                        goto not_exportacao_edit;
                    }

                    return $ret;
                }
                not_exportacao_edit:

                // exportacao_edit_periodico
                if (preg_match('#^/exportador/exportacao/(?P<id>[^/]++)/periodico$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'exportacao_edit_periodico']), array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\MiddlewareExportacaoController::periodicoAction',));
                    if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                        $allow = array_merge($allow, ['GET', 'POST']);
                        goto not_exportacao_edit_periodico;
                    }

                    return $ret;
                }
                not_exportacao_edit_periodico:

                // exportacao_delete
                if (preg_match('#^/exportador/exportacao/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'exportacao_delete']), array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\MiddlewareExportacaoController::deleteAction',));
                    if (!in_array($requestMethod, ['DELETE'])) {
                        $allow = array_merge($allow, ['DELETE']);
                        goto not_exportacao_delete;
                    }

                    return $ret;
                }
                not_exportacao_delete:

            }

            elseif (0 === strpos($pathinfo, '/exportador/relatorio')) {
                // exportador_relatorio_index
                if ('/exportador/relatorio' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\RelatorioController::indexAction',  '_route' => 'exportador_relatorio_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_exportador_relatorio_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'exportador_relatorio_index'));
                    }

                    return $ret;
                }
                not_exportador_relatorio_index:

                // exportador_realtorio_buscar_cursos
                if ('/exportador/relatorio/cursos-exportados' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\RelatorioController::buscarCursosExportacaoAction',  '_route' => 'exportador_realtorio_buscar_cursos',);
                }

                // exportador_relatorio_turma_participante
                if ('/exportador/relatorio/turmas_participante' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\RelatorioController::relatorioTurmasParticipantesAction',  '_route' => 'exportador_relatorio_turma_participante',);
                }

                // exportador_relatorio_turma_quantitativo
                if ('/exportador/relatorio/turmas_quantitativo' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\RelatorioController::relatorioTurmasQuantitativoAction',  '_route' => 'exportador_relatorio_turma_quantitativo',);
                }

                if (0 === strpos($pathinfo, '/exportador/relatorio/sie/alteracoes')) {
                    // exportador_realtorio_sie_alteracao
                    if ('/exportador/relatorio/sie/alteracoes' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\RelatorioController::relatorioSieAlteracaoAction',  '_route' => 'exportador_realtorio_sie_alteracao',);
                    }

                    // exportador_realtorio_sie_alteracao_busca
                    if ('/exportador/relatorio/sie/alteracoes/busca' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\RelatorioController::relatorioSieAlteracaoBuscaAction',  '_route' => 'exportador_realtorio_sie_alteracao_busca',);
                    }

                }

                // exportador_realtorio_sie_problema_cadastro_tipo
                if ('/exportador/relatorio/sie/problemas-cadastro-tipo-curso' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\RelatorioController::relatorioSieProblemaCadastroTipoAction',  '_route' => 'exportador_realtorio_sie_problema_cadastro_tipo',);
                }

                if (0 === strpos($pathinfo, '/exportador/relatorio/log')) {
                    // exportador_relatorio_log_index
                    if ('/exportador/relatorio/log' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\RelatorioController::logAction',  '_route' => 'exportador_relatorio_log_index',);
                    }

                    // exportador_relatorio_log_resumo
                    if (0 === strpos($pathinfo, '/exportador/relatorio/log/resumo') && preg_match('#^/exportador/relatorio/log/resumo/(?P<me>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'exportador_relatorio_log_resumo']), array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\RelatorioController::logResumoAction',));
                    }

                    // exportador_relatorio_log_detalhe
                    if (0 === strpos($pathinfo, '/exportador/relatorio/log/detalhe') && preg_match('#^/exportador/relatorio/log/detalhe/(?P<me>[^/]++)/(?P<uuid>[^/]++)(?:/(?P<tipo>[^/]++)(?:/(?P<op>[^/]++))?)?$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'exportador_relatorio_log_detalhe']), array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\RelatorioController::logDetalheAction',  'tipo' => NULL,  'op' => NULL,));
                    }

                    // exportador_relatorio_log_filtro
                    if ('/exportador/relatorio/log/filtro' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\RelatorioController::logFiltroAction',  '_route' => 'exportador_relatorio_log_filtro',);
                    }

                }

                // exportador_relatorio_verificar_exportacao_ativa
                if ('/exportador/relatorio/exportacao-ativas' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\RelatorioController::verificarExportacoesEmAndamentoAction',  '_route' => 'exportador_relatorio_verificar_exportacao_ativa',);
                }

                // exportador_relatorio_geral_fila_exportacao
                if ('/exportador/relatorio/geral' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\RelatorioController::relatorioGeralExportacaoAction',  '_route' => 'exportador_relatorio_geral_fila_exportacao',);
                }

            }

            // nte_aplicacao_exportador_homepage
            if ('/exportador' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\DefaultController::indexAction',  '_route' => 'nte_aplicacao_exportador_homepage',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_nte_aplicacao_exportador_homepage;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_aplicacao_exportador_homepage'));
                }

                return $ret;
            }
            not_nte_aplicacao_exportador_homepage:

            // nte_aplicacao_exportador_buscar_cursos
            if ('/exportador/buscar/cursos' === $pathinfo) {
                return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\DefaultController::buscarCursosAction',  '_route' => 'nte_aplicacao_exportador_buscar_cursos',);
            }

            // nte_aplicacao_exportador_atualizar_ofertas
            if ('/exportador/atualizar/ofertas' === $pathinfo) {
                return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\DefaultController::atualizarOfertasAction',  '_route' => 'nte_aplicacao_exportador_atualizar_ofertas',);
            }

            if (0 === strpos($pathinfo, '/exportador/regras')) {
                // nte_aplicacao_exportador_regras_index
                if ('/exportador/regras' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\DefaultController::indexRegrasAction',  '_route' => 'nte_aplicacao_exportador_regras_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_nte_aplicacao_exportador_regras_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_aplicacao_exportador_regras_index'));
                    }

                    return $ret;
                }
                not_nte_aplicacao_exportador_regras_index:

                // nte_aplicacao_exportador_regras_atualizar
                if ('/exportador/regras/atualizar' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\DefaultController::atualizarRegrasAction',  '_route' => 'nte_aplicacao_exportador_regras_atualizar',);
                }

            }

            // nte_aplicacao_exportador_cli_xverificar_categoria
            if ('/exportador/cli/xverificar-categoria' === $pathinfo) {
                return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\DefaultController::xverificarCategoriaAction',  '_route' => 'nte_aplicacao_exportador_cli_xverificar_categoria',);
            }

            // nte_aplicacao_exportador_cli_xexportacao
            if ('/exportador/cli/exportacao' === $pathinfo) {
                return array (  '_controller' => 'Nte\\Aplicacao\\ExportadorBundle\\Controller\\DefaultController::xExportacaoAction',  '_route' => 'nte_aplicacao_exportador_cli_xexportacao',);
            }

        }

        elseif (0 === strpos($pathinfo, '/nteconfigmoodle')) {
            // nteconfigmoodle_index
            if ('/nteconfigmoodle' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'Nte\\Config\\MoodleBundle\\Controller\\NteConfigMoodleController::indexAction',  '_route' => 'nteconfigmoodle_index',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_nteconfigmoodle_index;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nteconfigmoodle_index'));
                }

                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_nteconfigmoodle_index;
                }

                return $ret;
            }
            not_nteconfigmoodle_index:

            // nteconfigmoodle_new
            if ('/nteconfigmoodle/new' === $pathinfo) {
                $ret = array (  '_controller' => 'Nte\\Config\\MoodleBundle\\Controller\\NteConfigMoodleController::newAction',  '_route' => 'nteconfigmoodle_new',);
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_nteconfigmoodle_new;
                }

                return $ret;
            }
            not_nteconfigmoodle_new:

            // nteconfigmoodle_show
            if (preg_match('#^/nteconfigmoodle/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'nteconfigmoodle_show']), array (  '_controller' => 'Nte\\Config\\MoodleBundle\\Controller\\NteConfigMoodleController::showAction',));
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_nteconfigmoodle_show;
                }

                return $ret;
            }
            not_nteconfigmoodle_show:

            // nteconfigmoodle_edit
            if (preg_match('#^/nteconfigmoodle/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'nteconfigmoodle_edit']), array (  '_controller' => 'Nte\\Config\\MoodleBundle\\Controller\\NteConfigMoodleController::editAction',));
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_nteconfigmoodle_edit;
                }

                return $ret;
            }
            not_nteconfigmoodle_edit:

            // nteconfigmoodle_delete
            if (preg_match('#^/nteconfigmoodle/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'nteconfigmoodle_delete']), array (  '_controller' => 'Nte\\Config\\MoodleBundle\\Controller\\NteConfigMoodleController::deleteAction',));
                if (!in_array($requestMethod, ['DELETE'])) {
                    $allow = array_merge($allow, ['DELETE']);
                    goto not_nteconfigmoodle_delete;
                }

                return $ret;
            }
            not_nteconfigmoodle_delete:

        }

        // nte_admin_painel_homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'Nte\\Admin\\PainelBundle\\Controller\\DefaultController::indexAction',  '_route' => 'nte_admin_painel_homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_nte_admin_painel_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_admin_painel_homepage'));
            }

            return $ret;
        }
        not_nte_admin_painel_homepage:

        // nte_admin_teste
        if ('/teste.html.twig-url' === $pathinfo) {
            return array (  '_controller' => 'Nte\\Admin\\PainelBundle\\Controller\\DefaultController::teste.html.twigAction',  '_route' => 'nte_admin_teste',);
        }

        if (0 === strpos($pathinfo, '/c')) {
            // nte_admin_checagem_tarefa
            if ('/checagem/tarefa' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'Nte\\Admin\\PainelBundle\\Controller\\ChecagemController::indexTarefaAction',  '_route' => 'nte_admin_checagem_tarefa',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_nte_admin_checagem_tarefa;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_admin_checagem_tarefa'));
                }

                return $ret;
            }
            not_nte_admin_checagem_tarefa:

            // nte_admin_checagem_service
            if ('/checagem/service' === $pathinfo) {
                return array (  '_controller' => 'Nte\\Admin\\PainelBundle\\Controller\\ChecagemController::checkWSAction',  '_route' => 'nte_admin_checagem_service',);
            }

            if (0 === strpos($pathinfo, '/config')) {
                // nte_config_base_homepage
                if ('/config' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'Nte\\Config\\BaseBundle\\Controller\\DefaultController::indexAction',  '_route' => 'nte_config_base_homepage',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_nte_config_base_homepage;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_config_base_homepage'));
                    }

                    return $ret;
                }
                not_nte_config_base_homepage:

                if (0 === strpos($pathinfo, '/config/cron')) {
                    // nte_config_crontab
                    if ('/config/cron' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Config\\BaseBundle\\Controller\\CrontabController::indexAction',  '_route' => 'nte_config_crontab',);
                    }

                    // nte_config_crontab_listar
                    if ('/config/cron/listar' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Config\\BaseBundle\\Controller\\CrontabController::listarAction',  '_route' => 'nte_config_crontab_listar',);
                    }

                    // nte_config_crontab_habilitar_tarefas
                    if ('/config/cron/habilitar' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Config\\BaseBundle\\Controller\\CrontabController::habilitarTarefaAction',  '_route' => 'nte_config_crontab_habilitar_tarefas',);
                    }

                    // nte_config_crontab_editar
                    if (0 === strpos($pathinfo, '/config/cron/editar') && preg_match('#^/config/cron/editar/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_config_crontab_editar']), array (  '_controller' => 'Nte\\Config\\BaseBundle\\Controller\\CrontabController::editarAction',));
                    }

                    if (0 === strpos($pathinfo, '/config/cron/remover')) {
                        // nte_config_crontab_remover
                        if (preg_match('#^/config/cron/remover/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_config_crontab_remover']), array (  '_controller' => 'Nte\\Config\\BaseBundle\\Controller\\CrontabController::removerAction',));
                        }

                        // nte_config_crontab_remover_tarefas
                        if ('/config/cron/remover-tarefas' === $pathinfo) {
                            return array (  '_controller' => 'Nte\\Config\\BaseBundle\\Controller\\CrontabController::removerTarefaAction',  '_route' => 'nte_config_crontab_remover_tarefas',);
                        }

                    }

                    // nte_config_crontab_novo
                    if ('/config/cron/novo' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Config\\BaseBundle\\Controller\\CrontabController::novoAction',  '_route' => 'nte_config_crontab_novo',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/config/moodle')) {
                    // nte_config_moodle_homepage
                    if ('/config/moodle' === $trimmedPathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Config\\MoodleBundle\\Controller\\DefaultController::indexAction',  '_route' => 'nte_config_moodle_homepage',);
                        if ('/' === substr($pathinfo, -1)) {
                            // no-op
                        } elseif ('GET' !== $canonicalMethod) {
                            goto not_nte_config_moodle_homepage;
                        } else {
                            return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_config_moodle_homepage'));
                        }

                        return $ret;
                    }
                    not_nte_config_moodle_homepage:

                    // nte_config_moodle_listar
                    if ('/config/moodle/listar' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Config\\MoodleBundle\\Controller\\DefaultController::listarAction',  '_route' => 'nte_config_moodle_listar',);
                    }

                    // nte_config_moodle_habilitar
                    if ('/config/moodle/habilitar' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Config\\MoodleBundle\\Controller\\DefaultController::habilitarAction',  '_route' => 'nte_config_moodle_habilitar',);
                    }

                    // nte_config_moodle_editar
                    if (0 === strpos($pathinfo, '/config/moodle/editar') && preg_match('#^/config/moodle/editar/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_config_moodle_editar']), array (  '_controller' => 'Nte\\Config\\MoodleBundle\\Controller\\DefaultController::editarAction',));
                    }

                    if (0 === strpos($pathinfo, '/config/moodle/remover')) {
                        // nte_config_moodle_remover
                        if (preg_match('#^/config/moodle/remover/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_config_moodle_remover']), array (  '_controller' => 'Nte\\Config\\MoodleBundle\\Controller\\DefaultController::removerAction',));
                        }

                        // nte_config_moodle_remover_todos
                        if ('/config/moodle/remover-todos' === $pathinfo) {
                            return array (  '_controller' => 'Nte\\Config\\MoodleBundle\\Controller\\DefaultController::removerTodosAction',  '_route' => 'nte_config_moodle_remover_todos',);
                        }

                    }

                    // nte_config_moodle_novo
                    if ('/config/moodle/novo' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Config\\MoodleBundle\\Controller\\DefaultController::novoAction',  '_route' => 'nte_config_moodle_novo',);
                    }

                    // nte_config_moodle_capturar_token
                    if ('/config/moodle/token' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Config\\MoodleBundle\\Controller\\DefaultController::capturarTokenAction',  '_route' => 'nte_config_moodle_capturar_token',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/config/sie')) {
                    // nte_config_sie_homepage
                    if ('/config/sie' === $trimmedPathinfo) {
                        $ret = array (  '_controller' => 'Nte\\Config\\SieBundle\\Controller\\DefaultController::indexAction',  '_route' => 'nte_config_sie_homepage',);
                        if ('/' === substr($pathinfo, -1)) {
                            // no-op
                        } elseif ('GET' !== $canonicalMethod) {
                            goto not_nte_config_sie_homepage;
                        } else {
                            return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_config_sie_homepage'));
                        }

                        return $ret;
                    }
                    not_nte_config_sie_homepage:

                    // nte_config_sie_listar
                    if ('/config/sie/listar' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Config\\SieBundle\\Controller\\DefaultController::listarAction',  '_route' => 'nte_config_sie_listar',);
                    }

                    // nte_config_sie_configurar_inscricao
                    if ('/config/sie/inscricao' === $pathinfo) {
                        return array (  '_controller' => 'Nte\\Config\\SieBundle\\Controller\\DefaultController::configurarInscricaoAction',  '_route' => 'nte_config_sie_configurar_inscricao',);
                    }

                }

            }

        }

        elseif (0 === strpos($pathinfo, '/middleware/moodle')) {
            // nte_middleware_moodle_homepage
            if ('/middleware/moodle' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'Nte\\Middleware\\MoodleBundle\\Controller\\DefaultController::indexAction',  '_route' => 'nte_middleware_moodle_homepage',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_nte_middleware_moodle_homepage;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_middleware_moodle_homepage'));
                }

                return $ret;
            }
            not_nte_middleware_moodle_homepage:

            // nte_middleware_moodle_sincronizar
            if ('/middleware/moodle/sincronizar' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'NteMiddlewareMoodleBundle:Default:sincronizarIndex:index',  '_route' => 'nte_middleware_moodle_sincronizar',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_nte_middleware_moodle_sincronizar;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_middleware_moodle_sincronizar'));
                }

                return $ret;
            }
            not_nte_middleware_moodle_sincronizar:

        }

        elseif (0 === strpos($pathinfo, '/middleware/sie')) {
            // nte_middleware_sie_homepage
            if ('/middleware/sie' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'Nte\\Middleware\\SieBundle\\Controller\\DefaultController::indexAction',  '_route' => 'nte_middleware_sie_homepage',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_nte_middleware_sie_homepage;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_middleware_sie_homepage'));
                }

                return $ret;
            }
            not_nte_middleware_sie_homepage:

            // nte_middleware_sie_sincronizar
            if ('/middleware/sie' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'Nte\\Middleware\\SieBundle\\Controller\\DefaultController::indexAction',  '_route' => 'nte_middleware_sie_sincronizar',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_nte_middleware_sie_sincronizar;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_middleware_sie_sincronizar'));
                }

                return $ret;
            }
            not_nte_middleware_sie_sincronizar:

        }

        elseif (0 === strpos($pathinfo, '/buscador')) {
            // nte_aplicacao_buscador_index
            if ('/buscador' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'Nte\\Aplicacao\\BuscadorBundle\\Controller\\DefaultController::indexAction',  '_route' => 'nte_aplicacao_buscador_index',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_nte_aplicacao_buscador_index;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_aplicacao_buscador_index'));
                }

                return $ret;
            }
            not_nte_aplicacao_buscador_index:

            if (0 === strpos($pathinfo, '/buscador/pessoas')) {
                // buscador_pessoas_index
                if ('/buscador/pessoas' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'Nte\\Aplicacao\\BuscadorBundle\\Controller\\PessoaController::indexAction',  '_route' => 'buscador_pessoas_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_buscador_pessoas_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'buscador_pessoas_index'));
                    }

                    return $ret;
                }
                not_buscador_pessoas_index:

                // buscador_pessoas_perfil
                if (0 === strpos($pathinfo, '/buscador/pessoas/perfil') && preg_match('#^/buscador/pessoas/perfil(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'buscador_pessoas_perfil']), array (  '_controller' => 'Nte\\Aplicacao\\BuscadorBundle\\Controller\\PessoaController::buscarIdAction',  'id' => NULL,));
                }

                // buscador_pessoas_pesquisar
                if ('/buscador/pessoas/pesquisar.json' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\BuscadorBundle\\Controller\\PessoaController::buscarPessoasSieAction',  '_route' => 'buscador_pessoas_pesquisar',);
                }

                // buscador_pessoas_filtrar_nome_middleware
                if ('/buscador/pessoas/filtrar-nome.json' === $pathinfo) {
                    return array (  '_controller' => 'Nte\\Aplicacao\\BuscadorBundle\\Controller\\PessoaController::filtrarNomeMiddlewareAction',  '_route' => 'buscador_pessoas_filtrar_nome_middleware',);
                }

            }

            // nte_aplicacao_buscador_cursos_disciplina
            if (0 === strpos($pathinfo, '/buscador/disciplina') && preg_match('#^/buscador/disciplina/(?P<ano>[^/]++)/(?P<periodo>[^/]++)/(?P<curso>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_aplicacao_buscador_cursos_disciplina']), array (  '_controller' => 'Nte\\Aplicacao\\BuscadorBundle\\Controller\\DefaultController::buscarDisciplinasAction',));
            }

            // nte_aplicacao_buscador_cursos_disciplina_turmas
            if (0 === strpos($pathinfo, '/buscador/turmas') && preg_match('#^/buscador/turmas/(?P<turma>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_aplicacao_buscador_cursos_disciplina_turmas']), array (  '_controller' => 'Nte\\Aplicacao\\BuscadorBundle\\Controller\\DefaultController::buscarTurmasAction',));
            }

        }

        elseif (0 === strpos($pathinfo, '/login')) {
            // fos_user_security_login
            if ('/login' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.security.controller:loginAction',  '_route' => 'fos_user_security_login',);
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_security_login;
                }

                return $ret;
            }
            not_fos_user_security_login:

            // fos_user_security_check
            if ('/login_check' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.security.controller:checkAction',  '_route' => 'fos_user_security_check',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_fos_user_security_check;
                }

                return $ret;
            }
            not_fos_user_security_check:

        }

        // fos_user_security_logout
        if ('/logout' === $pathinfo) {
            $ret = array (  '_controller' => 'fos_user.security.controller:logoutAction',  '_route' => 'fos_user_security_logout',);
            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                $allow = array_merge($allow, ['GET', 'POST']);
                goto not_fos_user_security_logout;
            }

            return $ret;
        }
        not_fos_user_security_logout:

        if (0 === strpos($pathinfo, '/recuperar-senha')) {
            // fos_user_resetting_request
            if ('/recuperar-senha/request' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.resetting.controller:requestAction',  '_route' => 'fos_user_resetting_request',);
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_resetting_request;
                }

                return $ret;
            }
            not_fos_user_resetting_request:

            // fos_user_resetting_reset
            if (0 === strpos($pathinfo, '/recuperar-senha/reset') && preg_match('#^/recuperar\\-senha/reset/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'fos_user_resetting_reset']), array (  '_controller' => 'fos_user.resetting.controller:resetAction',));
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_resetting_reset;
                }

                return $ret;
            }
            not_fos_user_resetting_reset:

            // fos_user_resetting_send_email
            if ('/recuperar-senha/send-email' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.resetting.controller:sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_fos_user_resetting_send_email;
                }

                return $ret;
            }
            not_fos_user_resetting_send_email:

            // fos_user_resetting_check_email
            if ('/recuperar-senha/check-email' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.resetting.controller:checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_resetting_check_email;
                }

                return $ret;
            }
            not_fos_user_resetting_check_email:

        }

        elseif (0 === strpos($pathinfo, '/p')) {
            if (0 === strpos($pathinfo, '/profile')) {
                // fos_user_profile_show
                if ('/profile' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'fos_user.profile.controller:showAction',  '_route' => 'fos_user_profile_show',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_fos_user_profile_show;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'fos_user_profile_show'));
                    }

                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_fos_user_profile_show;
                    }

                    return $ret;
                }
                not_fos_user_profile_show:

                // fos_user_profile_edit
                if ('/profile/edit' === $pathinfo) {
                    $ret = array (  '_controller' => 'fos_user.profile.controller:editAction',  '_route' => 'fos_user_profile_edit',);
                    if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                        $allow = array_merge($allow, ['GET', 'POST']);
                        goto not_fos_user_profile_edit;
                    }

                    return $ret;
                }
                not_fos_user_profile_edit:

            }

            // nte_usuario_redefinir
            if ('/primeiro-acesso' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'Nte\\UsuarioBundle\\Controller\\UsuarioController::editarPrimeiroAcessoAction',  '_route' => 'nte_usuario_redefinir',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_nte_usuario_redefinir;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_usuario_redefinir'));
                }

                return $ret;
            }
            not_nte_usuario_redefinir:

            // nte_teste
            if ('/pablo-teste.html.twig' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'Nte\\Aplicacao\\PessoalExternoBundle\\Controller\\DefaultController::indexAction',  '_route' => 'nte_teste',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_nte_teste;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_teste'));
                }

                return $ret;
            }
            not_nte_teste:

        }

        elseif (0 === strpos($pathinfo, '/usuario')) {
            // nte_usuario_index
            if ('/usuario' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'Nte\\UsuarioBundle\\Controller\\UsuarioController::indexAction',  '_route' => 'nte_usuario_index',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_nte_usuario_index;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'nte_usuario_index'));
                }

                return $ret;
            }
            not_nte_usuario_index:

            // nte_usuario_teste
            if ('/usuario/teste.html.twig' === $pathinfo) {
                return array (  '_controller' => 'Nte\\UsuarioBundle\\Controller\\UsuarioController::teste.html.twigAction',  '_route' => 'nte_usuario_teste',);
            }

            // nte_usuario_perfil
            if (0 === strpos($pathinfo, '/usuario/perfil') && preg_match('#^/usuario/perfil(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_usuario_perfil']), array (  '_controller' => 'Nte\\UsuarioBundle\\Controller\\UsuarioController::perfilAction',  'id' => NULL,));
            }

            // nte_usuario_editar
            if (0 === strpos($pathinfo, '/usuario/editar') && preg_match('#^/usuario/editar/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_usuario_editar']), array (  '_controller' => 'Nte\\UsuarioBundle\\Controller\\UsuarioController::editarAction',));
            }

            // nte_usuario_ativar
            if (0 === strpos($pathinfo, '/usuario/ativar') && preg_match('#^/usuario/ativar/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_usuario_ativar']), array (  '_controller' => 'Nte\\UsuarioBundle\\Controller\\UsuarioController::ativarAction',));
            }

            // nte_usuario_update
            if (0 === strpos($pathinfo, '/usuario/atualizar') && preg_match('#^/usuario/atualizar/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_usuario_update']), array (  '_controller' => 'Nte\\UsuarioBundle\\Controller\\UsuarioController::updateAction',));
            }

            // nte_usuario_novo
            if ('/usuario/novo' === $pathinfo) {
                return array (  '_controller' => 'Nte\\UsuarioBundle\\Controller\\UsuarioController::novoAction',  '_route' => 'nte_usuario_novo',);
            }

            // nte_usuario_uploadimagem
            if (0 === strpos($pathinfo, '/usuario/upload-imagem') && preg_match('#^/usuario/upload\\-imagem(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_usuario_uploadimagem']), array (  '_controller' => 'Nte\\UsuarioBundle\\Controller\\FosArquivoPessoalController::uploadImagemAction',  'id' => NULL,));
            }

            // nte_usuario_imagem_perfil
            if (0 === strpos($pathinfo, '/usuario/imagem') && preg_match('#^/usuario/imagem/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'nte_usuario_imagem_perfil']), array (  '_controller' => 'Nte\\UsuarioBundle\\Controller\\FosArquivoPessoalController::getImagemPerfilAction',));
            }

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
