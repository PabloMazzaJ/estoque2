<?php

namespace Proxies\__CG__\Nte\Config\MoodleBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class NteConfigMoodle extends \Nte\Config\MoodleBundle\Entity\NteConfigMoodle implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'id', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'idGrupo', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'url', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'usuario', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'token', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'servico', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'nome', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'descricao', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'valido', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'dataCriacao'];
        }

        return ['__isInitialized__', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'id', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'idGrupo', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'url', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'usuario', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'token', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'servico', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'nome', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'descricao', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'valido', '' . "\0" . 'Nte\\Config\\MoodleBundle\\Entity\\NteConfigMoodle' . "\0" . 'dataCriacao'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (NteConfigMoodle $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setIdGrupo($idGrupo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIdGrupo', [$idGrupo]);

        return parent::setIdGrupo($idGrupo);
    }

    /**
     * {@inheritDoc}
     */
    public function getIdGrupo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIdGrupo', []);

        return parent::getIdGrupo();
    }

    /**
     * {@inheritDoc}
     */
    public function setUrl($url)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUrl', [$url]);

        return parent::setUrl($url);
    }

    /**
     * {@inheritDoc}
     */
    public function getUrl()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUrl', []);

        return parent::getUrl();
    }

    /**
     * {@inheritDoc}
     */
    public function setUsuario($usuario)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUsuario', [$usuario]);

        return parent::setUsuario($usuario);
    }

    /**
     * {@inheritDoc}
     */
    public function getUsuario()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUsuario', []);

        return parent::getUsuario();
    }

    /**
     * {@inheritDoc}
     */
    public function setToken($token)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setToken', [$token]);

        return parent::setToken($token);
    }

    /**
     * {@inheritDoc}
     */
    public function getToken()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getToken', []);

        return parent::getToken();
    }

    /**
     * {@inheritDoc}
     */
    public function setServico($servico)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setServico', [$servico]);

        return parent::setServico($servico);
    }

    /**
     * {@inheritDoc}
     */
    public function getServico()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getServico', []);

        return parent::getServico();
    }

    /**
     * {@inheritDoc}
     */
    public function setNome($nome)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNome', [$nome]);

        return parent::setNome($nome);
    }

    /**
     * {@inheritDoc}
     */
    public function getNome()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNome', []);

        return parent::getNome();
    }

    /**
     * {@inheritDoc}
     */
    public function setDescricao($descricao)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDescricao', [$descricao]);

        return parent::setDescricao($descricao);
    }

    /**
     * {@inheritDoc}
     */
    public function getDescricao()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDescricao', []);

        return parent::getDescricao();
    }

    /**
     * {@inheritDoc}
     */
    public function getValido()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getValido', []);

        return parent::getValido();
    }

    /**
     * {@inheritDoc}
     */
    public function setValido($valido)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setValido', [$valido]);

        return parent::setValido($valido);
    }

    /**
     * {@inheritDoc}
     */
    public function getDataCriacao()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDataCriacao', []);

        return parent::getDataCriacao();
    }

    /**
     * {@inheritDoc}
     */
    public function setDataCriacao($dataCriacao)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDataCriacao', [$dataCriacao]);

        return parent::setDataCriacao($dataCriacao);
    }

}
