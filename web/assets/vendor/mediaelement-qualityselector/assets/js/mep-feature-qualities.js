(function ($) {

    // add extra default options
    $.extend(mejs.MepDefaults, {
        // this will automatically turn on a <track>
        startQuality: '480p',

        qualitiesText: 'Qualidade'
    });

    $.extend(MediaElementPlayer.prototype, {

        buildqualities: function (player, controls, layers, media) {
            var t = this,
                i,
                options = '';

            // If list exists, nuke it
            if ($(".mejs-qualities-selector").length > 0) {
                $(".mejs-qualities-selector ul li").remove();
            } else {
                player.qualitiesButton =
                    $('<div class="mejs-button mejs-qualities-button">' +
                        '<button type="button" aria-controls="' + t.id + '" title="' + t.options.qualitiesText + '" aria-label="' + t.options.qualitiesText + '"></button>' +
                        '<div class="mejs-qualities-selector">' +
                        '<ul></ul>' +
                        '</div>' +
                        '</div>').appendTo(controls);
                player.selectedQuality = mejs.MepDefaults.startQuality;
            }

            player.sources = $(player.domNode).find("source");

            player.qualities = [];
            for (var i = 0; i < player.sources.length; i++) {
                if (player.sources[i].getAttribute("data-plugin-type") == player.media.pluginType) {
                    player.qualities.push(player.sources[i]);
                }
            }

            // if only one language then just make the button a toggle
            if (player.qualities.length == 1) {

                player.qualitiesButton.on('click', function () {

                });
            } else {
                // hover
                player.qualitiesButton.hover(function () {
                    $(this).find('.mejs-qualities-selector').css('visibility', 'visible');
                }, function () {
                    $(this).find('.mejs-qualities-selector').css('visibility', 'hidden');
                })

                // handle clicks to the quality radio buttons
                    .on('click', 'input[type=radio]', function () {
                        player.switchQuality($(this).siblings('label').text(), this.getAttribute("value"), this.getAttribute("data-mimetype"));
                    });
            }

            if (!player.options.alwaysShowControls) {
                // move with controls
                player.container
                    .bind('controlsshown', function () {
                        // push captions above controls
                        player.container.find('.mejs-qualities-position').addClass('mejs-qualities-position-hover');

                    })
                    .bind('controlshidden', function () {
                        if (!media.paused) {
                            // move back to normal place
                            player.container.find('.mejs-qualities-position').removeClass('mejs-qualities-position-hover');
                        }
                    });
            } else {
                player.container.find('.mejs-qualities-position').addClass('mejs-qualities-position-hover');
            }

            // Gets the index of pre-selected quality, default is the first quality
            var selectedIndex = 0;
            for (var i = 0; i < player.qualities.length; i++) {
                var q = player.qualities[i];
                if (q.getAttribute("data-quality") === player.selectedQuality) {
                    selectedIndex = i;
                    break;
                }
            }

            // Adds qualities to list and pre-selects the chosen one
            for (var i = 0; i < player.qualities.length; i++) {
                var q = player.qualities[i];
                var isSelected = selectedIndex == i;
                player.addQualityButton(q.getAttribute("data-quality"), q.getAttribute("src"), q.getAttribute("type"), isSelected);

                // Makes current player use the pre-selected quality
                if (isSelected) {
                    player.switchQuality(q.getAttribute("data-quality"), q.getAttribute("src"), q.getAttribute("type"));
                }
            }

        },

        addQualityButton: function (label, url, mimetype, isSelected) {
            var t = this;
            if (label === '') {
                label = "Unknown";
            }
            var checkedAttr = isSelected ? "checked" : "";

            t.qualitiesButton.find('ul').append(
                '<li>' +
                '<label for="' + t.id + '_qualities_' + label + '">' +
                '<input type="radio" name="' + t.id + '_qualities" id="' + t.id + '_qualities_' + label + '" value="' + url + '"' + ' data-mimetype="' + mimetype + '"' + checkedAttr + '/>' +
                label + '</label>' +
                '</li>'
            );
        },

        switchQuality: function (quality, url, mimetype) {
            var player = this;
            player.selectedQuality = quality;
            this.switchStream(url);

        },
        /**
         * Switches the current played file
         *
         * @param src new media SRC
         */
        switchStream: function (src) {

            /**
             * Our media object
             * @type {Object|string|*}
             */
            var media = this.media;

            /**
             * We do nothing if we are asked to to the same thing
             */
            if (media.currentSrc != src) {

                /**
                 * Storing currentTime
                 */
                var currentTime = media.currentTime;
                /**
                 * And current statu
                 */
                var paused = media.paused;

                /**
                 * Then we pause the video
                 */
                media.pause();

                /**
                 * And change source
                 */
                media.setSrc(src);

                /**
                 * When the new file is preloaded
                 */
                media.addEventListener('loadedmetadata', function (e) {
                    /**
                     * We continue playing where we stopped
                     */
                    media.currentTime = currentTime;
                }, true);


                /**
                 * On canPlay event
                 * @param e
                 */
                var canPlayAfterSourceSwitchHandler = function (e) {
                    /**
                     * If we weren't paused we play
                     */
                    if (!paused) {
                        media.play();
                    }
                    /**github
                     * And remove the listener
                     */
                    media.removeEventListener("canplay", canPlayAfterSourceSwitchHandler, true);
                };

                /**
                 * Whenever we're ready ...
                 */
                media.addEventListener('canplay', canPlayAfterSourceSwitchHandler, true);

                /**
                 * Then last be first of all we load the file
                 */
                media.load();
            }
        }

    });

})(mejs.$);
