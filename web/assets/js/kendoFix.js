function resizeGrid() {
    var header = $("#header-content");
    var content = $("#main-content");
    var grid = $("#grid");

    //Other variables
    var minimumAcceptableGridHeight = 100; //This is roughly 5 rows 
    var otherElementsHeight = 0;

    //Get Window Height 
    var windowHeight = $(window).innerHeight();

    //Get Header Height if its existing
    var hasHeader = header.length;
    var headerHeight = hasHeader ? header.outerHeight(true) : 0;

    //Get the Grid Element and Areas Inside It
    var contentArea = grid.find(".k-grid-content");  //This is the content Where Grid is located
    var otherGridElements = grid.children().not(".k-grid-content"); //This is anything ather than the Grid iteslf like header, commands, etc
    console.debug(otherGridElements);

    //Calcualte all Grid elements height
    otherGridElements.each(function() {
        otherElementsHeight += $(this).outerHeight(true);
    });

    //Get other elements same level as Grid
    var parentDiv = grid.parent("div");

    var hasMainContent = parentDiv.length;
    if (hasMainContent) {
        var otherSiblingElements = content.children()
                .not("#grid")
                .not("script");

        //Calculate all Sibling element height
        otherSiblingElements.each(function() {
            otherElementsHeight += $(this).outerHeight(true);
        });
    }

    //Padding you want to apply below your page
   // var bottomPadding = 85 + topoHeight;

    //Check if Calculated height is below threshold
    var calculatedHeight = windowHeight - headerHeight - otherElementsHeight - bottomPadding ;
    var finalHeight = calculatedHeight < minimumAcceptableGridHeight ? minimumAcceptableGridHeight : calculatedHeight;

    //Apply the height for the content area
    contentArea.height(finalHeight); 
}

$(window).resize(function() {
    resizeGrid();
});