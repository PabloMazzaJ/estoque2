var player = new MediaElementPlayer('#player', {
    enableAutosize: true,
    features: ['playpause',
        'progress',
        'current',
        'duration',
        'tracks',
        'volume',
        'fullscreen',
        'qualities',
        'timerailthumbnails'
    ],
    alwaysShowControls: false,
    iPadUseNativeControls: false,
    iPhoneUseNativeControls: false,
    AndroidUseNativeControls: false,
    alwaysShowHours: false,
    showTimecodeFrameCount: false,
    framesPerSecond: 25,
    enableKeyboard: true,
    timeRailThumbnailsSeconds: 2,
    flashName: '/assets/vendor/mediaelement/build/flashmediaelement.swf'
});
