$(function() {

    var module; // Current Network.js' module

    /*
     * Tooltips
     */

    $('.btn-group').tooltip();

    /*
     * UI
     */

    var UI = {
        $btnStart: $('[data-measure]'),
        $btnAll:   $('[data-measure-all]'),
        $btnAbort: $('[data-abort]'),
        $output: $('output'),
        start: function() {

            rawModule = $(this).data('measure');

            module = rawModule.charAt(0).toUpperCase() + rawModule.slice(1);

            UI.$btnStart.prop('disabled', true);
            UI.$btnAbort.prop('disabled', false);

            net[rawModule].start();

            // The latency module doesn't have a start event, we must trigger it manually.
            if (rawModule == 'latency') {
                net[rawModule].trigger('start');
            }
        },

        restart: function(size) {
            UI.notice(UI.delimiter(
                'O mínimo de atraso de ' + UI.value(8, 'segundos') + ' não foi atingido'
            ));

            UI.notice(UI.delimiter(
                'Reiniciando medição com  '
                + UI.value(size / 1024 / 1024, 'MB')
                + ' de dados...'
            ));
        },

        stop: function() {
            UI.notice(UI.delimiter('Medição Finalizada'));
            UI.$btnStart.prop('disabled', false);
            UI.$btnAbort.prop('disabled', true);
        },

        abort: function() {
            net.upload.abort();
            net.download.abort();
        },

        notice: function(text, newSection) {
            var $o = UI.$output,
                stickToBottom = ($o.scrollTop() + $o.outerHeight()) == $o.prop('scrollHeight');

            $o.append('<br>');
            newSection && $o.append('<br>');

            $o.append('<span class="yellow">[' + module + ']</span> ' + text);

            if (stickToBottom) {
                $o.scrollTop($o.prop('scrollHeight'));
            }
        },

        value: function(value, unit) {
            if (value != null) {

                return '<span class="blue">' + value.toFixed(3) + ' ' + unit + '</span>';
            } else {
                return '<span class="blue">null</span>';
            }
        },

        delimiter: function(text) {
            return '<span class="green">' + text + '</span>';
        }
    };

    /*
     * Network.js configuration
     */

    var net = new Network();

    function start(size) {
        UI.notice(UI.delimiter(
            'Iniciando a medição de ' + rawModule + ' '
            + (rawModule != 'latency' ? (' with ' + UI.value(size / 1024 / 1024, 'MB') + ' de dados') : '')
            + '...'
        ), true);
    }

    function progress(avg, instant) {
        var output = 'Velocidade atual: ' + UI.value(instant / 1024 / 1024, 'MBps');
            output += ' // Velocidade Média: ' + UI.value(avg / 1024 / 1024, 'MBps');
        var media= instant*8/1024/1000;


        //grafico.options.series[0].data = data[media];
        $('#container').highcharts().series[0].points[0].update(media);
        //UI.notice(output);
    }

    function end(avg) {
        $('#end-'+rawModule).text((avg *8/1024/1000).toFixed(2));
        $('#container').highcharts().series[0].points[0].update(0);
        //var chart = new Highcharts(grafico);
        UI.notice('Media final de velocidade: ' + UI.value(avg / 1024 / 1024, 'MBps'));
        UI.stop();
    }

    net.upload.on('start', start).on('progress', progress).on('restart', UI.restart).on('end', end);
    net.download.on('start', start).on('progress', progress).on('restart', UI.restart).on('end', end);

    net.latency
        .on('start', start)
        .on('end', function(avg, all) {
            all = all.map(function(latency) {
                return UI.value(latency, 'ms');
            });

            all = '[ ' + all.join(' , ') + ' ]';
            $('#end-latency').text(avg.toFixed(2));
            UI.notice('Instant latencies: ' + all);
            UI.notice('Latência média: ' + UI.value(avg, 'ms'));
            UI.stop();
        });

    /*
     * Bindings
     */

    UI.$btnStart.on('click', UI.start);
    UI.$btnAbort.on('click', UI.abort);
    UI.$btnAll.on('click', function(){
        console.log($(UI.$btnAll));
        UI.$btnAll.data('measure','upload');
        net.upload.start();
        console.log($(UI.$btnAll));
    });
});
