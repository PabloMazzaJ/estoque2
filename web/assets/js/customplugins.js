(function ($) {
    'use strict';
    /*
    $('#wPessoasForm').validate({
        submitHandler: function (form) {
            var $form = $(form),
                $submitButton = $(this.submitButton);
            $("#w4Erro").addClass('hidden');
            $submitButton.button('aguarde');
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: {
                    nome: $form.find('#wPessoasFormNome').val(),
                    email: $form.find('#wPessoasFormEmail').val(),
                    cpf: $form.find('#wPessoasFormCpf').val(),
                },
                dataType: 'json',
                complete: function (data) {
                    if (typeof data.responseJSON === 'object') {
                        if (data.responseJSON.response == 'success') {
                            $submitButton.button('reset');
                            return;
                        }
                    }
                    $form.find('.has-success')
                        .removeClass('has-success');
                    $submitButton.button('reset');

                }
            });
            $submitButton.button('reset');
            $.magnificPopup.close();
        }
    });
    $('#wFuncoesForm').validate({
        submitHandler: function (form) {
            var $form = $(form),
                $submitButton = $(this.submitButton);
            $("#w4Erro").addClass('hidden');
            $submitButton.button('aguarde');
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: {
                    descricao: $form.find('#wFuncoesFormDescricao').val(),
                },
                dataType: 'json',
                complete: function (data) {
                    if (typeof data.responseJSON === 'object') {
                        if (data.responseJSON.response == 'success') {
                            $submitButton.button('reset');
                            return;
                        }
                    }
                    $form.find('.has-success')
                        .removeClass('has-success');
                    $submitButton.button('reset');
                }
            });
            $submitButton.button('reset');
            $.magnificPopup.close();
        }
    });
    $('#wCategoriaForm').validate({
        submitHandler: function (form) {
            var $form = $(form),
                $submitButton = $(this.submitButton);
            $("#w4Erro").addClass('hidden');
            $submitButton.button('aguarde');
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: {
                    descricao: $form.find('#wCategoriaFormDescricao').val(),
                    categoriaPai: $form.find('#wCategoriaFormCategoria').val(),
                },
                dataType: 'json',
                complete: function (data) {
                    if (typeof data.responseJSON === 'object') {
                        if (data.responseJSON.response == 'success') {
                            $submitButton.button('reset');
                            return;
                        }
                    }
                    $form.find('.has-success')
                        .removeClass('has-success');
                    $submitButton.button('reset');

                }
            });
            $submitButton.button('reset');
            $.magnificPopup.close();
        }
    });

    60 - 1
    x - 41
60 * 41 = 1*x

x = 60/41
    60x - 35
    x = 35/60

    */

}).apply(this, [jQuery]);
