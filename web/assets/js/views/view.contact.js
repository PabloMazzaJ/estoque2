(function ($) {
    'use strict';
    $('#contactForm:not([data-type=advanced])').validate({
        submitHandler: function (form) {

            var $form = $(form),
                $messageSuccess = $('#contactSuccess'),
                $messageError = $('#contactError'),
                $submitButton = $(this.submitButton);

            $submitButton.button('loading');

            // Ajax Submit
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: {
                    nome: $form.find('#nome').val(),
                    email: $form.find('#email').val(),
                    assunto: $form.find('#assunto').val(),
                    telefone: $form.find('#telefonec').val(),
                    mensagem: $form.find('#mensagem').val(),
                },
                dataType: 'json',
                complete: function (data) {
                    if (typeof data.responseJSON === 'object') {
                        if (data.responseJSON.response == 'success') {
                            $messageSuccess.removeClass('hidden');
                            $messageError.addClass('hidden');
                            // Reset Form
                            $form.find('.form-control')
                                .val('')
                                .blur()
                                .parent()
                                .removeClass('has-success')
                                .removeClass('has-error')
                                .find('label.error')
                                .remove();
                            if (($messageSuccess.offset().top - 80) < $(window).scrollTop()) {
                                $('html, body').animate({
                                    scrollTop: $messageSuccess.offset().top - 80
                                }, 300);
                            }
                            $submitButton.button('reset');
                            return;
                        }
                    }

                    $messageError.removeClass('hidden');
                    $messageSuccess.addClass('hidden');

                    if (($messageError.offset().top - 80) < $(window).scrollTop()) {
                        $('html, body').animate({
                            scrollTop: $messageError.offset().top - 80
                        }, 300);
                    }

                    $form.find('.has-success')
                        .removeClass('has-success');

                    $submitButton.button('reset');

                }
            });
        }
    });
    $('#precadastroForm:not([data-type=advanced])').validate({
        submitHandler: function (form) {

            var $form = $(form),
                $messageSuccess = $('#precadastroSuccess'),
                $messageError = $('#precadastroError'),
                $submitButton = $(this.submitButton);

            $submitButton.button('loading');

            // Ajax Submit
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: {
                    nome: $form.find('#nome').val(),
                    cpf: $form.find('#cpf').val(),
                    email: $form.find('#email').val(),
                    rg: $form.find('#rg').val(),
                    telefone: $form.find('#telefone').val(),
                    telefone2: $form.find('#telefone2').val(),
                    rua: $form.find('#rua').val(),
                    numero: $form.find('#numero').val(),
                    complemento: $form.find('#complemento').val(),
                    referencia: $form.find('#referencia').val(),
                    bairro: $form.find('#bairro').val(),
                    cidade: $form.find('#cidade').val(),
                    uf: $form.find('#uf').val(),
                    cep1: $form.find('#cep1').val(),
                    planoc: $form.find('#planoc').val(),
                    negocia: $form.find('#negocia').val(),
                },
                dataType: 'json',
                complete: function (data) {
                    if (typeof data.responseJSON === 'object') {
                        if (data.responseJSON.response == 'success') {
                            $messageSuccess.removeClass('hidden');
                            $messageError.addClass('hidden');
                            $.magnificPopup.instance.close();
                            // Reset Form
                            $form.find('.form-control')
                                .val('')
                                .blur()
                                .parent()
                                .removeClass('has-success')
                                .removeClass('has-error')
                                .find('label.error')
                                .remove();
                            if (($messageSuccess.offset().top - 80) < $(window).scrollTop()) {
                                $('html, body').animate({
                                    scrollTop: $messageSuccess.offset().top - 80
                                }, 300);
                            }
                            $submitButton.button('reset');
                            return;
                        }
                    }

                    $messageError.removeClass('hidden');
                    $messageSuccess.addClass('hidden');

                    if (($messageError.offset().top - 80) < $(window).scrollTop()) {
                        $('html, body').animate({
                            scrollTop: $messageError.offset().top - 80
                        }, 300);
                    }

                    $form.find('.has-success')
                        .removeClass('has-success');

                    $submitButton.button('reset');

                }
            });
        }
    });
    $('#suporteForm:not([data-type=advanced])').validate({
        submitHandler: function (form) {

            var $form = $(form),
                $submitButton = $(this.submitButton);
            $("#suporteErro").addClass('hidden');
            $submitButton.button('loading');

            // Ajax Submit
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: {
                    cpf: $form.find('#suporteCpf').val(),
                    telefone: $form.find('#suporteTelefone').val(),
                },
                dataType: 'json',
                complete: function (data) {
                    if (typeof data.responseJSON === 'object') {
                        if (data.responseJSON.response == 'success') {
                            $('#suporteConfirmaForm').removeClass('hidden');

                            $('#suporteConfirmaFormCod').val(data.responseJSON.cod);
                            $('#suporteConfirmaFormIdCliente').val(data.responseJSON.idcliente);
                            $('#suporteConfirmaFormNome').val(data.responseJSON.nome);
                            $('#suporteConfirmaFormCpf').val(data.responseJSON.cpf);
                            $('#suporteConfirmaFormTelefone').val(data.responseJSON.telefone);
                            $('#suporteConfirmaFormEndereco').val(data.responseJSON.endereco);
                            $('#suporteConfirmaFormEmail').val(data.responseJSON.email);


                            $form.addClass('hidden');
                            // Reset Form
                            $form.find('.form-control')
                                .val('')
                                .blur()
                                .parent()
                                .removeClass('has-success')
                                .removeClass('has-error')
                                .find('label.error')
                                .remove();

                            $submitButton.button('reset');
                            return;

                        }
                    }
                    $("#suporteErro").removeClass('hidden');

                    if (($("#suporteErro").offset().top - 80) < $(window).scrollTop()) {
                        $('html, body').animate({
                            scrollTop: $("#suporteErro").offset().top - 80
                        }, 300);
                    }

                    $form.find('.has-success')
                        .removeClass('has-success');

                    $submitButton.button('reset');

                }
            });
        }
    });
    $('#suporteConfirmaForm:not([data-type=advanced])').validate({
        rules: {
            suporteConfirmaFormOperadora: {
                required: true
            },
            select: "required"
        },
        messages: {
            suporteConfirmaFormOperadora: "Por favor, selecione a operadora",
            select: "Por favor, selecione a operadora",

        },

        submitHandler: function (form) {

            var $form = $(form),
                $submitButton = $(this.submitButton);
            $("#suporteEnviarErro").addClass('hidden');
            $submitButton.button('loading');

            // Ajax Submit
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: {
                    cod: $('#suporteConfirmaFormCod').val(),
                    idcliente: $('#suporteConfirmaFormIdCliente').val(),
                    nome: $('#suporteConfirmaFormNome').val(),
                    cpf: $('#suporteConfirmaFormCpf').val(),
                    telefone: $('#suporteConfirmaFormTelefone').val(),
                    endereco: $('#suporteConfirmaFormEndereco').val(),
                    email: $('#suporteConfirmaFormEmail').val(),
                    departamento: $('#suporteConfirmaFormDepartamento').val(),
                    operadora: $('#suporteConfirmaFormOperadora').val(),
                    mensagem: $('#suporteConfirmaFormMensagem').val(),
                },
                dataType: 'json',
                complete: function (data) {
                    if (typeof data.responseJSON === 'object') {
                        if (data.responseJSON.response == 'success') {
                            $form.addClass('hidden');
                            $('#suporteFinal').removeClass('hidden');
                            $('#suporteFormTitulo').addClass('hidden');
                            $('#protocolo').html(data.responseJSON.msg);

                            $form.addClass('hidden');
                            // Reset Form
                            $form.find('.form-control')
                                .val('')
                                .blur()
                                .parent()
                                .removeClass('has-success')
                                .removeClass('has-error')
                                .find('label.error')
                                .remove();

                            $submitButton.button('reset');
                            return;

                        }
                    }
                    $("#suporteEnviarErro").removeClass('hidden');

                    if (($("#suporteEnviarErro").offset().top - 80) < $(window).scrollTop()) {
                        $('html, body').animate({
                            scrollTop: $("#suporteEnviarErro").offset().top - 80
                        }, 300);
                    }

                    $form.find('.has-success')
                        .removeClass('has-success');

                    $submitButton.button('reset');

                }
            })
            ;
        }
    });

    $('#contratoForm:not([data-type=advanced])').validate({
        submitHandler: function (form) {

            var $form = $(form),
                $messageSuccess = $('#contaratoSuccess'),
                $messageError = $('#contratoError'),
                $submitButton = $(this.submitButton);

            $submitButton.button('loading');

            // Ajax Submit
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: {
                    nome: $form.find('#nomecc').val(),
                    email: $form.find('#emailcc').val(),
                    telefone: $form.find('#telefonecc').val(),
                    cpf: $form.find('#cpfcc').val(),
                },
                dataType: 'json',
                complete: function (data) {
                    if (typeof data.responseJSON === 'object') {
                        if (data.responseJSON.response == 'success') {
                            $messageSuccess.removeClass('hidden');
                            $messageError.addClass('hidden');
                            // Reset Form
                            $form.find('.form-control')
                                .val('')
                                .blur()
                                .parent()
                                .removeClass('has-success')
                                .removeClass('has-error')
                                .find('label.error')
                                .remove();
                            if (($messageSuccess.offset().top - 80) < $(window).scrollTop()) {
                                $('html, body').animate({
                                    scrollTop: $messageSuccess.offset().top - 80
                                }, 300);
                            }
                            $submitButton.button('reset');
                            return;
                        }
                    }

                    $messageError.removeClass('hidden');
                    $messageSuccess.addClass('hidden');

                    if (($messageError.offset().top - 80) < $(window).scrollTop()) {
                        $('html, body').animate({
                            scrollTop: $messageError.offset().top - 80
                        }, 300);
                    }

                    $form.find('.has-success')
                        .removeClass('has-success');

                    $submitButton.button('reset');

                }
            });
        }
    });
    $('#ecommerceForm').validate();
    /*  $('#ecommerceForm').on.submit(function () {
     if ($('#ecommerceForm').valid()) {
     $('#ecommerceForm').submit();
     }
     })*/


    $('#pagamentoForm').validate({
        submitHandler: function (form) {

            var $form = $(form),
                $submitButton = $(this.submitButton);
            $("#w4Erro").addClass('hidden');
            $submitButton.button('aguarde');

            // Ajax Submit
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: {
                    valor: $("input[type='radio'][name='boleto']:checked").data('options').valor,
                    codigo: $("input[type='radio'][name='boleto']:checked").data('options').codigo,
                    validadeMes: $form.find('#pagamentoCartaoValidadeMes').val(),
                    validadeAno: $form.find('#pagamentoCartaoValidadeAno').val(),
                    holding: $form.find('#pagamentoCartaoNumero').val(),
                    cvv: $form.find('#pagamentoCartaoCcv').val(),
                    bandeira: $("input[type='radio'][name='pagamentoCartaoBandeira']:checked").val(),
                    cartaoTitular: $form.find('#pagamentoCartaoTitular').val(),
                    cartaoCpf: $form.find('#pagamentoCartaoCpf').val(),
                },
                dataType: 'json',
                beforeSend: function () {

                    $.magnificPopup.open({
                        items: {
                            src: $('#aguarde').children()
                        },
                        type: 'inline',
                        showCloseBtn: false,
                        removalDelay: 400,
                        mainClass: 'mfp-status'
                    });

                },
                complete: function (data) {
                    if (typeof data.responseJSON === 'object') {
                        if (data.responseJSON.response == 'success') {
                            $.ajax({
                                type: 'get',
                                url: "api/cielo/transacao/consulta/" + data.responseJSON.tid,
                                dataType: 'json',
                                complete: function (data) {

                                    switch (data.responseJSON.status) {

                                        case 6 || 4:
                                            $('#pagamentoConfirmaTitulo').addClass('fa-check-circle-o heading-tertiary');
                                            $('#pagamentoConfirmaMsg').text('Operação realizada com sucesso');
                                            break;
                                        default :
                                            $('#pagamentoConfirmaTitulo').addClass('fa-check-circle heading-tertiary');
                                            $('#pagamentoConfirmaMsg').text('Aguarde... Em breve seus créditos serão adicionados.');
                                            break;
                                    }
                                    $('#pagamentoConfirmaMsg').html(data.responseJSON.msg);
                                    $('#pagamentoConfirmaTid').html('TID:<strong>'+data.responseJSON.tid+"</strong>");
                                    $('#w4').bootstrapWizard('next');
                                    $.magnificPopup.close();

                                }
                            });
                            $.magnificPopup.close();
                            $submitButton.button('reset');
                            return;
                        }
                        $('#pagamentoFormErroMsg').text(data.responseJSON.msg);
                    }
                    $("#pagamentoFormErro").removeClass('hidden');
                    if (($("#pagamentoFormErro").offset().top - 80) < $(window).scrollTop()) {
                        $('html, body').animate({
                            scrollTop: $("#pagamentoFormErro").offset().top - 80
                        }, 300);
                    }
                    $form.find('.has-success')
                        .removeClass('has-success');
                    $submitButton.button('reset');
                }
            });

        }
    });

}).apply(this, [jQuery]);

$(document).ajaxStart(function () {
    console.log()
});