<?php

namespace Nte\UsuarioBundle\Controller;

use Nte\UsuarioBundle\Entity\FosArquivoPessoal;
use Nte\UsuarioBundle\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class FosArquivoPessoalController extends Controller
{

    /**
     * @param $name
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }


    /**
     * Upload da imagem do perfil do usuário
     * @param Request $request
     * @param Usuario $usuario
     * @return JsonResponse
     */
    public function uploadImagemAction(Request $request, Usuario $usuario)
    {
        $uploadDir = $this->container->getParameter('upload_dir');
        $uploadMIMEAllow = $this->container->getParameter('upload_mime_allow');
        $arquivoAntigo = null;
        $obj = new \stdClass();
        // $obj->error = false;
        $tokenForm = $request->request->get('xtk');
        $tokenHeander = $request->headers->get('X-CSRFToken');


        //Validadores
        if (!$this->isCsrfTokenValid('friga-upload-usuario-imagem-cabecalho', $tokenHeander)) {
            $obj->error = true;
            $obj->message = "Token Inválido (0)";
            $obj->title = "Erro ao anexar arquivo";
            return new JsonResponse($obj);
        }
        if (!$this->isCsrfTokenValid('friga-upload-usuario-imagem-form', $tokenForm)) {
            $obj->error = true;
            $obj->message = "Token Inválido (1)";
            $obj->title = "Erro ao anexar arquivo";
            return new JsonResponse($obj);
        }
        if ($request->files->get('file') == null) {
            $obj->error = true;
            $obj->message = "NULL FILE";
            $obj->title = "Erro ao anexar arquivo";
            return new JsonResponse($obj);
        }
        if (!$usuario) {
            $usuarioDir = "tmp";
        } else {
            $usuarioDir = $usuario->getId();
        }

        /** @var File $file */
        $file = $request->files->get('file');
        $error = $file->getError();

        if ($error == 0) {
            $md5 = md5_file($file->getRealPath());
            $extencao = $file->guessExtension();
            $fileName = "perfil_" . $md5 . "_." . $file->guessExtension();
            $mimeType = $file->getMimeType();
            if (array_search($mimeType, $uploadMIMEAllow) !== false) {
                try {
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777);
                }
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir . "/usuarios/$usuarioDir/", 0777);
                }
                $uploadDir .= "/usuarios/$usuarioDir/";
                list($width, $height) = getimagesize($file);

                $image = new \Imagick($file->getPathname());
                $image->setGravity(\Imagick::GRAVITY_CENTER);
                $image->cropThumbnailImage(600, 600);
                $image->writeImage($file->getPathname());

                $file->move($uploadDir, $fileName);
                $em = $this->getDoctrine()->getManager();
                $registroArquivo = $usuario->getArquivoImagemPerfil();

                if ($registroArquivo == null) {

                    $registroArquivo = new FosArquivoPessoal();
                } else {
                    $arquivoAntigo = $registroArquivo->getPath();
                }
                $registroArquivo
                    ->setHash($md5)
                    ->setPath($uploadDir . $fileName)
                    ->setNome($fileName)
                    ->setTitulo("Imagem do usuário " . $usuario->getNome())
                    ->setExtencao($extencao)
                    ->setTipo(0);
                $registroArquivo->addIdUsuario($usuario);
                $usuario->addIdArquivo($registroArquivo);
                $em->persist($registroArquivo);
                $em->flush();

                $usuario->setImg($registroArquivo->getPath());
                $em->persist($usuario);
                $em->flush();
                if ($arquivoAntigo) {
                    //      unlink($arquivoAntigo);
                }

                $obj->fileId = $registroArquivo->getId();
                $obj->hash = $registroArquivo->getHash();

                } catch (\Exception $e) {
                    unlink($file->getRealPath());
                    $obj->error = true;
                    $obj->message = $e->getMessage();
                    $obj->title = "Erro ao anexar arquivo";
                }

            } else {
                unlink($file->getRealPath());
                $obj->error = true;
                $obj->message = "Tipo de arquivo não suportado";
                $obj->title = "Erro ao anexar arquivo";
            }
        } else if ($error = 1) {
            $obj->error = true;
            $obj->message = "O arquivo enviado excede o limite definido";
            $obj->title = "Erro ao anexar arquivo";
        } else {
            $obj->error = true;
            $obj->message = "Houve um erro ao anexar arquivo";
            $obj->title = "Erro ao anexar arquivo";
        }
        return new JsonResponse($obj);
    }

    /**
     * Download de Arquivo
     * @param Request $request
     * @param FosArquivoPessoal $frigaArquivo
     * @return BinaryFileResponse
     */
    public function downloadArquivoAction(Request $request, FosArquivoPessoal $arquivo)
    {
        $response = new BinaryFileResponse($arquivo->getPath());
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        return $response;
    }

    /**
     * Capturar Imagem de perfil
     *
     * @param Request $request
     * @param $hash
     */
    public function getImagemPerfilAction(Request $request, Usuario $usuario)
    {
        $response = new BinaryFileResponse($usuario->getArquivoImagemPerfilPath());
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        return $response;

    }
}
