<?php

namespace Nte\UsuarioBundle\Controller;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\CoordenadorTemSolicitante;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Curso;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\UsuarioCurso;
use Nte\Aplicacao\CadastrosBundle\Entity\FrigaEdital;
use Nte\Aplicacao\CadastrosBundle\Entity\FrigaPessoa;
use Nte\Aplicacao\CadastrosBundle\Entity\FrigaRecurso;
use Nte\UsuarioBundle\Entity\Usuario;
use Nte\UsuarioBundle\Form\PrimeiroAcessoType;
use Nte\UsuarioBundle\Form\Type\SelfEditType;
use Nte\UsuarioBundle\Form\Type\SolicitanteEditType;
use Nte\UsuarioBundle\Form\UsuarioComumType;
use Nte\UsuarioBundle\Form\UsuarioType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;

class UsuarioController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function indexAction(Request $request)
    {
        if (!$this->isGranted(['ROLE_ADMIN']))
        {
            $lastRoute = $request->server->get('HTTP_REFERER');

            if ($lastRoute === null) {
                return $this->redirect($this->generateUrl('fos_user_security_login'));
            } else {
                $this->addFlash('danger', "Permissão negada!");
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }

        $em = $this->getDoctrine()->getManager();
        $u = $em->getRepository(Usuario::class)->findby([], ['nome' => 'asc']);
        return $this->render('NteUsuarioBundle:Usuario:Usuario.html.twig', ['usuarios' => $u]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function listaAction(Request $request)
    {
        if (!$this->isGranted(['ROLE_ADMIN']))
        {
            $lastRoute = $request->server->get('HTTP_REFERER');

            if ($lastRoute === null) {
                return $this->redirect($this->generateUrl('fos_user_security_login'));
            } else {
                $this->addFlash('danger', "Permissão negada!");
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }

        $em = $this->getDoctrine()->getManager();
        $u = $em->getRepository(Usuario::class)
            ->findby(['enabled' => 1], ['lastLogin' => 'desc']);
        return $this->render('NteUsuarioBundle:Usuario:lista.html.twig', ['usuarios' => $u]);
    }

    /**
     * Visualizar Usuário
     * @param $id
     * @return Response
     */
    public function perfilAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        if ($id == null)
        {
            $id = $this->getUser()->getId();
        }

        $usuario = $em->find(Usuario::class, $id);
        $cursos = $usuario->getCursosNomeInArray();

        return $this->render('@NteUsuario/Usuario/perfil.html.twig', [
            'usuario' => $usuario,
            'cursos' => $cursos
        ]);
    }

    /**
     * Editar Usuário
     * @param Request $request
     * @param Usuario $usuario
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function ativarAction(Request $request, Usuario $usuario)
    {
        if ($this->isGranted('ROLE_ADMIN_USER')) {
            $usuario->setEnabled(!$usuario->isEnabled());
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();
            return $this->redirect($this->generateUrl('nte_usuario_index'));
        } else {
            return $this->redirectToRoute('nte_usuario_perfil', ['id' => $usuario->getId()]);
        }
    }

    /**
     * Editar Usuário
     * @param Request $request
     * @param Usuario $usuario
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
//    public function editarAction(Request $request, Usuario $usuario)
//    {
//        $em = $this->getDoctrine()->getManager();
//        $form = $this->createForm(SolicitanteEditType::class, $usuario, [
//                'entity_manager' => $em,
//            ]);
//            $form->handleRequest($request);
//
//            if ($form->isValid() && $form->isSubmitted()) {
//                $usuario->setPlainPassword($form->get('plainPassword')->getData());
//                $usuario->setEnabled(TRUE);
//                $em->persist($usuario);
//                $em->flush();
//                return $this->redirect($this->generateUrl('nte_usuario_index'));
//            }
//
//            return $this->render('NteUsuarioBundle:Usuario:form.html.twig', array(
//                'entity' => $usuario,
//                'form' => $form->createView(),
//            ));
//    }


    public function gerenciarUsuariosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $obj = new \stdClass();
        $obj->paginaTotal = 1;
        $obj->paginaAtual = $request->query->get('p') ?? 1;
        $obj->paginaItens = $request->query->get('r')??10;
        $obj->paginacao = [];
        $obj->itens = [];
        $obj->itensTotal = 0;
        $obj->ordem = $request->query->get('ord') ?? false;
        $obj->busca0 = $request->query->get('b0') ?? false;
        $obj->busca1 = $request->query->get('b1') ?? false;


        /** @var QueryBuilder $usuario */
        $usuario = $em->createQueryBuilder();
        $usuario->select('u')->from(Usuario::class, 'u');

        if ($obj->ordem) {
            $usuario->orderBy('u.nome', $obj->ordem == "DESC" ? "DESC" : "ASC");
        } else {
            $usuario->orderBy('u.nome', 'ASC');

        }
        if ($obj->busca0) {
            $obj->busca0 = str_replace(" ", "%",$obj->busca0);
            $usuario
                ->andWhere('LOWER(u.nome) LIKE  :busca or LOWER(u.email) LIKE  :busca')
                ->setParameter('busca', "%" . strtolower($obj->busca0) . "%");
            $obj->busca0 = str_replace("%", " ",$obj->busca0);
        }
        if ($obj->busca1) {
            $usuario
                ->andWhere('LOWER(u.nome) LIKE  :busca')
                ->setParameter('busca', strtolower($obj->busca1) . "%");
        }

        $obj->paginacao = new Paginator($usuario->getQuery());
        $obj->itensTotal = $obj->paginacao->count();
        $obj->itensInicio = $obj->paginaItens * ($obj->paginaAtual - 1);
        $obj->paginaTotal = ceil($obj->paginacao->count() / $obj->paginaItens);
        $obj->itens = new ArrayCollection($obj->paginacao
            ->getQuery()
            ->setFirstResult($obj->itensInicio)
            ->setMaxResults($obj->paginaItens)
            ->getResult());

        return $this->render('@NteUsuario/Usuario/lista-usuarios.html.twig', [
            'dataset' => $obj,
        ]);
    }

    public function editOthersAsAdminAction(Request $request, Usuario $usuario)
    {
        $em = $this->getDoctrine()->getManager();
        $usrLogadoId = $this->getUser()->getId();
        $usrLogado = $this->getUser();
        $editUsuario = $usuario;

        $form = $this->createForm(UsuarioType::class, $editUsuario, [
            'entity_manager' => $em,
            'usrId' => $usrLogadoId
        ]);

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted())
        {
            $editUsuario->setPlainPassword($form->get('plainPassword')->getData());
            $editUsuario->setEnabled(TRUE);
            $editUsuario->setEnderecoBairro('teste');
            $em->persist($editUsuario);
            $em->flush();

            return $this->redirect($this->generateUrl('nte_usuario_index'));
        }
        return $this->render('NteUsuarioBundle:Usuario:form.html.twig', [
                'entity' => $editUsuario,
                'form' => $form->createView(),
                'usrLogado' => $usrLogado
            ]
        );
    }


    public function editarAction(Request $request, Usuario $usuario)
    {

        if (!$this->isGranted(['ROLE_ADMIN']))
        {
            $lastRoute = $request->server->get('HTTP_REFERER');

            if ($lastRoute === null) {
                return $this->redirect($this->generateUrl('fos_user_security_login'));
            } else {
                $this->addFlash('danger', "Permissão negada!");
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }

        $em = $this->getDoctrine()->getManager();
        $usrLogadoId = $this->getUser()->getId();
        $usrLogado = $this->getUser();
        $editUsuario = $usuario;

        $form = $this->createForm(SelfEditType::class, $editUsuario, [
            'entity_manager' => $em,
            'usrId' => $usrLogadoId
        ]);

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted())
        {
            $editUsuario->setPlainPassword($form->get('plainPassword')->getData());
            $editUsuario->setEnabled(TRUE);
            $editUsuario->setEnderecoBairro('teste');
            $em->persist($editUsuario);
            $em->flush();

            return $this->redirect($this->generateUrl('nte_usuario_index'));
        }
        return $this->render('NteUsuarioBundle:Usuario:form.html.twig', [
                'entity' => $editUsuario,
                'form' => $form->createView(),
                'usrLogado' => $usrLogado
            ]
        );
    }

    /**
     * Editar Usuário
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editarPrimeiroAcessoAction(Request $request)
    {
        $usuario = $this->getUser();
        $form = $this->createForm(PrimeiroAcessoType::class, $usuario);
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $usuario->setPlainPassword($form->get('plainPassword')->getData());
            $usuario->setEnabled(TRUE);
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();
            return $this->redirect($this->generateUrl('nte_admin_painel_homepage'));
        }
        return $this->render('NteUsuarioBundle:Usuario:form-primeiro-acesso.html.twig', array(
            'entity' => $usuario,
            'form' => $form->createView(),
        ));
    }


    public function novoAction(Request $request)
    {

        if (!$this->isGranted(['ROLE_ADMIN']))
        {
            $lastRoute = $request->server->get('HTTP_REFERER');

            if ($lastRoute === null) {
                return $this->redirect($this->generateUrl('fos_user_security_login'));
            } else {
                $this->addFlash('danger', "Permissão negada!");
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }

        $em = $this->getDoctrine()->getManager();
        $usrLogadoId = $this->getUser()->getId();
        $usrLogado = $this->getUser();
        $novoUsuario = new Usuario();
        $form = $this->createForm(UsuarioType::class, $novoUsuario, [
            'entity_manager' => $em,
            'usrId' => $usrLogadoId
        ]);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted())
        {
            $novoUsuario->setPlainPassword($form->get('plainPassword')->getData());
            $novoUsuario->setEnabled(TRUE);
            $selectedRole = $form->get('categoria')->getData();

            switch ($selectedRole)
            {
                case "Administrador do Sistema":
                    $novoUsuario->setCategoria("Administrador do Sistema");
                    $novoUsuario->addRole("ROLE_ADMIN");
                    break;
                case "Avaliador":
                    $novoUsuario->setCategoria("Avaliador");
                    $novoUsuario->addRole("ROLE_ADMINISTRATIVO");
                    break;
                case "Coordenador":
                    $novoUsuario->setCategoria("Coordenador");
                    $novoUsuario->addRole("ROLE_COORDENADOR");
                    break;
                case "Solicitante":
                    $novoUsuario->setCategoria("Solicitante");
                    $novoUsuario->addRole("ROLE_SOLICITANTE");
                    break;
            }

            $em->persist($novoUsuario);
            $em->flush();

            $cursosSelecionados = $form->get('cursos')->getData();

            foreach ($cursosSelecionados as $curso) {
                $cursoEntity = $em->getRepository(Curso::class)->findBy(['nomeCurso' => $curso]);
                $usuarioCurso = new UsuarioCurso();
                $usuarioCurso->setIdUsuario($novoUsuario);
                $usuarioCurso->setIdCurso($cursoEntity[0]);
                $em->persist($usuarioCurso);
                $em->flush();
            }

            return $this->redirect($this->generateUrl('nte_usuario_index'));
        }
        return $this->render('NteUsuarioBundle:Usuario:form.html.twig', [
                'entity' => $novoUsuario,
                'form' => $form->createView(),
                'usrLogado' => $usrLogado
            ]
        );
    }

    private function createEditForm(Usuario $entity)
    {
        $form = $this->createForm(RegistrationFormType::class, $entity, [
            'action' => $this->generateUrl('nte_usuario_update', ['id' => $entity->getId()]),
            'method' => 'PUT',
            'attr' => ['class' => 'form-horizontal']
        ]);
        return $form;
    }


}