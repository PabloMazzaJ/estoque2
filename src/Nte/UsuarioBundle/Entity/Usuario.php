<?php

namespace Nte\UsuarioBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Curso;


/**
 * Usuario
 *
 * @ORM\Table(name="fos_usuario")
 * @ORM\Entity(repositoryClass="Nte\UsuarioBundle\Entity\Repository\UsuarioRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Usuario extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="img", type="string", length=255, nullable=true)
     */
    protected $img = 'assets/img/default_user.png';

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=200, nullable=true)
     */
    private $nome;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_nascimento", type="date", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var string
     *
     * @ORM\Column(name="cpf", type="string", length=15, nullable=true)
     */
    private $cpf;

    /**
     * @var string
     *
     * @ORM\Column(name="rg_nro", type="string", length=25, nullable=true)
     */
    private $rgNro;

    /**
     * @var string
     *
     * @ORM\Column(name="rg_orgao_expedidor", type="string", length=25, nullable=true)
     */
    private $rgOrgaoExpedidor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rg_data_expedicao", type="date", nullable=true)
     */
    private $rgDataExpedicao;

    /**
     * @var string
     *
     * @ORM\Column(name="contato_telefone1", type="string", length=45, nullable=true)
     */
    private $contatoTelefone1;

    /**
     * @var string
     *
     * @ORM\Column(name="contato_telefone2", type="string", length=45, nullable=true)
     */
    private $contatoTelefone2;

    /**
     * @var string
     *
     * @ORM\Column(name="contato_email", type="string", length=100, nullable=true)
     */
    private $contatoEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco_cep", type="string", length=45, nullable=true)
     */
    private $enderecoCep;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco_bairro", type="string", length=200, nullable=true)
     */
    private $enderecoBairro;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco_logradouro", type="string", length=200, nullable=true)
     */
    private $enderecoLogradouro;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco_numero", type="string", length=45, nullable=true)
     */
    private $enderecoNumero;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco_complemento", type="string", length=200, nullable=true)
     */
    private $enderecoComplemento;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco_municipio", type="string", length=200, nullable=true)
     */
    private $enderecoMunicipio;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco_uf", type="string", length=2, nullable=true)
     */
    private $enderecoUf;

    /**
     * @ORM\OneToMany(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Requisicao", mappedBy="idUsuario")
     */
    private $idRequisicao;

    /**
     * @ORM\OneToMany(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\UsuarioCurso", mappedBy="idUsuario")
     */
    private $usuarioCurso;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=100, nullable=true)
     */
    private $categoria;

    /**
     * Usuario constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->roles = ['ROLE_USER'];
        $this->idRequisicao = new ArrayCollection();
        $this->usuarioCurso = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return Usuario
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     *
     * @return Usuario
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataNascimento()
    {
        return $this->dataNascimento;
    }

    /**
     * @param \DateTime $dataNascimento
     *
     * @return Usuario
     */
    public function setDataNascimento($dataNascimento)
    {
        $this->dataNascimento = $dataNascimento;

        return $this;
    }

    /**
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param string $cpf
     *
     * @return Usuario
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;

        return $this;
    }

    /**
     * @return string
     */
    public function getRgNro()
    {
        return $this->rgNro;
    }

    /**
     * @param string $rgNro
     *
     * @return Usuario
     */
    public function setRgNro($rgNro)
    {
        $this->rgNro = $rgNro;

        return $this;
    }

    /**
     * @return string
     */
    public function getRgOrgaoExpedidor()
    {
        return $this->rgOrgaoExpedidor;
    }

    /**
     * @param string $rgOrgaoExpedidor
     *
     * @return Usuario
     */
    public function setRgOrgaoExpedidor($rgOrgaoExpedidor)
    {
        $this->rgOrgaoExpedidor = $rgOrgaoExpedidor;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRgDataExpedicao()
    {
        return $this->rgDataExpedicao;
    }

    /**
     * @param \DateTime $rgDataExpedicao
     *
     * @return Usuario
     */
    public function setRgDataExpedicao($rgDataExpedicao)
    {
        $this->rgDataExpedicao = $rgDataExpedicao;

        return $this;
    }

    /**
     * @return string
     */
    public function getContatoTelefone1()
    {
        return $this->contatoTelefone1;
    }

    /**
     * @param string $contatoTelefone1
     *
     * @return Usuario
     */
    public function setContatoTelefone1($contatoTelefone1)
    {
        $this->contatoTelefone1 = $contatoTelefone1;

        return $this;
    }

    /**
     * @return string
     */
    public function getContatoTelefone2()
    {
        return $this->contatoTelefone2;
    }

    /**
     * @param string $contatoTelefone2
     *
     * @return Usuario
     */
    public function setContatoTelefone2($contatoTelefone2)
    {
        $this->contatoTelefone2 = $contatoTelefone2;

        return $this;
    }

    /**
     * @return string
     */
    public function getContatoEmail()
    {
        return $this->contatoEmail;
    }

    /**
     * @param string $contatoEmail
     *
     * @return Usuario
     */
    public function setContatoEmail($contatoEmail)
    {
        $this->contatoEmail = $contatoEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getEnderecoCep()
    {
        return $this->enderecoCep;
    }

    /**
     * @param string $enderecoCep
     *
     * @return Usuario
     */
    public function setEnderecoCep($enderecoCep)
    {
        $this->enderecoCep = $enderecoCep;

        return $this;
    }

    /**
     * @return string
     */
    public function getEnderecoBairro()
    {
        return $this->enderecoBairro;
    }

    /**
     * @param string $enderecoBairro
     *
     * @return Usuario
     */
    public function setEnderecoBairro($enderecoBairro)
    {
        $this->enderecoBairro = $enderecoBairro;

        return $this;
    }

    /**
     * @return string
     */
    public function getEnderecoLogradouro()
    {
        return $this->enderecoLogradouro;
    }

    /**
     * @param string $enderecoLogradouro
     *
     * @return Usuario
     */
    public function setEnderecoLogradouro($enderecoLogradouro)
    {
        $this->enderecoLogradouro = $enderecoLogradouro;

        return $this;
    }

    /**
     * @return string
     */
    public function getEnderecoNumero()
    {
        return $this->enderecoNumero;
    }

    /**
     * @param string $enderecoNumero
     *
     * @return Usuario
     */
    public function setEnderecoNumero($enderecoNumero)
    {
        $this->enderecoNumero = $enderecoNumero;

        return $this;
    }

    /**
     * @return string
     */
    public function getEnderecoComplemento()
    {
        return $this->enderecoComplemento;
    }

    /**
     * @param string $enderecoComplemento
     *
     * @return Usuario
     */
    public function setEnderecoComplemento($enderecoComplemento)
    {
        $this->enderecoComplemento = $enderecoComplemento;

        return $this;
    }

    /**
     * @return string
     */
    public function getEnderecoMunicipio()
    {
        return $this->enderecoMunicipio;
    }

    /**
     * @param string $enderecoMunicipio
     *
     * @return Usuario
     */
    public function setEnderecoMunicipio($enderecoMunicipio)
    {
        $this->enderecoMunicipio = $enderecoMunicipio;

        return $this;
    }

    /**
     * @return string
     */
    public function getEnderecoUf()
    {
        return $this->enderecoUf;
    }

    /**
     * @param string $enderecoUf
     *
     * @return Usuario
     */
    public function setEnderecoUf($enderecoUf)
    {
        $this->enderecoUf = $enderecoUf;

        return $this;
    }





    /**
     * @return array
     */
    public static function getRolesNames()
    {
        return [
            "Usuário Administrador" => "ROLE_ADMIN",
            "Usuário Gerente de Usuários" => "ROLE_ADMIN_USER",
            "Usuário Gerente de Edital" => "ROLE_ADMIN_EDITAL",
            "Usuário Suporte" => "ROLE_SUPORTE",
            "Usuário Gerencial" => "ROLE_IES",
            "Usuário Avaliador" => "ROLE_AVALIADOR",
            "Usuário Comum" => "ROLE_USER",
            "Usuário Bolsas" => "ROLE_CONFIGURA_CONTRATO"
        ];
    }

    /**
     * @return array
     */
    public static function getRoles2()
    {
        return [
            "ROLE_ADMIN" => "Usuário Administrador",
            "ROLE_ADMIN_USER" => "Usuário Gerente de Usuários",
            "ROLE_ADMIN_EDITAL" => "Usuário Gerente de Edital",
            "ROLE_IES" => "Usuário Gerencial",
            "ROLE_AVALIADOR" => "Usuário Avaliador",
            "ROLE_USER" => "Usuário Comum",
            "ROLE_SUPORTE" => "Usuário Suporte",
            "ROLE_CONFIGURA_CONTRATO" => "Usuário Bolsas"
        ];
    }

    public function getHighestRole()
    {
        $rolesSortedByImportance = ['ROLE_ADMIN', 'ROLE_COORDENADOR', 'ROLE_SOLICITANTE', 'ROLE_ADMINISTRATIVO'];
        foreach ($rolesSortedByImportance as $role) {
            if (in_array($role, $this->roles)) {
                return $role;
            }
        }
        return false;
    }

    public function getHighestRoleName()
    {
        return self::getRoles2()[$this->getHighestRole()];
    }

    /**
     * Set img
     * @param string $img
     * @return Usuario
     */
    public function setImg($img)
    {
        $this->img = $img;
        return $this;
    }

    /**
     * Get img
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function addRole($role)
    {

        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }
        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }
        return $this;
    }

    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles)
    {
        $this->roles = array();
        foreach ($roles as $role => $id) {
            $this->addRole($id->id);
        }
        return $this;
    }

    /**
     * Captura a imagem de perfil do usuario;
     * @return FrigaArquivo|null
     */
    public function getArquivoImagemPerfil()
    {
        /** @var FosArquivoPessoal $arquivo */
        foreach ($this->getIdArquivo() as $arquivo) {
            if ($arquivo->getTipo() == 0) {
                return $arquivo;
            }
        }
        return null;
    }

    /**
     * @return string
     */
    public function getArquivoImagemPerfilPath()
    {
        $arquivo = $this->getArquivoImagemPerfil();
        if ($arquivo) {
            return $arquivo->getPath();
        } else {
            return $this->getImg();
        }
    }

    /**
     * @return mixed
     */
    public function getIdRequisicao()
    {
        return $this->idRequisicao;
    }

    /**
     * @param mixed $idRequisicao
     */
    public function setIdRequisicao($idRequisicao)
    {
        $this->idRequisicao = $idRequisicao;
    }

    /**
     * @return mixed
     */
    public function getUsuarioCurso()
    {
        return $this->usuarioCurso;
    }

    /**
     * @param mixed $usuarioCurso
     */
    public function setUsuarioCurso($usuarioCurso)
    {
        $this->usuarioCurso = $usuarioCurso;
    }

    public function getCursosNomeInArray()
    {
        $cursos = $this->usuarioCurso;
        $nomes = [];

        foreach ($cursos as $curso)
        {
            $nomes[] = $curso->getIdCurso()->getNomeCurso();
        }

        return $nomes;
    }

    /**
     * @return string
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param string $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

}

