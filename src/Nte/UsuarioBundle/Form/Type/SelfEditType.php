<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 10/03/20
 * Time: 14:17
 */

namespace Nte\UsuarioBundle\Form\Type;

use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Curso;
use Nte\UsuarioBundle\Entity\Usuario;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SelfEditType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, ['label' => 'Usuário', 'attr' => ['placeholder' => 'Username']])
            ->add('email', EmailType::class, array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
            ->add('nome', TextType::class, ['label' => 'Nome Completo', 'attr' => ['placeholder' => 'Informe o nome completo']])
            ->add('cpf', TextType::class, [
                'label' => 'CPF',
                'attr' => [
                    'placeholder' => '000.000.000-00',
                    'data-rule-cpf'=>'true',
                ]
            ])
//            ->add('categoria', ChoiceType::class, [
//                'choices'  => [
//                    'Administrador do Sistema' => 'Administrador do Sistema',
//                    'Avaliador' => 'Avaliador',
//                    'Coordenador' => 'Coordenador',
//                    'Solicitante' => 'Solicitante'
//                ],
//                'mapped' => false,
//                'label' => 'Categoria',
//            ])
//            ->add('cursos', ChoiceType::class, [
//                'choices' => $this->cursosDoCoordenador($options),
//                'choice_label' => function ($choice, $key, $value) {
//                    return $value;
//                },
//                'mapped' => false,
//                'multiple' => true,
//                'attr' => ['style' => 'width:100%;', 'rows' => 10,],
//                'label' => 'Cursos',
//
//            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch'
            ])
        ;
    }

    public function cursosDoCoordenador($options)
    {

        /**
         * @var EntityManager $em
         */
        $em = $options['entity_manager'];
        $usrId = $options['usrId'];

        $usuario = $em->getRepository(Usuario::class)->find($usrId);

        //se usuário for admin, retorna array com todos os cursos, senão é porque é coordenador, então retorna todos os cursos que ele está vinculado
        $categoria = $usuario->getCategoria();

        if ($categoria != "Admin")
        {
            $arrayNomeCursos = [];

            foreach ($usuario->getUsuarioCurso() as $curso)
            {
                $arrayNomeCursos[] = $curso->getIdCurso()->getNomeCurso();
            }

        } else {

            $todosCursos = $em->getRepository(Curso::class)->findAll();

            foreach ($todosCursos as $curso)
            {
                $arrayNomeCursos[] = $curso->getNomeCurso();
            }
        }

        return $arrayNomeCursos;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Nte\UsuarioBundle\Entity\Usuario'
        ));

        $resolver->setRequired('usrId');
        $resolver->setRequired('entity_manager');
    }

    public function getBlockPrefix()
    {
        return 'usuarioEdit';
    }
}