<?php
namespace Nte\Aplicacao\Utils;

use Curl\Curl as BaseCurl;

// use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class Curl
 * @package Nte\Aplicacao
 */
class Curl extends BaseCurl
{
   // public $urlBase = "https://edge.cpd.ufsm.br/moodle-ws/webservice";
    public $urlBase = "https://portal.ufsm.br/moodle-ws/webservice";

    /**
     * Curl constructor.
     * @TODO Implementar captura de parametros globais
     */
    public function __construct()
    {
        parent::__construct();

        //     $container->getParameter('curl_user_agent');
        //$this->setUserAgent($this->getParameter('curl_user_agent'));
        $this->setUserAgent('NTE-FROM-HELL');
        $this->setHeader('Content-Type', 'application/json');
        $this->setHeader('X-UFSM-Device-ID', 'eadvm60');
    }
    /**
     * @param string $url pathname
     * @param array $data data
     * @param array $qs query string
     * @return BaseCurl
     */
    public function post($url, $data = [], $qs = [])
    {
        $queryString = null;
        if (!empty($qs)) {
            $queryString = '?';
            foreach ($qs as $parameter => $value) {
                $queryString .="$parameter=$value";
                $queryString .= $parameter === end($qs)?null:"&";
            }
        }
        $url = $this->urlBase . $url . $queryString;
        return parent::post($url, $data);
    }

    /**
     * Return php object
     * @return mixed
     */
    public function formatedResponse()
    {
        return json_decode($this->response);
    }
}