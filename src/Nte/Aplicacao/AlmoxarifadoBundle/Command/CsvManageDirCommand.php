<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 13/02/20
 * Time: 12:03
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Finder\Finder;


class CsvManageDirCommand extends ContainerAwareCommand
{
    /** @var EntityManager */
    private  $em;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:cleandatadir')
            ->setDescription('limpa diretório data (.csv produtos)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $io = new SymfonyStyle($input, $output);
        $io->title('limpando /Data...');
        $stopwatch = new Stopwatch();
        $stopwatch->start('eventName');

        $dataDir = 'src/Nte/Aplicacao/AlmoxarifadoBundle/Data';
        $finder = new Finder();
        $finder->files()->in($dataDir);
        $filesystem = new Filesystem();

        if ($finder->hasResults())
        {
            dump('has result, removendo o que existia');
            $filesystem->remove(['src/Nte/Aplicacao/AlmoxarifadoBundle/Data/produtos.csv']);

            foreach ($finder as $arquivo)
            {
                $arquivoPath = 'src/Nte/Aplicacao/AlmoxarifadoBundle/Data/produtos.csv' . $arquivo->getFilename();
//                $filesystem->remove(['src/Nte/Aplicacao/AlmoxarifadoBundle/Data/produtos.csv']);
                $filesystem->remove([$arquivoPath]);
            }
        }

        $io->progressFinish();
        $io->success('Fim do comando!');
        $event = $stopwatch->stop('app:cleandatadir');
        $timeTaken = $event->getDuration();
        dump('$timeTaken');
        dump($timeTaken);
    }
}