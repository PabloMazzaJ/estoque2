<?php

namespace Nte\Aplicacao\AlmoxarifadoBundle\Command;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Almoxarifado;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Item;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Produto;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class CsvImportCommand extends ContainerAwareCommand
{
    /** @var EntityManager */
    private  $em;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:importcsv')
            ->setDescription('importa arquivo csv');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws OptimisticLockException
     * @throws ORMException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $io = new SymfonyStyle($input, $output);
        $io->title('Iniciando importação de produtos..');
        $stopwatch = new Stopwatch();
        $stopwatch->start('app:importcsv');
        $csv = array_map('str_getcsv', file('src/Nte/Aplicacao/AlmoxarifadoBundle/Data/produtos.csv'));
        $io->progressStart(100);
        $lineSkip = 0;
        $localizacao = $csv[1][9];
        $checkLocalizacao = $em->getRepository(Almoxarifado::class)->findOneBy([
            'localizacao' => $localizacao
        ]);

        if (!$checkLocalizacao) {
            $almoxarifado = new Almoxarifado();
            $almoxarifado->setLocalizacao($localizacao);
            $em->persist($almoxarifado);
            $em->flush();
        } else {
            $almoxarifado = $em->find(Almoxarifado::class, 1);
        }

        foreach ($csv as $produto)
        {
            if ($lineSkip != 0) {

                isset($produto[0]) ? $idProduto = $produto[0] : $idProduto = "";
                isset($produto[1]) ? $codigoReduzido = $produto[1] : $codigoReduzido = "";
                isset($produto[2]) ? $produtoNome = $produto[2] : $produtoNome = "";
                isset($produto[6]) ? $descricaoProduto = $produto[6] : $descricaoProduto = "";

                if (isset($produto[7]))
                {
                    $valorUnit = str_replace([' ', '.'], '', $produto[7]);
                    $valorUnit = str_replace([','], '.', $valorUnit);
                }

                $checkProduto = $em->getRepository(Produto::class)->findOneBy([
                    'id' => $idProduto,
                ]);

                if (!$checkProduto)
                {
                    $produtoObj = new Produto();
                    $produtoObj->setIdProduto($idProduto);
                    $produtoObj->setNome($produtoNome);
                    $produtoObj->setDescricao($descricaoProduto);
                    $produtoObj->setCodReduzido($codigoReduzido);
                    $this->em->persist($produtoObj);
                    $this->em->flush();
                }

                $checkItem = $em->getRepository(Item::class)->findOneBy([
                    'produto' => $produtoObj,
                    'valorUnitario' => $valorUnit
                ]);

                if (!$checkItem)
                {
                    $item = new Item();
                    $item->setValorUnitario($valorUnit);
                    $item->setProduto($produtoObj);
                    $item->setAlmoxarifado($almoxarifado);
                    $em->persist($item);
                    $em->flush();
                }
            }
            $lineSkip++;
        }

        $io->progressFinish();
        $io->success('Dados importados com sucesso!');
        $event = $stopwatch->stop('app:importcsv');
        $timeTaken = $event->getDuration();
        dump('$timeTaken');
        dump($timeTaken);
    }
}
