<?php

namespace Nte\Aplicacao\AlmoxarifadoBundle\Controller;

use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Curso;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Requisicao;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\RequisicaoItem;
use Nte\UsuarioBundle\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;



class RequisicaoController extends Controller
{

    /**
     * @param $idCurso
     * @return float|int|null
     */
    public function calcularPedidos($idCurso)
    {
        $em = $this->getDoctrine()->getManager();
        $curso = $em->find(Curso::class, $idCurso);
        $totalPedidos = 0;
        $pedidos = $em->getRepository(Requisicao::class)->findAll();
        $totalPedidos = count($pedidos);

        $pedidosByCurso = $em->getRepository(Requisicao::class)->findBy(['curso' => $idCurso]);
        $pedidosDoCurso = count($pedidosByCurso);

        if ($totalPedidos != 0) {
            $percentage = ( $pedidosDoCurso / $totalPedidos ) * 100;
            return $percentage;
        }

        return null;
    }

    public function importarCsvProdutosAction(Request $request)
    {
        date_default_timezone_set("America/Sao_Paulo");
        $dataUpload = date('d/m/yy h:i:s a', time());
        $dateParts = explode(" " , $dataUpload);
        $diaUpload = $dateParts[0];
        $horarioUpload = $dateParts[1];

        $defaultData = ['message' => 'message'];
        $form = $this->createFormBuilder($defaultData)
            ->add('brochure', FileType::class, [
                'label' => 'Lista de Produtos (com extensão .csv)',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '15M',
//                        'mimeTypes' => [
//                            'text/csv',
//                            'text/plain'
//                        ],
//                        'mimeTypesMessage' => 'Por favor, faça o upload de um arquivo .CSV',
                    ])
                ],
            ])
            ->add('send', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            /** @var UploadedFile $listaProdutos */
            $listaProdutos = $form->get('brochure')->getData();

            $newFilename = 'produtos.csv';

            if ($listaProdutos)
            {
                try {
                    $listaProdutos->move(
                        $this->getParameter('produtos_csv_file'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    dump($e);
                    // flash + redirec to home..
                }
            }
        }

        return $this->render('@Almoxarifado/Requisicao/importa-csv-produtos.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function criarRequisicaoAction(Request $request)
    {
        if (!$this->isGranted(['ROLE_SOLICITANTE']))
        {
            $lastRoute = $request->server->get('HTTP_REFERER');

            if ($lastRoute === null) {
                return $this->redirect($this->generateUrl('fos_user_security_login'));
            } else {
                $this->addFlash('danger', "Permissão negada!");
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }

        $em = $this->getDoctrine()->getManager();

        $usuarioSolicitante = $this->getUser();
        $usuarioSolicitanteId = $this->getUser()->getId();
        $usuarioCursos = $this->getUser()->getUsuarioCurso();

        $cursosSolicitante = [];

        foreach ($usuarioCursos as $usuarioCurso) {
            $cursosSolicitante[] = $usuarioCurso->getIdCurso()->getNomeCurso();
        }

        $numeroDeCursos = sizeof($cursosSolicitante);
        $solicitanteEntity = $em->find(Usuario::class, $usuarioSolicitanteId);

        if ($numeroDeCursos == 1) {
            $almoxServiceContainer = $this->container->get('busca_cursos_solicitante');
            $cursosSolicitante = $almoxServiceContainer->buscaCursosUsuario($usuarioSolicitante);

            return $this->render('@Almoxarifado/Requisicao/criar-requisicao.html.twig', [
                'cursosSolicitante' => $cursosSolicitante,
                'quantidadeDeCursos' => $numeroDeCursos,
                'idSolicitante' => $usuarioSolicitante->getId(),
                'unicoCurso' => $cursosSolicitante[0],
            ]);

        } else {
            return $this->render('@Almoxarifado/Requisicao/criar-requisicao.html.twig', [
                'cursosSolicitante' => $cursosSolicitante,
                'quantidadeDeCursos' => $numeroDeCursos,
                'idSolicitante' => $usuarioSolicitante->getId(),
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function listarPedidosAdministrativoAction(Request $request)
    {
        if (!$this->isGranted(['ROLE_ADMINISTRATIVO']))
        {
            $lastRoute = $request->server->get('HTTP_REFERER');

            if ($lastRoute === null) {
                return $this->redirect($this->generateUrl('fos_user_security_login'));
            } else {
                $this->addFlash('danger', "Permissão negada!");
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }

        $em = $this->getDoctrine()->getManager();
        $pedidos = $em->getRepository(Requisicao::class)->getAllRequisicoesByReversedId();

        foreach ($pedidos as $requisicao)
        {
            $itens = $em->getRepository(RequisicaoItem::class)->findBy(['requisicao' => $requisicao]);

            $arrSituacoes = [];
            $numeroDeItens = sizeof($itens);
            $fullAprovado = $numeroDeItens * 1;
            $fullRejeitado = $numeroDeItens * 2;
            $pendente = false;
            $parcialmenteAprovado = false;
            $temPendente = false;

            foreach ($itens as $i)
            {
                $situacao = $i->getAvaliacaoItem();
                $arrSituacoes[] = $situacao;
                if ($situacao === 3) {
                    $pendente = true;
                    $temPendente = true;
                }
            }

            $arrSum = array_sum($arrSituacoes);

            if ($arrSum == $fullAprovado) {
                $requisicao->setStatusAvaliacao(1);
            } else if (($arrSum == $fullRejeitado) && ($temPendente == false)) {
                $requisicao->setStatusAvaliacao(2);
            } else if ($pendente == true) {
                $requisicao->setStatusAvaliacao(3);
            } else if (($pendente == false) && ($arrSum != $fullRejeitado) && ($arrSum != $fullRejeitado) ) {
                $requisicao->setStatusAvaliacao(4);
            }

            $em->persist($requisicao);
            $em->flush();
        }

        return $this->render('@Almoxarifado/Requisicao/lista-pedidos.html.twig', [
            'pedidos' => $pedidos
        ]);
    }

    /**
     * @param Request $request
     * @param Requisicao $pedido
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function mostrarDetalhesPedidoAction(Request $request, Requisicao $pedido)
    {
        if (!$this->isGranted(['ROLE_ADMINISTRATIVO']))
        {
            $lastRoute = $request->server->get('HTTP_REFERER');

            if ($lastRoute === null) {
                return $this->redirect($this->generateUrl('fos_user_security_login'));
            } else {
                $this->addFlash('danger', "Permissão negada!");
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }

        $em = $this->getDoctrine()->getManager();
        $p = $em->find(Requisicao::class, $pedido);
        $itens = $em->getRepository(RequisicaoItem::class)->getItensFromRequisicao($p);
        //obs solicitante
        $observacao = $pedido->getObservacao();
        //obs adm
        $observacaoAdm = $pedido->getObservacaoAdm();
        $pedidoId = $pedido->getId();
        $solicitante = $p->getIdUsuario();
        $curso = $p->getCurso();

        foreach ($itens as $item) {

            $quantidade = $item->getQuantidade();
            $valorUnitario = $item->getValorUnitario();
            $valorTotal = $quantidade * $valorUnitario;
            $delimiter = ".";
            $explodeTotal = explode($delimiter, $valorTotal);
            $explodeUnitario = explode($delimiter, $valorUnitario);

            if (sizeof($explodeUnitario) === 1)
            {
                $explodeUnitario = $explodeTotal[0] . '.00';
            }

            if (sizeof($explodeTotal) === 1)
            {
                $valorTotal = $explodeTotal[0] . '.00';
            }

            if (sizeof($explodeTotal) > 1) {

                if (strlen( $explodeTotal[1] ) === 1)
                {
                    $valorTotal = $explodeTotal[0] . $delimiter . $explodeTotal[1] . '0';
                }
            }

            if (is_array($explodeUnitario)) {
                if (sizeof($explodeUnitario) > 1) {

                    if (strlen( $explodeUnitario[1] ) === 1)
                    {
                        $valorUnitario = $explodeUnitario[0] . $delimiter . $explodeUnitario[1] . '0';
                    }
                }
            } else {
                $valorUnitario = $valorUnitario . $delimiter . '00';
            }

            $listaArr[] = [
                "id" => $item->getId(),
                "produtoNome" => $item->getNomeProduto(),
                "produtoCodReduzido" => $item->getProduto()->getCodReduzido(),
                "quantidade" => $quantidade,
                "valorUnitario" => $valorUnitario,
                "valorTotal" => $valorTotal,
                "motivo" => $item->getMotivo()
            ];
        }

        return $this->render('@Almoxarifado/Requisicao/detalhes-do-pedido.html.twig', [
            'itens' => $listaArr,
            'observacao' => $observacao,
            'observacaoAdm' => $observacaoAdm,
            'pedidoId' => $pedidoId,
            'solicitante' => $solicitante,
            'curso' => $curso
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function listarPedidosDoSolicitanteAction(Request $request)
    {
//        if ( !$this->isGranted(['ROLE_SOLICITANTE']) || !$this->isGranted(['ROLE_COORDENADOR'])  )
//        {
//            $lastRoute = $request->server->get('HTTP_REFERER');
//
//            if ($lastRoute === null) {
//                return $this->redirect($this->generateUrl('fos_user_security_login'));
//            } else {
//                $this->addFlash('danger', "Permissão negada!");
//                return $this->redirect($request->server->get('HTTP_REFERER'));
//            }
//        }


        if ($this->isGranted(['ROLE_SOLICITANTE']) || $this->isGranted(['ROLE_COORDENADOR'])  || $this->isGranted(['ROLE_ADMIN']))
        {
            $em = $this->getDoctrine()->getManager();
            $solicitanteLogado = $this->getUser();
            $solicitanteLogadoId = $solicitanteLogado->getId();
            $almoxServiceContainer = $this->container->get('busca_cursos_solicitante');
            $cursosSolicitante = $almoxServiceContainer->buscaCursosUsuario($solicitanteLogado);
            $cursosSolicitanteIds = [];

            foreach ($cursosSolicitante as $curso) {
                $cursosSolicitanteIds[] = $curso["idCurso"];
            }

            $meusPedidos = $em->getRepository(Requisicao::class)->findBy(['curso' => $cursosSolicitanteIds], ['id' => 'DESC']);

            foreach ($meusPedidos as $requisicao)
            {
                $itens = $em->getRepository(RequisicaoItem::class)->findBy(['requisicao' => $requisicao]);

                $arrSituacoes = [];
                $numeroDeItens = sizeof($itens);
                $fullAprovado = $numeroDeItens * 1;
                $fullRejeitado = $numeroDeItens * 2;
                $pendente = false;
                $parcialmenteAprovado = false;
                $temPendente = false;

                foreach ($itens as $i)
                {
                    $situacao = $i->getAvaliacaoItem();
                    $arrSituacoes[] = $situacao;
                    if ($situacao === 3) {
                        $pendente = true;
                        $temPendente = true;
                    }
                }

                $arrSum = array_sum($arrSituacoes);

                if ($arrSum == $fullAprovado) {
                    $requisicao->setStatusAvaliacao(1);
                } else if (($arrSum == $fullRejeitado) && ($temPendente == false)) {
                    $requisicao->setStatusAvaliacao(2);
                } else if ($pendente == true) {
                    $requisicao->setStatusAvaliacao(3);
                } else if (($pendente == false) && ($arrSum != $fullRejeitado) && ($arrSum != $fullRejeitado) ) {
                    $requisicao->setStatusAvaliacao(4);
                }

                $em->persist($requisicao);
                $em->flush();
            }

            return $this->render('@Almoxarifado/Requisicao/lista-pedidos-curso.html.twig', [
                'pedidos' => $meusPedidos
            ]);
        } else {
            $lastRoute = $request->server->get('HTTP_REFERER');

            if ($lastRoute === null) {
                return $this->redirect($this->generateUrl('fos_user_security_login'));
            } else {
                $this->addFlash('danger', "Permissão negada!");
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }


    }

    /**
     * @param Request $request
     * @param Requisicao $pedido
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function mostrarMeusPedidosDetalhesAction(Request $request, Requisicao $pedido)
    {
        if (!$this->isGranted(['ROLE_SOLICITANTE']) || !$this->isGranted(['ROLE_COORDENADOR']) )
        {
            $this->addFlash('danger', "Permissão negada!");
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }

        $em = $this->getDoctrine()->getManager();
        $p = $em->find(Requisicao::class, $pedido);
        $itens = $em->getRepository(RequisicaoItem::class)->getItensFromRequisicao($p);

        //obs solicitante
        $observacao = $pedido->getObservacao();

        //obs adm
        $observacaoAdm = $pedido->getObservacaoAdm();

        $pedidoId = $pedido->getId();
        $arrItens = [];

        foreach ($itens as $item)
        {
            $quantidade = $item->getQuantidade();
            $valorUnitario = $item->getValorUnitario();
            $valorTotal = $quantidade * $valorUnitario;
            $delimiter = ".";
            $explodeTotal = explode($delimiter, $valorTotal);
            $explodeUnitario = explode($delimiter, $valorUnitario);

            if (sizeof($explodeUnitario) === 1)
            {
                $explodeUnitario = $explodeTotal[0] . '.00';
            }

            if (sizeof($explodeTotal) === 1)
            {
                $valorTotal = $explodeTotal[0] . '.00';
            }

            if (sizeof($explodeTotal) > 1) {

                if (strlen( $explodeTotal[1] ) === 1)
                {
                    $valorTotal = $explodeTotal[0] . $delimiter . $explodeTotal[1] . '0';
                }
            }

            if (is_array($explodeUnitario)) {
                if (sizeof($explodeUnitario) > 1) {

                    if (strlen( $explodeUnitario[1] ) === 1)
                    {
                        $valorUnitario = $explodeUnitario[0] . $delimiter . $explodeUnitario[1] . '0';
                    }
                }
            } else {
                $valorUnitario = $valorUnitario . $delimiter . '00';
            }

            $listaArr[] = [
                "id" => $item->getId(),
                "produtoNome" => $item->getNomeProduto(),
                "produtoCodReduzido" => $item->getProduto()->getCodReduzido(),
                "quantidade" => $quantidade,
                "valorUnitario" => $valorUnitario,
                "valorTotal" => $valorTotal,
                "motivo" => $item->getMotivo(),
                "avaliacao" => $item->getAvaliacaoItem()
            ];
        }

        return $this->render('@Almoxarifado/Requisicao/detalhes-pedido-curso.html.twig', [
            'itens' => $listaArr,
            'observacao' => $observacao,
            'observacaoAdm' => $observacaoAdm,
            'pedidoId' => $pedidoId
        ]);
    }
}