<?php

namespace Nte\Aplicacao\AlmoxarifadoBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LogBigBlueButtonController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function logGraphAction(Request $request)
    {
        /**
         * 0 -> não há upload
         * 1 -> não há feedback no log
         * 2 -> apresenta log
         */
        $feedbackStatus = 0;
        $ratingsCounter = 0;

        if ($request->getMethod() == "POST")
        {
            $feedbackStatus = 1;
            $files = $request->files->all();

            if (isset($files))
            {
                foreach ($files as $file)
                {
                    foreach ($file as $f)
                    {
                        $my_file = fopen($f, "rw");
                        while (! feof ($my_file))
                            $linhas[] = fgets($my_file);
                        fclose($my_file);
                    }
                }
            }

            if ($files !== null && isset($linhas))
            {
                //browserCounter
                $mozillaCounter = 0;
                $chromeCounter = 0;
                $edgeCounter = 0;
                $safariCounter = 0;

                //horaCounter
                $counterI = 0;
                $counterII = 0;
                $counterIII = 0;
                $counterIV = 0;
                $counterV = 0;
                $counterVI = 0;
                $counterVII = 0;
                $counterVIII = 0;
                $counterIX = 0;
                $counterX = 0;
                $counterXI = 0;
                $counterXII = 0;
                $counterXIII = 0;
                $counterXIV = 0;
                $counterXV = 0;
                $counterXVI = 0;
                $counterXVII = 0;
                $counterXVIII = 0;
                $counterXIX = 0;
                $counterXX = 0;
                $counterXXI = 0;
                $counterXXII = 0;
                $counterXXIII = 0;

                foreach ($linhas as $linhaDecodificada)
                {
                    $linhaDecodificada = preg_replace_callback("(\\\\x([0-9a-f]{2}))i", function($a) {
                        return chr(hexdec($a[1]));}, $linhaDecodificada
                    );

                    $split = explode('[', $linhaDecodificada);

                    if (isset($split[0]))
                    {
                        $ip = $split[0];
                    }

                    if (isset($split[1]))
                    {
                        $data = substr($split[1], 0, -2);
                    }

                    if (isset($split[2]))
                    {
                        $json = $split[2];
                    }

                    if (isset($json))
                    {
                        $jsonData = explode(',', $json);

                        foreach ($jsonData as $jsonDataLine)
                        {
                            $logCodeLine = $this->contains_substr($jsonDataLine, '"logCode":', $loc = false);
                            if ($logCodeLine)
                            {
                                $findLogCode = substr($jsonDataLine, strpos($jsonDataLine, ":") + 1);
                                $logCode = str_replace('"', '', $findLogCode);
                            }

                            $ratingLine = $this->contains_substr($jsonDataLine, '"rating":', $loc = false);
                            if ($ratingLine)
                            {
                                $findRating = substr($jsonDataLine, strpos($jsonDataLine, ":") + 1);
                                $ratingFeedback = str_replace('"', '', $findRating);
                                $rating = substr($ratingFeedback, -1);
                            }

                            $userAgentLine = $this->contains_substr($jsonDataLine, '"userAgent":', $loc = false);

                            if (isset($logCode))
                            {
                                if ($logCode === "feedback_functionality" && $userAgentLine)
                                {
                                    //browser
                                    $userAgentAux = substr($json, strpos($json, "userAgent") + 12);
                                    $userAgentFormat = substr($userAgentAux, 0, -14);
                                    $userAgentData = $this->getBrowser($userAgentFormat);
                                    $browser = $userAgentData["name"];

                                    //time
                                    $horaFilter = substr($data, strpos($data, "T") + 1);
                                    $explodeHora = explode(':', $horaFilter);
                                    $hora = $explodeHora[0];

                                    switch ($hora)
                                    {
                                        case "01":
                                            $counterI++;
                                            $ratingsI[] = $rating;
                                            break;

                                        case "02":
                                            $counterII++;
                                            $ratingsII[] = $rating;
                                            break;

                                        case "03":
                                            $counterIII++;
                                            $ratingsIII[] = $rating;
                                            break;

                                        case "04":
                                            $counterIV++;
                                            $ratingsIV[] = $rating;
                                            break;

                                        case "05":
                                            $counterV++;
                                            $ratingsV[] = $rating;
                                            break;

                                        case "06":
                                            $counterVI++;
                                            $ratingsVI[] = $rating;
                                            break;

                                        case "07":
                                            $counterVII++;
                                            $ratingsVII[] = $rating;
                                            break;

                                        case "08":
                                            $counterVIII++;
                                            $ratingsVIII[] = $rating;
                                            break;

                                        case "09":
                                            $counterIX++;
                                            $ratingsIX[] = $rating;
                                            break;

                                        case "10":
                                            $counterX++;
                                            $ratingsX[] = $rating;
                                            break;

                                        case "11":
                                            $counterXI++;
                                            $ratingsXI[] = $rating;
                                            break;

                                        case "12":
                                            $counterXII++;
                                            $ratingsXII[] = $rating;
                                            break;

                                        case "13":
                                            $counterXIII++;
                                            $ratingsXIII[] = $rating;
                                            break;

                                        case "14":
                                            $counterXIV++;
                                            $ratingsXIV[] = $rating;
                                            break;

                                        case "15":
                                            $counterXV++;
                                            $ratingsXV[] = $rating;
                                            break;

                                        case "16":
                                            $counterXVI++;
                                            $ratingsXVI[] = $rating;
                                            break;

                                        case "17":
                                            $counterXVII++;
                                            $ratingsXVII[] = $rating;
                                            break;

                                        case "18":
                                            $counterXVIII++;
                                            $ratingsXVIII[] = $rating;
                                            break;

                                        case "19":
                                            $counterXIX++;
                                            $ratingsXIX[] = $rating;
                                            break;

                                        case "20":
                                            $counterXX++;
                                            $ratingsXX[] = $rating;
                                            break;

                                        case "21":
                                            $counterXXI++;
                                            $ratingsXXI[] = $rating;
                                            break;

                                        case "22":
                                            $counterXXII++;
                                            $ratingsXXII[] = $rating;
                                            break;

                                        case "23":
                                            $counterXXIII++;
                                            $ratingsXXIII[] = $rating;
                                            break;

                                        default:
                                            throw new Exception('não encontrou hora');
                                    }

                                    switch ($browser)
                                    {
                                        case "Google Chrome":
                                            $chromeCounter++;
                                            $ratingsChrome[] = $rating;
                                            $ratingsCounter++;
                                            break;

                                        case "Apple Safari":
                                            $safariCounter++;
                                            $ratingsSafari[] = $rating;
                                            $ratingsCounter++;
                                            break;

                                        case "Microsoft Edge":
                                            $edgeCounter++;
                                            $ratingsEdge[] = $rating;
                                            $ratingsCounter++;
                                            break;

                                        case "Mozilla Firefox":
                                            $mozillaCounter++;
                                            $ratingsMozilla[] = $rating;
                                            $ratingsCounter++;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                //foreach end
                }

                if (isset($ratingsChrome))
                {
                    $chromeRatings = array_filter($ratingsChrome);
                    if(count($chromeRatings)) {
                        $chromeAverage = array_sum($chromeRatings)/count($chromeRatings);
                    }
                }

                if (isset($ratingsSafari))
                {
                    $safariRatings = array_filter($ratingsSafari);
                    if(count($safariRatings)) {
                        $safariAverage = array_sum($safariRatings)/count($safariRatings);
                    }
                }

                if (isset($ratingsEdge))
                {
                    $edgeRatings = array_filter($ratingsEdge);
                    if(count($edgeRatings)) {
                        $edgeAverage = array_sum($edgeRatings)/count($edgeRatings);
                    }
                }

                if (isset($ratingsMozilla))
                {
                    $mozillaRatingsAverage = array_filter($ratingsMozilla);
                    if(count($mozillaRatingsAverage)) {
                        $mozillaAverage = array_sum($mozillaRatingsAverage)/count($mozillaRatingsAverage);
                    }
                }

                $browserDataList = [
                    "mozilla" => $mozillaCounter,
                    "chrome" => $chromeCounter,
                    "edge" => $edgeCounter,
                    "safari" => $safariCounter,
                ];

                if (isset($ratingsI))
                {
                    $IRatings = array_filter($ratingsI);
                    if(count($IRatings)) {
                        $IAverage = array_sum($IRatings)/count($IRatings);
                        $arrayHoras[1] = $IAverage;
                    }
                }

                if (isset($ratingsII))
                {
                    $IIRatings = array_filter($ratingsII);
                    if(count($IIRatings)) {
                        $IIAverage = array_sum($IIRatings)/count($IIRatings);
                        $arrayHoras[2] = $IIAverage;
                    }
                }

                if (isset($ratingsIII))
                {
                    $IIIRatings = array_filter($ratingsIII);
                    if(count($IIIRatings)) {
                        $IIIAverage = array_sum($IIIRatings)/count($IIIRatings);
                        $arrayHoras[3] = $IIIAverage;
                    }
                }

                if (isset($ratingsIV))
                {
                    $IVRatings = array_filter($ratingsIV);
                    if(count($IVRatings)) {
                        $IVAverage = array_sum($IVRatings)/count($IVRatings);
                        $arrayHoras[4] = $IVAverage;
                    }
                }

                if (isset($ratingsV))
                {
                    $VRatings = array_filter($ratingsV);
                    if(count($VRatings)) {
                        $VAverage = array_sum($VRatings)/count($VRatings);
                        $arrayHoras[5] = $VAverage;
                    }
                }

                if (isset($ratingsVI))
                {
                    $VIRatings = array_filter($ratingsVI);
                    if(count($VIRatings)) {
                        $VIAverage = array_sum($VIRatings)/count($VIRatings);
                        $arrayHoras[7] = $VIAverage;
                    }
                }

                if (isset($ratingsVII))
                {
                    $VIIRatings = array_filter($ratingsVII);
                    if(count($VIIRatings)) {
                        $VIIAverage = array_sum($VIIRatings)/count($VIIRatings);
                        $arrayHoras[7] = $VIIAverage;
                    }
                }

                if (isset($ratingsVIII))
                {
                    $VIIIRatings = array_filter($ratingsVIII);
                    if(count($VIIIRatings)) {
                        $VIIIAverage = array_sum($VIIIRatings)/count($VIIIRatings);
                        $arrayHoras[8] = $VIIIAverage;
                    }
                }

                if (isset($ratingsIX))
                {
                    $IXRatings = array_filter($ratingsIX);
                    if(count($IXRatings)) {
                        $IXAverage = array_sum($IXRatings)/count($IXRatings);
                        $arrayHoras[9] = $IXAverage;
                    }
                }

                if (isset($ratingsX))
                {
                    $XRatings = array_filter($ratingsX);
                    if(count($XRatings)) {
                        $XAverage = array_sum($XRatings)/count($XRatings);
                        $arrayHoras[10] = $XAverage;
                    }
                }

                if (isset($ratingsXI))
                {
                    $XIRatings = array_filter($ratingsXI);
                    if(count($XIRatings)) {
                        $XIAverage = array_sum($XIRatings)/count($XIRatings);
                        $arrayHoras[11] = $XIAverage;
                    }
                }

                if (isset($ratingsXII))
                {
                    $XIIRatings = array_filter($ratingsXII);
                    if(count($XIIRatings)) {
                        $XIIAverage = array_sum($XIIRatings)/count($XIIRatings);
                        $arrayHoras[12] = $XIIAverage;
                    }
                }

                if (isset($ratingsXIII))
                {
                    $XIIIRatings = array_filter($ratingsXIII);
                    if(count($XIIIRatings)) {
                        $XIIIAverage = array_sum($XIIIRatings)/count($XIIIRatings);
                        $arrayHoras[13] = $XIIIAverage;
                    }
                }

                if (isset($ratingsXIV))
                {
                    $XIVRatings = array_filter($ratingsXIV);
                    if(count($XIVRatings)) {
                        $XIVAverage = array_sum($XIVRatings)/count($XIVRatings);
                        $arrayHoras[14] = $XIVAverage;
                    }
                }

                if (isset($ratingsXV))
                {
                    $XVRatings = array_filter($ratingsXV);
                    if(count($XVRatings)) {
                        $XVAverage = array_sum($XVRatings)/count($XVRatings);
                        $arrayHoras[15] = $XVAverage;
                    }
                }

                if (isset($ratingsXVI))
                {
                    $XVIRatings = array_filter($ratingsXVI);
                    if(count($XVIRatings)) {
                        $XVIAverage = array_sum($XVIRatings)/count($XVIRatings);
                        $arrayHoras[16] = $XVIAverage;
                    }
                }

                if (isset($ratingsXVII))
                {
                    $XVIIRatings = array_filter($ratingsXVII);
                    if(count($XVIIRatings)) {
                        $XVIIAverage = array_sum($XVIIRatings)/count($XVIIRatings);
                        $arrayHoras[17] = $XVIIAverage;
                    }
                }

                if (isset($ratingsXVIII))
                {
                    $XVIIIRatings = array_filter($ratingsXVIII);
                    if(count($XVIIIRatings)) {
                        $XVIIIAverage = array_sum($XVIIIRatings)/count($XVIIIRatings);
                        $arrayHoras[18] = $XVIIIAverage;
                    }
                }

                if (isset($ratingsXIX))
                {
                    $XIXRatings = array_filter($ratingsXIX);
                    if(count($XIXRatings)) {
                        $XIXAverage = array_sum($XIXRatings)/count($XIXRatings);
                        $arrayHoras[19] = $XIXAverage;
                    }
                }

                if (isset($ratingsXX))
                {
                    $XXRatings = array_filter($ratingsXX);
                    if(count($XXRatings)) {
                        $XXAverage = array_sum($XXRatings)/count($XXRatings);
                        $arrayHoras[20] = $XXAverage;
                    }
                }

                if (isset($ratingsXXI))
                {
                    $XXIRatings = array_filter($ratingsXXI);
                    if(count($XXIRatings)) {
                        $XXIAverage = array_sum($XXIRatings)/count($XXIRatings);
                        $arrayHoras[21] = $XXIAverage;
                    }
                }

                if (isset($ratingsXXII))
                {
                    $XXIIRatings = array_filter($ratingsXXII);
                    if(count($XXIIRatings)) {
                        $XXIIAverage = array_sum($XXIIRatings)/count($XXIIRatings);
                        $arrayHoras[22] = $XXIIAverage;
                    }
                }

                if (isset($ratingsXXIII))
                {
                    $XXIIIRatings = array_filter($ratingsXXIII);
                    if(count($XXIIIRatings)) {
                        $XXIIIAverage = array_sum($XXIIIRatings)/count($XXIIIRatings);
                        $arrayHoras[23] = $XXIIIAverage;
                    }
                }

                if (isset($arrayHoras))
                {
                    foreach ($arrayHoras as $key => $value)
                    {
                        $arrayHorasGraph[] = [$key, $value];
                    }
                }

                if (isset($browserDataList))
                {
                    foreach ($browserDataList as $key => $value)
                    {
                        $qtdBrowsers[] = [ $key, $value ];
                    }

                    dump('$qtdBrowsers');
                    dump($qtdBrowsers);
                    dump('$arrayHorasGraph');
                    dump($arrayHorasGraph);
                }

                if (isset($browserDataList))
                {
                    if (!isset($chromeAverage)) $chromeAverage = 0;
                    if (!isset($mozillaAverage)) $mozillaAverage = 0;
                    if (!isset($edgeAverage)) $edgeAverage = 0;
                    if (!isset($safariAverage)) $safariAverage = 0;

                    $twigParameters = [
                        'chromeRatingAverage' => number_format((float)$chromeAverage, 2, '.', ''),
                        'mozillaRatingAverage' => number_format((float)$mozillaAverage, 2, '.', ''),
                        'edgeRatingAverage' => number_format((float)$edgeAverage, 2, '.', ''),
                        'safariRatingAverage' => number_format((float)$safariAverage, 2, '.', ''),
                        'data' => $browserDataList,
                        'feedbackStatus' => $feedbackStatus,
                        'qtdBrowsers' => $qtdBrowsers,
                        'ratingsCounter' => $ratingsCounter,
                        'arrayHoras' => $arrayHoras,
                        'arrayHorasGraph' => $arrayHorasGraph
                    ];

                    return $this->render('@Almoxarifado/LogBigBlueButton/graph.html.twig', $twigParameters);
                }
            }
        }

        if (!isset($chromeAverage)) $chromeAverage = 0;
        if (!isset($mozillaAverage)) $mozillaAverage = 0;
        if (!isset($edgeAverage)) $edgeAverage = 0;
        if (!isset($safariAverage)) $safariAverage = 0;

        if (isset($arrayHoras)) {
            $twigParameters["arrayHoras"] = $arrayHoras;
        }

        if (isset($qtdBrowsers)) {
            $twigParameters["qtdBrowsers"] = $qtdBrowsers;
        }

        $twigParameters = [
            'feedbackStatus' => $feedbackStatus,
            'chromeRatingAverage' => number_format((float)$chromeAverage, 2, '.', ''),
            'mozillaRatingAverage' => number_format((float)$mozillaAverage, 2, '.', ''),
            'edgeRatingAverage' => number_format((float)$edgeAverage, 2, '.', ''),
            'safariRatingAverage' => number_format((float)$safariAverage, 2, '.', ''),
            'ratingsCounter' => $ratingsCounter,
        ];

        return $this->render('@Almoxarifado/LogBigBlueButton/graph.html.twig', $twigParameters
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function fileUploadHandlerAction(Request $request)
    {
        /**
         * 0 -> não há upload
         * 1 -> não há feedback no log
         * 2 -> apresenta log
         */
        $feedbackStatus = 0;

        if ($request->getMethod() == "POST")
        {
            $feedbackStatus = 1;
            $files = $request->files->all();

            if (isset($files))
            {
                foreach ($files as $file)
                {
                    foreach ($file as $f)
                    {
                        $my_file = fopen($f, "rw");
                        while (! feof ($my_file))
                            $linhas[] = fgets($my_file);
                        fclose($my_file);
                    }
                }
            }

            if ($files !== null && isset($linhas))
            {
                foreach ($linhas as $linhaDecodificada)
                {
                    $linhaDecodificada = preg_replace_callback("(\\\\x([0-9a-f]{2}))i", function($a) {
                        return chr(hexdec($a[1]));}, $linhaDecodificada
                    );
                    $split = explode('[', $linhaDecodificada);

                    if (isset($split[0]))
                    {
                        $ip = $split[0];
                    }

                    if (isset($split[1]))
                    {
                        $data = substr($split[1], 0, -2);
                    }

                    if (isset($split[2]))
                    {
                        $json = $split[2];
                    }

                    if (isset($json))
                    {
                        $jsonData = explode(',', $json);

                        //tratando dados json
                        foreach ($jsonData as $jsonDataLine)
                        {
                            //acha logcode:
                            $logCodeLine = $this->contains_substr($jsonDataLine, '"logCode":', $loc = false);
                            if ($logCodeLine)
                            {
                                $findLogCode = substr($jsonDataLine, strpos($jsonDataLine, ":") + 1);
                                $logCode = str_replace('"', '', $findLogCode);
                            }

                            $fullnameLine = $this->contains_substr($jsonDataLine, '"fullname":', $loc = false);
                            if ($fullnameLine)
                            {
                                $findFullname = substr($jsonDataLine, strpos($jsonDataLine, ":") + 1);
                                $fullname = str_replace('"', '', $findFullname);
                            }

                            $confnameLine = $this->contains_substr($jsonDataLine, '"confname":', $loc = false);
                            if ($confnameLine)
                            {
                                $findConfname = substr($jsonDataLine, strpos($jsonDataLine, ":") + 1);
                                $confname = str_replace('"', '', $findConfname);
                            }

                            $userAgentLine = $this->contains_substr($jsonDataLine, '"userAgent":', $loc = false);
                            if ($userAgentLine)
                            {
                                $findUserAgent = substr($jsonDataLine, strpos($jsonDataLine, ":") + 1);
                                $userAgent = str_replace('"', '', $findUserAgent);
                            }

                            $timeLine = $this->contains_substr($jsonDataLine, '"time":', $loc = false);
                            if ($timeLine)
                            {
                                $findTime = substr($jsonDataLine, strpos($jsonDataLine, ":") + 1);
                                $time = str_replace('"', '', $findTime);
                            }

                            $meetingIdLine = $this->contains_substr($jsonDataLine, '"meetingId":', $loc = false);
                            if ($meetingIdLine)
                            {
                                $findMeetingId = substr($jsonDataLine, strpos($jsonDataLine, ":") + 1);
                                $meetingId = str_replace('"', '', $findMeetingId);
                            }

                            $ratingLine = $this->contains_substr($jsonDataLine, '"rating":', $loc = false);
                            if ($ratingLine)
                            {
                                $findRating = substr($jsonDataLine, strpos($jsonDataLine, ":") + 1);
                                $ratingFeedback = str_replace('"', '', $findRating);
                                $rating = substr($ratingFeedback, -1);
                            }

                            $commentLine = $this->contains_substr($jsonDataLine, '"comment":', $loc = false);
                            if ($commentLine)
                            {
                                $findComment = substr($jsonDataLine, strpos($jsonDataLine, ":") + 1);
                                $comment = str_replace('"', '', $findComment);
                            }

                            $externUserIDLine = $this->contains_substr($jsonDataLine, '"externUserID":', $loc = false);
                            if ($externUserIDLine)
                            {
                                $findexternUserID = substr($jsonDataLine, strpos($jsonDataLine, ":") + 1);
                                $externUserID = str_replace('"', '', $findexternUserID);
                            }

                            $userRoleLine = $this->contains_substr($jsonDataLine, '"userRole":', $loc = false);
                            if ($userRoleLine)
                            {
                                $finduserRoleLine = substr($jsonDataLine, strpos($jsonDataLine, ":") + 1);
                                $userRole = str_replace('"', '', $finduserRoleLine);
                                $userRoleFormat = substr($userRole, 0, -2);
                            }
                        }

                        if (!isset($fullname) || $fullname === null || $fullname === ' ')
                        {
                            $fullname = 'campo vazio';
                        }

                        if (!isset($confname) || $confname === null || $confname === ' ')
                        {
                            $confname = 'campo vazio';
                        }

                        if (!isset($userAgent) || $userAgent === null || $userAgent === ' ')
                        {
                            $userAgent = 'campo vazio';
                        }

                        if (!isset($time) || $time === null || $time === ' ')
                        {
                            $time = 'campo vazio';
                        }

                        if (!isset($meetingId) || $meetingId === null || $meetingId === ' ')
                        {
                            $meetingId = 'campo vazio';
                        }

                        //só vai mostrar se o logcode for feedback_functionality
                        if (isset($logCode))
                        {
                            $feedbackStatus = 2;
                            if ($logCode === "feedback_functionality")
                            {
                                $jsonDataList[] = [
                                    "ip" => $ip,
                                    "data" => $data,
                                    "fullname" => $fullname,
                                    "externUserID" => $externUserID,
                                    "confname" => $confname,
                                    "userAgent" => $userAgent,
                                    "time" => $time,
                                    "userRole" => $userRoleFormat,
                                    "meetingId" => $meetingId,
                                    "rating" => $rating,
                                    "comment" => $comment
                                ];
                            }
                        }
                    }
                }

                if (isset($jsonDataList))
                {
                    return $this->render('@Almoxarifado/LogBigBlueButton/show.html.twig', ['data' => $jsonDataList, 'feedbackStatus' => $feedbackStatus]);
                }
            }
        }

        return $this->render('@Almoxarifado/LogBigBlueButton/show.html.twig', ['feedbackStatus' => $feedbackStatus]);
    }

    function getBrowser($userAgent){
        $u_agent = $userAgent;
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";

        if (preg_match('/linux|android/i', $u_agent)) {
            $platform = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        }
        elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        if (preg_match('/android/i', $u_agent)) {
            $bname = 'Android';
            $ub = "Android";
        }
        elseif(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        }
        elseif(preg_match('/Windows NT 10/i',$u_agent) && preg_match('/Edge/i',$u_agent)){
            $bname = 'Microsoft Edge';
            $ub = "Edge";
        }
        elseif(preg_match('/Firefox/i',$u_agent))
        {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        }
        elseif(preg_match('/Chrome/i',$u_agent))
        {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        }
        elseif(preg_match('/Safari/i',$u_agent))
        {
            $bname = 'Apple Safari';
            $ub = "Safari";
        }
        elseif(preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Opera';
            $ub = "Opera";
        }
        elseif(preg_match('/Netscape/i',$u_agent))
        {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        $i = count($matches['browser']);
        if ($i != 1) {

            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            }
            else {
                $version= $matches['version'][1];
            }
        }
        else {
            $version= $matches['version'][0];
        }

        if ($version==null || $version=="") {$version="?";}

        //echo "Your browser: " . $browser['name'] . " " . $browser['version'] . " on " .$browser['platform'] . " reports: <br >" . $browser['userAgent'];

        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern
        );
    }


    function contains_substr($mainStr, $str, $loc = false)
    {
        if ($loc === false) return (strpos($mainStr, $str) !== false);
        if (strlen($mainStr) < strlen($str)) return false;
        if (($loc + strlen($str)) > strlen($mainStr)) return false;
        return (strcmp(substr($mainStr, $loc, strlen($str)), $str) == 0);
    }
}
