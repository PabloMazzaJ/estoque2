<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 13/12/19
 * Time: 16:26
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Controller;

use DateTime;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Curso;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Requisicao;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\RequisicaoItem;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RelatoriosController
 * @package Nte\Aplicacao\AlmoxarifadoBundle\Controller
 */
class RelatoriosController extends Controller
{
    /**
     * @return RedirectResponse|Response
     *
     * Lista de cursos EAD para visualização de relatório individual
     *
     */
    public function menuRelatorioCursosAction()
    {
        if (!$this->isGranted(['ROLE_ADMINISTRATIVO']))
        {
            $lastRoute = $request->server->get('HTTP_REFERER');

            if ($lastRoute === null) {
                return $this->redirect($this->generateUrl('fos_user_security_login'));
            } else {
                $this->addFlash('danger', "Permissão negada!");
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }

        $em = $this->getDoctrine()->getManager();
        $cursos = $em->getRepository(Curso::class)->getNomeCursosOrderedAsc();

        return $this->render('@Almoxarifado/Relatorios/menu-relatorio-cursos.html.twig', [
            'cursos' => $cursos,
        ]);
    }

    /**
     * @param Request $request
     * @param $startDate
     * @param $endDate
     * @return Response
     *
     * Apresenta relatórios (número de pedidos e custo) de todos os cursos EAD
     *
     */
    public function mainRelatoriosGeraisAction(Request $request, $startDate, $endDate)
    {
        $lastRoute = $request->server->get('HTTP_REFERER');

        if (!$this->isGranted(['ROLE_ADMINISTRATIVO']))
        {
            if ($lastRoute === null) {
                return $this->redirect($this->generateUrl('fos_user_security_login'));
            } else {
                $this->addFlash('danger', "Permissão negada!");
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }

        if ($startDate == null && $endDate == null)
        {
            $endDate = date("d-m-Y");
            $newDate = explode('-', $endDate);
            $inicioAno = $newDate[2];
            $startDate = "01" . "-" . "01" . "-" . $inicioAno;
        }

        $em = $this->getDoctrine()->getManager();
        $cursos = $em->getRepository(Curso::class)->findAll();
        $inicio = date_create_from_format('d-m-Y', $startDate);
        $fim = date_create_from_format('d-m-Y', $endDate);

        /**
         * chart data - pedidos realizados e custo
         */
        $somatorioCustoTodosCursos = 0;
        $somatorioTotalDePedidosRealizados = 0;

        foreach ($cursos as $curso)
        {
            $pedidosDoCurso = $em->getRepository(Requisicao::class)->getRequisicoesBetweenAux($curso, $inicio, $fim);
            $nomeDoCurso = $curso->getNomeCurso();

            $qtdDePedidosDoCursoNoPeriodo = $this->calcularQuantidadePedidosNoPeriodo($curso, $inicio, $fim);
            $custoTotalDoCursoNoPeriodo = $this->calcularGastoTotalDoCursoNoPeriodo($curso, $startDate, $endDate);

            $pedidosCursos[] = [
                $nomeDoCurso,
                $qtdDePedidosDoCursoNoPeriodo
            ];

            $custoCursos[] = [
                $nomeDoCurso,
                $custoTotalDoCursoNoPeriodo
            ];

            $somatorioTotalDePedidosRealizados = $somatorioTotalDePedidosRealizados + $qtdDePedidosDoCursoNoPeriodo;
            $somatorioCustoTodosCursos = $somatorioCustoTodosCursos + $custoTotalDoCursoNoPeriodo;
        }

        /**
         * Situação dos pedidos (status_avaliacao)
         * 4 - parcialmente aprovado
         * 3 - pendente
         * 2 - rejeitado
         * 1 - aprovado
         */

        $total = 0;
        $aprovados = 0;
        $parcialmenteAprovados = 0;
        $pendentes = 0;
        $rejeitados = 0;

        $pedidos = $em->getRepository(Requisicao::class)->getRequisicoesBetweenPeriodFromAllCourses($inicio, $fim);

        foreach ($pedidos as $pedido)
        {
           $total++;
           $status = $pedido->getStatusAvaliacao();

            switch ($status) {
                case 1:
                    $aprovados++;
                    break;
                case 2:
                    $rejeitados++;
                    break;
                case 3:
                    $pendentes++;
                    break;
                case 4:
                    $parcialmenteAprovados++;
                    break;
            }
        }

        $startDateTitle = str_replace('-', '/', $startDate);
        $endDateTitle = str_replace('-', '/', $endDate);

        if ($somatorioCustoTodosCursos === 0 || $somatorioTotalDePedidosRealizados === 0 ) {
            $chartDataExists = 0;
        } else {
            $chartDataExists = 1;
        }

        $twigParameters = [
            'pedidosCursos' => $pedidosCursos,
            'custoCursos' => $custoCursos,
            'somatorioPedidosRealizados' => $somatorioTotalDePedidosRealizados,
            'aprovados' => $aprovados,
            'aprovadosParcialmente' => $parcialmenteAprovados,
            'rejeitados' => $rejeitados,
            'pendentes' => $pendentes,
            'somatorioCustoTodosCursos' => $somatorioCustoTodosCursos,
            'startDate' => $startDateTitle,
            'endDate' => $endDateTitle,
            'chartDataExists' => $chartDataExists,
        ];

        if ($lastRoute != null) {
            $twigParameters['lastRoute'] = $lastRoute;
        } else {
            $twigParameters['lastRoute'] = "";
        }

        return $this->render('@Almoxarifado/Relatorios/main-relatorios-globais.html.twig', $twigParameters);
    }

    /**
     * @param Request $request
     * @param Curso $curso
     * @param $startDate
     * @param $endDate
     * @return Response
     */
    public function relatorioDoCursoAction(Request $request, Curso $curso, $startDate, $endDate)
    {
        $lastRoute = $request->server->get('HTTP_REFERER');
        $chartDataExists = true;

        if (!$this->isGranted(['ROLE_ADMINISTRATIVO']))
        {
            if ($lastRoute === null) {
                return $this->redirect($this->generateUrl('fos_user_security_login'));
            } else {
                $this->addFlash('danger', "Permissão negada!");
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }

        $routeName = $request->get('_route');
        $routePath = $this->get('router')->getRouteCollection()->get($routeName)->getPath();

        if ($startDate == null && $endDate == null)
        {
            $endDate = date("d-m-Y");
            $newDate = explode('-', $endDate);
            $inicioAno = $newDate[2];
            $startDate = "01" . "-" . "01" . "-" . $inicioAno;
        }

        $em = $this->getDoctrine()->getManager();
        $inicio = date_create_from_format('d-m-Y', $startDate);
        $fim = date_create_from_format('d-m-Y', $endDate);
        $cursoEnt = $em->find(Curso::class, $curso);
        $nomeCurso = $cursoEnt->getNomeCurso();

        $custoTotal = $this->calcularGastoTotalDoCursoNoPeriodo($curso->getId(), $startDate, $endDate);

        $totalPedidos = 0;
        $aprovados = 0;
        $parcialmenteAprovados = 0;
        $pendentes = 0;
        $rejeitados = 0;

        $pedidos = $em->getRepository(Requisicao::class)->getRequisicoesBetweenAux($cursoEnt, $inicio, $fim);

        foreach ($pedidos as $pedido)
        {
            $totalPedidos++;
            $status = $pedido->getStatusAvaliacao();

            switch ($status) {
                case 1:
                    $aprovados++;
                    break;
                case 2:
                    $rejeitados++;
                    break;
                case 3:
                    $pendentes++;
                    break;
                case 4:
                    $parcialmenteAprovados++;
                    break;
            }
        }

        $arrGastoMensal = $this->cursoRelatorioGastoMensalAction($curso, $startDate, $endDate);
        $gastoMensalMeses = $arrGastoMensal[0][0];
        $gastoMensalReais = $arrGastoMensal[0][1];

        $arrItensMaisPedidos = $this->cursoRelatorioItensMaisPedidosAction($curso, $startDate, $endDate);
        $itensMaisPedidosProduto = $arrItensMaisPedidos[0][1];
        $itensMaisPedidosQuantidade = $arrItensMaisPedidos[0][0];

        $startDateTitle = str_replace('-', '/', $startDate);
        $endDateTitle = str_replace('-', '/', $endDate);

        if ($arrGastoMensal === false || $custoTotal === false) {
            $chartDataExists = 0;
        } else {
            $chartDataExists = 1;
        }

        $twigParameters = [
            'endDate' => $endDateTitle,
            'startDate' => $startDateTitle,
            'lastRoute' => $lastRoute,
            'idCurso' => $curso->getId(),
            'nomeCurso' => $nomeCurso,
            'pedidos' => $totalPedidos,
            'custo' => $custoTotal,
            'aprovados' => $aprovados,
            'aprovadosParcialmente' => $parcialmenteAprovados,
            'rejeitados' => $rejeitados,
            'pendentes' => $pendentes,
            'gastoMensalMeses' => $gastoMensalMeses,
            'gastoMensalReais' => $gastoMensalReais,
            'itensMaisPedidosProduto' => $itensMaisPedidosProduto,
            'itensMaisPedidosQuantidade' => $itensMaisPedidosQuantidade,
            'chartDataExists' => $chartDataExists,
            'routePath' => $routePath
        ];

        return $this->render('@Almoxarifado/Relatorios/relatorio-do-curso.html.twig', $twigParameters);
    }

    /**
     * @param Curso $curso
     * @param $startDate
     * @param $endDate
     * @return array|bool
     */
    public function cursoRelatorioGastoMensalAction(Curso $curso, $startDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();
        $cursoEnt = $em->find(Curso::class, $curso);
        $nomeCurso = $cursoEnt->getNomeCurso();

        /**
         * Se não houver data inicial
         */
        if ($startDate === null || $startDate === "")
        {
            $endDate = date("d-m-Y");
            $startDate = date("d-m-Y", strtotime('-1 year'));
        }

        $inicio = date_create_from_format('d-m-Y', $startDate);
        $fim = date_create_from_format('d-m-Y', $endDate);

        $pedidosEmDeterminadoPeriodo = $em->getRepository(Requisicao::class)->getRequisicoesBetweenAux($curso, $inicio, $fim);

        foreach ($pedidosEmDeterminadoPeriodo as $pedido)
        {
            $valorTotalFormatado = number_format((float)$pedido->getValorTotalPedido(), 2, '.', '');
            $c = new stdClass();
            $mes = $pedido->getRegistroDataCriacao()->format('M');

            switch ($mes) {
                case "jan":
                    $mes = "Janeiro";
                    break;

                case "Feb":
                    $mes = "Fevereiro";
                    break;

                case "Mar":
                    $mes = "Março";
                    break;

                case "Apr":
                    $mes = "Abril";
                    break;

                case "May":
                    $mes = "Maio";
                    break;

                case "Jun":
                    $mes = "Junho";
                    break;

                case "Jul":
                    $mes = "Julho";
                    break;

                case "Aug":
                    $mes = "Agosto";
                    break;

                case "Sep":
                    $mes = "Setembro";
                    break;

                case "Oct":
                    $mes = "Outubro";
                    break;

                case "Nov":
                    $mes = "Novembro";
                    break;

                case "Dec":
                    $mes = "Dezembro";
                    break;
            }

            $ano = $pedido->getRegistroDataCriacao()->format('Y');
            $date = $ano . ' - ' . $mes;

            if (isset($custoMeses))
            {
                foreach ($custoMeses as $chartMes)
                {
                    //se a data já existir, update valor.
                    if ($chartMes->name === $date) {

                        $valorExistenteNaData = array_column($custoMeses, 'low');
                        $valorSomatorio = $valorTotalFormatado + $valorExistenteNaData[0];
                        $chartMes->low = $valorSomatorio;

                    } else {

                        $custoMeses[$date] = (object) array(
                            'name' => $date,
                            'low' => (double) $valorTotalFormatado
                        );
                    }
                }

            } else {
                $custoMeses[] = (object) array(
                    'name' => $date,
                    'low' => (double) $valorTotalFormatado
                );
            }
        }

        if (isset($custoMeses)) {
            foreach ($custoMeses as $f)
            {
                $newCustoMeses[] = (object) array(
                    'name' => $f->name,
                    'low' => $f->low
                );
            }
        }

        if (!isset($newCustoMeses))
        {
            $val = false;
        } else {

            foreach ($newCustoMeses as $cada)
            {
                $chartDataMonths[] = $cada->name;
                $chartDataReais[] = $cada->low;
            }

            $val[] = [$chartDataMonths, $chartDataReais];
        }

        return $val;
    }

    /**
     * @param Curso $curso
     * @param $startDate
     * @param $endDate
     * @return array|bool
     */
    public function cursoRelatorioItensMaisPedidosAction(Curso $curso, $startDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();
        $arrayQuantidadeProdutos = [];
        $cursoEnt = $em->find(Curso::class, $curso);
        $nomeCurso = $cursoEnt->getNomeCurso();

        if ($startDate === null || $startDate === "")
        {
            $startDate = date("d-m-Y");
            $endDate = date("d-m-Y", strtotime('-1 year'));
        }

        $inicio = date_create_from_format('d-m-Y', $startDate);
        $fim = date_create_from_format('d-m-Y', $endDate);

        $pedidosDoCurso = $em->getRepository(Requisicao::class)->getRequisicoesBetweenAux($curso, $inicio, $fim);

        foreach ($pedidosDoCurso as $pedido)
        {
            $itensDoPedido = $pedido->getItens();

            foreach ($itensDoPedido as $item)
            {
                if (array_key_exists($item->getProduto()->getNome(), $arrayQuantidadeProdutos))
                {
                    $valorExistente = $arrayQuantidadeProdutos[$item->getProduto()->getNome()];
                    $somatorio = $valorExistente["quantidade"] + $item->getQuantidade();

                    $arrayQuantidadeProdutos[$item->getProduto()->getNome()] = [
                        'quantidade' => $somatorio
                    ];

                } else {
                    $arrayQuantidadeProdutos[$item->getProduto()->getNome()] = [
                        'quantidade' => $item->getQuantidade()
                    ];
                }
            }
        }

        foreach ($arrayQuantidadeProdutos as $key => $item) {

            $nomeProduto = $key;
            $quantidadeProduto = array_values($item)[0];

            $produtosDataPieChart[] = (object) array(
                'name' => $nomeProduto,
                'low' => $quantidadeProduto
            );
       }

        if (!isset($produtosDataPieChart))
        {
           $val = false;
        } else {

            foreach ($produtosDataPieChart as $cada)
            {
                $chartDataQuantidade[] = $cada->name;
                $chartDataProdutos[] = $cada->low;
            }

            $val[] = [$chartDataProdutos, $chartDataQuantidade];
        }

        return $val;
    }

    /**
     * @param Request $request
     * @param $ano
     * @return Response
     */
    public function tabelaGastoAnualAction(Request $request, $ano)
    {
        if (!$this->isGranted(['ROLE_ADMINISTRATIVO']))
        {
            $lastRoute = $request->server->get('HTTP_REFERER');

            if ($lastRoute === null) {
                return $this->redirect($this->generateUrl('fos_user_security_login'));
            } else {
                $this->addFlash('danger', "Permissão negada!");
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }

        if ($ano === null) $ano = strval(2020);

        $em = $this->getDoctrine()->getManager();
        $cursos = $em->getRepository(Curso::class)->getNomeCursosOrderedAsc();

        $meses[] = ['nome' => 'Janeiro'];
        $meses[] = ['nome' => 'Fevereiro'];
        $meses[] = ['nome' => 'Março'];
        $meses[] = ['nome' => 'Abril'];
        $meses[] = ['nome' => 'Maio'];
        $meses[] = ['nome' => 'Junho'];
        $meses[] = ['nome' => 'Julho'];
        $meses[] = ['nome' => 'Agosto'];
        $meses[] = ['nome' => 'Setembro'];
        $meses[] = ['nome' => 'Outubro'];
        $meses[] = ['nome' => 'Novembro'];
        $meses[] = ['nome' => 'Dezembro'];

        foreach ($cursos as $curso)
        {
            $pegaTodasRequisicoesDeUmCurso = $em->getRepository(Requisicao::class)->findBy(['curso' => $curso]);

            foreach ($pegaTodasRequisicoesDeUmCurso as $req)
            {
                //Janeiro
                if ($req->getregistroDataCriacao()->format('Y') === $ano && $req->getregistroDataCriacao()->format('M') === "Jan")
                {
                    $requisicoesJaneiro[] = [
                        'idCurso' => $req->getCurso(),
                        'idRequisicao' => $req->getId()
                    ];
                }

                //Fevereiro
                if ($req->getregistroDataCriacao()->format('Y') === $ano && $req->getregistroDataCriacao()->format('M') === "Feb")
                {
                    $requisicoesFevereiro[] = [
                        'idCurso' => $req->getCurso(),
                        'idRequisicao' => $req->getId()
                    ];
                }

                //Março
                if ($req->getregistroDataCriacao()->format('Y') === $ano && $req->getregistroDataCriacao()->format('M') === "Mar")
                {
                    $requisicoesMarco[] = [
                        'idCurso' => $req->getCurso(),
                        'idRequisicao' => $req->getId()
                    ];
                }

                //Abril
                if ($req->getregistroDataCriacao()->format('Y') === $ano && $req->getregistroDataCriacao()->format('M') === "Apr")
                {
                    $requisicoesAbril[] = [
                        'idCurso' => $req->getCurso(),
                        'idRequisicao' => $req->getId()
                    ];
                }

                //Maio
                if ($req->getregistroDataCriacao()->format('Y') === $ano && $req->getregistroDataCriacao()->format('M') === "May")
                {
                    $requisicoesMaio[] = [
                        'idCurso' => $req->getCurso(),
                        'idRequisicao' => $req->getId()
                    ];
                }

                //Junho
                if ($req->getregistroDataCriacao()->format('Y') === $ano && $req->getregistroDataCriacao()->format('M') === "Jun")
                {
                    $requisicoesJunho[] = [
                        'idCurso' => $req->getCurso(),
                        'idRequisicao' => $req->getId()
                    ];
                }

                //Julho
                if ($req->getregistroDataCriacao()->format('Y') === $ano && $req->getregistroDataCriacao()->format('M') === "Jul")
                {
                    $requisicoesJulho[] = [
                        'idCurso' => $req->getCurso(),
                        'idRequisicao' => $req->getId()
                    ];
                }

                //Agosto
                if ($req->getregistroDataCriacao()->format('Y') === $ano && $req->getregistroDataCriacao()->format('M') === "Aug")
                {
                    $requisicoesAgosto[] = [
                        'idCurso' => $req->getCurso(),
                        'idRequisicao' => $req->getId()
                    ];
                }

                //Setembro
                if ($req->getregistroDataCriacao()->format('Y') === $ano && $req->getregistroDataCriacao()->format('M') === "Sep")
                {
                    $requisicoesSetembro[] = [
                        'idCurso' => $req->getCurso(),
                        'idRequisicao' => $req->getId()
                    ];
                }

                //Outubro
                if ($req->getregistroDataCriacao()->format('Y') === $ano && $req->getregistroDataCriacao()->format('M') === "Oct")
                {
                    $requisicoesOutubro[] = [
                        'idCurso' => $req->getCurso(),
                        'idRequisicao' => $req->getId()
                    ];
                }

                //Novembro
                if ($req->getregistroDataCriacao()->format('Y') === $ano && $req->getregistroDataCriacao()->format('M') === "Nov")
                {
                    $requisicoesNovembro[] = [
                        'idCurso' => $req->getCurso(),
                        'idRequisicao' => $req->getId()
                    ];
                }

                //Dezembro
                if ($req->getregistroDataCriacao()->format('Y') === $ano && $req->getregistroDataCriacao()->format('M') === "Dec")
                {
                    $requisicoesDezembro[] = [
                        'idCurso' => $req->getCurso(),
                        'idRequisicao' => $req->getId()
                    ];
                }
            }

            $cursoData[] = [
                'idCurso' => $curso->getId(),
                'nomeCurso' => $curso->getNomeCurso(),
                'janeiro' => !isset($requisicoesJaneiro) ? 0 : $this->calculaGastosNoMes($requisicoesJaneiro),
                'fevereiro' => !isset($requisicoesFevereiro) ? 0 : $this->calculaGastosNoMes($requisicoesFevereiro),
                'marco' => !isset($requisicoesMarco) ? 0 : $this->calculaGastosNoMes($requisicoesMarco),
                'abril' => !isset($requisicoesAbril) ? 0 : $this->calculaGastosNoMes($requisicoesAbril),
                'maio' => !isset($requisicoesMaio) ? 0 : $this->calculaGastosNoMes($requisicoesMaio),
                'junho' => !isset($requisicoesJunho) ? 0 : $this->calculaGastosNoMes($requisicoesJunho),
                'julho' => !isset($requisicoesJulho) ? 0 : $this->calculaGastosNoMes($requisicoesJulho),
                'agosto' => !isset($requisicoesAgosto) ? 0 : $this->calculaGastosNoMes($requisicoesAgosto),
                'setembro' => !isset($requisicoesSetembro) ? 0 : $this->calculaGastosNoMes($requisicoesSetembro),
                'outubro' => !isset($requisicoesOutubro) ? 0 : $this->calculaGastosNoMes($requisicoesOutubro),
                'novembro' => !isset($requisicoesNovembro) ? 0 : $this->calculaGastosNoMes($requisicoesNovembro),
                'dezembro' => !isset($requisicoesDezembro) ? 0 : $this->calculaGastosNoMes($requisicoesDezembro),
            ];

            $requisicoesJaneiro = [];
            $requisicoesFevereiro = [];
            $requisicoesMarco = [];
            $requisicoesAbril = [];
            $requisicoesMaio = [];
            $requisicoesJunho = [];
            $requisicoesJulho = [];
            $requisicoesAgosto = [];
            $requisicoesSetembro = [];
            $requisicoesOutubro = [];
            $requisicoesNovembro = [];
            $requisicoesDezembro = [];
        }

        return $this->render('@Almoxarifado/Relatorios/tabela-gasto-anual.twig', [
            'cursos' => $cursos,
            'meses' => $meses,
            'main' => $cursoData,
            'ano' => $ano
        ]);
    }

    /**
     * @param $pedidosNoMes
     * @return float|int
     */
    public function calculaGastosNoMes($pedidosNoMes)
    {
        $em = $this->getDoctrine()->getManager();

        $accValoresTotais = 0;

        foreach ($pedidosNoMes as $r)
        {
            $requisicaoEnt = $em->find(Requisicao::class, $r["idRequisicao"]);
            if ($requisicaoEnt->getStatusAvaliacao() == 1) {
                $accValoresTotais = $accValoresTotais + $requisicaoEnt->getValorTotalPedido();
            }
        }

        return $accValoresTotais;
    }

    /**
     * Calcula a quantia de pedidos realizado por um curso (período = total)
     *
     * @param $idCurso
     * @return int
     */
    public function calcularPedidosCompleto($idCurso)
    {
        $em = $this->getDoctrine()->getManager();
        $curso = $em->find(Curso::class, $idCurso);
        $totalPedidos = 0;
        $pedidosByCurso = $em->getRepository(Requisicao::class)->findBy(['curso' => $idCurso]);
        $pedidosDoCurso = count($pedidosByCurso);

        return $pedidosDoCurso;
    }

    /**
     * retorna o total de pedidos de um curso em um intervalo de tempo
     *
     * @param $idCurso
     * @param $inicio
     * @param $fim
     * @return int
     */
    public function calcularQuantidadePedidosNoPeriodo($idCurso, $inicio, $fim)
    {
        $em = $this->getDoctrine()->getManager();
        $curso = $em->find(Curso::class, $idCurso);
        $totalPedidos = 0;
        $pedidos = $em->getRepository(Requisicao::class)->getRequisicoesBetweenAux($curso, $inicio, $fim);
        $totalPedidos = count($pedidos);

        return $totalPedidos;
    }

    /**
     * Calcula o gasto total de um curso (período = total)
     *
     * @param $idCurso
     * @return int
     */
    public function calcularGastoTotalDoCurso($idCurso)
    {
        $em = $this->getDoctrine()->getManager();
        $curso = $em->find(Curso::class, $idCurso);
        $pedidos = $em->getRepository(Requisicao::class)->findBy(['curso' => $curso]);
        $custo = 0;

        foreach ($pedidos as $pedido)
        {
            $custo = $custo + $pedido->getValorTotalPedido();
        }

        return $custo;
    }

    /**
     * @param $idCurso
     * @param $inicio
     * @param $fim
     * @return int
     */
    public function calcularGastoTotalDoCursoNoPeriodo($idCurso, $inicio, $fim)
    {
        $em = $this->getDoctrine()->getManager();
        $curso = $em->find(Curso::class, $idCurso);

        $inicio = date_create_from_format('d-m-Y', $inicio);
        $fim = date_create_from_format('d-m-Y', $fim);

        $pedidos = $em->getRepository(Requisicao::class)->getRequisicoesBetweenAprovadosAux($curso, $inicio, $fim);
        $custo = 0;

        foreach ($pedidos as $pedido)
        {
            $custo = $custo + $pedido->getValorTotalPedido();
        }

        return $custo;
    }
}