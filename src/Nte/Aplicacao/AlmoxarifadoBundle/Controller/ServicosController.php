<?php

namespace Nte\Aplicacao\AlmoxarifadoBundle\Controller;

use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Curso;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Interacao;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Item;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Requisicao;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\RequisicaoItem;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\UsuarioCurso;
use Nte\UsuarioBundle\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ServicosController extends Controller
{
    private $encoders;
    private $normalizers;
    private $serializer;

    /**
     * ServicosController constructor.
     */
    public function __construct()
    {
        $this->encoders = [new JsonEncoder()];
        $normalizer = new ObjectNormalizer();
        $this->normalizers = array($normalizer);
        $this->serializer = new Serializer($this->normalizers, $this->encoders);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function cancelarRequisicaoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $requisicaoId = $request->request->get('requisicao');

        $requisicao = $em->find(Requisicao::class, $requisicaoId);
        $itensDaRequisicao = $em->getRepository(RequisicaoItem::class)->findBy(['requisicao' => $requisicao]);

        foreach ($itensDaRequisicao as $item) {
            $em->remove($item);
            $em->flush();
        }

        $em->remove($requisicao);
        $em->flush();

        return new JsonResponse('requisicao removida');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function persitirRequisicaoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioSolicitante = $this->getUser();
        $produtos = $request->request->get('produtos');
        $observacao = $request->request->get('observacao');
        $idCurso = $request->request->get('curso');
        $curso = $em->find(Curso::class, $idCurso);

        $requisicao = new Requisicao();
        $requisicao->setIdUsuario($usuarioSolicitante);
        $requisicao->setCurso($curso);
        $requisicao->setStatusAvaliacao(2);
        $observacao === null ? $requisicao->setObservacao(null) : $requisicao->setObservacao($observacao);
        $em->persist($requisicao);
        $em->flush();

        foreach ($produtos as $produto)
        {
            $idItem = $produto['idItem'];
            $itemNoEstoque = $em->find(Item::class, $idItem);
            $produtoItemEstoque = $itemNoEstoque->getProduto();
            $produtoNomeEstoqueAtm = $produtoItemEstoque->getNome();
            $requisicaoItem = new RequisicaoItem();
            $requisicaoItem->setQuantidade($produto['quantidade']);
            $requisicaoItem->setAvaliacaoItem(3);
            $requisicaoItem->setProduto($produtoItemEstoque);
            $requisicaoItem->setValorUnitario($produto['preco']);
            $requisicaoItem->setRequisicao($requisicao);
            $requisicaoItem->setNomeProduto($produtoNomeEstoqueAtm);

            $em->persist($requisicaoItem);
            $em->flush();
        }

        return new JsonResponse($requisicao->getId());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function listarProdutosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $produtos = $em->getRepository(Item::class)
            ->getItensData();

        foreach ($produtos as $produto) {
            $dataProdutos[] = [
                'idItem' => $produto->getId(),
                'nome' => $produto->getProduto()->getNome(),
                'valor' => $produto->getValorUnitario(),
                'almoxarifado' => $produto->getAlmoxarifado()->getLocalizacao()
            ];
        }

        return new JsonResponse($this->serializer->serialize($dataProdutos, 'json'));
    }


    /**
     * @param Request $request
     * @return JsonResponse
     *
     * cria resposta à obs do coordenador em rota detalhes_pedido
     */
    public function iniciarInteracaoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $requisicaoid = $request->request->get('requisicao');
        $respostaText = $request->request->get('resposta');

        $requisicao = $em->find(Requisicao::class, $requisicaoid[0]);

        $respostaInteracao = new Interacao();
        $respostaInteracao->setRespostaObservacao($respostaText);
        $respostaInteracao->setRequisicao($requisicao);
        $em->persist($respostaInteracao);
        $em->flush();

        return new JsonResponse($this->serializer->serialize('iniciar interaçao return', 'json'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function enviarAvaliacaoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $itens = $request->request->get('itens');
        $requisicaoId = $request->request->get('requisicao');
        $requisicao = $em->find(Requisicao::class, $requisicaoId);
        $numeroDeItens = sizeof($itens);
        $itensAprovados = 0;

        foreach ($itens as $item)
        {
            $switchValue = $item["itemAval"];
            $itemId = intval($item["itemId"]);
            $itemMotivo = $item["itemMotivo"];

            $itemRequisicao = $em->find(RequisicaoItem::class, $itemId);

            if ($switchValue === 'aprovado') {
                $itemRequisicao->setAvaliacaoItem(1);
                $itemRequisicao->setMotivo(null);
                $itensAprovados++;
            } else if ($switchValue === 'rejeitado') {
                $itemRequisicao->setAvaliacaoItem(2);
                $itemRequisicao->setMotivo($itemMotivo);

            } else if ($switchValue === 'pendente') {
                $itemRequisicao->setAvaliacaoItem(3);
                $itemRequisicao->setMotivo(null);
            }

            $em->persist($itemRequisicao);
            $em->flush();
        }

        return new JsonResponse($this->serializer->serialize('enviarAvaliacao return', 'json'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function adicionarMotivoRejeicaoParcialAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $motivos = $request->request->get('irejeitados');
        $requisicaoId = $request->request->get('requisicao');
        $requisicao = $em->find(Requisicao::class, $requisicaoId);

        if (isset($motivos)) {
            foreach ($motivos as $motivo)
            {
                $motivoAvaliacao = intval($motivo["avaliacao"]);
                $motivoId = intval($motivo["id"]);
                $motivoText = $motivo["motivo"];
            }
        }

        return new JsonResponse($this->serializer->serialize('retorno add motivo no item', 'json'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function checkMotivoItemPorItemExistsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $requisicaoId = $request->request->get('requisicao');
        $requisicao = $em->find(Requisicao::class, $requisicaoId);
        $itens = $em->getRepository(RequisicaoItem::class)->findBy(['requisicao' => $requisicao]);
        $temMotivoUnico = '';
        $temMotivoTodos = '';

        //procura por motivo global da requisicao
        $motivoRejeicaoTotal = $requisicao->getMotivoRejeicaoTotal();

        if (strlen($motivoRejeicaoTotal) > 0) {
            $temMotivoTodos = true;
        } else {
            foreach ($itens as $item) {
                $motivo = $item->getMotivo();
                if (strlen($motivo) > 0) {
                    $temMotivoUnico = true;
                }
            }
        }

        if ($temMotivoTodos === true) {
            $tipoDeMotivo = 'todos';
        } else if ($temMotivoUnico === true) {
            $tipoDeMotivo = 'itens';
        } else {
            $tipoDeMotivo = 'nada';
        }

        return new JsonResponse($this->serializer->serialize($tipoDeMotivo, 'json'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function verificarSituacaoItensAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $requisicaoId = $request->request->get('pedido');
        $requisicao = $em->find(Requisicao::class, $requisicaoId);
        $itens = $em->getRepository(RequisicaoItem::class)->findBy(['requisicao' => $requisicao]);
        $arrItensSituacao = [];

        foreach ($itens as $i) {
            $avaliacao = $i->getAvaliacaoItem();
            $arrItensSituacao[] = ['id' => $i->getId(), 'situacao' => $avaliacao, 'motivo' => $i->getMotivo()];
        }

        return new JsonResponse($this->serializer->serialize($arrItensSituacao, 'json'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function buscaMotivoRejeicaoItemAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idItem = $request->request->get('item');
        $item = $em->find(RequisicaoItem::class, $idItem);
        $motivo = $item->getMotivo();

        return new JsonResponse($this->serializer->serialize($motivo, 'json'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function persistirCodigoRequisicaoSieAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $codigoRequisicaoSie = $request->request->get('requisicaoSie');
        $requisicaoId = $request->request->get('requisicaoId');
        $requisicao = $em->find(Requisicao::class, $requisicaoId);
        $requisicao->setCodigoRequisicaoSie($codigoRequisicaoSie);
        $em->persist($requisicao);
        $em->flush();

        return new JsonResponse($this->serializer->serialize($codigoRequisicaoSie, 'json'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function buscaCursosSolicitanteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $solicitanteId = $request->request->get('idSolicitante');
        $solicitante = $em->find(Usuario::class, $solicitanteId);
        $cursosDoSolicitante = $em->getRepository(UsuarioCurso::class)->findBy(['idUsuario' => $solicitanteId]);
        $cursos = [];
        foreach ($cursosDoSolicitante as $c) {
            $cursos[] = [
                'id' => $c->getIdCurso()->getId(),
                'nome' => $c->getIdCurso()->getNomeCurso(),
            ];
        }

        return new JsonResponse($this->serializer->serialize($cursos, 'json'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function verificarCodigoRequisicaoSieAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idRequisicao = $request->request->get('requisicao');
        $requisicao = $em->find(Requisicao::class, $idRequisicao);
        $codigoRequisicaoSie = $requisicao->getCodigoRequisicaoSie();

        if ($codigoRequisicaoSie == null || $codigoRequisicaoSie == "") {
            $retornoCodReqSie = "";
        } else {
            $retornoCodReqSie = $codigoRequisicaoSie;
        }

        return new JsonResponse($this->serializer->serialize($retornoCodReqSie, 'json'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function persistirObservacaoSolicitanteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idRequisicao = $request->request->get('requisicao');
        $observacao = $request->request->get('observacao');
        $req = $em->find(Requisicao::class, $idRequisicao);
        $req->setObservacao($observacao);
        $em->persist($req);
        $em->flush();

        return new JsonResponse($this->serializer->serialize('success', 'json'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function persistirObservacaoAdministrativoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idRequisicao = $request->request->get('requisicao');
        $observacao = $request->request->get('observacao');
        $req = $em->find(Requisicao::class, $idRequisicao);
        $req->setObservacaoAdm($observacao);
        $em->persist($req);
        $em->flush();

        return new JsonResponse($this->serializer->serialize('success', 'json'));
    }
}























