<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 15/01/20
 * Time: 13:07
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Controller;


use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Requisicao;
use Nte\UsuarioBundle\Entity\Usuario;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class HomepageController extends Controller
{
    /**
     * @return Response
     */
    public function homepageAction()
    {
        $currentDate = date("d-m-Y");
        $explodedCurrentDate = explode("-", $currentDate);
        $anoAtual = $explodedCurrentDate[2];

        $em = $this->getDoctrine()->getManager();
        $this->checkFirstLogin($this->getUser());

        $userRole = $this->getUser()->getRoles();
        $isAvaliador = 0;

        if (in_array("ROLE_ADMINISTRATIVO", $userRole)) {
            $isAvaliador = 1;
        }

        $pedidosPendentes = $em->getRepository(Requisicao::class)->findBy(['statusAvaliacao' => 3]);
        $quantidadePedidosPendentes = count($pedidosPendentes);

        $twigParameters = [
            'isAvaliador' => $isAvaliador,
            'anoAtual' => $anoAtual,
            'quantidadePedidosPendentes' => $quantidadePedidosPendentes,
            'username' => $this->getUser()->getNome()
        ];

        return $this->render('@Almoxarifado/Homepage/homepage.html.twig', $twigParameters);
    }

    //redireciona para form first login, para completar dados, caso seja o primeiro login do usuário

    /**
     * @param $usuario
     * @return bool
     */
    public function checkFirstLogin($usuario)
    {
        $lastLogin = $usuario->getLastLogin();
        $isFirstLogin = false;
        $lastLogin === null ? $isFirstLogin = true  : $isFirstLogin = false;
        return $isFirstLogin;
    }
}