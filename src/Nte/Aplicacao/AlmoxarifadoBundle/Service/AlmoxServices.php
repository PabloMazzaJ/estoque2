<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 12/12/19
 * Time: 09:11
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Service;


use Doctrine\ORM\EntityManager;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\UsuarioCurso;
use Nte\UsuarioBundle\Entity\Usuario;

class AlmoxServices
{
    /**
     *
     * @var EntityManager
     */
    protected $em;

    /**
     * @param Usuario $usuario
     * @return array
     *
     * Recebe um usuário e retorna um array contendo os cursos (id, nome) que ele está vinculado (tabela almox_usuario_curso)
     *
     */
    public function buscaCursosUsuario(Usuario $usuario)
    {
        $usuarioCursoByUsr = $this->em->getRepository(UsuarioCurso::class)->findBy(['idUsuario' => $usuario]);
        $cursos = [];

        foreach ($usuarioCursoByUsr as $u)
        {
            $cursos[] = [
                'idCurso' => $u->getIdCurso()->getId(),
                'nomeCurso' => $u->getIdCurso()->getNomeCurso()
            ];
        }

        return $cursos;
    }

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

}