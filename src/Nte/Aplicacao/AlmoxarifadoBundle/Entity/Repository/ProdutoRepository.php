<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 07/11/19
 * Time: 10:47
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;

class ProdutoRepository extends EntityRepository
{
    public function getNomesProdutoOrdenados()
    {
        return $this->createQueryBuilder('produto')
            ->orderBy('produto.nome', 'ASC')
            ->groupBy('produto.id')
            ->getQuery()
            ->getResult()
        ;
    }
}