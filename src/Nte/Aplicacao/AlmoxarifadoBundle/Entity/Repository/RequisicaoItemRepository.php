<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 03/12/19
 * Time: 09:29
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Requisicao;

class RequisicaoItemRepository extends EntityRepository
{

    public function orderItensByReversedId()
    {
        return $this->createQueryBuilder('item')
            ->orderBy('item.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getItensFromRequisicao(Requisicao $requisicao)
    {
        return $this->createQueryBuilder('item')
            ->where('item.requisicao = :requisicao')
            ->orderBy('item.id', 'DESC')
            ->setParameter('requisicao', $requisicao)
            ->getQuery()
            ->getResult()
        ;
    }

}