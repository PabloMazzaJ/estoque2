<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 07/11/19
 * Time: 11:02
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;

class ItemRepository extends EntityRepository
{
    public function getItensData()
    {
        return $this->createQueryBuilder('item')
            ->join('item.produto', 'produto')
//            ->join('item.almoxarifado', 'almoxarifado')
            ->orderBy('produto.nome', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }



}