<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 16/12/19
 * Time: 12:25
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;

class CursoRepository extends EntityRepository
{
    public function getNomeCursosOrderedAsc()
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.nomeCurso', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }



}