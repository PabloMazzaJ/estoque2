<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 03/12/19
 * Time: 09:34
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity\Repository;


use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Nte\Aplicacao\AlmoxarifadoBundle\Entity\Curso;
use Nte\UsuarioBundle\Entity\Usuario;
use DateTime;

class RequisicaoRepository extends EntityRepository
{
    public function getAllRequisicoesByReversedId()
    {
        return $this->createQueryBuilder('r')
            ->orderBy('r.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }


    public function getRequisitanteRequisicoesByReversedId(Usuario $usr)
    {
        return $this->createQueryBuilder('r')
            ->where('r.idUsuario = :usr')
            ->orderBy('r.id', 'DESC')
            ->setParameter('usr', $usr)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param $curso
     * @return mixed
     * @throws \Exception
     *
     * Retorna todas requisicoes de um curso entre determinado periodo, de mês para outro mês
     */
    public function getRequisicoesBetweenPeriodos($curso)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.registroDataCriacao BETWEEN :inicio AND :fim')
            ->andWhere('r.curso = :curso')
            ->setParameter('inicio', new DateTime('-12 months'), Type::DATETIME)
            ->setParameter('fim', new DateTime())
            ->setParameter('curso', $curso)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getRequisicoesBetweenAux($curso, $inicio, $fim)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.registroDataCriacao BETWEEN :inicio AND :fim')
            ->andWhere('r.curso = :curso')
            ->setParameter('inicio', $inicio )
            ->setParameter('fim', $fim)
            ->setParameter('curso', $curso)
            ->orderBy('r.registroDataCriacao', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getRequisicoesBetweenAprovadosAux($curso, $inicio, $fim)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.registroDataCriacao BETWEEN :inicio AND :fim')
            ->andWhere('r.curso = :curso')
            ->andWhere('r.statusAvaliacao = 1')
            ->setParameter('inicio', $inicio )
            ->setParameter('fim', $fim)
            ->setParameter('curso', $curso)
            ->orderBy('r.registroDataCriacao', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getRequisicoesBetweenPeriodFromAllCourses($inicio, $fim)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.registroDataCriacao BETWEEN :inicio AND :fim')
            ->setParameter('inicio', $inicio )
            ->setParameter('fim', $fim)
            ->orderBy('r.registroDataCriacao', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }


}