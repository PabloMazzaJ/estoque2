<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 30/10/19
 * Time: 15:06
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Table(name="almox_item_estoque")
 * @ORM\Entity(repositoryClass="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Repository\ItemRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Produto", inversedBy="itens")
     * @JoinColumn(name="id_produto", referencedColumnName="id")
     */
    private $produto;

    /**
     * @ORM\ManyToOne(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Almoxarifado",cascade={"persist"})
     * @JoinColumn(name="id_almoxarifado", referencedColumnName="id")
     */
    private $almoxarifado;

    /**
     * @var integer
     *
     *  @ORM\Column(name="valor_unitario", type="integer", nullable=true)
     */
    private $valorUnitario;

    /**
     * @return integer
     */
    public function getValorUnitario()
    {
        return ($this->valorUnitario / 1000);
    }

    /**
     * @param integer $valorUnitario
     */
    public function setValorUnitario($valorUnitario)
    {
        $this->valorUnitario = $valorUnitario * 1000;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param mixed $produto
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
    }

    /**
     * @return mixed
     */
    public function getAlmoxarifado()
    {
        return $this->almoxarifado;
    }

    /**
     * @param mixed
     */
    public function setAlmoxarifado($almoxarifado)
    {
        $this->almoxarifado = $almoxarifado;
    }






}