<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 30/10/19
 * Time: 12:29
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="almox_item_requisicao")
 * @ORM\Entity(repositoryClass="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Repository\RequisicaoItemRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class RequisicaoItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Requisicao", inversedBy="itens")
     * @JoinColumn(name="requisicao_id", referencedColumnName="id")
     */
    private $requisicao;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade", type="integer", length=255, nullable=true);
     */
    private $quantidade;

    /**
     * @var integer
     *
     *  @ORM\Column(name="valor_unitario", type="integer", nullable=true)
     */
    private $valorUnitario;

    /**
     * @var Produto
     *
     * @ORM\ManyToOne(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Produto", inversedBy="itensRequisitados")
     * @JoinColumn(name="id_produto", referencedColumnName="id")
     */
    private $produto;

    /**
     * @var string
     *
     *  @ORM\Column(name="nome_produto", type="string", length=255, nullable=true)
     */
    private $nomeProduto;

    /**
     * @var integer
     *
     * 1 - aprovado
     * 2 - rejeitado
     * 3 - pendente
     *
     *  @ORM\Column(name="avaliacao_item", type="integer", nullable=true)
     */
    private $avaliacaoItem;

    /**
     * Motivo da rejeição
     * @var string
     * @ORM\Column(name="motivo_rejeicao", type="string", length=255, nullable=true);
     */
    private $motivo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registro_data_atualizacao", type="datetime", nullable=true)
     */
    private $registroDataAtualizacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registro_data_criacao", type="datetime", nullable=true)
     */
    private $registroDataCriacao;

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        if ($args->hasChangedField('registroDataCriacao')) {
            $this->registroDataCriacao = $args->getOldValue('registroDataCriacao');
            $this->registroDataAtualizacao = new \DateTime();
        }

    }

    /**
     * @ORM\PrePersist
     */
    public function PrePersist()
    {
        $this->registroDataCriacao = new \DateTime();
        $this->registroDataAtualizacao = new \DateTime();
    }

    /**
     * Set registroDataAtualizacao
     *
     * @param \DateTime $registroDataAtualizacao
     *
     * @return RequisicaoItem
     */
    public function setRegistroDataAtualizacao($registroDataAtualizacao)
    {
        $this->registroDataAtualizacao = $registroDataAtualizacao;

        return $this;
    }

    /**
     * Get registroDataAtualizacao
     *
     * @return \DateTime
     */
    public function getRegistroDataAtualizacao()
    {
        return $this->registroDataAtualizacao;
    }

    /**
     * Set registroDataCriacao
     *
     * @param \DateTime $registroDataCriacao
     *
     * @return RequisicaoItem
     */
    public function setRegistroDataCriacao($registroDataCriacao)
    {
        $this->registroDataCriacao = $registroDataCriacao;

        return $this;
    }

    /**
     * Get registroDataCriacao
     *
     * @return \DateTime
     */
    public function getRegistroDataCriacao()
    {
        return $this->registroDataCriacao;
    }

    /**
     * @return int
     */
    public function getAvaliacaoItem()
    {
        return $this->avaliacaoItem;
    }

    /**
     * @param int $avaliacaoItem
     */
    public function setAvaliacaoItem($avaliacaoItem)
    {
        $this->avaliacaoItem = $avaliacaoItem;
    }

    /**
     * @return string
     */
    public function getNomeProduto()
    {
        return $this->nomeProduto;
    }

    /**
     * @param string $nomeProduto
     */
    public function setNomeProduto($nomeProduto)
    {
        $this->nomeProduto = $nomeProduto;
    }

    /**
     * @return string
     */
    public function getMotivo()
    {
        return $this->motivo;
    }

    /**
     * @param string $motivo
     */
    public function setMotivo($motivo)
    {
        $this->motivo = $motivo;
    }


    /**
     * @return mixed
     */
    public function getRequisicao()
    {
        return $this->requisicao;
    }

    /**
     * @param mixed $requisicao
     */
    public function setRequisicao($requisicao)
    {
        $this->requisicao = $requisicao;
    }

    /**
     * @return integer
     */
    public function getValorUnitario()
    {
        return ($this->valorUnitario / 1000);
    }

    /**
     * @param integer $valorUnitario
     */
    public function setValorUnitario($valorUnitario)
    {
        $this->valorUnitario = $valorUnitario * 1000;
    }

    /**
     * @return string
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param string $quantidade
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Produto
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param Produto $produto
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
    }
}