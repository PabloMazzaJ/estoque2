<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 30/10/19
 * Time: 14:49
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="almox_produto")
 * @ORM\Entity(repositoryClass="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Repository\ProdutoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Produto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(name="id_produto", type="integer", length=255, nullable=false);
     */
    private $idProduto;

    /**
     * @ORM\OneToMany(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Item", mappedBy="produto")
     */
    private $itens;

    /**
     * @ORM\OneToMany(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\RequisicaoItem", mappedBy="produto")
     */
    private $itensRequisitados;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, nullable=false);
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=255, nullable=true);
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="cod_reduzido", type="string", length=255, nullable=true);
     */
    private $codReduzido;

    /**
     * Produto constructor.
     */
    public function __construct()
    {
        $this->itens = new ArrayCollection();
        $this->itensRequisitados = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getIdProduto()
    {
        return $this->idProduto;
    }

    /**
     * @param int $idProduto
     */
    public function setIdProduto($idProduto)
    {
        $this->idProduto = $idProduto;
    }

    /**
     * @return string
     */
    public function getCodReduzido()
    {
        return $this->codReduzido;
    }

    /**
     * @param string $codReduzido
     */
    public function setCodReduzido($codReduzido)
    {
        $this->codReduzido = $codReduzido;
    }

    /**
     * @return mixed
     */
    public function getItensRequisitados()
    {
        return $this->itensRequisitados;
    }

    /**
     * @param mixed $itensRequisitados
     */
    public function setItensRequisitados($itensRequisitados)
    {
        $this->itensRequisitados = $itensRequisitados;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getItens()
    {
        return $this->itens;
    }

    /**
     * @param mixed $itens
     */
    public function setItens($itens)
    {
        $this->itens = $itens;
    }






}