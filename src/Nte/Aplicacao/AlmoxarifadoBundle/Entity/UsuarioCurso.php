<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 11/12/19
 * Time: 16:26
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Table(name="almox_usuario_curso")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class UsuarioCurso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Nte\UsuarioBundle\Entity\Usuario", inversedBy="usuarioCurso")
     * @JoinColumn(name="id_usuario", referencedColumnName="id")
     */
    private $idUsuario;

    /**
     * @ManyToOne(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Curso", inversedBy="usuarioCurso")
     * @JoinColumn(name="id_curso", referencedColumnName="id")
     */
    private $idCurso;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="validade", type="datetime", nullable=true)
     */
    private $validade;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * @param mixed $idUsuario
     */
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;
    }

    /**
     * @return mixed
     */
    public function getIdCurso()
    {
        return $this->idCurso;
    }

    /**
     * @param mixed $idCurso
     */
    public function setIdCurso($idCurso)
    {
        $this->idCurso = $idCurso;
    }

    /**
     * @return DateTime
     */
    public function getValidade()
    {
        return $this->validade;
    }

    /**
     * @param DateTime $validade
     */
    public function setValidade($validade)
    {
        $this->validade = $validade;
    }






}