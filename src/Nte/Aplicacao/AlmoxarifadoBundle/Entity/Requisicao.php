<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 30/10/19
 * Time: 10:41
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="almox_requisicao")
 * @ORM\Entity(repositoryClass="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Repository\RequisicaoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Requisicao
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Nte\UsuarioBundle\Entity\Usuario", inversedBy="idRequisicao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $idUsuario;

    /**
     * @ORM\ManyToOne(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Curso", inversedBy="requisicoes")
     * @ORM\JoinColumn(name="curso_id", referencedColumnName="id")
     */
    private $curso;

    /**
     * @ORM\OneToMany(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\RequisicaoItem", mappedBy="requisicao")
     */
    private $itens;

    /**
     * Esta é a obs do Solicitante
     * @var string
     *
     * @ORM\Column(name="observacao", type="string", length=255, nullable=true)
     */
    private $observacao;

    /**
     * Esta é a obs da Administração
     * @var string
     *
     * @ORM\Column(name="observacao_adm", type="string", length=255, nullable=true)
     */
    private $observacaoAdm;

    /**
     * @var integer
     *
     * 4 - parcialmente aprovado
     * 3 - pendente
     * 2 - rejeitado
     * 1 - aprovado
     *
     * @ORM\Column(name="status_avaliacao", type="integer", nullable=true)
     */
    private $statusAvaliacao;

    /**
     * @ORM\Column(name="codigo_requisicao_sie", type="string", nullable=true)
     */
    private $codigoRequisicaoSie;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registro_data_atualizacao", type="datetime", nullable=true)
     */
    private $registroDataAtualizacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registro_data_criacao", type="datetime", nullable=true)
     */
    private $registroDataCriacao;

    /**
     * @ORM\OneToMany(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Interacao", mappedBy="requisicao")
     */
    private $interacao;

    public function __construct() {
        $this->itens = new ArrayCollection();
        $this->interacao = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getObservacaoAdm()
    {
        return $this->observacaoAdm;
    }

    /**
     * @param string $observacaoAdm
     */
    public function setObservacaoAdm($observacaoAdm)
    {
        $this->observacaoAdm = $observacaoAdm;
    }

    /**
     * @return mixed
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * @param mixed $curso
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;
    }

    /**
     * @return mixed
     */
    public function getCodigoRequisicaoSie()
    {
        return $this->codigoRequisicaoSie;
    }

    /**
     * @param mixed $codigoRequisicaoSie
     */
    public function setCodigoRequisicaoSie($codigoRequisicaoSie)
    {
        $this->codigoRequisicaoSie = $codigoRequisicaoSie;
    }

    /**
     * @return integer
     */
    public function getStatusAvaliacao()
    {
        return $this->statusAvaliacao;
    }

    /**
     * @param integer $statusAvaliacao
     */
    public function setStatusAvaliacao($statusAvaliacao)
    {
        $this->statusAvaliacao = $statusAvaliacao;
    }

    /**
     * @return mixed
     */
    public function getItens()
    {
        return $this->itens;
    }

    /**
     * @param mixed $itens
     */
    public function setItens($itens)
    {
        $this->itens = $itens;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * @param mixed $idUsuario
     */
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;
    }

    /**
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param string $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return mixed
     */
    public function getInteracao()
    {
        return $this->interacao;
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        if ($args->hasChangedField('registroDataCriacao')) {
            $this->registroDataCriacao = $args->getOldValue('registroDataCriacao');
            $this->registroDataAtualizacao = new \DateTime();
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function PrePersist()
    {
        $this->registroDataCriacao = new \DateTime();
        $this->registroDataAtualizacao = new \DateTime();
    }

    /**
     * @param $registroDataAtualizacao
     * @return $this
     */
    public function setRegistroDataAtualizacao($registroDataAtualizacao)
    {
        $this->registroDataAtualizacao = $registroDataAtualizacao;

        return $this;
    }

    /**
     * Get registroDataAtualizacao
     *
     * @return \DateTime
     */
    public function getRegistroDataAtualizacao()
    {
        return $this->registroDataAtualizacao;
    }

    /**
     * @param $registroDataCriacao
     * @return $this
     */
    public function setRegistroDataCriacao($registroDataCriacao)
    {
        $this->registroDataCriacao = $registroDataCriacao;

        return $this;
    }

    /**
     * Get registroDataCriacao
     *
     * @return \DateTime
     */
    public function getRegistroDataCriacao()
    {
        return $this->registroDataCriacao;
    }

    public function getValorTotalPedido()
    {
        $itens = $this->itens;
        $save = [];

        foreach ($itens as $item)
        {
            $save[] = $item->getQuantidade() * $item->getValorUnitario();
        }

        $sum = array_sum($save);

        return $sum;
    }

}