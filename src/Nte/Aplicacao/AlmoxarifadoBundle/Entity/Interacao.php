<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 19/11/19
 * Time: 10:42
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Event\PreUpdateEventArgs;


/**
 * @ORM\Table(name="almox_interacao")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Interacao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="resposta_observacao", type="text", length=500, nullable=true)
     */
    private $respostaObservacao;

    /**
     * @var string
     *
     * @ORM\Column(name="msg_coordenador", type="text", length=500, nullable=true)
     */
    private $mensagemCoordenador;

    /**
     * @var string
     *
     * @ORM\Column(name="msg_admnistrativo", type="text", length=500, nullable=true)
     */
    private $mensagemAdministrativo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registro_data_atualizacao", type="datetime", nullable=true)
     */
    private $registroDataAtualizacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registro_data_criacao", type="datetime", nullable=true)
     */
    private $registroDataCriacao;

    /**
     * @ManyToOne(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Requisicao", inversedBy="interacao")
     * @JoinColumn(name="requisicao_id", referencedColumnName="id")
     */
    private $requisicao;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getMensagemCoordenador()
    {
        return $this->mensagemCoordenador;
    }

    /**
     * @param string $mensagemCoordenador
     */
    public function setMensagemCoordenador($mensagemCoordenador)
    {
        $this->mensagemCoordenador = $mensagemCoordenador;
    }

    /**
     * @return string
     */
    public function getMensagemAdministrativo()
    {
        return $this->mensagemAdministrativo;
    }

    /**
     * @param string $mensagemAdministrativo
     */
    public function setMensagemAdministrativo($mensagemAdministrativo)
    {
        $this->mensagemAdministrativo = $mensagemAdministrativo;
    }

    /**
     * @ORM\PreUpdate
     * @param PreUpdateEventArgs $args
     * @throws \Exception
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        if ($args->hasChangedField('registroDataCriacao')) {
            $this->registroDataCriacao = $args->getOldValue('registroDataCriacao');
            $this->registroDataAtualizacao = new \DateTime();
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function PrePersist()
    {
        $this->registroDataCriacao = new \DateTime();
        $this->registroDataAtualizacao = new \DateTime();
    }

    /**
     * Set registroDataAtualizacao
     *
     * @param \DateTime $registroDataAtualizacao
     *
     * @return Interacao
     */
    public function setRegistroDataAtualizacao($registroDataAtualizacao)
    {
        $this->registroDataAtualizacao = $registroDataAtualizacao;

        return $this;
    }

    /**
     * Get registroDataAtualizacao
     *
     * @return \DateTime
     */
    public function getRegistroDataAtualizacao()
    {
        return $this->registroDataAtualizacao;
    }

    /**
     * Set registroDataCriacao
     *
     * @param \DateTime $registroDataCriacao
     *
     * @return Interacao
     */
    public function setRegistroDataCriacao($registroDataCriacao)
    {
        $this->registroDataCriacao = $registroDataCriacao;

        return $this;
    }

    /**
     * Get registroDataCriacao
     *
     * @return \DateTime
     */
    public function getRegistroDataCriacao()
    {
        return $this->registroDataCriacao;
    }

    /**
     * @return mixed
     */
    public function getRequisicao()
    {
        return $this->requisicao;
    }

    /**
     * @param mixed $requisicao
     */
    public function setRequisicao($requisicao)
    {
        $this->requisicao = $requisicao;
    }

    /**
     * @return string
     */
    public function getRespostaObservacao()
    {
        return $this->respostaObservacao;
    }

    /**
     * @param string $respostaObservacao
     */
    public function setRespostaObservacao($respostaObservacao)
    {
        $this->respostaObservacao = $respostaObservacao;
    }




}