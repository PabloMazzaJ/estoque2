<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 05/02/20
 * Time: 14:03
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Nte\UsuarioBundle\Entity\Usuario;

/**
 * @ORM\Table(name="almox_coordenador_tem_solicitante")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class CoordenadorTemSolicitante
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Nte\UsuarioBundle\Entity\Usuario")
     * @JoinColumn(name="id_coordenador", referencedColumnName="id")
     */
    private $idCoordenador;

    /**
     * @ManyToOne(targetEntity="Nte\UsuarioBundle\Entity\Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id")
     */
    private $idSolicitante;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdCoordenador()
    {
        return $this->idCoordenador;
    }

    /**
     * @param mixed $idCoordenador
     */
    public function setIdCoordenador($idCoordenador)
    {
        $this->idCoordenador = $idCoordenador;
    }

    /**
     * @return mixed
     */
    public function getIdSolicitante()
    {
        return $this->idSolicitante;
    }

    /**
     * @param mixed $idSolicitante
     */
    public function setIdSolicitante($idSolicitante)
    {
        $this->idSolicitante = $idSolicitante;
    }

}