<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 30/10/19
 * Time: 14:49
 */

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="almox_almoxarifado")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Almoxarifado
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="localizacao", type="string", nullable=true)
     */
    private $localizacao;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLocalizacao()
    {
        return $this->localizacao;
    }

    /**
     * @param string $localizacao
     */
    public function setLocalizacao($localizacao)
    {
        $this->localizacao = $localizacao;
    }





}