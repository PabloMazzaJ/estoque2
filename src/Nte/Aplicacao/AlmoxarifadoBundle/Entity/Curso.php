<?php

namespace Nte\Aplicacao\AlmoxarifadoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Nte\UsuarioBundle\Entity\Usuario;

/**
 * @ORM\Table(name="almox_curso")
 * @ORM\Entity(repositoryClass="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Repository\CursoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Curso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="curso", type="string", length=255, nullable=true);
     */
    private $nomeCurso;

    /**
     * @ORM\OneToMany(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\Requisicao", mappedBy="curso")
     */
    private $requisicoes;

    /**
     * @ORM\OneToMany(targetEntity="Nte\Aplicacao\AlmoxarifadoBundle\Entity\UsuarioCurso", mappedBy="idCurso")
     */
    private $usuarioCurso;


    public function __construct()
    {
        $this->usuarioCurso = new ArrayCollection();
        $this->requisicoes = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNomeCurso()
    {
        return $this->nomeCurso;
    }

    /**
     * @param string $nomeCurso
     */
    public function setNomeCurso($nomeCurso)
    {
        $this->nomeCurso = $nomeCurso;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registro_data_atualizacao", type="datetime", nullable=true)
     */
    private $registroDataAtualizacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registro_data_criacao", type="datetime", nullable=true)
     */
    private $registroDataCriacao;

    /**
     * @ORM\PreUpdate
     * @param PreUpdateEventArgs $args
     * @throws \Exception
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        if ($args->hasChangedField('registroDataCriacao')) {
            $this->registroDataCriacao = $args->getOldValue('registroDataCriacao');
            $this->registroDataAtualizacao = new \DateTime();
        }

    }

    /**
     * @ORM\PrePersist
     * @throws \Exception
     */
    public function PrePersist()
    {
        $this->registroDataCriacao = new \DateTime();
        $this->registroDataAtualizacao = new \DateTime();
    }

    /**
     * Set registroDataAtualizacao
     *
     * @param \DateTime $registroDataAtualizacao
     *
     * @return Curso
     */
    public function setRegistroDataAtualizacao($registroDataAtualizacao)
    {
        $this->registroDataAtualizacao = $registroDataAtualizacao;

        return $this;
    }

    /**
     * Get registroDataAtualizacao
     *
     * @return \DateTime
     */
    public function getRegistroDataAtualizacao()
    {
        return $this->registroDataAtualizacao;
    }

    /**
     * Set registroDataCriacao
     *
     * @param \DateTime $registroDataCriacao
     *
     * @return Curso
     */
    public function setRegistroDataCriacao($registroDataCriacao)
    {
        $this->registroDataCriacao = $registroDataCriacao;

        return $this;
    }

    /**
     * Get registroDataCriacao
     *
     * @return \DateTime
     */
    public function getRegistroDataCriacao()
    {
        return $this->registroDataCriacao;
    }

    /**
     * @return mixed
     */
    public function getRequisicoes()
    {
        return $this->requisicoes;
    }

    /**
     * @param mixed $requisicoes
     */
    public function setRequisicoes($requisicoes)
    {
        $this->requisicoes = $requisicoes;
    }

    /**
     * @return mixed
     */
    public function getUsuarioCurso()
    {
        return $this->usuarioCurso;
    }

    /**
     * @param mixed $usuarioCurso
     */
    public function setUsuarioCurso($usuarioCurso)
    {
        $this->usuarioCurso = $usuarioCurso;
    }

    public function getRequisicoesDoCurso()
    {
        return $this->requisicoes;
    }


}