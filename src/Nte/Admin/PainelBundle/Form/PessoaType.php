<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 11/02/19
 * Time: 10:26
 */

namespace Nte\Admin\PainelBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PessoaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome', TextType::class)
            ->add('email', EmailType::class)
            ->add('tipo', IntegerType::class)
            ->add('Cpf', IntegerType::class)
            ->add('disciplina', TextType::class)
            ->add('polos', TextType::class)
            ->add('submit', SubmitType::class);



    }
}