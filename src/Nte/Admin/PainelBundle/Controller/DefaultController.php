<?php

namespace Nte\Admin\PainelBundle\Controller;

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\Query\Expr;
use DoctrineExtensions\Query\Mysql\DateFormat;
use Nte\Aplicacao\ExportadorBundle\Entity\MiddlewareExportacao;
use Nte\Aplicacao\ExportadorBundle\Entity\MiddlewareExportacaoLog;
use Nte\Aplicacao\ExportadorBundle\Entity\MiddlewareExportacaoLogResumo;
use Nte\Aplicacao\ExportadorBundle\Entity\VMiddlewareLog;
use Nte\Config\MoodleBundle\Entity\NteConfigMoodle;
use Nte\Middleware\MoodleBundle\Service\MiddlewareQuery;
use Nte\Middleware\MoodleBundle\Utils\Curl;
use Nte\Middleware\SieBundle\Entity\SieAlteracao;
use Nte\Middleware\SieBundle\Entity\SieAlteracaoSituacao;
use Nte\Middleware\SieBundle\Entity\SieCurso;
use Nte\Middleware\SieBundle\Entity\SieCursoOferta;
use Nte\Middleware\SieBundle\Entity\SieCursoPeriodo;
use Nte\Middleware\SieBundle\Entity\SieDisciplina;
use Nte\Middleware\SieBundle\Entity\SieTurma;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction() {
        return $this->render('NteAdminPainelBundle:Default:index.html.twig', []);
    }
}
