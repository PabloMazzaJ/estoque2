<?php

namespace Nte\Admin\PainelBundle\Controller;

use Nte\Config\SieBundle\Entity\NteConfigSie;
use Nte\Middleware\SieBundle\Entity\SieAlteracaoSituacao;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\Query\Expr;
class ChecagemController extends Controller
{

//    /**
//     *
//     * @return JsonResponse
//     */
//    public function checkWSAction(){
//
//        $tarefas = $this->checkTask(true);
//
//        $obj = new \stdClass();
//        $obj->tarefa = new \stdClass();
//        $obj->tarefa->contagem = count($tarefas);
//        $obj->tarefa->listagem = $tarefas;
//        return new JsonResponse($obj);
//    }
//
//    /**
//     * Tarefa Index
//     * @return \Symfony\Component\HttpFoundation\Response
//     */
//    public function indexTarefaAction()
//    {
//        return $this->render('NteAdminPainelBundle:Checagem:index-tarefa.html.twig',[
//            'itens'=>$this->checkTask(),
//            'stats'=>(count($this->checkTask(true))*100/count($this->checkTask()))
//        ]);
//    }

//    /**
//     *  Verifica se existe alguma tarefa pendente
//     * @param boolean $filter
//     * @return array
//     */
//    public function checkTask($filter = false){
//
//        $tasks = [];
//
//        // Verifica se todos os metodos de inscrição foram checados
//        $obj = new \stdClass();
//        $obj->titulo = "Situação de Inscrição";
//        $obj->subtitulo = "Existe uma situação ainda não configurada";
//        $obj->check = $this->checkSieInscricao();
//        $obj->url = $this->generateUrl('nte_config_sie_homepage');
//        $obj->icone = 'fa fa-user';
//        $tasks[] = $obj;
//
//        // Verifica se o moodle foi configurado
//        $obj = new \stdClass();
//        $obj->titulo = "Configuração do moodle";
//        $obj->subtitulo = "Não existem moodle configurados";
//        $obj->check = false;
//        $obj->url = $this->generateUrl('nte_config_moodle_homepage');
//        $obj->icone = 'fa fa-graduation-cap';
//        $tasks[] = $obj;
//
//        // Verifica se o moodle foi configurado
//        $obj = new \stdClass();
//        $obj->titulo = "Conectividade do moodle";
//        $obj->subtitulo = "Não existem conectividade em algum moodle configurado";
//        $obj->check = false;
//        $obj->url = $this->generateUrl('nte_config_moodle_homepage');
//        $obj->icone = 'fa fa-cloud-download';
//        $tasks[] = $obj;
//
//
//        // Verifica se o moodle foi configurado
//        $obj = new \stdClass();
//        $obj->titulo = "Configuração do Crontab";
//        $obj->subtitulo = "O crontab não possui nenhuma tarefa";
//        $obj->check = false;
//        $obj->url = $this->generateUrl('nte_config_crontab');
//        $obj->icone = 'fa fa-calendar';
//        $tasks[] = $obj;
//
//        // Verifica se o moodle foi configurado
//        $obj = new \stdClass();
//        $obj->titulo = "Lista de Exportação";
//        $obj->subtitulo = "Não existe nenhum curso na fila  de exportação";
//        $obj->check = false;
//        $obj->url = $this->generateUrl('nte_aplicacao_exportador_homepage');
//        $obj->icone = 'fa fa-th';
//        $tasks[] = $obj;
//
//        if($filter){
//            $arr = [];
//            foreach ($tasks as $t){
//                if($t->check == true){
//                    $arr[] = $t;
//                }
//            }
//            return $arr;
//        }else{
//            return $tasks;
//        }
//    }


//    /**
//     * Checagem de inscrições
//     * @return bool
//     */
//    private function  checkSieInscricao(){
//
//        $status = $this->getDoctrine()
//            ->getManager()
//            ->createQueryBuilder()
//            ->select('sas.id, sas.descricao, ncs.valor')
//            ->from(SieAlteracaoSituacao::class, 'sas')
//            ->leftJoin(NteConfigSie::class, 'ncs', Expr\Join::WITH, 'sas.id = ncs.idSie and ncs.tipo =1')
//            ->where('ncs.valor is null')
//            ->orderBy('sas.descricao', 'asc')
//            ->getQuery()->getResult();
//        return count($status)? true:false;
//    }

//    /**
//     * Checagem de conectividade com o moodle
//     * @return bool
//     */
//    private function checkMoodleConnect(){
//        return false;
//    }
}
