#####
# Script de backup diário
#

#!/bin/bash
URL_BKP=fsnte.nte.ufsm.br
DIR_BKP=/home/backup
DIR_REM_BKP=/media/sdb/narfi
LOG_FILE=/var/log/backup.log
echo "Inicio $(date +%d-%m-%y-%H:%M)"                                               >> $LOG_FILE
ntpdate ntp.ufsm.br                                                                 >> $LOG_FILE
scp -Crp /etc/*                              $DIR_BKP/etc/                          >> $LOG_FILE
scp -Crp /var/spool/cron/crontabs/*          $DIR_BKP/crontabs/                     >> $LOG_FILE
scp -Crp /usr/local/bin/*                    $DIR_BKP/bin/                          >> $LOG_FILE
scp -Crp /var/www/narfi                      $DIR_BKP/www/                          >> $LOG_FILE
MYSQL_PWD=UABead12  mysqldump -u root   --databases narfi --skip-comments  > /home/backup/db/backup.sql

#remove o log e o cache da aplicação
rm -fr /home/backup/www/narfi/var/cache/dev/*
rm -fr /home/backup/www/narfi/var/cache/prod/*
rm -fr /home/backup/www/narfi/var/logs/*

scp -CrpP2223 $DIR_BKP/* $URL_BKP:$DIR_REM_BKP/                                     >> $LOG_FILE 2>>$LOG_FILE

#Sincronizando arquivos com o GIT
cd /home/backup/
git add bin/*                                                                       >> $LOG_FILE 2>>$LOG_FILE  &
git add etc/*                                                                       >> $LOG_FILE 2>>$LOG_FILE  &
git add crontabs/*                                                                  >> $LOG_FILE 2>>$LOG_FILE  &
git add www/*                                                                       >> $LOG_FILE 2>>$LOG_FILE  &
git commit -a -m "Backup realizado em $(date +%d-%m-%y-%H:%M)"                      >> $LOG_FILE 2>>$LOG_FILE  &
git push -u origin master                                                           >> $LOG_FILE 2>>$LOG_FILE  &
echo " Fim $(date +%d-%m-%y-%H:%M)"                                                 >> $LOG_FILE
scp -CrpP2223 $LOG_FILE  $URL_BKP:$DIR_REM_BKP/                                     >> $LOG_FILE