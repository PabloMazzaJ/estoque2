#!/usr/bin/env ruby
#
# Clean Vagrant Machine to LAMP Stack in Debian 7 (wheezy) with PHP5.5, Apache, MySQL, MongoDB, Redis
#
# Dependencies
#   Vagrant >= 1.5.0
#   Kernel  >= 3.5
#   lxc     >= 0.7.5
# OR
#   Virtualbox 4.3.10+
#   irtualbox Extension Pack
#
# For LXC in Debian/Ubuntu
#   sudo apt-get install lxc lxc-templates cgroup-lite redir
#   vagrant plugin install vagrant-lxc
#   vagrant lxc sudoers
#
# PS.: If your project have a directory php, public, public_html, web or www
# inside of a directory specified in variable folder in Vagrantfile,
# it will be used as Document Root of the Web Server.
# Otherwise the variable folder will be assumed.

# Change this for your requirements.
hostname = "narfi"   # Internal hostname for machine
address  = "192.168.50.2"  # Private IP address of the machine, Only Virtualbox
folder   = "./"            # The shared folder is the same as the current folder Vagrantfile
forward  = true            # Forward ports from guest to host

# Do not change from here.
Vagrant.require_version ">= 1.5.0"

ENV['VAGRANT_DEFAULT_PROVIDER'] ||= 'lxc'

Vagrant.configure("2") do |config|
  config.vm.hostname = hostname

  if ENV['VAGRANT_DEFAULT_PROVIDER'] == 'lxc'
    config.vm.box = "fgrehm/wheezy64-lxc"
    config.vm.synced_folder folder, "/var/www"
  else
    config.vm.box = "vogtmann/debian-7.5-64"
    config.vm.network "private_network", ip: address
    config.vm.synced_folder folder, "/var/www", owner: "www-data", group: "www-data"
  end

  # Forward Ports
  if forward
    config.vm.network "forwarded_port", guest: 27017, host: 27017
    config.vm.network "forwarded_port", guest: 80,    host: 8080
    config.vm.network "forwarded_port", guest: 3306,  host: 3306
    config.vm.network "forwarded_port", guest: 5432,  host: 54320
  end

  config.vm.provider :lxc do |lxc|
    lxc.container_name = hostname
    lxc.customize 'cgroup.memory.limit_in_bytes', '1024M'

  end

  config.vm.provider :virtualbox do |virtualbox|
    virtualbox.name = hostname
    virtualbox.customize ["modifyvm", :id, "--memory", 1024, "--natdnshostresolver1", "on"]
  end

  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"

  config.vm.provision :shell, :path => "Vagrantshell"

end